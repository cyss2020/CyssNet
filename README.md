# Cyss.Net

#### 介绍
一个适合中小企业的简单轻量的分布式服务架构(vs2019+net5)<br>
前端:blazor <br>
后端:webapi<br>
任务管理器: winform+web<br>
技术应用:<br>
1: netcore webapi<br>
2:efcore<br>
3:redis<br>
4:RabbitMQ<br>
5:IOC<br>
....<br>

丰富的帮助类<br>
1: pdf帮助类<br>
2：Excel帮助类<br>
3：OCR帮助类<br>
4：Razor帮助类<br>
5: ZXing帮助类<br>
....<br>

#### 软件架构
软件架构说明
分布式架构
webapi+redis+mq

#### 安装教程

1.  vs2019+sql2012
安装 .net core sdk 最新版 <a href="http://www.microsoft.com/net/download" rel="nofollow">官方网址</a><br>
安装 Visual Studio 2019 最新版 <a href="https://visualstudio.microsoft.com/vs/getting-started/" rel="nofollow">官方网址</a><br>
获取本项目代码  <a href="https://gitee.com/cyss2020/cyss">Cyss.Net</a> <br>

#### 使用说明

1.  环境 vs2019+net5

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
