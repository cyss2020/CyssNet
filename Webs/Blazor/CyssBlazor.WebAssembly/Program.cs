using Account.Api.Client;
using Blazored.LocalStorage;
using BootstrapBlazor.Components;
using Common.Api.Client;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using CyssBlazor.Shared;
using CyssBlazor.Shared.Infrastructure;
using CyssBlazor.WebAssembly.Infrastructure;
using FluentValidation;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Order.Api.Client;
using Order.Api.Dtos;
using Order.Api.Dtos.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace CyssBlazor.WebAssembly
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            await builder.AddWasmServices();
            var Host = builder.Build();
             Host.Services.RegisterServiceProvider();
            await Host.RunAsync();
        }



    }
}
