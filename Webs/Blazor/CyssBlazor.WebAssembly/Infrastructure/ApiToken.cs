﻿using Cyss.Core;
using Cyss.Core.Infrastructure;
using CyssBlazor.Shared.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.WebAssembly.Infrastructure
{
    public class ApiToken : IApiToken
    {
        public async Task<string> GetTotkenAsync()
        {
            var workContext = IOCEngine.Resolve<IBlazorWorkContext>();
            if (string.IsNullOrWhiteSpace(workContext.Token))
            {

                var savedToken = await IOCFactory.LocalStorageService.GetItemAsync<string>(SystemDefaultKey.AuthTokenKey);
                if (!string.IsNullOrWhiteSpace(savedToken))
                {
                    workContext.Token = savedToken;
                }
            }
            return workContext.Token;
        }
    }
}
