﻿using Blazored.LocalStorage;
using Cyss.Core;
using Microsoft.AspNetCore.Components;

namespace CyssBlazor.Shared.Infrastructure
{
    public static class IOCFactory
    {
        public static ILocalStorageService LocalStorageService
        {
            get
            {
                return IOCEngine.Resolve<ILocalStorageService>();
            }
        }


        public static IWebWorkContext WebWorkContext
        {
            get
            {
                return IOCEngine.Resolve<IWebWorkContext>();
            }
        }

        public static DialogHelper DialogHelper { get { return IOCEngine.Resolve<DialogHelper>(); } }

    }
}
