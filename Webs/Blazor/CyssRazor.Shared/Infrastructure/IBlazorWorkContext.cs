﻿using Account.Api.Dtos;
using BootstrapBlazor.Components;
using Cyss.Core;
using System.Collections.Generic;

namespace CyssBlazor.Shared.Infrastructure
{
    public interface IBlazorWorkContext : IWorkContext
    {
        bool IsLogin { set; get; }


        List<int> UserRoles { set; get; }

        List<PermissionModel> Permissions { set; get; }


        List<MenuModel> Menus { set; get; }

        List<MenuItem> MenuItems { set; get; }

        /// <summary>
        /// 用户认证token
        /// </summary>
        string Token { set; get; }

        //OperateResult<LoginReturnModel> ApiResponse { set; get; }

        /// <summary>
        /// 当前页面 的权限Id
        /// </summary>
        string CurrentPageUrl { set; get; }

    }

 
}
