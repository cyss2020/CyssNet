﻿using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Infrastructure
{
    public interface IUserAuthStateProvider
    {

        Task<bool> GetAuthenticationStateAsync();

        void LogUserOut();

        void Login();
    }
}
