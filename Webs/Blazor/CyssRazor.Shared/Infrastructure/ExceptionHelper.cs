﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Infrastructure
{
    public static class ExceptionHelper
    {
        static ExceptionHelper()
        {
            Messages = new List<Message>();
        }

        private static IList<Message> Messages { set; get; }

        public static void AddError(string content, string action)
        {
            if (Messages.Count > 50)
            {
                Messages.RemoveAt(0);
            }
            Messages.Add(new Message { Action = action, Time = DateTime.Now, Content = content });
        }

        public static IList<Message> GetMessages { get { return Messages; } }

        public class Message
        {
            public DateTime Time { set; get; }

            public string Action { set; get; }

            public string Content { set; get; }
        }
    }
}
