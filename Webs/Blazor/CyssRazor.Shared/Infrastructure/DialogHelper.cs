﻿using BootstrapBlazor.Components;
using CyssBlazor.Shared.Components;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Infrastructure
{

    public class DialogHelper
    {

        private DialogService _dialogService;
        private ToastService _toastService;
        private SwalService _swalService;
        private MessageService _messageService;
        private IJSRuntime _iJSRuntime;
        private ModalService _modalService;
        public DialogHelper(DialogService dialogService,
            ToastService toastService,
            SwalService swalService,
            MessageService messageService,
            ModalService modalService,
            IJSRuntime iJSRuntime)
        {
            _dialogService = dialogService;
            _toastService = toastService;
            _swalService = swalService;
            _messageService = messageService;
            _iJSRuntime = iJSRuntime;
            _modalService = modalService;
        }

        public async Task Alert(string Msg)
        {
            await _iJSRuntime.InvokeVoidAsync("fun_Alert", Msg);
        }
        public async Task AlertImage(string Url)
        {
            await _iJSRuntime.InvokeVoidAsync("fun_AlertImage", Url);
        }
        public async Task AlertHtml(string Msg)
        {
            await _iJSRuntime.InvokeVoidAsync("fun_AlertHtml", Msg);
        }
        public async Task<bool> ShowConfirm(string Msg, SwalCategory cate = SwalCategory.Information)
        {
            return await _swalService.ShowModal(new SwalOption()
            {
                Category = cate,
                Title = Msg
            });
        }

        private async Task Alert(Type type, bool ShowClose = true)
        {
            var op = new SwalOption()
            {
                ShowClose = ShowClose,
                BodyTemplate = new RenderFragment(builder =>
                {
                    builder.OpenElement(0, "div");
                    builder.AddAttribute(1, "class", "text-center");
                    builder.OpenComponent(2, type);
                    builder.CloseComponent();
                    builder.CloseElement();
                })
            };
            await _swalService.Show(op);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="Title"></param>
        /// <param name="BodyContext"></param>
        /// <param name="Width"></param>
        /// <param name="OnCallbackAsync"></param>
        public void Modal<T>(BasePage page, string Title, Object BodyContext = null, int Width = 0, Func<object, Task> OnCallbackAsync = null) where T : BaseDialogPage
        {
            ModalOption option = new ModalOption();
            option.Title = Title;
            option.BodyContext = BodyContext;
            option.Width = Width;
            option.BodyTemplate = builder =>
            {
                var index = 0;
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add(nameof(BaseDialogPage.ParentPage), page);
                parameters.Add(nameof(BaseDialogPage.Dialog), option);
                parameters.Add(nameof(BodyContext), BodyContext);
                var component = BootstrapDynamicComponent.CreateComponent<T>(parameters);
                builder.AddContent(index, component.Render());
            };
            _modalService.Show(option);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="Title"></param>
        /// <param name="BodyContext"></param>
        /// <param name="Width"></param>
        /// <param name="OnCallbackAsync"></param>
        public void Modal<T, M>(BasePage page, string Title, object ObjectModel = null, int Width = 0, Func<object, Task> OnCallbackAsync = null) where T : BaseDialogPage<M> where M : class
        {
            ModalOption option = new ModalOption();
            option.Title = Title;
            option.BodyContext = ObjectModel;
            option.Width = Width;
            option.BodyTemplate = builder =>
            {
                var index = 0;
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add(nameof(BaseDialogPage.ParentPage), page);
                parameters.Add(nameof(BaseDialogPage.Dialog), option);
                parameters.Add(nameof(ObjectModel), ObjectModel);
                var component = BootstrapDynamicComponent.CreateComponent<T>(parameters);
                builder.AddContent(index, component.Render());
            };
            _modalService.Show(option);
        }


        public async Task Toast(string Content, string Title = "系统提示", ToastCategory toastCategory = ToastCategory.Information)
        {
            await _toastService.Show(new ToastOption()
            {
                Category = toastCategory,
                Title = Title,
                Content = Content
            });
        }

        public async Task Message(string Content, string Icon = "fa fa-info-circle")
        {
            await _messageService.Show(new MessageOption()
            {
                Content = Content,
                Icon = Icon,
                ShowDismiss = true,
                Color = Color.Danger
            });
        }
    }

}

