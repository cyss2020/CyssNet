﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Infrastructure
{
    /// <summary>
    /// 系统默认KEY
    /// </summary>
    public class SystemDefaultKey
    {
        public const string AuthTokenKey = "authToken";
        public const string AuthUserNameKey = "authUser";
    }
}
