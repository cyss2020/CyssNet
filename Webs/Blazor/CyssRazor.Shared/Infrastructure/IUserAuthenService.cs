﻿using Account.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Models;
using CyssBlazor.Shared.Models;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Infrastructure
{
    public interface IUserAuthenService
    {
        /// <summary>
        /// 获取上一次登陆用户名
        /// </summary>
        /// <returns></returns>
        Task<string> GetLoginUserName();

        /// <summary>
        /// 获取当前登陆用户
        /// </summary>
        /// <returns></returns>
        Task<AuthenticationUser> GetCurrentUserAsync();

        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        Task<bool> LoginAsync(LoginViewModel loginModel);

        /// <summary>
        /// 退货登陆
        /// </summary>
        /// <returns></returns>
        Task LogoutAsync();

        /// <summary>
        /// 验证token信息
        /// </summary>
        /// <returns></returns>
        Task<OperateResult<bool>> ValiToken();

    }
}
