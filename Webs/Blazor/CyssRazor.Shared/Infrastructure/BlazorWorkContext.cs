﻿using Account.Api.Dtos;
using BootstrapBlazor.Components;
using Cyss.Core;
using Cyss.Core.Models;
using System.Collections.Generic;

namespace CyssBlazor.Shared.Infrastructure
{
 

    public class BlazorWorkContext : IBlazorWorkContext
    {
        public BlazorWorkContext()
        {
            UserRoles = new List<int>();
        }

        public AuthenticationUser User { set; get; }

        public SystemConfig SystemConfig { set; get; }

        public bool IsLogin { set; get; }


        public List<int> UserRoles { set; get; }

        public List<PermissionModel> Permissions { set; get; }

        public List<MenuModel> Menus { set; get; }

        public List<MenuItem> MenuItems { set; get; }
        /// <summary>
        /// 用户认证token
        /// </summary>
        public string Token { set; get; }

        //public OperateResult<LoginReturnModel> ApiResponse { set; get; }


        private string _CurrentPageUrl;
        /// <summary>
        /// 当前页面 的权限Id
        /// </summary>
        public string CurrentPageUrl
        {
            set
            {
                _CurrentPageUrl = value;
            }
            get
            {
                return _CurrentPageUrl;
            }
        }

    }
}
