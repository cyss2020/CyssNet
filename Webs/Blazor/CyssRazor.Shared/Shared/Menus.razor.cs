﻿using Account.Api.Dtos;
using BootstrapBlazor.Components;
using Cyss.Core;
using CyssBlazor.Shared.Components;
using CyssBlazor.Shared.Infrastructure;
using CyssBlazor.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Shared
{
    public partial class Menus
    {
        [Inject]
        protected IBlazorWorkContext _workContext { set; get; }

        [Inject]
        private NavigationManager navigationManager { set; get; }
        /// <summary>
        /// 获得/设置 IJSRuntime 实例
        /// </summary>
        [Inject]
        [NotNull]
        protected IJSRuntime? JSRuntime { get; set; }


        public static List<MenuItem> MenuItems { get; set; } = new List<MenuItem>(10000);


        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                InitMenus();
                await InvokeAsync(StateHasChanged);
            }

            if (string.IsNullOrWhiteSpace(_workContext.CurrentPageUrl))
            {
                var action = navigationManager.Uri.Replace(navigationManager.BaseUri, "/");
                if (_workContext.Menus != null)
                {
                    var menu = _workContext.Menus.Cast<MenuModel>().FirstOrDefault(x => x.Rotue == action);
                    if (menu != null)
                    {
                        _workContext.CurrentPageUrl = menu.Rotue;
                    }
                }
            }
            await base.OnAfterRenderAsync(firstRender);
        }

        private Task OnClickMenu(MenuItem item)
        {
            if (!item.Items.Any())
            {
                _workContext.CurrentPageUrl = item.Url;
                StateHasChanged();
            }
            return Task.CompletedTask;
        }

        public void InitMenus()
        {
            MenuItems = new List<MenuItem>();

            if (_workContext.Menus != null)
            {
                var menus = _workContext.Menus.Cast<MenuModel>();
                ExceptionHelper.AddError(menus.ToSerializeObject(), "InitMenus");
                foreach (var menu in menus.Where(x => x.ParentMenuId == 0).OrderBy(x => x.DisplayOrder))
                {
                    if (menus.Count(x => x.ParentMenuId == menu.Id) > 0)
                    {
                        var item = new MenuItem()
                        {
                            Text = menu.Name,
                            Icon = menu.Icon,
                        };
                        if (!string.IsNullOrWhiteSpace(menu.Rotue))
                        {
                            item.Url = menu.Rotue;
                        }
                        MenuItems.Add(item);
                        CreateChildMenu(item, menu);
                    }
                }
                _workContext.MenuItems=MenuItems;
            }
            ExceptionHelper.AddError(MenuItems.ToSerializeObject(), "InitMenus2");
            StateHasChanged();
        }

        /// <summary>
        /// 创建子菜单
        /// </summary>
        /// <param name="item"></param>
        /// <param name="prermission"></param>
        private void CreateChildMenu(MenuItem item, MenuModel menu)
        {
            List<MenuItem> items = new List<MenuItem>();
            foreach (var pre in _workContext.Menus.Where(x => x.ParentMenuId == menu.Id).OrderBy(x => x.DisplayOrder))
            {
                var newItem = new MenuItem()
                {
                    Text = pre.Name,
                    Icon = pre.Icon,
                };
                if (!string.IsNullOrWhiteSpace(pre.Rotue))
                {
                    newItem.Url = pre.Rotue;
                }
                newItem.IsActive = true;

                items.Add(newItem);
                CreateChildMenu(newItem, pre);
            }
            item.Items = items;
        }

    }
}
