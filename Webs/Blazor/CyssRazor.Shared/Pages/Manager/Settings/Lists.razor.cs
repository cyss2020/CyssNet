﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using Common.Api.Dtos;
using CyssBlazor.Shared.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.Manager.Settings
{
    public partial class Lists
    {

        /// <summary>
        /// 主表
        /// </summary>
        private MyTable<SettingTypeModel> table { set; get; }

        private SettingTypeModel CurrentClickRow = new SettingTypeModel();


        private async Task ClickRowCallback(SettingTypeModel model)
        {
            this.CurrentClickRow = model;
            await InvokeAsync(StateHasChanged);
        }

        /// <summary>
        ///数据查询方法
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected async Task<QueryData<SettingTypeModel>> OnQueryAsync(QueryPageOptions options)
        {
            //TODO: 此处代码后期精简
            var apiResponse = await CommonClientFactory.Setting.GetSettingTypes();
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                this.CurrentClickRow = null;
                return await Task.FromResult(new QueryData<SettingTypeModel>()
                {
                    Items = apiResponse.Data,
                    TotalCount = apiResponse.Data.Count()
                });
            }
            return null;
        }


    }
}
