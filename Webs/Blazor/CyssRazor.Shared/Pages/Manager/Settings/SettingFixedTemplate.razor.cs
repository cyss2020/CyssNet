﻿using Common.Api.Client;
using Common.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.Manager.Settings
{
    public partial class SettingFixedTemplate
    {
        [Parameter]
        public BaseSettingModel Model { set; get; } = new BaseSettingModel();

        /// <summary>
        /// 是否加载完成
        /// </summary>
        [Parameter]
        public bool IsLoadComplete { set; get; }

        /// <summary>
        /// 获得/设置 TableHeader 实例
        /// </summary>
        [Parameter]
        public RenderFragment? ChildContent { get; set; }


        /// <summary>
        /// 类型
        /// </summary>
        [Parameter]
        public SettingTypeModel SettingType { set; get; } = new SettingTypeModel();


        /// <summary>
        /// 获得/设置 按钮点击后的回调方法
        /// </summary>
        [Parameter]
        public Func<Task>? OnSave { get; set; }

        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        protected virtual async Task Save()
        {
            await OnSave?.Invoke();
        }


    }
}
