﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using Common.Api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.Manager.Tasks
{
    public partial class List
    {
        protected async override Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);
        }
        private JobModel CurrentJobModel { set; get; }

        public TaskSearchModel searchModel { set; get; } = new TaskSearchModel();


        /// <summary>
        ///数据查询方法
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected async Task<QueryData<JobModel>> OnQueryAsync(QueryPageOptions options)
        {
            //TODO: 此处代码后期精简
            searchModel.pageIndex = options.PageIndex;
            searchModel.pageSize = options.PageItems;
            searchModel.SearchName = options.SearchText;
            var apiResponse = await CommonClientFactory.Task.GetPageListTasks(searchModel);
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                return await Task.FromResult(new QueryData<JobModel>()
                {
                    Items = apiResponse.Data.Data,
                    TotalCount = apiResponse.Data.Total
                });
            }
            return null;
        }

        protected async Task<bool> OnSaveAsync(JobModel options, ItemChangedType itemChangedType)
        {
            if (options.Id == 0)
            {
                var apiResponse = await CommonClientFactory.Task.CreateTask(options);
                return apiResponse.IsSuccess;
            }
            else
            {
                var apiResponse = await CommonClientFactory.Task.EditTask(options);
                return apiResponse.IsSuccess;
            }
        }

        protected async Task<bool> OnDeleteAsync(IEnumerable<JobModel> options)
        {
            var apiResponse = await CommonClientFactory.Task.DeleteTask(options.FirstOrDefault().Id);
            return apiResponse.IsSuccess;
        }

    }

}
