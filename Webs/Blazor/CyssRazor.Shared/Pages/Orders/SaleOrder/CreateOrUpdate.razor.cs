﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using CyssBlazor.Shared.Infrastructure;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Order.Api.Client;
using Order.Api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.Orders.SaleOrder
{
    public partial class CreateOrUpdate
    {


        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                RefreshIdempotenceToken();
            }
            return base.OnAfterRenderAsync(firstRender);
        }


        private async Task SaveAsync(EditContext context)
        {
            if (Model.Id == 0)
            {
                if (!string.IsNullOrWhiteSpace(this.IdempotenceToken))
                {
                    var apiResponse = await OrderClientFactory.SaleOrder.CreateSaleOrder(Model, this.IdempotenceToken);
                    RefreshIdempotenceToken();
                    await this.Alert("保存成功");
                }
            }
            else
            {
                var apiResponse = await OrderClientFactory.SaleOrder.EditSaleOrder(Model);
                RefreshIdempotenceToken();
                if (apiResponse.IsSuccess)
                {
                    await this.Alert("保存成功");
                }
                else
                {
                    await this.Alert("保存失败:" + apiResponse.Message);
                }

            }
        }
    }
}
