﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using Microsoft.AspNetCore.Components;
using Order.Api.Client;
using Order.Api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.Orders.SaleOrder
{
    public partial class SaleOrderItems
    {
        [Parameter]
        public int SaleOrderId { set; get; }

        private string IdempotenceId { set; get; }

        private SaleOrderItemSearchModel searchModel = new SaleOrderItemSearchModel();


        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                ResetIdempotence();
            }
            return base.OnAfterRenderAsync(firstRender);
        }

        /// <summary>
        ///数据查询方法
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected async Task<QueryData<SaleOrderItemModel>> OnQueryAsync(QueryPageOptions options)
        {
            if (SaleOrderId <= 0)
            {
                return await Task.FromResult(new QueryData<SaleOrderItemModel>()
                {
                    Items=new List<SaleOrderItemModel>(),
                    TotalCount =0
                });
            }

            //TODO: 此处代码后期精简
            searchModel.pageIndex = options.PageIndex;
            searchModel.pageSize = options.PageItems;
            searchModel.SaleOrderId = SaleOrderId;
            var apiResponse = await OrderClientFactory.SaleOrder.GetPageListSaleOrderItems(searchModel);
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                return await Task.FromResult(new QueryData<SaleOrderItemModel>()
                {
                    Items = apiResponse.Data.Data,
                    TotalCount = apiResponse.Data.Total
                });
            }
            return null;
        }

        /// <summary>
        /// 重置
        /// </summary>
        private async void ResetIdempotence()
        {
            var operateResult = await CommonClientFactory.Helper.GetIdempotence();
            if (operateResult.IsSuccess)
            {
                IdempotenceId = operateResult.Data;
            }
        }
        protected async Task<bool> OnSaveAsync(SaleOrderItemModel options, ItemChangedType itemChangedType)
        {
            if (options.Id == 0)
            {
                if (!string.IsNullOrWhiteSpace(IdempotenceId))
                {
                    options.SaleOrderId = SaleOrderId;
                    var apiResponse = await OrderClientFactory.SaleOrder.CreateSaleOrderItem(options, IdempotenceId);
                    ResetIdempotence();
                    return apiResponse.IsSuccess;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                var apiResponse = await OrderClientFactory.SaleOrder.EditSaleOrderItem(options);
                return apiResponse.IsSuccess;
            }
        }

        protected async Task<bool> OnDeleteAsync(IEnumerable<SaleOrderItemModel> options)
        {
            var apiResponse = await OrderClientFactory.SaleOrder.DeleteSaleOrderItem(options.FirstOrDefault().Id);
            return apiResponse.IsSuccess;
        }
    }
}
