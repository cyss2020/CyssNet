﻿using Account.Api.Client;
using Account.Api.Dtos;
using BootstrapBlazor.Components;
using Cyss.Core;
using CyssBlazor.Shared.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.AccountManager.Menus
{
    public partial class List
    {

        private Table<MenuTree> table;
        private MenuModel Model { set; get; }

        public MenuSearchModel searchModel { set; get; } = new MenuSearchModel();

        private IList<MenuModel> MenuModels { set; get; }
        /// <summary>
        ///数据查询方法
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected async Task<QueryData<MenuTree>> OnQueryAsync(QueryPageOptions options)
        {
            //TODO: 此处代码后期精简
            searchModel.pageIndex = options.PageIndex;
            searchModel.pageSize = options.PageItems;
            searchModel.SearchName = options.SearchText;
            var apiResponse = await AccountClientFactory.Menu.GetPageListMenus(searchModel);
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                MenuModels = apiResponse.Data.Data.ToList();
                return await Task.FromResult(new QueryData<MenuTree>()
                {
                    Items = apiResponse.Data.Data.Where(x => x.ParentMenuId == 0).OrderBy(x => x.DisplayOrder).Select(x => { var m = x.ToObject<MenuTree>(); m.HasChildren = true; return m; })
                });
            }
            return null;
        }

        protected async Task<bool> OnSaveAsync(MenuModel options, ItemChangedType itemChangedType)
        {
            if (options.Id == 0)
            {
                var apiResponse = await AccountClientFactory.Menu.CreateMenu(options);
                return apiResponse.IsSuccess;
            }
            else
            {
                var apiResponse = await AccountClientFactory.Menu.EditMenu(options);
                return apiResponse.IsSuccess;
            }
        }

 

        private async Task<IEnumerable<TableTreeNode<MenuTree>>> TreeNodeConverter(IEnumerable<MenuTree> items)
        {
            // 构造树状数据结构
            var ret = BuildTreeNodes(items);
            return await Task.FromResult(ret);

            IEnumerable<TableTreeNode<MenuTree>> BuildTreeNodes(IEnumerable<MenuTree> items)
            {
                var ret = new List<TableTreeNode<MenuTree>>();
                ret.AddRange(items.OrderBy(x => x.DisplayOrder).Select((foo, index) => new TableTreeNode<MenuTree>(foo)
                {
                    // 此处为示例，假设偶行数据都有子数据
                    HasChildren =foo.HasChildren,
                    // 如果子项集合有值 则默认展开此节点
                    IsExpand =false,
                    // 获得子项集合
                    Items = BuildTreeNodes(foo.Children)
                }));
                return ret;
            }


        }

        protected async Task<bool> OnDeleteAsync(IEnumerable<MenuModel> options)
        {
            var apiResponse = await AccountClientFactory.Menu.DeleteMenu(options.FirstOrDefault().Id);
            return apiResponse.IsSuccess;
        }

        private async Task CreateChildrenMenu(MenuTree menu)
        {
            this.ShowModal<CreateOrUpdate, MenuTree>("创建子菜单", menu, 1200);
            await Task.CompletedTask;
        }


        public override async Task Refresh(object state)
        {
            await table.QueryAsync();
        }


        private async Task CreatePermission(MenuTree menu)
        {
            this.ShowModal<PermissionList>($"{menu.Name} -权限管理", menu, 1200);
            await Task.CompletedTask;
        }
    }
    public class MenuTree : MenuModel
    {
        public IEnumerable<MenuTree>? Children { get; set; }

        public bool IsMenu { set; get; }

        public bool HasChildren { get; set; }
    }

}
