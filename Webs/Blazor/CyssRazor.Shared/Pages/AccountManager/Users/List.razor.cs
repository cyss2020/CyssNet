﻿using Account.Api.Client;
using Account.Api.Dtos;
using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.AccountManager.Users
{
    public partial class List
    {
        protected async override Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);
        }
        private UserModel CurrentUserModel { set; get; }

        public UserSearchModel searchModel { set; get; } = new UserSearchModel();

        private async Task ResertAsync()
        {
            //var apiResponse = await AccountClientFactory.User.ResetPassWord(CurrentUserModel.UserId);
            //if (apiResponse.IsSuccess == true)
            //    await IOCFactory.DialogHelper.Toast("密码重置成功！！！");
            //else
            //    await IOCFactory.DialogHelper.Toast(apiResponse.Message);
        }

        /// <summary>
        ///数据查询方法
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected async Task<QueryData<UserModel>> OnQueryAsync(QueryPageOptions options)
        {
            //TODO: 此处代码后期精简
            searchModel.pageIndex = options.PageIndex;
            searchModel.pageSize = options.PageItems;
            searchModel.SearchName = options.SearchText;
            var apiResponse = await AccountClientFactory.User.GetPageListUsers(searchModel);
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                return await Task.FromResult(new QueryData<UserModel>()
                {
                    Items = apiResponse.Data.Data,
                    TotalCount = apiResponse.Data.Total
                });
            }
            return null;
        }

        protected async Task<bool> OnSaveAsync(UserModel options, ItemChangedType itemChangedType)
        {
            if (options.Id == 0)
            {
                var apiResponse = await AccountClientFactory.User.CreateUser(options);
                return apiResponse.IsSuccess;
            }
            else
            {
                var apiResponse = await AccountClientFactory.User.EditUser(options);
                return apiResponse.IsSuccess;
            }
        }

        protected async Task<bool> OnDeleteAsync(IEnumerable<UserModel> options)
        {
            var apiResponse = await AccountClientFactory.User.DeleteUser(options.FirstOrDefault().Id);
            return apiResponse.IsSuccess;
        }
    }

}
