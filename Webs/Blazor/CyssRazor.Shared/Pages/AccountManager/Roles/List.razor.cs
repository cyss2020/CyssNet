﻿using Account.Api.Client;
using Account.Api.Dtos;
using BootstrapBlazor.Components;
using CyssBlazor.Shared.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Pages.AccountManager.Roles
{
    public partial class List
    {
        protected async override Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);
        }
        private RoleModel CurrentRoleModel { set; get; }

        public RoleSearchModel searchModel { set; get; } = new RoleSearchModel();


        /// <summary>
        ///数据查询方法
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected async Task<QueryData<RoleModel>> OnQueryAsync(QueryPageOptions options)
        {
            //TODO: 此处代码后期精简
            searchModel.pageIndex = options.PageIndex;
            searchModel.pageSize = options.PageItems;
            searchModel.SearchName = options.SearchText;
            var apiResponse = await AccountClientFactory.Role.GetPageListRoles(searchModel);
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                return await Task.FromResult(new QueryData<RoleModel>()
                {
                    Items = apiResponse.Data.Data,
                    TotalCount = apiResponse.Data.Total
                });
            }
            return null;
        }

        protected async Task<bool> OnSaveAsync(RoleModel options, ItemChangedType itemChangedType)
        {
            if (options.Id == 0)
            {
                var apiResponse = await AccountClientFactory.Role.CreateRole(options);
                return apiResponse.IsSuccess;
            }
            else
            {
                var apiResponse = await AccountClientFactory.Role.EditRole(options);
                return apiResponse.IsSuccess;
            }
        }

        protected async Task<bool> OnDeleteAsync(IEnumerable<RoleModel> options)
        {
            var apiResponse = await AccountClientFactory.Role.DeleteRole(options.FirstOrDefault().Id);
            return apiResponse.IsSuccess;
        }

        private async Task EditRolePermission(RoleModel role)
        {
            this.ShowModal<RolePermission>($"编辑角色-{role.Name}-的权限", role, 1200);
            await Task.CompletedTask;
        }

    }
}
