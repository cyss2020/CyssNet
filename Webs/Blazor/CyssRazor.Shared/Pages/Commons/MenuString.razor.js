﻿(function ($) {
    var this_page_MenuString;
    $.extend({
        bb_set_menuString: function (p) {
            this_page_MenuString = p;
        },
        bb_refresh_menuString: function (p) {
            this_page_MenuString.invokeMethodAsync('RefreshMenuString');
        },
    })
})(jQuery);
