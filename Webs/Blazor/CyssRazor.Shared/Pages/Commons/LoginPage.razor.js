﻿(function ($) {
    var this_page;
    $.extend({
        login_set_page: function (p) {
            this_page = p;
        },
    })
    $(document).keydown(function (event) {
        if (this_page != undefined) {
            this_page.invokeMethodAsync('Keydowns', event.keyCode);
        }
    });

})(jQuery);
