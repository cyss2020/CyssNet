﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Models
{
    public class LoginViewModel
    {
        public string UserNo { get; set; }
        public string UserPwd { get; set; }

        public string Message { set; get; }
    }
}
