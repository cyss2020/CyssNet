﻿using BootstrapBlazor.Components;
using Cyss.Core;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class InputFileBase : BaseValidateComponent<int>
    {
        /// <summary>
        /// 组件Id
        /// </summary>
        protected string Id = Guid.NewGuid().ToString().Replace("-", "");

        /// <summary>
        /// 文件名
        /// </summary>
        protected string FileName { set; get; }

        /// <summary>
        /// 上传input
        /// </summary>
        protected InputFile inputFile { set; get; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsDisabled { set; get; }

        [Parameter]
        public virtual string AllowFileType { set; get; }

        /// <summary>
        /// 最多同时传入文件个数
        /// </summary>
        [Parameter]
        public int MaximumFileCount { set; get; } = 1;

        /// <summary>
        /// 单个文件最大字节  默认5m
        /// </summary>
        [Parameter]
        public int MaxAllowedSize { set; get; } = 5 * 1024 * 1024;

        [Parameter]
        public bool IsMultiple { set; get; } = false;


        /// <summary>
        /// 是否禁用
        /// </summary>
        /// <param name="isDisabled"></param>
        public void SetDisabled(bool isDisabled)
        {
            IsDisabled = isDisabled;
            StateHasChanged();
        }

        /// <summary>
        /// 获得/设置 按钮颜色
        /// </summary>
        [Parameter]
        public Color Color { get; set; } = Color.Primary;

        /// <summary>
        /// 获得/设置 Size 大小
        /// </summary>
        [Parameter]
        public Size Size { get; set; } = Size.None;


        /// <summary>
        /// 获得 按钮样式集合
        /// </summary>
        /// <returns></returns>
        protected string? ClassName => CssBuilder.Default("btn btn-browser")
            .AddClass($"btn-{Color.ToDescriptionString()}", Color != Color.None)
            .AddClass($"btn-{Size.ToDescriptionString()}", Size != Size.None)
            .Build();


        /// <summary>
        /// 点击按钮
        /// </summary>
        /// <returns></returns>
        protected async Task Select()
        {
            await _jSRuntime.InvokeVoidAsync(null, "bb_simulated_click", Id);
        }

        protected async Task<bool> Verification(InputFileChangeEventArgs e)
        {
            if (e.FileCount <= 0)
            {
                await this.Alert("没有上传文件！");
                return false;
            }
            if (e.FileCount > MaximumFileCount)
            {
                await this.Alert($"最多能同时传入{MaximumFileCount}个文件");
                return false;
            }
            return true;
        }

        protected async Task<bool> Verification(IReadOnlyList<IBrowserFile> files)
        {
            foreach (var file in files)
            {
                if (file.Size > MaxAllowedSize)
                {
                    await this.Alert($"单个文件最大上传限制为{BaseFileModelExtensions.GetSizes(MaxAllowedSize)}");
                    return false;
                }

                if (!string.IsNullOrWhiteSpace(AllowFileType) && !AllowFileType.Split(",").Contains(System.IO.Path.GetExtension(file.Name)))
                {
                    await this.Alert($"只允许上传的文件类型为{AllowFileType}");
                    return false;
                }
            }
            return true;
        }


    }
}
