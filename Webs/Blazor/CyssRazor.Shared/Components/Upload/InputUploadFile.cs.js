﻿(function ($) {
    $.extend({
        bb_add_button: function (el, id) {
            var set_header_canter_init = setInterval(() => {
                var $btn = $("#" + id);
                var $group = $(el).find(".input-group");
                if ($btn.length > 0 && $group.length > 0) {
                    $group.append($btn);
                    clearInterval(set_header_canter_init);
                }
            }, 10);
        },
        bb_show_button: function (id) {
            var $btn = $("#" + id);
            $btn.show();
        },
        bb_hide_button: function (id) {
            var $btn = $("#" + id);
            $btn.hide();
        },
    })
})(jQuery);
