﻿using Account.Api.Client;
using Account.Api.Dtos;
using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class SelectRole<TItem> : BaseSelect<TItem>
    {
        public SelectRole()
        {
            this.DisplayText="角色";
            this.ShowLabel=true;
        }

        protected override async Task<List<SelectedItem>> GetSelectedItems()
        {
            var searchModel = new RoleSearchModel();
            searchModel.getOnlyData = true;
            searchModel.pageSize = int.MaxValue;
            var apiResponse = await AccountClientFactory.Role.GetPageListRoles(searchModel);
            if (apiResponse.IsSuccess)
            {
                return apiResponse.Data.Data.Select(item => new SelectedItem { Value = item.Id.ToString(), Text = item.Name }).ToList();
            }
            return new List<SelectedItem>();
        }
    }
}
