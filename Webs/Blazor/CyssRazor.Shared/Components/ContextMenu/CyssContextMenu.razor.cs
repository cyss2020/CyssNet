﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public partial class CyssContextMenu
    {

        /// <summary>
        /// 获得/设置 IJSRuntime 实例
        /// </summary>
        [Inject]
        protected IJSRuntime _JSRuntime { get; set; }

        /// <summary>
        /// 获得/设置 TableHeader 实例
        /// </summary>
        [Parameter]
        public RenderFragment ChildContent { get; set; }

        /// <summary>
        ///是否显示复制按钮
        /// </summary>
        [Parameter]
        public bool IsShowCopyButton { get; set; } = true;


        [Parameter]
        public object Model { set; get; }

        protected async override Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await _JSRuntime.InvokeVoidAsync("$.bb_contextmenu_init", this.Element);
            }
            await base.OnAfterRenderAsync(firstRender);
        }

        private async Task Copy()
        {
            await _JSRuntime.InvokeVoidAsync("$.bb_copyToClipboard");
        }
    }
}
