﻿(function ($) {
    $.extend({
        bb_contextmenu_init: function (el) {
            var $group = $(el).parent();
            $group.addClass("contextmenu-group")
            $group.data("contextmenu", $(el))
        },
        bb_contextmenu_binding: function (el) {
            $(".table-fixed-body,.table-bordered tbody").each(function () {
                var iscontextmenu = $(this).data("iscontextmenu");
                if (iscontextmenu != 1) {
                    $(this).data("iscontextmenu", 1);
                    $(this).contextmenu(function (e) {
                        var $group = $(this).parents(".contextmenu-group").first();
                        var $contextmenu = $group.data("contextmenu");
                        if ($contextmenu != undefined) {
                            var winWidth = $(document).width();
                            var winHeight = $(document).height();
                            // 鼠标点击位置坐标
                            var mouseX = e.pageX;
                            var mouseY = e.pageY;
                            // ul标签的宽高
                            var menuWidth = $contextmenu.find(".contextmenu").width();
                            var menuHeight = $contextmenu.find(".contextmenu").height();
                            // 最小边缘margin(具体窗口边缘最小的距离)
                            var minEdgeMargin = 10;
                            // 以下判断用于检测ul标签出现的地方是否超出窗口范围
                            // 第一种情况：右下角超出窗口
                            if (mouseX + menuWidth + minEdgeMargin >= winWidth &&
                                mouseY + menuHeight + minEdgeMargin >= winHeight) {
                                menuLeft = mouseX - menuWidth - minEdgeMargin + "px";
                                menuTop = mouseY - menuHeight - minEdgeMargin + "px";
                            }
                            // 第二种情况：右边超出窗口
                            else if (mouseX + menuWidth + minEdgeMargin >= winWidth) {
                                menuLeft = mouseX - menuWidth - minEdgeMargin + "px";
                                menuTop = mouseY + minEdgeMargin + "px";
                            }
                            // 第三种情况：下边超出窗口
                            else if (mouseY + menuHeight + minEdgeMargin >= winHeight) {
                                menuLeft = mouseX + minEdgeMargin + "px";
                                menuTop = mouseY - menuHeight - minEdgeMargin + "px";
                            }
                            // 其他情况：未超出窗口
                            else {
                                menuLeft = mouseX + minEdgeMargin + "px";
                                menuTop = mouseY + minEdgeMargin + "px";
                            };
                            // ul菜单出现
                            $contextmenu.find(".contextmenu").css({
                                "left": menuLeft,
                                "top": menuTop
                            }).show();
                            // 阻止浏览器默认的右键菜单事件
                            return false;
                        }
                    });
                }

            });
        },
        bb_copyToClipboard: function () {
            const el = document.createElement('textarea');
            el.value = contextmenu_SelectionText;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            const selected =
                document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;
            el.select();
            document.execCommand('Copy'); // 执行浏览器的复制命令
            document.body.removeChild(el);
            if (selected) {
                document.getSelection().removeAllRanges();
                document.getSelection().addRange(selected);
            }
        }
    });

    var contextmenu_SelectionText = "";
    // 文档加载后激活函数 
    $(document).ready(function () {

        var ints = self.setInterval(() => { $.bb_contextmenu_binding(); }, 100);
        // 点击之后，右键菜单隐藏
        $(document).click(function () {
            $(".contextmenu").hide();
        });
        $(document).mouseup(function () {
            var text = contextmenu_SelectionText;
            if (window.getSelection) {
                text = window.getSelection().toString();
            }
            else {
                text = document.selection.createRange().text;
            }
            if (text != undefined) {
                text = text.trim();
                if (text != "") {
                    contextmenu_SelectionText = text;
                }
            }
        });
    });

})(jQuery);



