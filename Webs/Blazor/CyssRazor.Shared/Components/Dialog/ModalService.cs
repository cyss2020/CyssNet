﻿using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class ModalService
    {
        public List<ModalOption> Options { set; get; } = new List<ModalOption>();
        public delegate void ModalEventHander(ModalOption e);
        public event ModalEventHander ShowEvent;
        public event ModalEventHander CloseEvent;
        public event ModalEventHander RemoveEvent;

        public void Show(ModalOption option)
        {
            Options.Add(option);
            ShowEvent?.Invoke(option);
        }

        public void Close(string Id)
        {
            var option = this.Options.FirstOrDefault(x => x.Id == Id);
            if (option != null)
            {
                CloseEvent?.Invoke(option);
            }
        }

        public void Remove(string Id)
        {
            var option = this.Options.FirstOrDefault(x => x.Id == Id);
            if (option != null)
            {
                RemoveEvent?.Invoke(option);
            }
        }
    }
}
