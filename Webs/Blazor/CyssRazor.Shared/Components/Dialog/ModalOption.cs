﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class ModalOption
    {
        public string Title { set; get; }

        public int Width { set; get; }

        public string Id { set; get; } = Guid.NewGuid().ToString().Replace("-", "");

        //
        // 摘要:
        //     获得/设置 相关连数据，多用于传值使用
        public object? BodyContext
        {
            get;
            set;
        }


        public RenderFragment? BodyTemplate
        {
            get;
            set;
        }


    }
}
