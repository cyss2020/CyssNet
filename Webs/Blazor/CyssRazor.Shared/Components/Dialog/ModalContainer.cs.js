﻿(function ($) {
    $.extend({
        bb_modal_show: function (id) {
            $(document.body).append($('#' + id));
            $('#' + id).modal({ backdrop: 'static', keyboard: false });
            $('#' + id).modal('show');
        },
        bb_modal_hide: function (id) {
            $('#' + id).modal('hide');
        },
        bb_modal_toggle: function (id) {
            $('#' + id).modal('toggle');
        },
    })

    var zindex = 1055;

    $(document).ready(function () {
        var ints = self.setInterval(() => { setDialog(); }, 100);
        $(document).on("show.bs.modal", ".modal", function () {
            $(this).draggable({
                handle: ".modal-header"   // 只能点击头部拖动
            });
            $(this).css("overflow-y", "hidden");
            zindex = zindex + 10;
            $(this).css("z-index", zindex);
            // 防止出现滚动条，出现的话，你会把滚动条一起拖着走的
        });
        $(document).on("shown.bs.modal", ".modal", function () {
            $(this).next().css("z-index", zindex - 5);
            // 防止出现滚动条，出现的话，你会把滚动条一起拖着走的
        });

        $(document).on("hidden.bs.modal", ".modal", function () {
            zindex = zindex - 10;
            var zindexs = [];
            $("div.modal.show").each(function () {
                zindexs.push($(this).css("z-index"));
            });
            $("div.modal-backdrop.show").each(function () {
                $(this).css("z-index", Math.max.apply(null, zindexs) - 1);
            });

        });
    });

    function setDialog() {
        var w_height = $(window).height() - 50;
        $(".modal-dialog").each(function () {
            var $modal = $(this).children(".modal-content");
            if ($modal == undefined || $modal.length == 0) {
                return true
            }
            if ($modal.height() <= w_height) {
                return true
            }
            $modal.css("max-height", w_height);
            $modal.children(".modal-body")
                .css("overflow", "auto")
                .css("max-height", w_height - $modal.children(".modal-header").outerHeight() - $modal.children(".modal-footer").outerHeight());
        });
    }
})(jQuery);