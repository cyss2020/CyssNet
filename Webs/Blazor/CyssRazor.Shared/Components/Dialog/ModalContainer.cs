﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public partial class ModalContainer : ComponentBase
    {
        [Inject]
        private ModalService modalService { set; get; }


        private bool IsShow { set; get; }
        private bool IsClose { set; get; }

        private bool IsRemove { set; get; }

        protected override async void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                modalService.ShowEvent += ModalService_ShowEvent;
                modalService.CloseEvent += ModalService_CloseEvent;
                modalService.RemoveEvent += ModalService_RemoveEvent;
            }
            if (IsShow)
            {
                await Show();
                IsShow = false;
            }
            if (IsClose)
            {
                await Close();
                IsClose = false;
            }
            if (IsRemove)
            {
                await Remove();
                IsRemove = false;
            }
            base.OnAfterRender(firstRender);
        }

        private void ModalService_RemoveEvent(ModalOption e)
        {
            NewId = e.Id;
            IsRemove = true;
            StateHasChanged();
        }

        private void ModalService_CloseEvent(ModalOption e)
        {
            NewId = e.Id;
            IsClose = true;
            StateHasChanged();
        }

        private void ModalService_ShowEvent(ModalOption e)
        {
            NewId = e.Id;
            IsShow = true;
            StateHasChanged();
        }

        public override async Task SetParametersAsync(ParameterView parameters)
        {
            await base.SetParametersAsync(parameters);
            await InvokeAsync(StateHasChanged);
        }

        private string NewId { set; get; }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            int index = 0;
            foreach (var option in modalService.Options)
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add(nameof(ModalPopup.ChildContent), option.BodyTemplate);
                parameters.Add(nameof(ModalPopup.Id), option.Id);
                parameters.Add(nameof(ModalPopup.Title), option.Title);
                parameters.Add(nameof(ModalPopup.Width), option.Width);
                var component = BootstrapDynamicComponent.CreateComponent<ModalPopup>(parameters);
                builder.AddContent(index, component.Render());
            }
        }

        [Inject]
        private IJSRuntime iJSRuntime { set; get; }
        private async Task Show()
        {
            await iJSRuntime.InvokeVoidAsync("$.bb_modal_show", NewId);
        }
        private async Task Close()
        {
            await iJSRuntime.InvokeVoidAsync("$.bb_modal_hide", NewId);
        }
        private async Task Remove()
        {
            await iJSRuntime.InvokeVoidAsync("$.bb_modal_hide", NewId);
            this.modalService.Options.Remove(this.modalService.Options.FirstOrDefault(x => x.Id == NewId));
            await InvokeAsync(StateHasChanged);
        }
    }
}
