﻿using BootstrapBlazor.Components;
using Cyss.Core;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public static class ButtonBaseException
    {
        /// <summary>
        /// 模拟点击
        /// </summary>
        /// <returns></returns>
        public static async Task SimulatedClick(this Button button, IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeVoidAsync("$.bb_simulated_click", button.Id);
        }
    }
}
