﻿using Common.Api.Client;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class PictureBase : BaseComponent
    {
        public PictureBase()
        {
            this.ImageUrl = this.PictureNull;
        }

        /// <summary>
        /// 图片Id
        /// </summary>
        [Parameter]
        public int PictureId { set; get; }

        /// <summary>
        /// 图片url
        /// </summary>
        [Parameter]
        public string ImageUrl { set; get; }


        /// <summary>
        /// 正在加载图片
        /// </summary>
        private string PictureLoading = "_content/CyssBlazor.Shared/img/picture_loading.gif";

        /// <summary>
        /// 暂无图片
        /// </summary>
        private string PictureNull = "_content/CyssBlazor.Shared/img/picture_null.jpg";

        /// <summary>
        /// 加载失败图片
        /// </summary>
        private string PictureFail = "_content/CyssBlazor.Shared/img/pictrue_fail.jpg";

        /// <summary>
        /// 高度
        /// </summary>
        [Parameter]
        public int Height { set; get; } = 30;

        /// <summary>
        /// 宽度
        /// </summary>
        [Parameter]
        public int Width { set; get; } = 30;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                //await LoadPicture();
            }
            await base.OnAfterRenderAsync(firstRender);
        }

        /// <summary>
        /// 初始加载图片
        /// </summary>
        protected async Task LoadPicture()
        {
            if (this.PictureId > 0)
            {
                this.ImageUrl = this.PictureLoading;
                await InvokeAsync(StateHasChanged);
                var operateResult = await CommonClientFactory.Picture.GetPictureUrlById(this.PictureId, 100);
                if (operateResult.IsSuccess)
                {
                    this.ImageUrl = operateResult.Data.ImageUrl;
                }
                else
                {
                    this.ImageUrl = this.PictureFail;
                    await this.Toast("图像加载失败!");
                }
                await InvokeAsync(StateHasChanged);
            }
        }

    }
}
