﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using Cyss.Core;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public partial class BootstrapInputPicture
    {

        public BootstrapInputPicture()
        {
            this.ImageUrl = this.PictureNull;
        }

 
        private string ImageUrl { set; get; }

        /// <summary>
        /// 获得/设置 IJSRuntime 实例
        /// </summary>
        [Inject]
        protected IJSRuntime? JSRuntime { get; set; }

        /// <summary>
        /// 正在加载图片
        /// </summary>
        private string PictureLoading = "_content/CyssBlazor.Shared/img/picture_loading.gif";

        /// <summary>
        /// 暂无图片
        /// </summary>
        private string PictureNull = "_content/CyssBlazor.Shared/img/picture_null.jpg";

        /// <summary>
        /// 加载失败图片
        /// </summary>
        private string PictureFail = "_content/CyssBlazor.Shared/img/pictrue_fail.jpg";



        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await LoadPicture();
            }
            await base.OnAfterRenderAsync(firstRender);
        }

        /// <summary>
        /// 初始加载图片
        /// </summary>
        private async Task LoadPicture()
        {
            if (this.Value > 0)
            {
                this.ImageUrl = this.PictureLoading;
                await InvokeAsync(StateHasChanged);
                var operateResult = await CommonClientFactory.Picture.GetPictureUrlById(this.Value, 100);
                if (operateResult.IsSuccess)
                {
                    this.ImageUrl = operateResult.Data.ImageUrl;
                }
                else
                {
                    this.ImageUrl = this.PictureFail;
                    await this.Toast("图像加载失败!");
                }
                await InvokeAsync(StateHasChanged);
            }
        }


        /// <summary>
        /// 文件改变事件
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private async Task OnInputFileChange(InputFileChangeEventArgs e)
        {

            if ((await Verification(e)) == false)
            {
                await InvokeAsync(StateHasChanged);
                return;
            }

            var files = e.GetMultipleFiles(maximumFileCount: MaximumFileCount);

            if ((await Verification(files)) == false)
            {
                await InvokeAsync(StateHasChanged);
                return;
            }
            this.ImageUrl = this.PictureLoading;
            SetDisabled(true);
            /// <summary>
            ///
            /// </summary>

            var imageFile = files.FirstOrDefault();
            var operateResult = await CommonClientFactory.Picture.AsyncUpload(imageFile.OpenReadStream(maxAllowedSize: MaxAllowedSize), imageFile.Name);
            if (operateResult.IsSuccess)
            {
                this.Value = operateResult.Data.PictureId;
                this.ImageUrl = operateResult.Data.ImageUrl;
            }
            else
            {
                await this.Alert("上传失败!");
            }
            SetDisabled(false);
            await InvokeAsync(StateHasChanged);

        }
    }
}
