﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    [CascadingTypeParameter(nameof(TItem))]
    public class MyTable<TItem> : Table<TItem> where TItem : class, new()
    {

        [Parameter]
        public TItem CurrentClickItem { set; get; } = new TItem();

        public MyTable()
        {
            this.IsFixedHeader = true;
            this.IsPagination = true;
            this.ShowToolbar = true;
            this.ShowRefresh = true;
            this.IsStriped = true;
            this.IsBordered = true;
            this.ShowLineNo = true;
            this.ShowLoading = true;
            this.OnClickRowCallback = ClickRowCallback;
            this.SetRowClassFormatter = RowClassFormatter;
            this.SearchDialogSize = Size.ExtraLarge;

        }

        //
        // 摘要:
        //   新的  获得/设置 双击行回调委托方法
        [Parameter]
        public Func<TItem, Task>? OnNewClickRowCallback { get; set; }

        //
        // 摘要:
        //   新的  获得/设置 行样式格式回调委托
        [Parameter]
        public Func<TItem, string?>? SetNewRowClassFormatter { get; set; }


        private async Task ClickRowCallback(TItem model)
        {
            this.CurrentClickItem = model;
            if (OnNewClickRowCallback != null)
            {
                await OnNewClickRowCallback.Invoke(model);
            }
            await InvokeAsync(StateHasChanged);
        }

        private string RowClassFormatter(TItem model)
        {
            string css = string.Empty;
            if (model == this.CurrentClickItem)
            {
                css = "click";
            }
            if (SetNewRowClassFormatter != null)
            {
                var str = SetNewRowClassFormatter.Invoke(model);
                css += " " + str;
            }
            return css;
        }

    }

}
