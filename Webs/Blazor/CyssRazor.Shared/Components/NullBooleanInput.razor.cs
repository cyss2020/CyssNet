﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;

namespace CyssBlazor.Shared.Components
{
    public partial class NullBooleanInput
    {
        [Parameter]
        public bool IsSelect { set; get; }

        private IEnumerable<SelectedItem>? SelectedItems { get; set; }

        [Parameter]
        public string RadioDisplayText { set; get; }


        private string _StrValue { set; get; }
        public string StrValue
        {
            get => GetStrValue();
            set
            {
                if (_StrValue == value) return;
                _StrValue = value;
                Value = SetValue(_StrValue);
                ValueChanged.InvokeAsync(Value);
                StrValueChanged.InvokeAsync(value);
            }
        }

        private string GetStrValue()
        {
            if (Value == null)
            {
                return "1";
            }
            if (Value == true)
            {
                return "2";
            }
            if (Value == false)
            {
                return "3";
            }
            return "1";
        }



        private bool? SetValue(string strValue)
        {
            if (strValue == "1")
                return null;
            if (strValue == "2")
                return true;
            if (strValue == "3")
                return false;
            return null;
        }

        [Parameter]
        public EventCallback<string> StrValueChanged { get; set; }

        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override void OnInitialized()
        {
            base.OnInitialized();

            var strs = this.RadioDisplayText.Split<string>(",").ToArray();
            if (strs.Count() == 3)
            {
                SelectedItems = new List<SelectedItem>
                {
                new SelectedItem("1", strs[0]),
                new SelectedItem("2",strs[1]),
                new SelectedItem("3",strs[2]),
            };
            }
            else
            {
                SelectedItems = new List<SelectedItem>
               {
                new SelectedItem("1", "全部"),
                new SelectedItem("2","是"),
                new SelectedItem("3", "否"),
            };
            }



        }


    }

}
