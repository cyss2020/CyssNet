﻿using BootstrapBlazor.Components;
using Cyss.Core;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public partial class TableFormLayoutField<TValue>
    {

        /// <summary>
        /// 获得/设置 用户自定义属性
        /// </summary>
        [Parameter(CaptureUnmatchedValues = true)]
        public IDictionary<string, object>? AdditionalAttributes { get; set; }

        /// <summary>
        /// 获得 按钮样式集合
        /// </summary>
        /// <returns></returns>
        protected string? ClassName => CssBuilder.Default("table-value")
            .AddClassFromAttributes(AdditionalAttributes)
            .Build();


        [Parameter]
        public string DisplayText { set; get; }

        [Parameter]
        public TValue Value { set; get; }

        /// <summary>
        /// Gets or sets a callback that updates the bound value.
        /// </summary>
        [Parameter]
        public EventCallback<TValue> ValueChanged { get; set; }

        /// <summary>
        /// Gets or sets an expression that identifies the bound value.
        /// </summary>
        [Parameter]
        public Expression<Func<TValue>>? ValueExpression { get; set; }

        [Parameter]
        public int Colspan { set; get; } = 1;

        private int GetColspan
        {
            get
            {
                if (this.Colspan<=1)
                {
                    return 1;
                }
                return 2*this.Colspan;

            }
        }

        /// <summary>
        /// 获得/设置 格式化字符串 如时间类型设置 yyyy-MM-dd
        /// </summary>
        [Parameter]
        public string? FormatString { get; set; }



        private string FormatValueAsString
        {
            get
            {
                if (this.Value==null)
                {
                    return string.Empty;
                }
                if (!string.IsNullOrWhiteSpace(FormatString))
                {
                    return Utility.Format(this.Value, FormatString);
                }
                var type = typeof(TValue);
                if (type.IsEnum())
                {
                    return EnumHelper.GetEnumDisplay(this.Value);
                }
                return this.Value.ToString();
            }
        }

        private string DisplayTextString
        {
            get
            {

                if (string.IsNullOrWhiteSpace(DisplayText))
                {
                    if (ValueExpression!=null)
                    {
                        var fieldIdentifier = Microsoft.AspNetCore.Components.Forms.FieldIdentifier.Create(ValueExpression);
                        DisplayText=fieldIdentifier.GetDisplayName();
                    }
                }
                return DisplayText;
            }

        }
    }
}
