﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class BaseTablePage<T, S> : BasePage where S : BaseSearchModel, new() where T:BaseModel
    {
        protected IEnumerable<T> SelectedRows = new List<T>();

        protected S SearchModel = new S();

        protected async Task OnResetSearch(S model)
        {
            SearchModel = new S();
            await InvokeAsync(StateHasChanged);
        }

        #region 导出数据


        /// <summary>
        /// 批量下载当前选中记录标签
        /// </summary>
        /// <param name="TrackingNumber"></param>
        /// <returns></returns>
        private async Task DownloadSelect()
        {
            if (!SelectedRows.Any())
            {
                await this.Toast("请选择记录！");
                return;
            }
            try
            {
                var model = new S();
                //model.Ids = SelectedRows.Select(x => x.ge).ToList();
                await DownloadXLS(model);

            }
            catch (Exception ex)
            {
                await this.Toast("下载失败！" + ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task DownloadCurrentPage()
        {
            try
            {
                await DownloadXLS(SearchModel);
            }
            catch (Exception ex)
            {
                await this.Toast("下载失败！" + ex.Message);
            }
        }
        /// <summary>
        /// 批量下载当前条件所有标签
        /// </summary>
        /// <param name="TrackingNumber"></param>
        /// <returns></returns>
        private async Task DownloadAll()
        {
            try
            {
                var newSearchModel = JsonHelper.DeserializeObject<S>(JsonHelper.SerializeObject(SearchModel));
                newSearchModel.pageIndex = 1;
                newSearchModel.pageSize = int.MaxValue;
                await DownloadXLS(newSearchModel);
            }
            catch (Exception ex)
            {
                await this.Toast("下载失败！" + ex.Message);
            }
        }
        protected virtual async Task DownloadXLS(S model)
        {
            await Task.CompletedTask;
        }
        #endregion
    }
}
