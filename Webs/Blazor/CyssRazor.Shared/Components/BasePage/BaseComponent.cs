﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using CyssBlazor.Shared.Infrastructure;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class BaseComponent : ComponentBase, IDisposable
    {
        [Inject]
        protected DialogHelper dialogHelper { set; get; }

        [Inject]
        protected IJSRuntime _jSRuntime { set; get; }

        /// <summary>
        /// 工作上下文信息
        /// </summary>
        [Inject]
        [NotNull]
        protected IBlazorWorkContext _workContext { set; get; }

        /// <summary>
        /// 
        /// </summary>
        [Inject]
        [NotNull]
        protected NavigationManager _navigationManager { set; get; }


        /// <summary>
        /// 幂等性Id
        /// </summary>
        protected string IdempotenceToken { set; get; }


        private bool isDispose = false;
        /// <summary>
        /// 组件是否释放
        /// </summary>
        protected bool IsDispose { get { return this.isDispose; } }

        /// <summary>
        /// 获得/设置  组件引用
        /// </summary>
        /// <value></value>
        public ElementReference Element { get; set; }


        /// <summary>
        /// 刷新幂等性Token
        /// </summary>
        protected async void RefreshIdempotenceToken()
        {
            var operateResult = await CommonClientFactory.Helper.GetIdempotence();
            if (operateResult.IsSuccess)
            {
                IdempotenceToken = operateResult.Data;
            }
        }


        public void Refresh()
        {
            StateHasChanged();
        }

        public async Task AsyncRefresh()
        {
            await InvokeAsync(StateHasChanged);
        }

        public virtual void Dispose()
        {
            isDispose = true;
        }

        protected async Task<bool> ShowConfirm(string msg)
        {
            return await dialogHelper.ShowConfirm(msg);
        }

        protected async Task OpenUrl(string url, string type = "_blank", int height = 700, int width = 1000)
        {
            await _jSRuntime.InvokeVoidAsync("$.bb_open_url", url, type, height, width);
        }

        /// <summary>
        /// 提示框
        /// </summary>
        /// <param name="Msg"></param>
        /// <returns></returns>
        protected async Task Alert(string Msg)
        {
            await dialogHelper.Alert(Msg);
        }

        protected async Task Toast(string Content, string Title = "系统提示", ToastCategory toastCategory = ToastCategory.Information)
        {
            await dialogHelper.Toast(Content, Title, toastCategory);
        }
        protected async Task AlertHtml(string msg)
        {
            await dialogHelper.AlertHtml(msg);
        }

        /// <summary>
        /// 弹出框
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Title"></param>
        /// <param name="BodyContext"></param>
        /// <param name="size"></param>
        /// <param name="OnCallbackAsync"></param>
        /// <returns></returns>
        protected virtual void ShowModal<T>(string Title, Object BodyContext = null, int Width = 0) where T : BaseDialogPage
        {
            dialogHelper.Modal<T>(null, Title, BodyContext, Width);
        }

        /// <summary>
        /// 弹出框
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Title"></param>
        /// <param name="BodyContext"></param>
        /// <param name="size"></param>
        /// <param name="OnCallbackAsync"></param>
        /// <returns></returns>
        protected virtual void ShowModal<T, M>(string Title, object BodyContext = null, int Width = 0) where T : BaseDialogPage<M> where M : class
        {
            dialogHelper.Modal<T, M>(null, Title, BodyContext, Width);
        }
    }
}
