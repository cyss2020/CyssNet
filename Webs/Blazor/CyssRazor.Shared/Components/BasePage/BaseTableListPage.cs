﻿using BootstrapBlazor.Components;
using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public class BaseTableListPage<TItem> : BasePage where TItem : BaseModel, new()
    {


        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                this.mainTable.IsFixedHeader = true;
                this.mainTable.IsPagination = true;
                this.mainTable.ShowToolbar = true;
                this.mainTable.ShowRefresh = true;
                this.mainTable.IsStriped = true;
                this.mainTable.IsBordered = true;
                this.mainTable.ShowLineNo = true;
                this.mainTable.ShowLoading=true;
                this.mainTable.OnClickRowCallback = ClickRowCallback;
                this.mainTable.SetRowClassFormatter = RowClassFormatter;
                this.mainTable.SearchDialogSize=Size.ExtraLarge;
                this.mainTable.SelectedRows=SelectedRows;
                this.mainTable.OnQueryAsync=BindQueryAsync;
            }
            base.OnAfterRender(firstRender);
        }

        /// <summary>
        /// 主table
        /// </summary>
        protected Table<TItem> mainTable { set; get; }

        /// <summary>
        /// 当前点单击行
        /// </summary>
        protected TItem CurrentClickRow { set; get; } = new TItem();

        /// <summary>
        /// 选中数据
        /// </summary>
        protected List<TItem> SelectedRows = new List<TItem>();

        /// <summary>
        /// 单击行事件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected virtual async Task ClickRowCallback(TItem model)
        {
            this.CurrentClickRow = model;
            await InvokeAsync(StateHasChanged);
        }

        /// <summary>
        /// 行颜色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected virtual string RowClassFormatter(TItem model)
        {
            string css = string.Empty;
            if (model == this.CurrentClickRow)
            {
                css = "click";
            }
            return css;
        }

        /// <summary>
        ///绑定table 查询
        /// </summary>
        /// <param name="items"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        protected virtual async Task<QueryData<TItem>> BindQueryAsync(QueryPageOptions options)
        {
            this.CurrentClickRow=new TItem();
            this.SelectedRows.Clear();
            var listModel = await TableQueryAsync(options);
            if (listModel!=null)
            {
                //获取订单的平台名称
                return await Task.FromResult(new QueryData<TItem>()
                {
                    Items = listModel.Data,
                    TotalCount = listModel.Total
                });
            }
            else
            {
                return await Task.FromResult(new QueryData<TItem>()
                {
                    Items = new List<TItem>(),
                    TotalCount = 0
                });
            }
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        protected virtual async Task<BasePagedListModel<TItem>> TableQueryAsync(QueryPageOptions options)
        {
            return await Task.FromResult<BasePagedListModel<TItem>>(null);
        }


        /// <summary>
        /// 刷新主表
        /// </summary>
        /// <returns></returns>
        protected virtual async Task TableRefreshAsync()
        {
            await this.mainTable.QueryAsync();
        }

        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public override async Task Refresh(object state)
        {
            await this.mainTable.QueryAsync();
        }
    }

}
