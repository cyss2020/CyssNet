﻿(function ($) {
    var _base_pages = [];
    $.extend({
        bb_set_page: function (p) {
            _base_pages.push(p);

        },
        bb_dispose_page: function (p) {
            _base_pages.pop();
        },
        bb_open_url: function (url, type, height, width) {
            window.open(url, type, "height=" + height + ", width=" + width + ", top=100, left=100, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no");
        }
    })
    $(document).keydown(function (event) {
        if (_base_pages.length <= 0) {
            return;
        }
        var this_page = _base_pages[_base_pages.length - 1];
        this_page.invokeMethodAsync('Keydowns', event.keyCode);
    });

})(jQuery);
