﻿using BootstrapBlazor.Components;
using CyssBlazor.Shared.Infrastructure;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CyssBlazor.Shared.Components
{
    public partial class BasePage : BaseComponent
    {
        /// <summary>
        /// 页面编码,配合权限使用
        /// </summary>
        protected string PageCode { set; get; }

        /// <summary>
        /// 页面Id(对应数据库Menu Id)
        /// </summary>
        private int PageId { set; get; }

        /// <summary>
        /// 获得/设置 内容组件
        /// </summary>
        [Parameter]
        public RenderFragment? ChildContent { get; set; }


        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                //向js注册当前页面
                await _jSRuntime.InvokeVoidAsync("$.bb_set_page", DotNetObjectReference.Create(this));
            }
            await base.OnAfterRenderAsync(firstRender);
        }

        /// <summary>
        /// 释放页面方法
        /// </summary>
        public override async void Dispose()
        {
            try
            {
                await _jSRuntime.InvokeVoidAsync("$.bb_dispose_page", DotNetObjectReference.Create(this));

            }
            catch
            {

            }
            base.Dispose();
        }

        /// <summary>
        /// 敲击键盘时js调用方法
        /// </summary>
        /// <param name="KeyCode"></param>
        [JSInvokable]
        public void Keydowns(int KeyCode)
        {
            Keydown(KeyCode);
        }

        /// <summary>
        /// 敲击键盘事件
        /// </summary>
        /// <param name="KeyCode"></param>
        protected virtual void Keydown(int KeyCode)
        {

        }
        /// <summary>
        /// 是否有权限
        /// </summary>
        /// <param name="OperationType"></param>
        /// <param name="ParentTrId">一般不用填,系统会默认当前页面</param>
        /// <returns></returns>
        public bool IsPermission(string PermissionCode, string CurrentPageUrl = "")
        {
            if (string.IsNullOrWhiteSpace(CurrentPageUrl))
            {
                CurrentPageUrl = _workContext.CurrentPageUrl;
            }
            if (string.IsNullOrWhiteSpace(CurrentPageUrl))
            {
                CurrentPageUrl = _navigationManager.Uri.Replace(_navigationManager.BaseUri, "/");
                _workContext.CurrentPageUrl = CurrentPageUrl;
            }
            if (_workContext.Permissions == null)
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(PageCode))
            {
                var menu = _workContext.Menus.FirstOrDefault(x => x.Rotue == _workContext.CurrentPageUrl);
                PageCode = menu?.Code;
                PageId = menu != null ? 0 : menu.Id;
            }
            if (PageId <= 0)
            {
                var menu = _workContext.Menus.FirstOrDefault(x => x.Code == PageCode);
                PageId = menu == null ? 0 : menu.Id;
            }
            return _workContext.Permissions.Any(x => x.MenuId == PageId && x.PermissionCode == PermissionCode);
        }


        protected override void ShowModal<T, M>(string Title, object BodyContext = null, int Width = 0)
        {
            base.ShowModal<T, M>(Title, BodyContext, Width);
        }

        protected override void ShowModal<T>(string Title, object BodyContext = null, int Width = 0)
        {
            base.ShowModal<T>(Title, BodyContext, Width);
        }

        //protected override void ShowModal<T>(string Title, object BodyContext = null, int Width = 0) where T : BaseDialogPage
        //{
        //    dialogHelper.Modal<T>(this, Title, BodyContext, Width);
        //}


        /// <summary>
        /// 刷新父页面(主要用于弹窗调用)
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public virtual async Task Refresh(object state)
        {
            await this.AsyncRefresh();
        }
    }
}
