﻿using BootstrapBlazor.Components;
using Common.Api.Client;
using Cyss.Core;
using Microsoft.AspNetCore.Components;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
namespace CyssBlazor.Shared.Components
{
    public class BaseDialogPage : BasePage
    {

        /// <summary>
        ///父页面对象
        /// </summary>
        [Parameter]
        public BasePage ParentPage { set; get; }

        /// <summary>
        /// 当前模态框对象
        /// </summary>
        [Parameter]
        public ModalOption Dialog { set; get; }

        /// <summary>
        /// 
        /// </summary>
        private ModalService modalService { set; get; }

        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <returns></returns>
        protected virtual void Close()
        {
            base.Dispose();
            modalService.Close(Dialog.Id);
        }

    }

    /// <summary>
    /// 弹框基础类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class BaseDialogPage<T> : BaseDialogPage
    {
        /// <summary>
        /// 传入的数据对象副本
        /// </summary>
        protected T Model { set; get; }


        [Parameter]
        public T ObjectModel { set; get; }

        protected override void OnParametersSet()
        {
            Model = ObjectModel.Copy();
            base.OnParametersSet();
        }

        /// <summary>
        /// 获得/设置提交成功回调委托
        /// </summary>
        [Parameter]
        [NotNull]
        public Func<T, Task>? OnSuccessCallbackAsync { get; set; }

        /// <summary>
        /// 获得/设置提交失败回调委托
        /// </summary>
        [Parameter]
        [NotNull]
        public Func<T, Task>? OnFailCallbackAsync { get; set; }


        /// <summary>
        /// 保存方法
        /// </summary>
        /// <returns></returns>
        protected virtual Task<bool> OnSubmitAsync()
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// 获得/设置 保存回调委托
        /// </summary>

        public async Task OnSubmit()
        {
            var fa = await OnSubmitAsync();
            if (fa && OnSuccessCallbackAsync != null)
            {
                await OnSuccessCallbackAsync(Model);
            }
        }

    }
}
