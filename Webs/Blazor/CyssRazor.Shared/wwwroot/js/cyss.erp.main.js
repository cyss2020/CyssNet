﻿
//(function () {
//    document.addEventListener('DOMContentLoaded', function () {
//        var deviceWidth = document.documentElement.clientWidth;
//        document.documentElement.style.fontSize = deviceWidth / 150 + 'px';

//    }, false);

//    window.onresize = function () {
//        var deviceWidth = document.documentElement.clientWidth;
//        document.documentElement.style.fontSize = deviceWidth / 150+ 'px';
//    };

//})();


$(document).ready(function () {
    $(window).resize(function () {           //当浏览器大小变化时

    });
    var ints = self.setInterval(() => { setTableFixed(); }, 100);
});


function setTableFixed() {
    $(".table-group").each(function () {
        var $group = $(this);
        $group.css("max-width", $group.parent().width());
        var w_height = $(window).height() - 50;
        if ($group.parent().hasClass("modal-body")) {
            var $modal = $group.parent().parent();
            var modalheaderHeight = $modal.children(".modal-header").outerHeight();
            var modalfooterHeight = $modal.children(".modal-footer").outerHeight();
            if (modalheaderHeight == undefined) {
                modalheaderHeight = 0;
            }
            if (modalfooterHeight == undefined) {
                modalfooterHeight = 0;
            }
            var d_height = w_height - modalheaderHeight - modalfooterHeight - ($modal.children(".modal-body").outerHeight() - $modal.children(".modal-body").height());

            var tableHeight = 0;
            if ($group.find(".table-container table.table").length > 0) {
                tableHeight = $group.find(".table-container table.table").outerHeight();
            }
            if ($group.find(".table-fixed-body table.table").length > 0) {
                tableHeight = $group.find(".table-fixed-body table.table").outerHeight();
            }
            var t_height = tableHeight + $group.find(".table-toolbar").outerHeight() + 15;
            if (d_height > t_height) {
                d_height = t_height;
            }
            if ($group.height() != d_height) {
                $group.css("height", d_height);
            }
        }
        var width = $group.width();
        var height = $group.height();
        if (height < 200) {
            height == 200;
            $group.css("height", height);
        }
        var table = $group.children(".table-container");
        tableContainer(table, width, height)
    });
}
function tableContainer($table, width, height) {

    if ($table.height() != height || $table.width() != width) {
        $table.css("width", width);
        $table.css("height", height);
    }



    var toolbarHeight = 0;
    var paginationHeight = 0;
    var borderedHeight = 0;
    var fixedheaderHeight = 0;
    var fixedbodyHeight = 0;

    var $toolbar = $table.find(".table-toolbar");
    var $pagination = $table.find(".table-pagination");
    var $bordered = $table.find(".table-wrapper");

    var $fixedheader = $bordered.children(".table-fixed-header");
    var $fixedbody = $bordered.children(".table-fixed-body");

    if ($toolbar.length > 0) {
        var h = $toolbar.height();
        if (h != undefined) {
            toolbarHeight = h;
        }
    }
    if ($pagination.length > 0) {
        var h = $pagination.height();
        if (h != undefined) {
            paginationHeight = h;
        }
    }
    if ($bordered.length > 0) {
        var h = $bordered.height();
        if (h != undefined) {
            borderedHeight = h;
        }
    }
    if ($fixedbody.length > 0) {
        var h = $fixedbody.height();
        if (h != undefined) {
            fixedbodyHeight = h;
        }
    }
    if ($fixedheader.length > 0) {
        var h = $fixedheader.height();
        if (h != undefined) {
            fixedheaderHeight = h;
        }
    }
    borderedHeight = height - toolbarHeight - paginationHeight - 10;
    fixedbodyHeight = borderedHeight - fixedheaderHeight - 5;
    if (borderedHeight != $bordered.height()) {
        $bordered.css("height", borderedHeight);
    }

    if ($fixedheader.length > 0) {
        if ($fixedbody.height() != fixedbodyHeight) {
            $fixedbody.css("height", fixedbodyHeight);
            $fixedbody.css("max-height", fixedbodyHeight);

        }
    }
}

var center_witdh = 0;
function showMenu(o) {
    var $o = $(o);
    if ($o.hasClass("is-collapsed")) {
        $o.removeClass("is-collapsed");
        $("#main_left").css("width", 250);
        $("#main_center").css("width", center_witdh);
        $(".left-navbar-brand").show();
    }
    else {
        center_witdh = $("#main_center").width();
        $o.addClass("is-collapsed");
        $("#main_left").css("width", 0);
        $(".left-navbar-brand").hide();
    }
}


function fun_Alert(msg) {
    layer.alert(msg);
}

function fun_AlertHtml(content) {
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['500px', '600px'], //宽高
        content: content
    });
}

function fun_AlertImage(url) {
    layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: ['auto'],
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: "<img width='1000px' height='700px' src=" + url + ">"
    });
}

function onError(error, inputElement) {  // 'this' is the form element
    alert(error);
}


function header_canter_init(el) {
    $("#header-center").append($(el));
}


(function ($) {
    $.extend({
        bb_simulated_click: function (id) {
            $("#" + id).click();
        },
    })
})(jQuery);
