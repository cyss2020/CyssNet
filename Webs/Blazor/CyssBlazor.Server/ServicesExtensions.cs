﻿using Account.Api.Client;
using BlazorDownloadFile;
using Blazored.LocalStorage;
using Common.Api.Client;
using Cyss.Core.Api;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using CyssBlazor.Server.Infrastructure;
using CyssBlazor.Shared.Components;
using CyssBlazor.Shared.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Order.Api.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyssBlazor.Server
{
    public static class ServicesExtensions
    {
        public static IServiceProvider Services;
        public static IServiceCollection AddServices(this IServiceCollection Services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = new System.Globalization.CultureInfo("zh-CN", true)
            {
                DateTimeFormat = { ShortDatePattern = "yyyy-MM-dd", FullDateTimePattern = "yyyy-MM-dd HH:mm:ss", LongTimePattern = "HH:mm:ss" },
            };

            Services.RegisterCoreSerivce();

            Services.RegisterClientConfig(configuration);

            Services.AddBlazoredLocalStorage(config =>
            {
                config.JsonSerializerOptions.WriteIndented = true;
            });

            // 添加本行代码
            Services.AddBootstrapBlazor();

            Services.AddScoped<ModalService>();

            Services.AddBlazorDownloadFile(ServiceLifetime.Scoped);

            //注册api客户端
            Services.AddSingleton<IApiToken, Infrastructure.ApiToken>();
            Services.RegisterAccountClient();
            Services.RegisterCommonClient();
            Services.RegisterOrderClient();

            Services.AddScoped<DialogHelper>();

            Services.AddSingleton<BootstrapBlazor.Components.MessageService>();

            Services.AddScoped<IUserAuthenService, UserAuthenService>();
            Services.AddScoped<IUserAuthStateProvider, UserAuthStateProvider>();

            Services.AddScoped<IBlazorWorkContext, BlazorWorkContext>();

            //Services.AddDistributedRedisCache(option =>
            //{
            //    //redis 数据库连接字符串
            //    option.Configuration =ConfigCore.GetConfig<CacheConfig>().RedisCachingConnectionString;
            //    //redis 实例名
            //    option.InstanceName = "master";
            //}
            //);
            //在wasm中没有默认配置，所以需要设置一下
            Services.AddAuthorizationCore();

            return Services;
        }
    }
}
