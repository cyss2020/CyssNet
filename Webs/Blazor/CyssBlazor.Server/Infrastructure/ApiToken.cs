﻿using Cyss.Core;
using Cyss.Core.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyssBlazor.Server.Infrastructure
{
    public class ApiToken : IApiToken
    {
        public async Task<string> GetTotkenAsync()
        {
            var httpContextAccessor = IOCEngine.Resolve<IHttpContextAccessor>();
            return httpContextAccessor.GetToken();
        }
    }
}
