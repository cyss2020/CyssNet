﻿using Account.Api.Client;
using Blazored.LocalStorage;
using Cyss.Core;
using Cyss.Core.Models;
using CyssBlazor.Shared.Infrastructure;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyssBlazor.Server.Infrastructure
{
    public class UserAuthStateProvider : IUserAuthStateProvider
    {
        private ILocalStorageService _localStorage;
        private IBlazorWorkContext _workContext;
        private IHttpContextAccessor _httpContextAccessor;
        public UserAuthStateProvider(IBlazorWorkContext workContext,
            ILocalStorageService localStorage,
            IHttpContextAccessor httpContextAccessor
            )
        {
            _localStorage =localStorage;
            _workContext=workContext;
            _httpContextAccessor=httpContextAccessor;
        }


        public async Task<bool> GetAuthenticationStateAsync()
        {
            if (_workContext.User != null)
            {
                return true;
            }
            var savedToken = await _localStorage.GetItemAsync<string>(SystemDefaultKey.AuthTokenKey);

            if (string.IsNullOrWhiteSpace(savedToken))
            {
                return false;
            }
            var apiResponse = await AccountClientFactory.User.TokenLogin(savedToken);
            if (apiResponse.IsSuccess == false)
            {
                return false;
            }
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                _workContext.User = new AuthenticationUser();
                _workContext.User.Id = apiResponse.Data.Id;
                _workContext.User.Name = apiResponse.Data.Name;
                _workContext.UserRoles = apiResponse.Data.UserRoles.ToList();
                _workContext.Menus=apiResponse.Data.Menus.ToList();
                _workContext.Token = savedToken;
                _workContext.IsLogin = true;
                _httpContextAccessor.SetToken(savedToken);
            }
            return true;
        }

        public void LogUserOut()
        {
            var identity = new ClaimsIdentity();
            var user = new ClaimsPrincipal(identity);
        }

        public void Login()
        {

        }
    }
}
