﻿using Cyss.Core;
using Cyss.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyssBlazor.Server.Infrastructure
{
    public class BlazorServiceWorkContext : IWebWorkContext
    {
        public AuthenticationUser User
        {
            set; get;
        }

        public string LoginToken { set; get; }

        public string CurrentPageUrl { set; get; }

        public IEnumerable<object> Menus { set; get; }
    }
}
