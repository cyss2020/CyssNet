﻿using CyssBlazor.Shared.Infrastructure;
using Microsoft.AspNetCore.Http;

namespace CyssBlazor.Server.Infrastructure
{
    public static class IHttpContextAccessorExtensions
    {
        public static void SetToken(this IHttpContextAccessor httpContextAccessor, string token)
        {
            if (httpContextAccessor==null)
            {
                return;
            }
            if (httpContextAccessor.HttpContext==null)
            {
                return;
            }
            httpContextAccessor.HttpContext.Items[SystemDefaultKey.AuthTokenKey]=token;
        }

        public static void ClearToken(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor==null)
            {
                return;
            }
            if (httpContextAccessor.HttpContext==null)
            {
                return;
            }
            if (httpContextAccessor.HttpContext.Items.ContainsKey(SystemDefaultKey.AuthTokenKey))
            {
                httpContextAccessor.HttpContext.Items.Remove(SystemDefaultKey.AuthTokenKey);
            }
        }

        public static string GetToken(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor==null)
            {
                return string.Empty;
            }
            if (httpContextAccessor.HttpContext==null)
            {
                return string.Empty;
            }
            if (httpContextAccessor.HttpContext.Items.ContainsKey(SystemDefaultKey.AuthTokenKey))
            {
               return  httpContextAccessor.HttpContext.Items[SystemDefaultKey.AuthTokenKey].ToString();
            }
            return string.Empty;
        }
    }
}
