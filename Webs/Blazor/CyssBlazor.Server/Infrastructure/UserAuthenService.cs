﻿using Account.Api.Client;
using Account.Api.Dtos;
using Blazored.LocalStorage;
using Cyss.Core;
using Cyss.Core.Models;
using CyssBlazor.Shared.Infrastructure;
using CyssBlazor.Shared.Models;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Threading.Tasks;

namespace CyssBlazor.Server.Infrastructure
{
    public class UserAuthenService : IUserAuthenService
    {
        private IUserAuthStateProvider _authenticationStateProvider;
        private ILocalStorageService _localStorage;
        private IBlazorWorkContext _workContext;
        private IHttpContextAccessor _httpContextAccessor;
        public UserAuthenService(
            IUserAuthStateProvider authenticationStateProvider,
             IBlazorWorkContext workContext,
            ILocalStorageService localStorage,
            IHttpContextAccessor httpContextAccessor
            )
        {
            _workContext=workContext;
            _localStorage =localStorage;
            _authenticationStateProvider = authenticationStateProvider;
            _httpContextAccessor=httpContextAccessor;
        }

        public async Task<string> GetLoginUserName()
        {
            var userName = await _localStorage.GetItemAsStringAsync(SystemDefaultKey.AuthUserNameKey);
            if (string.IsNullOrWhiteSpace(userName))
            {
                userName = string.Empty;
            }
            return userName;
        }

        public async Task<AuthenticationUser> GetCurrentUserAsync()
        {
            if (_workContext.User != null)
            {
                return _workContext.User;
            }
            var savedToken = await _localStorage.GetItemAsync<string>(SystemDefaultKey.AuthTokenKey);
            if (string.IsNullOrWhiteSpace(savedToken))
            {
                return null;
            }

            var apiResponse = await AccountClientFactory.User.TokenLogin(savedToken);
            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                _workContext.User = new AuthenticationUser();
                _workContext.User.Id = apiResponse.Data.Id;
                _workContext.User.Name = apiResponse.Data.Name;
                _workContext.User.UserNo = apiResponse.Data.No;
                _workContext.Menus=apiResponse.Data.Menus.ToList();
                _workContext.UserRoles = apiResponse.Data.UserRoles.ToList();
                _workContext.Token = savedToken;
                _workContext.IsLogin = true;
                _httpContextAccessor.SetToken(savedToken);
                return _workContext.User;
            }

            return null;

        }

        public async Task<bool> LoginAsync(LoginViewModel loginModel)
        {

            var apiResponse = await AccountClientFactory.User.Login(loginModel.ToObject<LoginModel>());

            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {
                await _localStorage.SetItemAsync<string>(SystemDefaultKey.AuthUserNameKey, apiResponse.Data.Name);
                await _localStorage.SetItemAsync<string>(SystemDefaultKey.AuthTokenKey, apiResponse.Data.Token.TokenStr);

                _workContext.Token = apiResponse.Data.Token.TokenStr;
                _workContext.User = new AuthenticationUser();
                _workContext.User.Id = apiResponse.Data.Id;
                _workContext.User.Name = apiResponse.Data.Name;
                _workContext.User.UserNo = apiResponse.Data.No;
                _workContext.UserRoles = apiResponse.Data.UserRoles.ToList();
                _workContext.IsLogin = true;
                _httpContextAccessor.SetToken(apiResponse.Data.Token.TokenStr);
                _authenticationStateProvider.Login();
                return true;
            }
            _workContext.IsLogin = false;
            return false;
        }

        public async Task LogoutAsync()
        {
            _workContext.IsLogin = false;
            _workContext.Token = string.Empty;
            _workContext.User = null;
            await _localStorage.RemoveItemAsync(SystemDefaultKey.AuthTokenKey);
            _httpContextAccessor.ClearToken();
            _authenticationStateProvider.LogUserOut();
        }

        public async Task<OperateResult<bool>> ValiToken()
        {
            OperateResult<bool> apiResponse = new OperateResult<bool>();
            var savedToken = await _localStorage.GetItemAsync<string>(SystemDefaultKey.AuthTokenKey);
            if (string.IsNullOrWhiteSpace(savedToken))
            {
                apiResponse.IsSuccess = true;
                apiResponse.Data = false;
                return apiResponse;
            }
            var responseToken = await AccountClientFactory.User.ValiToken(savedToken);
            return responseToken;
        }



    }
}
