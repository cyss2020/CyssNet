﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Account.Api.Dtos;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取部门 Api接口客户端
    /// </summary>
    public class DepartmentClient : AccountBaseClient
    {
        /// <summary>
        /// 获取部门 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public DepartmentClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 部门增删该查

        /// <summary>
        /// 获取部门信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<DepartmentModel>> GetDepartmentById(int Id)
        {
            return await this.Get<DepartmentModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取部门记录
        /// </summary>
        /// <param name="Id">父部门Id</param>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<DepartmentModel>>> GetDeptsByParentDeptId(int Id)
        {
            return await this.Get<IEnumerable<DepartmentModel>>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取部门树
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<DeptTreeModel>>> GetDeptTrees()
        {
            return await this.Get<IEnumerable<DeptTreeModel>>(new UrlParameters());
        }

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<DepartmentListModel>> GetPageListDepartments(DepartmentSearchModel model)
        {
            return await this.Post<DepartmentListModel>(model);
        }

        /// <summary>
        /// 创建部门
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateDepartment(DepartmentModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑部门
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditDepartment(DepartmentModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteDepartment(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

    }
}





