﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Account.Api.Dtos;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取权限 Api接口客户端
    /// </summary>
    public class PermissionClient : AccountBaseClient
    {
        /// <summary>
        /// 获取权限 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public PermissionClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 权限增删该查

        /// <summary>
        /// 获取权限信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<PermissionModel>> GetPermissionById(int Id)
        {
            return await this.Get<PermissionModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取权限列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<PermissionListModel>> GetPageListPermissions(PermissionSearchModel model)
        {
            return await this.Post<PermissionListModel>(model);
        }

        /// <summary>
        /// 创建权限
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreatePermission(PermissionModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑权限
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditPermission(PermissionModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除权限
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeletePermission(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        /// <summary>
        /// 获取权限记录
        /// </summary>
        /// <param name="MenuId"></param>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<PermissionModel>>> GetPermissionByMenuId(int MenuId)
        {
            return await this.Get<IEnumerable<PermissionModel>>(new UrlParameters(MenuId));

        }

        /// <summary>
        /// 获取所有权限
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<PermissionModel>>> GetPermissions()
        {
            return await this.Get<IEnumerable<PermissionModel>>(new UrlParameters());

        }

        /// <summary>
        /// 获取权限树
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<PermissionTreeModel>>> GetPermissionTrees()
        {
            return await this.Get<IEnumerable<PermissionTreeModel>>(new UrlParameters());

        }

    }
}





