﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Account.Api.Dtos;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取菜单 Api接口客户端
    /// </summary>
    public class MenuClient : AccountBaseClient
    {
        /// <summary>
        /// 获取菜单 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public MenuClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 菜单增删该查

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<MenuModel>> GetMenuById(int Id)
        {
            return await this.Get<MenuModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取所有菜单
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<MenuModel>>> GetMenus()
        {
            return await this.Get<IEnumerable<MenuModel>>(new UrlParameters());

        }

        /// <summary>
        /// 获取菜单树
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IList<MenuTreeModel>>> GetMenuTrees()
        {
            return await this.Get<IList<MenuTreeModel>>(new UrlParameters());
        }


        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<MenuListModel>> GetPageListMenus(MenuSearchModel model)
        {
            return await this.Post<MenuListModel>(model);
        }

        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateMenu(MenuModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑菜单
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditMenu(MenuModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteMenu(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

    }
}





