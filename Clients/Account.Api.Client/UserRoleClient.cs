﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Account.Api.Dtos;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取用户角色 Api接口客户端
    /// </summary>
    public class UserRoleClient : AccountBaseClient
    {
        /// <summary>
        /// 获取用户角色 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public UserRoleClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 用户角色增删该查

        /// <summary>
        /// 获取用户角色信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<UserRoleModel>> GetUserRoleById(int Id)
        {
            return await this.Get<UserRoleModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取用户角色列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<UserRoleListModel>> GetPageListUserRoles(UserRoleSearchModel model)
        {
            return await this.Post<UserRoleListModel>(model);
        }

        /// <summary>
        /// 创建用户角色
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateUserRole(UserRoleModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑用户角色
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditUserRole(UserRoleModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除用户角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteUserRole(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        /// <summary>
        /// 获取用户角色记录
        /// </summary>
        /// <param name="UserId">用户Id</param>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<UserRoleModel>>> GetUserRoleByUserId(int UserId)
        {
            return await this.Get<IEnumerable<UserRoleModel>>(new UrlParameters(UserId));
        }
    }
}





