﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class AccountWebAddress : IWebApiAddress
    {
        public string Value { set; get; }
    }
}
