﻿using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;

namespace Account.Api.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class AccountBaseClient : HttpClientBase
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginToken"></param>
        public AccountBaseClient(IApiToken loginToken) : base(loginToken)
        {
            this.HttpClientFactoryName="Account";
        }


    }
}
