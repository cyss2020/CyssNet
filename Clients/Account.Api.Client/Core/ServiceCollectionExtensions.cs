﻿using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;

namespace Account.Api.Client
{

    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        /// <summary>
        /// 注册所有Order 库api接口
        /// </summary>
        /// <param name="services"></param>
        public static void RegisterAccountClient(this IServiceCollection services)
        {
            services.RegisterBaseClient<AccountBaseClient>(Assembly.GetExecutingAssembly(), "Account",
                   ConfigCore.GetConfig<AccountWebAddress>().Value);
        }

    }
}
