﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Client
{
    /// <summary>
    /// 账户Client工厂
    /// </summary>
    public class AccountClientFactory
    {
        /// <summary>
        /// 用户客户端
        /// </summary>
        public static UserClient User
        {
            get
            {
                return IOCEngine.Resolve<UserClient>();
            }
        }

        /// <summary>
        /// 角色客户端
        /// </summary>
        public static RoleClient Role
        {
            get
            {
                return IOCEngine.Resolve<RoleClient>();
            }
        }

        /// <summary>
        /// 角色客户端
        /// </summary>
        public static MenuClient Menu
        {
            get
            {
                return IOCEngine.Resolve<MenuClient>();
            }
        }

        /// <summary>
        /// 部门客户端
        /// </summary>
        public static DepartmentClient Dept
        {
            get
            {
                return IOCEngine.Resolve<DepartmentClient>();
            }
        }

        /// <summary>
        /// 权限客户端
        /// </summary>
        public static PermissionClient Permission
        {
            get
            {
                return IOCEngine.Resolve<PermissionClient>();
            }
        }

        /// <summary>
        /// 角色权限客户端
        /// </summary>
        public static RolePermissionClient RolePermission
        {
            get
            {
                return IOCEngine.Resolve<RolePermissionClient>();
            }
        }


        /// <summary>
        /// 用户角色客户端
        /// </summary>
        public static UserRoleClient UserRole
        {
            get
            {
                return IOCEngine.Resolve<UserRoleClient>();
            }
        }
    }
}
