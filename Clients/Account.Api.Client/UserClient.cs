﻿using Account.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using System.Threading.Tasks;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取会员 Api接口客户端
    /// </summary>
    public class UserClient : AccountBaseClient
    {
        /// <summary>
        /// 获取会员 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public UserClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 会员增删该查

        /// <summary>
        /// 获取会员信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<UserModel>> GetUserById(int Id)
        {
            return await this.Get<UserModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取会员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<UserListModel>> GetPageListUsers(UserSearchModel model)
        {
            return await this.Post<UserListModel>(model);
        }

        /// <summary>
        /// 创建会员
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateUser(UserModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑会员
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditUser(UserModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteUser(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        #region 扩展

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        public async Task<OperateResult<LoginReturnModel>> Login(LoginModel loginModel)
        {
            return await this.Post<LoginReturnModel>(loginModel);
        }

        /// <summary>
        /// token验证
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<OperateResult<bool>> ValiToken(string token)
        {
            return await this.Post<bool>(token);
        }

        /// <summary>
        /// Token登录
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<OperateResult<LoginReturnModel>> TokenLogin(string token)
        {
            return await this.Post<LoginReturnModel>(token);
        }

        /// <summary>
        /// 用户修改密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult> EdidUserPassword(EditPasswordModel model)
        {
            return await this.Post(model);
        }
        #endregion
    }
}





