﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Account.Api.Dtos;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取角色权限 Api接口客户端
    /// </summary>
    public class RolePermissionClient : AccountBaseClient
    {
        /// <summary>
        /// 获取角色权限 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public RolePermissionClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 角色权限增删该查

        /// <summary>
        /// 获取角色权限信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<RolePermissionModel>> GetRolePermissionById(int Id)
        {
            return await this.Get<RolePermissionModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取角色权限列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<RolePermissionListModel>> GetPageListRolePermissions(RolePermissionSearchModel model)
        {
            return await this.Post<RolePermissionListModel>(model);
        }

        /// <summary>
        /// 创建角色权限
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateRolePermission(RolePermissionModel model)
        {
            return await this.Post<int>(model);
        }


        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteRolePermission(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        /// <summary>
        /// 获取角色权限记录
        /// </summary>
        /// <param name="RoleId">角色Id</param>
        /// <returns></returns>

        public async Task<OperateResult<IEnumerable<RolePermissionModel>>> GetRolePermissionByRoleId(int RoleId)
        {
            return await this.Get<IEnumerable<RolePermissionModel>>(new UrlParameters(RoleId));
        }

        /// <summary>
        /// 编辑角色权限
        /// </summary>
        /// <param name="models">角色权限列表</param>
        /// <returns></returns>
        public async Task<OperateResult> EditRolePermission(IEnumerable<RolePermissionModel> models)
        {
            return await this.Post(models);

        }
    }
}





