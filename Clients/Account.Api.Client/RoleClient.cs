﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Account.Api.Dtos;

namespace Account.Api.Client
{

    /// <summary>
    /// 获取角色 Api接口客户端
    /// </summary>
    public class RoleClient : AccountBaseClient
    {
        /// <summary>
        /// 获取角色 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public RoleClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 角色增删该查

        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<RoleModel>> GetRoleById(int Id)
        {
            return await this.Get<RoleModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<RoleListModel>> GetPageListRoles(RoleSearchModel model)
        {
            return await this.Post<RoleListModel>(model);
        }

        /// <summary>
        /// 创建角色
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateRole(RoleModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑角色
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditRole(RoleModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteRole(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

    }
}





