﻿using Common.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Api.Client
{

    /// <summary>
    /// 获取API服务 Api接口客户端
    /// </summary>
    public class ApiServiceClient : CommonBaseClient
    {
        /// <summary>
        /// 获取API服务 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public ApiServiceClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region API服务增删该查

        /// <summary>
        /// 获取API服务信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<ApiServiceModel>> GetApiServiceById(int Id)
        {
            return await this.Get<ApiServiceModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取API服务列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<ApiServiceListModel>> GetPageListApiServices(ApiServiceSearchModel model)
        {
            return await this.Post<ApiServiceListModel>(model);
        }

        /// <summary>
        /// 获取API服务列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<ApiServiceModel>>> GetApiServices()
        {
            return await this.Get<IEnumerable<ApiServiceModel>>(new UrlParameters());
        }

        /// <summary>
        /// 创建API服务
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateApiService(ApiServiceModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑API服务
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditApiService(ApiServiceModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除API服务
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteApiService(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

    }
}





