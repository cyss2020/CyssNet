﻿using Common.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Client
{
    public class DownloadClient : CommonBaseClient
    {
        /// <summary>
        /// 获取基础配置 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public DownloadClient(IApiToken apiToken) : base(apiToken)
        {


        }

        public async Task<OperateResult<DownloadModel>> GetDownloadById(int Id)
        {
            return await this.Get<DownloadModel>(new UrlParameters(Id));
        }
        //do not validate request token (XSRF)
        public virtual async Task<OperateResult<FileResult>> AsyncUpload(Stream fileStream, string fileName)
        {
            return await this.PostFileAsync<FileResult>(fileStream, fileName);
        }

        public virtual async Task<Stream> GetFileById(int Id)
        {
            return await this.GetStream(new UrlParameters(Id));
        }
    }
}
