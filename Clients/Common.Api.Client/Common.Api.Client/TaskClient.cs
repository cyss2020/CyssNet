﻿using Common.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using System.Threading.Tasks;

namespace Common.Api.Client
{

    /// <summary>
    /// 获取任务 Api接口客户端
    /// </summary>
    public class TaskClient : CommonBaseClient
    {
        /// <summary>
        /// 获取任务 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public TaskClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 任务增删该查

        /// <summary>
        /// 获取任务信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<JobModel>> GetTaskById(int Id)
        {
            return await this.Get<JobModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取任务列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<TaskListModel>> GetPageListTasks(TaskSearchModel model)
        {
            return await this.Post<TaskListModel>(model);
        }

        /// <summary>
        /// 创建任务
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateTask(JobModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑任务
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditTask(JobModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除任务
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteTask(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        #region 任务配置表增删该查

        /// <summary>
        /// 获取任务配置表信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<TaskConfigModel>> GetTaskConfigById(int Id)
        {
            return await this.Get<TaskConfigModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取任务配置表列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<TaskConfigListModel>> GetPageListTaskConfigs(TaskConfigSearchModel model)
        {
            return await this.Post<TaskConfigListModel>(model);
        }

        /// <summary>
        /// 创建任务配置表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateTaskConfig(TaskConfigModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑任务配置表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditTaskConfig(TaskConfigModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除任务配置表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteTaskConfig(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        #region 任务日志增删该查

        /// <summary>
        /// 获取任务日志信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<TaskRecordModel>> GetTaskRecordById(int Id)
        {
            return await this.Get<TaskRecordModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取任务日志列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<TaskRecordListModel>> GetPageListTaskRecords(TaskRecordSearchModel model)
        {
            return await this.Post<TaskRecordListModel>(model);
        }

        /// <summary>
        /// 创建任务日志
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateTaskRecord(TaskRecordModel model)
        {
            return await this.Post<int>(model);
        }

        /// <summary>
        /// 编辑任务日志
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditTaskRecord(TaskRecordModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除任务日志
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteTaskRecord(int Id)
        {
            return await this.Post(Id);
        }
        #endregion
    }
}





