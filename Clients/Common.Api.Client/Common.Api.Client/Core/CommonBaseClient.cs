﻿using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;

namespace Common.Api.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class CommonBaseClient : HttpClientBase
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginToken"></param>
        public CommonBaseClient(IApiToken loginToken) : base(loginToken)
        {
            this.HttpClientFactoryName="Common";
        }


    }
}
