﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class CommonClientFactory
    {
        /// <summary>
        /// 销售订单
        /// </summary>
        public static HelperClient Helper
        {
            get
            {
                return IOCEngine.Resolve<HelperClient>();
            }
        }

        /// <summary>
        /// 任务管理
        /// </summary>
        public static TaskClient Task
        {
            get
            {
                return IOCEngine.Resolve<TaskClient>();
            }
        }

        /// <summary>
        /// API服务管理
        /// </summary>
        public static ApiServiceClient ApiService
        {
            get
            {
                return IOCEngine.Resolve<ApiServiceClient>();
            }
        }

        /// <summary>
        /// 基础配置API
        /// </summary>
        public static SettingClient Setting
        {
            get
            {
                return IOCEngine.Resolve<SettingClient>();
            }
        }

        /// <summary>
        /// 图片 api
        /// </summary>
        public static PictureClient Picture
        {
            get
            {
                return IOCEngine.Resolve<PictureClient>();
            }
        }

        /// <summary>
        /// 文件 api
        /// </summary>
        public static DownloadClient Download
        {
            get
            {
                return IOCEngine.Resolve<DownloadClient>();
            }
        }
        
    }
}
