﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Client
{
    public class CommonWebAddress : IWebApiAddress
    {
        public string Value { set; get; }
    }
}
