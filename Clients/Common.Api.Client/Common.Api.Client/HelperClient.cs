﻿using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using System.Threading.Tasks;

namespace Common.Api.Client
{

    /// <summary>
    /// 帮助接口
    /// </summary>
    public class HelperClient : CommonBaseClient
    {
        /// <summary>
        /// 获取会员 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public HelperClient(IApiToken apiToken) : base(apiToken)
        {

        }

        /// <summary>
        /// 获取幂等性Id
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<string>> GetIdempotence()
        {
            return await this.Get<string>(new UrlParameters());
        }
    }
}
