﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Common.Api.Dtos;

namespace Common.Api.Client
{

    /// <summary>
    /// 获取基础配置 Api接口客户端
    /// </summary>
    public class SettingClient : CommonBaseClient
    {
        /// <summary>
        /// 获取基础配置 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public SettingClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 基础配置增删该查

        /// <summary>
        /// 获取配置类型集合
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<SettingTypeModel>>> GetSettingTypes()
        {
            return await this.Get<IEnumerable<SettingTypeModel>>();
        }

        /// <summary>
        /// 获取配置类型键值集合
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<IEnumerable<SettingModel>>> GetSettings(string TypeName)
        {
            return await this.Get<IEnumerable<SettingModel>>(new UrlParameters(TypeName));
        }


        /// <summary>
        /// 更新配置
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public async Task<OperateResult> UpdateSettings(IEnumerable<SettingModel> models)
        {
            return await this.Post(models);
        }


        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<SystemSettingModel>> GetSytemSetting()
        {
            return await this.Get<SystemSettingModel>();
        }


        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<T>> GetSettingByTypeName<T>(string TypeName)
        {
            return await this.Get<T>(new UrlParameters(TypeName));
        }

        /// <summary>
        /// 更新配置
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult> UpdateSetting(EditSettingModel model)
        {
            return await this.Post(model);
        }

        #endregion

    }
}





