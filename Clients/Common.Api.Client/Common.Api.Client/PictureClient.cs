﻿using Common.Api.Dtos;
using Cyss.Core;
using Cyss.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cyss.Core.Api.Client;

namespace Common.Api.Client
{
    public class PictureClient : CommonBaseClient
    {
        /// <summary>
        /// 获取基础配置 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public PictureClient(IApiToken apiToken) : base(apiToken)
        {


        }


        //do not validate request token (XSRF)
        public virtual async Task<OperateResult<PictureResult>> AsyncUpload(Stream fileStream, string fileName)
        {
            return await this.PostFileAsync<PictureResult>(fileStream, fileName);
        }

        public virtual async Task<OperateResult<PictureResult>> GetPictureUrlById(int pictureId, int targetSize)
        {
            return await this.Get<PictureResult>(new UrlParameters(pictureId, targetSize));
        }
    }
}
