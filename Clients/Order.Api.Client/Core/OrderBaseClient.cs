﻿using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;

namespace Order.Api.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderBaseClient : HttpClientBase
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="apiToken"></param>
        public OrderBaseClient(IApiToken apiToken) : base(apiToken)
        {
            this.HttpClientFactoryName="Order";

        }
    }
}
