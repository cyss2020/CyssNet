﻿using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Order.Api.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Order.Api.Client
{

    /// <summary>
    /// 获取订单表 Api接口客户端
    /// </summary>
    public class SaleOrderClient : OrderBaseClient
    {
        /// <summary>
        /// 获取订单表 客户端构造函数
        /// </summary>
        /// <param name="apiToken">apiToken</param>
        public SaleOrderClient(IApiToken apiToken) : base(apiToken)
        {

        }

        #region 订单表增删该查

        /// <summary>
        /// 获取订单表信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<SaleOrderModel>> GetSaleOrderById(int Id)
        {
            return await this.Get<SaleOrderModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取订单表列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<SaleOrderListModel>> GetPageListSaleOrders(SaleOrderSearchModel model)
        {
            return await this.Post<SaleOrderListModel>(model);
        }

        /// <summary>
        /// 创建订单表
        /// </summary>
        /// <param name="model">订单实体</param>
        /// <param name="IdempotenceId">验证幂等性Id</param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateSaleOrder(SaleOrderModel model, string IdempotenceId)
        {
            Dictionary<string, string> Headers = new Dictionary<string, string>();
            Headers.Add("IdempotenceId", IdempotenceId);
            return await this.Post<int>(model, Headers);
        }

        /// <summary>
        /// 编辑订单表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditSaleOrder(SaleOrderModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除订单表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteSaleOrder(int Id)
        {
            return await this.Post(Id);
        }
        #endregion

        #region 订单产品表增删该查

        /// <summary>
        /// 获取订单产品表信息
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public async Task<OperateResult<SaleOrderItemModel>> GetSaleOrderItemById(int Id)
        {
            return await this.Get<SaleOrderItemModel>(new UrlParameters(Id));
        }

        /// <summary>
        /// 获取订单产品表列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<OperateResult<SaleOrderItemListModel>> GetPageListSaleOrderItems(SaleOrderItemSearchModel model)
        {
            return await this.Post<SaleOrderItemListModel>(model);
        }

        /// <summary>
        ///  创建订单产品表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="IdempotenceId"></param>
        /// <returns></returns>
        public async Task<OperateResult<int>> CreateSaleOrderItem(SaleOrderItemModel model,string IdempotenceId)
        {
            Dictionary<string, string> Headers = new Dictionary<string, string>();
            Headers.Add("IdempotenceId", IdempotenceId);
            return await this.Post<int>(model, Headers);
        }

        /// <summary>
        /// 编辑订单产品表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        public async Task<OperateResult> EditSaleOrderItem(SaleOrderItemModel model)
        {
            return await this.Post(model);
        }

        /// <summary>
        /// 删除订单产品表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<OperateResult> DeleteSaleOrderItem(int Id)
        {
            return await this.Post(Id);
        }
        #endregion
    }
}





