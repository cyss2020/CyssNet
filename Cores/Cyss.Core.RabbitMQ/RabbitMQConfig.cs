﻿using System;
using System.Collections.Generic;
using System.Text;
using Cyss.Core;

namespace Cyss.Core.RabbitMQ
{
    public class RabbitMQConfig: BaseConfig
    {
        public string UserName { set; get; }

        public string Password { set; get; }

        public int Port { set; get; }
        public string VirtualHost { set; get; }

        public string HostName { set; get; }

        public string RabbitMQConnection { set; get; }
    }
}
