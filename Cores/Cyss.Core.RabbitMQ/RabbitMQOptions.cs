﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.RabbitMQ
{
    public class RabbitMQOptions
    {
        public const string ExchangeName = "default";

        public const string ExchangeType = "topic";

        public const string RoutingKey = "default";

        //队列名称
        public const string QueueName = "howdy.queue";
    }
}
