﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.RabbitMQ
{
    /// <summary>
    /// 消息模型
    /// </summary>
    internal class MessageModel
    {
        /// <summary>
        /// 消息Id
        /// </summary>
        public string Id { set; get; }

        /// <summary>
        /// 事务Id
        /// </summary>
        public string TransactionId { set; get; }

        public object Content { set; get; }

        /// <summary>
        /// 消息状态
        /// </summary>
        public int Status { set; get; }
    }
}
