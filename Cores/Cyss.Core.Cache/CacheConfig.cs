﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Cache
{
    public class CacheConfig:BaseConfig
    {
        public string RedisCachingConnectionString { set; get; }
    }
}
