using Cyss.Core;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cyss.Core.Cache
{
    public partial class RedisCacheManager : IStaticCacheManager
    {
        #region Fields
        private readonly IRedisConnectionWrapper _connectionWrapper;

        private List<DatabaseModel> Databases { set; get; }
        #endregion

        #region Ctor

        public RedisCacheManager(
            IRedisConnectionWrapper connectionWrapper,
            CacheConfig config)
        {
            if (string.IsNullOrEmpty(config.RedisCachingConnectionString))
                throw new Exception("Redis connection string is empty");

            this._connectionWrapper = connectionWrapper;

            var database = _connectionWrapper.GetDatabase(0);
            this.Databases = new List<DatabaseModel>();
            this.Databases.Add(new DatabaseModel { Database = database, db = 0 });
        }

        #endregion

        #region Utilities
        private class DatabaseModel
        {
            public IDatabase Database { set; get; }
            public int db { set; get; }
        }
        private static object lockobj { set; get; } = new object();
        public IDatabase GetDatabase(int db = 0)
        {
            var database = this.Databases.FirstOrDefault(x => x.db == db);
            if (database == null)
            {
                lock (lockobj)
                {
                    database = this.Databases.FirstOrDefault(x => x.db == db);
                    if (database != null)
                    {
                        return database.Database;
                    }
                    var newdb = _connectionWrapper.GetDatabase(db);
                    database = new DatabaseModel { Database = newdb, db = db };
                    this.Databases.Add(database);
                }
            }
            return database.Database;
        }

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Key of cached item</param>
        /// <returns>The cached value associated with the specified key</returns>
        protected virtual T StringGet<T>(string key, int db = 0)
        {
            var _db = GetDatabase(db);
            var serializedItem = _db.StringGet(key);
            if (!serializedItem.HasValue)
                return default(T);
            if (typeof(T)==typeof(string))
            {
                return (T)Convert.ChangeType(serializedItem.ToString(), typeof(T));
            }
            var item = System.Text.Json.JsonSerializer.Deserialize<T>(serializedItem);
            if (item == null)
                return default(T);
            return item;
        }
        protected virtual async Task<T> StringGetAsync<T>(string key, int db = 0)
        {
            var _db = GetDatabase(db);
            var serializedItem = await _db.StringGetAsync(key);
            if (!serializedItem.HasValue)
                return default(T);

            if (typeof(T)==typeof(string))
            {
                return (T)Convert.ChangeType(serializedItem.ToString(), typeof(T));
            }
            //deserialize item
            var item = System.Text.Json.JsonSerializer.Deserialize<T>(serializedItem);
            if (item == null)
                return default(T);
            return item;
        }
        /// <summary>
        /// Adds the specified key and object to the cache
        /// </summary>
        /// <param name="key">Key of cached item</param>
        /// <param name="data">Value for caching</param>
        /// <param name="cacheTime">Cache time in minutes</param>
        protected virtual async Task<bool> StringSetAsync(string key, object data, int cacheTime = 0, int db = 0)
        {
            if (data == null)
                return false;

            string serializedItem = string.Empty;
            if (data.GetType()==typeof(string))
            {
                serializedItem=data.ToString();
            }
            else
            {
                serializedItem = System.Text.Json.JsonSerializer.Serialize(data);
            }
            //set cache time
            //serialize item
            //and set it to cache
            var _db = GetDatabase(db);
            if (cacheTime>0)
            {
                return await _db.StringSetAsync(key, serializedItem, TimeSpan.FromMinutes(cacheTime));
            }
            else
            {
                return await _db.StringSetAsync(key, serializedItem);
            }
        }
        /// <summary>
        /// Adds the specified key and object to the cache
        /// </summary>
        /// <param name="key">Key of cached item</param>
        /// <param name="data">Value for caching</param>
        /// <param name="cacheTime">Cache time in minutes</param>
        protected virtual bool StringSet(string key, object data, int cacheTime = 0, int db = 0)
        {
            if (data == null)
                return false;

            string serializedItem = string.Empty;
            if (data.GetType()==typeof(string))
            {
                serializedItem=data.ToString();
            }
            else
            {
                serializedItem = System.Text.Json.JsonSerializer.Serialize(data);
            }

            //and set it to cache
            var _db = GetDatabase(db);
            if (cacheTime>0)
            {
                return _db.StringSet(key, serializedItem, TimeSpan.FromMinutes(cacheTime));
            }
            else
            {
                return _db.StringSet(key, serializedItem);
            }
        }

        /// <summary>
        /// Removes the value with the specified key from the cache
        /// </summary>
        /// <param name="key">Key of cached item</param>
        protected virtual async Task<bool> KeyDeleteAsync(string key, int db = 0)
        {
            if (key.Equals(NopCachingDefaults.RedisDataProtectionKey, StringComparison.OrdinalIgnoreCase))
                return false;
            var _db = GetDatabase(db);
            return await _db.KeyDeleteAsync(key);
        }
        /// <summary>
        /// Removes the value with the specified key from the cache
        /// </summary>
        /// <param name="key">Key of cached item</param>
        protected virtual bool KeyDelete(string key, int db = 0)
        {
            if (key.Equals(NopCachingDefaults.RedisDataProtectionKey, StringComparison.OrdinalIgnoreCase))
                return false;
            var _db = GetDatabase(db);
            return _db.KeyDelete(key);
        }

        /// <summary>
        /// Removes items by key pattern
        /// </summary>
        /// <param name="pattern">String key pattern</param>
        protected virtual async Task DeleteByPatternAsync(string pattern, int db = 0)
        {
            var _db = GetDatabase(db);
            foreach (var endPoint in _connectionWrapper.GetEndPoints())
            {
                var server = _connectionWrapper.GetServer(endPoint);
                var keys = server.Keys(database: _db.Database, pattern: $"*{pattern}*");
                keys = keys.Where(key => !key.ToString().Equals(NopCachingDefaults.RedisDataProtectionKey, StringComparison.OrdinalIgnoreCase));
                await _db.KeyDeleteAsync(keys.ToArray());
            }
        }

        /// <summary>
        /// Removes items by key pattern
        /// </summary>
        /// <param name="pattern">String key pattern</param>
        protected virtual void DeleteByPattern(string pattern, int db = 0)
        {
            var _db = GetDatabase(db);
            foreach (var endPoint in _connectionWrapper.GetEndPoints())
            {
                var server = _connectionWrapper.GetServer(endPoint);
                var keys = server.Keys(database: _db.Database, pattern: $"*{pattern}*");

                keys = keys.Where(key => !key.ToString().Equals(NopCachingDefaults.RedisDataProtectionKey, StringComparison.OrdinalIgnoreCase));

                _db.KeyDelete(keys.ToArray());
            }
        }
        #endregion

        #region Methods

        public T Get<T>(string key, Func<T> acquire, int? cacheTime = null, int db = 0)
        {
            var _db = GetDatabase(db);
            //item already is in cache, so return it
            if (_db.KeyExists(key))
                return this.StringGet<T>(key, db);

            if (acquire!=null)
            {
                //or create it using passed function
                var result = acquire();

                //and set in cache (if cache time is defined)
                if ((cacheTime ?? NopCachingDefaults.CacheTime) > 0)
                    this.Set(key, result, cacheTime ?? NopCachingDefaults.CacheTime, db);
                return result;
            }
            return default(T);
        }
        public T Get<T>(string key, int db = 0)
        {
            return this.Get<T>(key, null, null, db);
        }

        public async Task<T> GetAsync<T>(string key, Func<T> acquire, int? cacheTime = null, int db = 0)
        {
            var _db = GetDatabase(db);
            //item already is in cache, so return it
            if (await _db.KeyExistsAsync(key))
                return await this.StringGetAsync<T>(key, db);

            if (acquire!=null)
            {
                //or create it using passed function
                var result = acquire();

                //and set in cache (if cache time is defined)
                if ((cacheTime ?? NopCachingDefaults.CacheTime) > 0)
                    await this.StringSetAsync(key, result, cacheTime ?? NopCachingDefaults.CacheTime, db);

                return result;
            }
            return default(T);
        }

        public async Task<T> GetAsync<T>(string key, Func<Task<T>> acquire, int? cacheTime = null, int db = 0)
        {
            var _db = GetDatabase(db);
            //item already is in cache, so return it
            if (await _db.KeyExistsAsync(key))
                return await this.StringGetAsync<T>(key, db);

            if (acquire!=null)
            {
                //or create it using passed function
                var result = await acquire();

                //and set in cache (if cache time is defined)
                if ((cacheTime ?? NopCachingDefaults.CacheTime) > 0)
                    await this.StringSetAsync(key, result, cacheTime ?? NopCachingDefaults.CacheTime, db);
                return result;
            }
            return default(T);

        }

        public async Task<T> GetAsync<T>(string key, int db = 0)
        {
            var _db = GetDatabase(db);
            //item already is in cache, so return it
            if (await _db.KeyExistsAsync(key))
                return await this.StringGetAsync<T>(key, db);
            return default(T);
        }


        public bool Set(string key, object data, int cacheTime = 0, int db = 0)
        {
            return this.StringSet(key, data, cacheTime, db);
        }

        public async Task<bool> SetAsync(string key, object data, int cacheTime = 0, int db = 0)
        {
            return await this.StringSetAsync(key, data, cacheTime, db);
        }


        public async Task<bool> IsSetAsync(string key, int db = 0)
        {
            var _db = GetDatabase(db);
            return await _db.KeyExistsAsync(key);
        }
        public virtual bool IsSet(string key, int db = 0)
        {
            var _db = GetDatabase(db);
            return _db.KeyExists(key);
        }


        public bool Remove(string key, int db = 0)
        {
            return this.KeyDelete(key, db);
        }
        public async Task<bool> RemoveAsync(string key, int db = 0)
        {
            return await this.KeyDeleteAsync(key, db);
        }

        public async Task RemoveByPatternAsync(string pattern, int db = 0)
        {
            await this.DeleteByPatternAsync(pattern, db);
        }

        public async Task ClearAsync(int db = 0)
        {
            var _db = GetDatabase(db);
            foreach (var endPoint in _connectionWrapper.GetEndPoints())
            {
                var server = _connectionWrapper.GetServer(endPoint);
                var keys = server.Keys(database: _db.Database);
                keys = keys.Where(key => !key.ToString().Equals(NopCachingDefaults.RedisDataProtectionKey, StringComparison.OrdinalIgnoreCase));
                await _db.KeyDeleteAsync(keys.ToArray());
            }
        }



        public void RemoveByPattern(string pattern, int db = 0)
        {
            this.DeleteByPattern(pattern, db);
        }

        public void Clear(int db = 0)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if (_connectionWrapper != null)
                _connectionWrapper.Dispose();
        }

        #endregion

        #region List

        /// <summary>
        /// 在列表头部插入值。如果键不存在，先创建再插入值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="redisValue"></param>
        /// <returns></returns>
        public long ListLeftPush<T>(string redisKey, T redisValue, int db = 0)
        {
            var _db = GetDatabase(db);
            return _db.ListLeftPush(redisKey, JsonHelper.SerializeObject(redisValue));
        }
        /// <summary>
        /// 在列表尾部插入值。如果键不存在，先创建再插入值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="redisValue"></param> 
        /// <returns></returns>
        public long ListRightPush<T>(string redisKey, T redisValue, int db = 0)
        {
            var _db = GetDatabase(db);
            return _db.ListRightPush(redisKey, JsonHelper.SerializeObject(redisValue));
        }

        /// <summary>
        /// 在列表尾部插入数组集合。如果键不存在，先创建再插入值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="redisValue"></param>
        /// <returns></returns>
        public long ListRightPush<T>(string redisKey, IEnumerable<T> redisValue, int db = 0)
        {
            var redislist = new List<RedisValue>();
            foreach (var item in redisValue)
            {
                redislist.Add(JsonHelper.SerializeObject(item));
            }
            var _db = GetDatabase(db);
            return _db.ListRightPush(redisKey, redislist.ToArray());
        }


        /// <summary>
        /// 移除并返回存储在该键列表的第一个元素  反序列化
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        public T ListLeftPop<T>(string redisKey, int db = 0)
        {
            var _db = GetDatabase(db);
            return JsonHelper.DeserializeObject<T>(_db.ListLeftPop(redisKey));
        }

        /// <summary>
        /// 移除并返回存储在该键列表的最后一个元素   反序列化
        /// 只能是对象集合
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        public T ListRightPop<T>(string redisKey, int db = 0)
        {
            var _db = GetDatabase(db);
            return JsonHelper.DeserializeObject<T>(_db.ListRightPop(redisKey));
        }

        /// <summary>
        /// 列表长度
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long ListLength(string redisKey, int db = 0)
        {
            var _db = GetDatabase(db);
            return _db.ListLength(redisKey);
        }

        /// <summary>
        /// 返回在该列表上键所对应的元素
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        public IEnumerable<T> ListRange<T>(string redisKey, int db = 0)
        {
            var _db = GetDatabase(db);
            var result = _db.ListRange(redisKey);
            return result.Select(o => JsonHelper.DeserializeObject<T>(o.ToString()));
        }

        /// <summary>
        /// 根据索引获取指定位置数据
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<T> ListRange<T>(string redisKey, int start, int stop, int db = 0)
        {
            var _db = GetDatabase(db);
            var result = _db.ListRange(redisKey, start, stop);
            return result.Select(o => JsonHelper.DeserializeObject<T>(o.ToString()));
        }

        /// <summary>
        /// 删除List中的元素 并返回删除的个数
        /// </summary>
        /// <param name="redisKey">key</param>
        /// <param name="redisValue">元素</param>
        /// <param name="type">大于零 : 从表头开始向表尾搜索，小于零 : 从表尾开始向表头搜索，等于零：移除表中所有与 VALUE 相等的值</param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long ListDelRange<T>(string redisKey, T redisValue, long type = 0, int db = 0)
        {
            var _db = GetDatabase(db);
            return _db.ListRemove(redisKey, JsonHelper.SerializeObject(redisValue), type);
        }

        /// <summary>
        /// 清空List
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        public void ListClear(string redisKey, int db = 0)
        {
            var _db = GetDatabase(db);
            _db.ListTrim(redisKey, 1, 0);
        }

        #endregion

        #region Hash

        /// <summary>
        /// 判断该字段是否存在 hash 中
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public bool HashExists(string redisKey, string hashField, int db = -1)
        {

            var _db = GetDatabase(db);
            return _db.HashExists(redisKey, hashField);

        }

        /// <summary>
        /// 从 hash 中移除指定字段
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public bool HashDelete(string redisKey, string hashField, int db = -1)
        {
            var _db = GetDatabase(db);
            return _db.HashDelete(redisKey, hashField);
        }

        /// <summary>
        /// 从 hash 中移除多个指定字段
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public long HashDelete(string redisKey, IEnumerable<RedisValue> hashField, int db = -1)
        {
            var _db = GetDatabase(db);
            return _db.HashDelete(redisKey, hashField.ToArray());
        }

        /// <summary>
        /// 递增  默认按1递增  可用于计数
        /// </summary>
        /// <param name="key"></param>
        /// <param name="span"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long HashIncrement(string redisKey, string hashField, TimeSpan? span = null, int db = -1)
        {

            var _db = GetDatabase(db);
            var result = _db.HashIncrement(redisKey, hashField);
            //设置过期时间
            if (span != null)
            {
                //设置过期时间
                _db.KeyExpire(redisKey, span);
            }
            return result;

        }

        /// <summary>
        /// 递减  默认按1递减   可用于抢购类的案例
        /// </summary>
        /// <param name="key"></param>
        /// <param name="span"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long HashDecrement(string redisKey, string hashField, TimeSpan? span = null, int db = -1)
        {

            var _db = GetDatabase(db);
            var result = _db.HashDecrement(redisKey, hashField);
            //设置过期时间
            if (span != null)
            {
                //设置过期时间
                _db.KeyExpire(redisKey, span);
            }
            return result;

        }


        /// <summary>
        /// 在 hash 中保存或修改一个值   字符类型
        /// set or update the HashValue for string key 
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <param name="value"></param>
        /// <param name="span">过期时间,可空</param>
        /// <returns></returns>
        public bool HashSet(string redisKey, string hashField, string value, TimeSpan? span = null, int db = -1)
        {

            var _db = GetDatabase(db);
            if (span != null)
            {
                //设置过期时间
                _db.KeyExpire(redisKey, span);
            }
            return _db.HashSet(redisKey, hashField, value);

        }

        /// <summary>
        /// 保存一个字符串类型集合  
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey">Redis Key</param>
        /// <param name="list">数据集合</param>
        /// <param name="getFiledKey">字段key</param>
        public void HashSetList(string redisKey, IEnumerable<string> list, Func<string, string> getFiledKey, TimeSpan? span = null, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                List<HashEntry> listHashEntry = new List<HashEntry>();
                foreach (var item in list)
                {
                    listHashEntry.Add(new HashEntry(getFiledKey(item), item));
                }
                _db.HashSet(redisKey, listHashEntry.ToArray());

                if (span != null)
                {
                    //设置过期时间
                    _db.KeyExpire(redisKey, span);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// 保存多个对象集合  非序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">表名前缀</param>
        /// <param name="list">数据集合</param>
        /// <param name="getModelId">字段key</param>
        public void HashSetObjectList<T>(string tableName, IEnumerable<T> list, Func<T, string> getModelId, int db = -1) where T : class
        {
            var _db = GetDatabase(db);
            foreach (var item in list)
            {
                List<HashEntry> listHashEntry = new List<HashEntry>();
                Type t = item.GetType();//获得该类的Type
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    string name = pi.Name; //获得属性的名字,后面就可以根据名字判断来进行些自己想要的操作
                    var value = pi.GetValue(item, null);  //用pi.GetValue获得值
                    listHashEntry.Add(new HashEntry(name, value.ToString()));
                }
                _db.HashSet(tableName + getModelId(item), listHashEntry.ToArray());
            }
        }


        /// <summary>
        /// 保存或修改一个hash对象（序列化）
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool HashSet<T>(string redisKey, string hashField, T value, TimeSpan? span = null, int db = -1) where T : class
        {

            var _db = GetDatabase(db);
            var json = JsonHelper.SerializeObject(value);
            if (span != null)
            {
                //设置过期时间
                _db.KeyExpire(redisKey, span);
            }
            return _db.HashSet(redisKey, hashField, json);
        }


        /// <summary>
        /// 保存Hash对象集合 序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey">Redis Key</param>
        /// <param name="list">数据集合</param>
        /// <param name="getModelId"></param>
        public void HashSet<T>(string redisKey, IEnumerable<T> list, Func<T, string> getModelId, TimeSpan? span = null, int db = -1) where T : class
        {

            var _db = GetDatabase(db);
            List<HashEntry> listHashEntry = new List<HashEntry>();
            foreach (var item in list)
            {
                string json = JsonHelper.SerializeObject(item);
                listHashEntry.Add(new HashEntry(getModelId(item), json));
            }
            _db.HashSet(redisKey, listHashEntry.ToArray());

            if (span != null)
            {
                //设置过期时间
                _db.KeyExpire(redisKey, span);
            }

        }

        /// <summary>
        /// 从 hash 中获取对象（反序列化）
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public T HashGet<T>(string redisKey, string hashField, int db = -1) where T : class
        {

            var _db = GetDatabase(db);
            return JsonHelper.DeserializeObject<T>(_db.HashGet(redisKey, hashField));

        }

        /// <summary>
        ///  根据hashkey获取所有的值  序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public List<T> HashGetAll<T>(string redisKey, int db = -1) where T : class
        {
            List<T> result = new List<T>();

            var _db = GetDatabase(db);
            HashEntry[] arr = _db.HashGetAll(redisKey);
            foreach (var item in arr)
            {
                if (!item.Value.IsNullOrEmpty)
                {
                    var t = JsonHelper.DeserializeObject<T>(item.Value);
                    if (t != null)
                    {
                        result.Add(t);
                    }
                }
            }
            return result;
        }


        /// <summary>
        ///  根据hashkey获取所有的值  非序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<Dictionary<RedisValue, RedisValue>> HashGetAll(IEnumerable<string> redisKey, int db = -1)
        {
            List<Dictionary<RedisValue, RedisValue>> diclist = new List<Dictionary<RedisValue, RedisValue>>();

            var _db = GetDatabase(db);
            foreach (var item in redisKey)
            {
                HashEntry[] arr = _db.HashGetAll(item);
                foreach (var he in arr)
                {
                    Dictionary<RedisValue, RedisValue> dic = new Dictionary<RedisValue, RedisValue>();
                    if (!he.Value.IsNullOrEmpty)
                    {
                        dic.Add(he.Name, he.Value);
                    }
                    diclist.Add(dic);
                }
            }
            return diclist;
        }


        /// <summary>
        /// 根据hashkey获取单个字段hashField的值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public RedisValue HashGet(string redisKey, string hashField, int db = -1)
        {
            var _db = GetDatabase(db);
            return _db.HashGet(redisKey, hashField);

        }

        /// <summary>
        /// 根据hashkey获取多个字段hashField的值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="hashField"></param>
        /// <returns></returns>
        public RedisValue[] HashGet(string redisKey, RedisValue[] hashField, int db = -1)
        {
            var _db = GetDatabase(db);
            return _db.HashGet(redisKey, hashField);
        }

        /// <summary>
        /// 根据hashkey返回所有的HashFiled
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        public IEnumerable<RedisValue> HashKeys(string redisKey, int db = -1)
        {
            var _db = GetDatabase(db);
            return _db.HashKeys(redisKey);
        }

        /// <summary>
        /// 根据hashkey返回所有HashValue值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        public RedisValue[] HashValues(string redisKey, int db = -1)
        {
            var _db = GetDatabase(db);
            return _db.HashValues(redisKey);
        }

        #endregion

        #region Set

        /// <summary>
        /// set添加单个元素   
        /// 具有唯一性   比如在线人数/点赞人数/收藏人数等
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public bool SetAdd(string key, string value, TimeSpan? span = null, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                //设置过期时间
                if (span != null)
                {
                    _db.KeyExpire(key, span);
                }
                return _db.SetAdd(key, value);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// set添加多个元素集合
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long SetAdd(string key, List<string> arryList, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                RedisValue[] valueList = arryList.Select(u => (RedisValue)u).ToArray();
                return _db.SetAdd(key, valueList);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// set添加多个对象集合   序列化
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long SetAddList<T>(string key, IEnumerable<T> list, int db = -1) where T : class
        {
            try
            {
                var _db = GetDatabase(db);
                List<RedisValue> listRedisValue = new List<RedisValue>();
                foreach (var item in list)
                {
                    string json = JsonHelper.SerializeObject(item);
                    listRedisValue.Add(json);
                }
                return _db.SetAdd(key, listRedisValue.ToArray());
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// 获取set集合的长度
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public long SetLength(string key, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                return _db.SetLength(key);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// 检查元素是否属于Set集合
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public bool ExistsInSet(string key, string value, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                return _db.SetContains(key, value);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 根据key获取所有Set元素
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<string> GetMembers(string redisKey, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                var rValue = _db.SetMembers(redisKey);
                return rValue.Select(ob => ob.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 根据key获取所有Set对象集合  反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public List<T> GetAllMembers<T>(string redisKey, int db = -1) where T : class
        {
            List<T> result = new List<T>();
            try
            {
                var _db = GetDatabase(db);
                var arr = _db.SetMembers(redisKey);
                foreach (var item in arr)
                {
                    if (!item.IsNullOrEmpty)
                    {
                        var t = JsonHelper.DeserializeObject<T>(item);
                        if (t != null)
                        {
                            result.Add(t);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            return result;
        }


        /// <summary>
        /// 根据key随机获取Set中的1个元素   不删除该元素
        /// 可应用于中奖类的案例
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public string GetRandomMember(string redisKey, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                var rValue = _db.SetRandomMember(redisKey);
                return rValue.ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 根据key随机获取Set中的n个元素   不删除该元素
        /// 可应用于中奖类的案例
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="count"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<string> GetRandomMembers(string redisKey, long count, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                var rValue = _db.SetRandomMembers(redisKey, count);
                return rValue.Select(ob => ob.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// 随机删除指定key集合中的一个值，并返回这些值  
        /// 可应用于抽奖类的案例
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public string GetRandomRemovePop(string redisKey, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                var rValue = _db.SetPop(redisKey);
                return rValue.ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 随机删除指定key集合中的n个值，并返回这些值  
        /// 可应用于抽奖类的案例
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="count"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<string> GetRandomRemovePops(string redisKey, long count, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                var rValue = _db.SetPop(redisKey, count);
                return rValue.Select(ob => ob.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 返回指定rediskey集合的交集、差集、并集  
        /// 只能在同一个库内查询，跨库则不行
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="keyList"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<string> GetCombines(SetOperation operation, List<string> keyList, int db = -1)
        {
            try
            {
                var _db = GetDatabase(db);
                RedisKey[] valueList = keyList.Select(u => (RedisKey)u).ToArray();
                var rValue = _db.SetCombine(operation, valueList);
                return rValue.Select(ob => ob.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Stream 

        public string StreamAdd<T>(string redisKey, string streamField, string streamValue, int db = 0)
        {
            var _db = GetDatabase(db);
            return _db.StreamAdd(redisKey, streamField, streamValue);// messageId = 1518951480106-0
        }

        public string StreamAdd<T>(string redisKey, Dictionary<string, string> keyValues, int db = 0)
        {
            var values = keyValues.Select(x => new NameValueEntry(x.Key, x.Value));
            var _db = GetDatabase(db);
            var messageId = _db.StreamAdd("sensor_stream", values.ToArray());
            return messageId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="redisKey"></param>
        /// <param name="acquire">消费消息的方法</param>
        /// <param name="db"></param>
        public int StreamRead<T>(string redisKey, Func<T, bool> acquire, int db = 0)
        {
            var _db = GetDatabase(db);
            var messages = _db.StreamRead(redisKey, 1, 1);
            List<RedisValue> redisValues = new List<RedisValue>();
            foreach (var item in messages)
            {
                var t = JsonHelper.DeserializeObject<T>(item.Values.FirstOrDefault().Value.ToString());
                var isSuccess = acquire(t);
                if (isSuccess)
                {
                    redisValues.Add(item.Id);
                }
            }
            if (redisValues.Any())
            {
                _db.StreamDelete(redisKey, redisValues.ToArray());
            }
            return messages.Count();
        }

        public async Task<int> StreamRead<T>(string redisKey, Func<T, Task<bool>> acquire, int db = 0)
        {
            var _db = GetDatabase(db);
            var messages = _db.StreamRead(redisKey, 1, 1);
            List<RedisValue> redisValues = new List<RedisValue>();
            foreach (var item in messages)
            {
                var t = JsonHelper.DeserializeObject<T>(item.Values.FirstOrDefault().Value.ToString());
                var isSuccess = await acquire(t);
                if (isSuccess)
                {
                    redisValues.Add(item.Id);
                }
            }
            if (redisValues.Any())
            {
                _db.StreamDelete(redisKey, redisValues.ToArray());
            }
            return messages.Count();
        }
        #endregion

        #region 锁

        /// <summary>
        /// 缓存锁
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="Token"></param>
        /// <param name="expiry">锁的有效期</param>
        /// <returns></returns>
        public bool LockTake(string redisKey, string Token, int expiry = 120)
        {
            var _db = GetDatabase(0);
            return _db.LockTake(redisKey, Token, TimeSpan.FromSeconds(expiry));
        }

        /// <summary>
        /// 释放锁
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        public bool LockRelease(string redisKey, string Token)
        {
            var _db = GetDatabase(0);
            return _db.LockRelease(redisKey, Token);
        }

        #endregion

    }

}