﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Repository
{
    internal static class LogicalTransactionExtension
    {
        internal static void Insert<TEntity>(IEnumerable<TEntity> entitys, IDbContext dbContext) where TEntity : BaseEntity, new()
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                LogicalTransaction.GetLogicalTransaction.Insert(entitys, dbContext);
            }
        }

        internal static void Insert<TEntity>(TEntity entity, IDbContext dbContext) where TEntity : BaseEntity
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                LogicalTransaction.GetLogicalTransaction.Insert(entity, dbContext);
            }
        }

        internal static List<DataBaseWork> Change<TEntity>(IDbContext dbContext, IEnumerable<TEntity> entitys) where TEntity : BaseEntity
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                return LogicalTransaction.GetLogicalTransaction.Change(dbContext, entitys);
            }
            return null;
        }
        internal static DataBaseWork Change<TEntity>(IDbContext dbContext, TEntity entity) where TEntity : BaseEntity
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                return LogicalTransaction.GetLogicalTransaction.Change(dbContext, entity);
            }
            return null;
        }
        internal static void Update(List<DataBaseWork> dataWorks)
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                LogicalTransaction.GetLogicalTransaction.Update(dataWorks);
            }
        }

        internal static void Update(DataBaseWork dataWork)
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                LogicalTransaction.GetLogicalTransaction.Update(dataWork);
            }
        }

        internal static void Delete(DataBaseWork dataWork)
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                LogicalTransaction.GetLogicalTransaction.Delete(dataWork);
            }

        }

        internal static void Delete(List<DataBaseWork> dataWorks)
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                LogicalTransaction.GetLogicalTransaction.Delete(dataWorks);
            }
        }
        internal static List<DataBaseWork> DeleteOriginal<TEntity>(IDbContext dbContext, IEnumerable<TEntity> entitys) where TEntity : BaseEntity
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                return LogicalTransaction.GetLogicalTransaction.DeleteOriginal(dbContext, entitys);
            }
            return null;
        }
        internal static DataBaseWork DeleteOriginal<TEntity>(IDbContext dbContext, TEntity entity) where TEntity : BaseEntity
        {
            if (LogicalTransaction.IsOpenLogicalTransaction)
            {
                return LogicalTransaction.GetLogicalTransaction.DeleteOriginal(dbContext, entity);
            }
            return null;
        }
    }
}
