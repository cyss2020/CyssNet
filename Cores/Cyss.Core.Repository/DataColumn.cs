﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Repository
{

    public class DataFieldTable
    {
        public string TableName { set; get; }

        public IEnumerable<DataField> DataFileds { set; get; }
    }

    /// <summary>
    /// 数据库列
    /// </summary>
    public class DataField
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 字段说明
        /// </summary>
        public string Details { set; get; }

        /// <summary>
        /// 类型
        /// </summary>
        public string Type { set; get; }

        /// <summary>
        /// 长度
        /// </summary>
        public int Lenght { set; get; }

        /// <summary>
        /// 是否可null
        /// </summary>
        public bool IsNullable { set; get; }

        /// <summary>
        /// 是否是主键
        /// </summary>
        public bool IsPrimaryKey { set; get; }

        /// <summary>
        /// 是否自增
        /// </summary>
        public bool IsIdentity { set; get; }
    }
}
