﻿using Account.Api.Client;
using Account.Api.Dtos.Extensions;
using Cyss.Core.Cache;
using Cyss.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Librarys
{
    public static class CacheDataCenter
    {
        private static IStaticCacheManager _staticCacheManager;

        static CacheDataCenter()
        {
            _staticCacheManager = IOCEngine.Resolve<IStaticCacheManager>();
        }

        public static async Task<UserNameModel> GetUserNameById(int Id)
        {
            return await _staticCacheManager.GetAsync<UserNameModel>($"UserName_{Id}", async () =>
              {
                  var operateResult = await AccountClientFactory.User.GetUserById(Id);
                  if (operateResult.IsSuccess && operateResult.Data != null)
                  {
                      return new UserNameModel { UserId = operateResult.Data.Id, UserName = operateResult.Data.Name };
                  }
                  return null;
              });
        }

        public static async Task<IEnumerable<UserNameModel>> GetUserNameByIds(IEnumerable<int> Ids)
        {
            IList<UserNameModel> models = new List<UserNameModel>();
            foreach (var id in Ids)
            {
                var user = await GetUserNameById(id);
                if (user == null) continue;
                models.Add(user);
            }
            return models;
        }
    }
}
