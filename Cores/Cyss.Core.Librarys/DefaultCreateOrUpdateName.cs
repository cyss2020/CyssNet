﻿using Cyss.Core.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Librarys
{
    public class DefaultCreateOrUpdateName : IDefaultCreateOrUpdateName
    {
        public async Task Default(BaseDefaultEntityModel model)
        {
            var userName = await CacheDataCenter.GetUserNameById(model.CreateById);
            model.CreateByName = userName?.UserName;

            var userName2 = await CacheDataCenter.GetUserNameById(model.UpdateById);
            model.UpdateByName = userName2?.UserName;
        }
    }
}
