﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Api
{
    public class DefaultRequestLogging : IRequestLogging
    {
        public void Insert(HttpClientMessageModel model)
        {
            LogSevice.Write(model.GetLogText(), "requests", true);
        }
    }
}
