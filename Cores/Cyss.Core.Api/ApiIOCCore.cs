﻿using Cyss.Core;
using Microsoft.AspNetCore.Http;
using System;

namespace Cyss.Core.Api
{
    internal class ApiIOCCore : IOCCore
    {


        /// <summary>
        /// Get IServiceProvider
        /// </summary>
        /// <returns>IServiceProvider</returns>
        protected override IServiceProvider GetServiceProvider()
        {
            if (IOCEngine.IsRegistered<IHttpContextAccessor>())
            {
                var accessor = ServiceProviderFactory.Services?.GetService(typeof(IHttpContextAccessor)) as IHttpContextAccessor;
                var context = accessor?.HttpContext;
                return context?.RequestServices ?? ServiceProviderFactory.Services;
            }
            return ServiceProviderFactory.Services;
        }
    }


}
