﻿using Cyss.Core.Api.Authorized;
using Cyss.Core.Api.JWT;
using Cyss.Core.Helper;
using Cyss.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Cyss.Core.Api
{
    /// <summary>
    /// Api 基础功能类
    /// </summary>
    //[ServiceFilter(typeof(ApiKeyFiter))]
    [CustomExceptionFilter]
    [ApiController]
    [Route("{controller}/{action}")]
    public class BaseApiController : ControllerBase
    {


        /// <summary>
        /// 消息上下文
        /// </summary>
        protected IMessageContext MessageContext { set; get; }

        /// <summary>
        /// 工作上下文
        /// </summary>
        protected IWorkContext WorkContext { set; get; }

        /// <summary>
        /// API 返回模型
        /// </summary>
        protected OperateResult _OperateResult { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public BaseApiController()
        {
            MessageContext = IOCEngine.Resolve<IMessageContext>();
            WorkContext = IOCEngine.Resolve<IWorkContext>();
            _OperateResult = new OperateResult();
        }

        /// <summary>
        /// 返回api 结果对象
        /// </summary>
        /// <param name="IsSuccess"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        protected OperateResult OperateResultContent(bool IsSuccess, string Message = "")
        {
            if (string.IsNullOrWhiteSpace(Message))
            {
                Message = MessageContext.GetMessage();
            }
            OperateResult OperateResult = new OperateResult();
            OperateResult.IsSuccess = IsSuccess;
            OperateResult.Message = Message;
            return OperateResult;
        }

        /// <summary>
        /// 返回Api结果对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="IsSuccess"></param>
        /// <param name="data"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        protected OperateResult<T> OperateResultContent<T>(bool IsSuccess, T data, string Message = "", bool IsZip = false)
        {
            if (string.IsNullOrWhiteSpace(Message))
            {
                Message = MessageContext.GetMessage();
            }
            OperateResult<T> OperateResult = new OperateResult<T>();
            OperateResult.IsSuccess = IsSuccess;
            OperateResult.Data = data;
            OperateResult.Message = Message;
            OperateResult.IsZip = IsZip;
            return OperateResult;
        }


        /// <summary>
        /// 当前登录账户
        /// </summary>
        /// <returns></returns>
        protected virtual AuthenticationUser CurrentAccountUser
        {
            get
            {
                return WorkContext?.User;
            }
        }

    }
}
