﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Api.Models
{
    /// <summary>
    /// Api认证配置
    /// </summary>
    public class ApiKeyConfig : BaseConfig
    {
        /// <summary>
        /// Api认证Key
        /// </summary>
        public string ApiKey { set; get; }


        /// <summary>
        /// 用于加密的药匙
        /// </summary>
        public string EncryptedKey { set; get; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { set; get; }
    }
}
