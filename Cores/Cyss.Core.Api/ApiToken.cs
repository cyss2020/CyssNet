﻿using Cyss.Core.Infrastructure;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Cyss.Core.Api
{
    public class ApiToken : IApiToken
    {
        public Task<string> GetTotkenAsync()
        {
            var httpContextAccessor = IOCEngine.Resolve<IHttpContextAccessor>();
            var tokenobj = httpContextAccessor.HttpContext.Request.Headers["ApiToken"];
            if (tokenobj.Count <= 0)
            {
                return Task.FromResult(string.Empty);
            }
            string token = tokenobj[0];
            if (string.IsNullOrWhiteSpace(token))
            {
                return Task.FromResult(string.Empty);
            }
            return Task.FromResult(token);
        }
    }
}
