﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laluz.Core.Helper
{
    public class BarcodeLibHelper
    {
        /// <summary>
        /// 生成条形码(png格式)
        /// </summary>
        /// <param name="data">文本</param>
        /// <param name="barcodeFormat">格式</param>
        /// <param name="width">宽度</param>
        /// <param name="height">高度</param>
        /// <returns></returns>
        public static byte[] CreateBarCode(string code, BarcodeLib.TYPE type, bool IncludeLabel = false, int width = 300, int height = 100)
        {
            MemoryStream ms = new MemoryStream();
            Image image = null;
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.BackColor = System.Drawing.Color.White;//图片背景颜色  
            b.ForeColor = System.Drawing.Color.Black;//条码颜色  
            b.IncludeLabel = IncludeLabel;
            b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            b.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;//图片格式  
            System.Drawing.Font font = new System.Drawing.Font("Arial", 6);//字体设置  
            b.AlternateLabel=code;
            b.LabelFont = font;
            b.Height = height;//图片高度设置(px单位)  
            b.Width = width;//图片宽度设置(px单位)  

            image = b.Encode(type, code);//生成图片  
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
    }
}
