﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Helper
{
    /// <summary>
    /// Spire.PDF 帮助类
    /// </summary>
    public static class SpirePDFHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes">pdf文件字节</param>
        /// <returns></returns>
        public static Image ToImage(byte[] bytes)
        {
            //初始化PdfDocument实例
            PdfDocument doc = new PdfDocument();
            //加载PDF文档
            doc.LoadFromBytes(bytes);

            //遍历PDF每一页
            for (int i = 0; i < doc.Pages.Count; i++)
            {
                var page = doc.Pages[i];
                //将PDF页转换成bitmap图形 像素要高，不然模糊
                System.Drawing.Image bmp = doc.SaveAsImage(i, (int)page.Size.Width * 2, (int)page.Size.Height * 2);
                bmp.Save(@"d:\test\asdfasdfsf.png", ImageFormat.Png);
                return bmp;
            }
            return null;
        }

    }
}
