﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing;

namespace Cyss.Core.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class ZXingHelper
    {
        /// <summary>
        /// 扫描图片上的二维码
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static string QRDecoder(Image image)
        {
            BarcodeReader<Bitmap> reader = new BarcodeReader<Bitmap>(null);
            reader.Options.CharacterSet = "UTF-8";
            using (Bitmap bmp = new Bitmap(image))
            {
                try
                {
                    Result result = reader.Decode(bmp);
                    if (result == null)
                    {
                        return string.Empty;
                    }
                    return result.Text;
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// 读取一个图片上的多个条码
        /// </summary>
        /// <param name="image"></param>
        /// <param name="BarCodeHeight">条码的大致高度</param>
        /// <returns></returns>
        public static List<string> MultipleQRDecoder(Image image, int BarCodeHeight = 250)
        {
            List<string> Trackings = new List<string>();
            var originalImagebts = image.ToBytes();
            var height = image.Height;
            var width = image.Width;
            int shearHeight = height > BarCodeHeight ? BarCodeHeight : height;
            for (var i = 0; i < height / shearHeight; i++)
            {
                var imgs = GetShearImage(image, 0, shearHeight * i, width, shearHeight);
                var code = QRDecoder(imgs);
                if (!string.IsNullOrWhiteSpace(code) && !Trackings.Contains(code))
                {
                    Trackings.Add(code);
                }
            }
            return Trackings;
        }

        /// <summary>
        /// 剪切图片
        /// </summary>
        /// <param name="originalImage"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        private static Image GetShearImage(Image originalImage, int x, int y, int w, int h)
        {
            //Image originalImage = Image.FromStream(stream);
            //新建一个bmp图片  
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(w, h);
            //新建一个画板  
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
            //设置高质量插值法  
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            //设置高质量,低速度呈现平滑程度  
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //清空画布并以透明背景色填充  
            g.Clear(System.Drawing.Color.White);
            //在指定位置并且按指定大小绘制原图片的指定部分  
            g.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, w, h),
                        new System.Drawing.Rectangle(x, y, w, h),
                        System.Drawing.GraphicsUnit.Pixel);
            return bitmap;
        }

    }
}
