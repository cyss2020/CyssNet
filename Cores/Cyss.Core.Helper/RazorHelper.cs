﻿using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Helper
{
    public static class RazorHelper
    {


        public static string RunCompile(string tempPath, object model, DynamicViewBag dynamicViewBag = null)
        {
            return RunCompileTemplate(File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + tempPath), model, dynamicViewBag);
        }

        public static string RunCompileTemplate(string razorTemplateStr, object model, DynamicViewBag dynamicViewBag = null)
        {
            if (string.IsNullOrWhiteSpace(razorTemplateStr))
                throw new ArgumentException("Razor模版不能为空");
            var htmlString = Engine.Razor.RunCompile(razorTemplateStr, razorTemplateStr.GetHashCode().ToString(), null, model, dynamicViewBag);
            return htmlString.Trim();
        }
    }
}
