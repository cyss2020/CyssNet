﻿using System.Collections.Generic;

namespace Cyss.Core.Helper
{
    public static class LevenshteinHelper
    {

        public static int Levenshtein(this string source, string source2)
        {
            return Fastenshtein.Levenshtein.Distance(source, source2);
        }

        public static string Levenshtein(this IEnumerable<string> strs, string source, bool IsSubstring = false)
        {
            string value = string.Empty;
            int Levenshtein = source.Length;
            foreach (var str in strs)
            {
                string s = str;
                if (IsSubstring)
                {
                    s = str.Substring(0, str.Length > source.Length ? source.Length : str.Length);
                }
                var levenshtein = s.Levenshtein(source);
                if (levenshtein < Levenshtein)
                {
                    value = str;
                    Levenshtein = levenshtein;
                }
            }
            return value;
        }
    }
}
