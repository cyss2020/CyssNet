﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Helper
{
    /// <summary>
    /// 单位转换帮助类
    /// </summary>
    public class UnitHelper
    {

        private static double PoundsToKilogramsRate = 0.45359237;
        private static double InchesToCentimetersRate = 0.393700787402;
        /// <summary>
        /// 英镑转千克
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double PoundsToKilograms(double value)
        {
            return value * PoundsToKilogramsRate;
        }
        

        /// <summary>
        /// 千克转英镑
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>

        public static double KilogramsToPounds(double value)
        {
            return value/PoundsToKilogramsRate;
        }


        /// <summary>
        /// 英寸转厘米
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double InchesToCentimeters(double value)
        {
            return value/InchesToCentimetersRate;
        }

        /// <summary>
        /// 厘米转英寸
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double CentimetersToInches(double value)
        {
            return value*InchesToCentimetersRate;
        }
    }
}
