﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Cyss.Core.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public class CsvHelper
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FilePath">路径</param>
        /// <returns></returns>
        public static DataTable GetDatable(string FilePath)
        {
            return GetDatable(File.ReadAllBytes(FilePath).ToMemoryStream(), 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FilePath">路径</param>
        /// <param name="StartRow">起始行</param>
        /// <returns></returns>
        public static DataTable GetDatable(string FilePath, int StartRow = 1)
        {
            return GetDatable(File.ReadAllBytes(FilePath).ToMemoryStream(), StartRow);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FilePath">路径</param>
        /// <param name="SkipContent">跳过内容</param>
        /// <returns></returns>
        public static DataTable GetDatable(string FilePath, string SkipContent = "")
        {
            var bytes = File.ReadAllBytes(FilePath);
            int index = 0;
            if (!string.IsNullOrWhiteSpace(SkipContent))
            {
                index = GeRowOf(bytes.ToStream(), SkipContent);
            }
            return GetDatable(bytes.ToStream(), index + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="SkipContent">要跳过的内容行</param>
        /// <returns></returns>
        public static DataTable GetDatable(Stream stream, string SkipContent = "")
        {
            int index = 0;
            if (!string.IsNullOrWhiteSpace(SkipContent))
            {
                var bytes = stream.ToBytes();
                index = GeRowOf(bytes.ToStream(), SkipContent);
            }
            return GetDatable(stream, index + 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// /// <returns></returns>
        public static DataTable GetDatable(Stream stream)
        {
            return GetDatable(stream, 1);
        }

        /// <summary>
        /// 将.csv文件转换成DataTable
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="StartRow">起始行</param>
        /// <returns></returns>
        public static DataTable GetDatable(Stream stream, int StartRow = 1)
        {
            try
            {
                using (stream)
                {
                    //编码格式要选对，否则会乱码
                    using (StreamReader input = new StreamReader(stream, Encoding.UTF8))
                    {
                        using (CsvReader csv = new CsvReader(input, false))
                        {

                            for (int i = 1; i <= StartRow; i++)
                            {
                                csv.ReadNextRecord();
                            }
                            DataTable dt = new DataTable();
                            //第一行字段数量
                            int columnCount = csv.FieldCount;

                            //循环添加标题行
                            for (int i = 0; i < columnCount; i++)
                            {
                                dt.Columns.Add(csv[i]);
                            }
                            //循环添加列数据
                            while (csv.ReadNextRecord())
                            {
                                bool IsValidRow = false;
                                DataRow dr = dt.NewRow();
                                for (int i = 0; i < columnCount; i++)
                                {
                                    if (!string.IsNullOrWhiteSpace(csv[i]))
                                    {
                                        IsValidRow = true;
                                        dr[i] = csv[i];
                                    }
                                }
                                if (IsValidRow)
                                {
                                    dt.Rows.Add(dr);
                                }
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// 检索指定的内容在第几行
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="Content"></param>
        /// <returns></returns>
        public static int GeRowOf(Stream stream, string Content)
        {
            using (stream)
            {
                //编码格式要选对，否则会乱码
                using (StreamReader input = new StreamReader(stream, Encoding.UTF8))
                {
                    using (CsvReader csv = new CsvReader(input, false))
                    {
                        int columnCount = csv.FieldCount;
                        int index = 1;
                        //循环添加列数据
                        while (csv.ReadNextRecord())
                        {
                            for (int i = 0; i < columnCount; i++)
                            {
                                if (!string.IsNullOrWhiteSpace(csv[i]))
                                {
                                    if (csv[i].Contains(Content))
                                    {
                                        return index;
                                    }
                                }
                            }
                            index++;
                        }
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetList<T>(Stream stream) where T : new()
        {
            var dataTable = GetDatable(stream);
            return dataTable.GetList<T>();
        }


        /// <summary>
        /// 创建csv
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static Stream Create<T>(IEnumerable<T> items)
        {
            return null;
        }
    }
}

