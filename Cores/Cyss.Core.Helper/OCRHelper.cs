﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;

namespace Cyss.Core.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class OCRHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="segMode">SparseText/SparseTextOsd/SingleBlock</param>
        /// <returns></returns>
        public static string GetImageString(byte[] bytes, PageSegMode segMode = PageSegMode.SparseTextOsd)
        {
            using (TesseractEngine engine = new TesseractEngine(@"C:\tessdata\itextocr\", "eng", EngineMode.Default))
            {
                Page page = engine.Process(Pix.LoadFromMemory(bytes), segMode);
                return page.GetText();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="segMode"></param>
        /// <returns></returns>
        public static string GetImageString(Stream stream, PageSegMode segMode = PageSegMode.SparseTextOsd)
        {
            return GetImageString(stream.ToBytes(), segMode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="segMode"></param>
        /// <returns></returns>
        public static string GetImageString(Image image, PageSegMode segMode = PageSegMode.SparseTextOsd)
        {
            return GetImageString(image.ToBytes(), segMode);
        }
    }
}
