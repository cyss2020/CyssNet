﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Cyss.Core.Helper
{

     
    /// <summary>
    /// 日志帮助类
    /// </summary>
    public class LogHelper
    {

        /// <summary>
        /// 写入日志到文本文件
        /// </summary>
        /// <param name="strMessage">消息</param>
        /// <param name="action">消息action</param>
        /// <param name="fileName">文件名</param>
        /// <param name="IsHourSplit">文件名</param>
        public static void WriteTextLog(string strMessage, string action = "", string fileName = "", bool IsHourSplit = false)
        {
            LogSevice.Write($"Time:{DateTime.Now.ToString()}{Environment.NewLine}Action:{action}{Environment.NewLine}Message:{strMessage}{Environment.NewLine}", fileName, IsHourSplit);
        }

    }



}
