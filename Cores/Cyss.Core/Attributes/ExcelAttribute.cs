﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    ///Excel序列化属性 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ExcelAttribute : Attribute
    {

        public ExcelAttribute()
        {
            Type = CellValueType.String;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="colName"></param>
        /// <param name="width"></param>
        public ExcelAttribute(string colName)
        {
            ColName = colName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="colName"></param>
        /// <param name="width"></param>
        public ExcelAttribute(string colName, int width, string formatString = "", CellValueType type = CellValueType.String)
        {
            ColName = colName;
            Width = width;
            Type = type;
            FormatString = formatString;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="colName"></param>
        /// <param name="width"></param>
        public ExcelAttribute(string colName, string formatString = "", CellValueType type = CellValueType.String)
        {
            ColName = colName;
            Type = type;
            FormatString = formatString;
        }

        /// <summary>
        /// csv表头
        /// </summary>
        public string ColName { set; get; }

        /// <summary>
        /// 列宽
        /// </summary>
        public int Width { set; get; }

        /// <summary>
        /// 显示数据类型
        /// </summary>

        public CellValueType Type { set; get; }

        /// <summary>
        /// 格式
        /// </summary>
        public string FormatString { set; get; }


    }

    /// <summary>
    /// 数据类型
    /// </summary>
    public enum CellValueType
    {
        String,
        Int,
        DateTime
    }

    /// <summary>
    /// 自定义列合计
    /// </summary>
    public class CustomColumns
    {
        private List<CustomColumn> Columns { set; get; }

        public CustomColumns()
        {
            Columns = new List<CustomColumn>();
        }

        public IEnumerable<CustomColumn> Items { get { return Columns; } }

        public void Add(string colName, object value, CellValueType type, int width = 0)
        {
            Columns.Add(new CustomColumn { ColName = colName, Value = value, Type = type, Width = width });
        }
    }

    /// <summary>
    /// 自定义列
    /// </summary>
    public class CustomColumn
    {
        public CustomColumn()
        {
            Type = CellValueType.String;
        }


        public string ColName { set; get; }

        public object Value { set; get; }

        public int Width { set; get; }

        public CellValueType Type { set; get; }
    }

    /// <summary>
    /// 修改excel 单元格值 对象
    /// </summary>
    public class ExcelEidtCell
    {

        public ExcelEidtCell()
        {
            ValueType = CellValueType.String;
        }

        /// <summary>
        /// 类型
        /// </summary>
        public CellValueType ValueType { set; get; }

        /// <summary>
        /// 值
        /// </summary>
        public object Value { set; get; }

        /// <summary>
        /// 行
        /// </summary>
        public int RowIndex { set; get; }

        /// <summary>
        /// 列
        /// </summary>
        public int CellIndex { set; get; }

        /// <summary>
        /// 格式
        /// </summary>
        public string FormatString { set; get; }
    }
}
