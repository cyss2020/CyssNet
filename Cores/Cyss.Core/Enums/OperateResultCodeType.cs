﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public enum OperateResultCodeType
    {
        /// <summary>
        /// 接口锁定
        /// </summary>
        Lock = 1000,

        /// <summary>
        /// api key 验证失败
        /// </summary>
        ApiKey = 1001,

        /// <summary>
        /// 幂等性验证失败
        /// </summary>
        Idempotence = 1002,

        /// <summary>
        /// Token 验证失败
        /// </summary>
        TokenFail = 1003,

        /// <summary>
        /// 权限验证失败
        /// </summary>
        PermissionFail = 1004,
    }
}
