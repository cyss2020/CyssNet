﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// Resize types
    /// </summary>
    public enum ResizeType
    {
        /// <summary>
        /// Longest side
        /// </summary>
        LongestSide,

        /// <summary>
        /// Width
        /// </summary>
        Width,

        /// <summary>
        /// Height
        /// </summary>
        Height
    }
}
