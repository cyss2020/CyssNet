﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.AutoMapper
{
    public class MapperType
    {
        public Type sourceType { set; get; }

        public Type destinationType { set; get; }
    }
}
