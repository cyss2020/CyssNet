﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.AutoMapper
{
    public interface IDefaultCreateOrUpdateName
    {
        Task Default(BaseDefaultEntityModel model);
    }
}
