﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 日志接口
    /// </summary>
    public interface ILogging
    {
        /// <summary>
        /// 插入日志
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="action"></param>
        /// <param name="fileName"></param>
        void Insert(string strMessage, string action, string fileName = "", bool IsHourSplit = false);
    }
}
