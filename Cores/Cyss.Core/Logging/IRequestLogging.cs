﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// API请求日志接口
    /// </summary>
    public interface IRequestLogging
    {
        /// <summary>
        /// 插入日志
        /// </summary>
        /// <param name="model"></param>
        void Insert(HttpClientMessageModel model);
    }
}
