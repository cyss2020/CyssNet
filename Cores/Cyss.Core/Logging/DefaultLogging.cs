﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public class DefaultLogging : ILogging
    {
        public void Insert(string strMessage, string action, string fileName = "", bool IsHourSplit = false)
        {
            LogSevice.Write($"Time:{DateTime.Now.ToString()}{Environment.NewLine}Action:{action}{Environment.NewLine}Message:{strMessage}{Environment.NewLine}", fileName, IsHourSplit);
        }
    }
}
