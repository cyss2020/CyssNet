﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 客户端调用API日志
    /// </summary>
    public interface IClientLogging
    {
        /// <summary>
        /// 插入日志
        /// </summary>
        /// <param name="model"></param>
        void Insert(HttpClientMessageModel model);
    }
}
