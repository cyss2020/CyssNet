﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 系统核心配置
    /// </summary>
    public class SystemConfig : BaseConfig
    {
        /// <summary>
        /// 是否开启Swagger文档
        /// </summary>
        public bool IsSwagger { set; get; }

        /// <summary>
        /// 是否是测试环境
        /// </summary>
        public bool IsTest { set; get; }

        /// <summary>
        /// 日志保留天数
        /// </summary>
        public int LogDays { set; get; } = 15;

        /// <summary>
        /// 上传文件路径
        /// </summary>
        public string Upload { set; get; } = @"C:\iis\Files\";
    }
}
