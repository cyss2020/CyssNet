﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class EnumSelectItem
    {
        public int Id { set; get; }

        public string Name { set; get; }

        public string Text { set; get; }

        /// <summary>
        /// 多语言键
        /// </summary>
        public string ResouresKey { set; get; }
    }
}
