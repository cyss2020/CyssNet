﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 文件扩展类
    /// </summary>
    public static class BaseFileModelExtensions
    {
        public static Stream GetStream(this BaseFileModel baseFile)
        {
            return baseFile.GetBytes().ToStream();
        }
        public static string GetBase64String(this BaseFileModel baseFile)
        {
            return Convert.ToBase64String(baseFile.FileBytes);
        }
        public static void SetStream(this BaseFileModel baseFile, Stream stream)
        {
            baseFile.FileBytes = stream.ToBytes();
        }
        public static void SetBytes(this BaseFileModel baseFile, byte[] bytes)
        {
            baseFile.FileBytes = bytes;
        }
        public static byte[] GetBytes(this BaseFileModel baseFile)
        {
            return baseFile.FileBytes;
        }
        public static string GetSizes(this BaseFileModel baseFile)
        {
            return GetSizes(baseFile.Size);
        }

        public static void Save(this BaseFileModel baseFile, string folder = "")
        {
            SystemConfig systemConfig = IOCEngine.Resolve<SystemConfig>();
            var path = systemConfig.Upload + @"\Uploads\";
            if (!string.IsNullOrWhiteSpace(folder))
            {
                path += folder;
            }
            if (!Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            string FileName = $"{Path.GetFileNameWithoutExtension(baseFile.FileName)}---{DateTime.Now.ToString("yyyyMMddHHmmssfffffff")}{Path.GetExtension(baseFile.FileName)}";
            using (System.IO.FileStream fs = new System.IO.FileStream(@$"{path}\{FileName}", System.IO.FileMode.CreateNew))
            {
                fs.Write(baseFile.FileBytes, 0, baseFile.FileBytes.Length);
            }
        }
        public static string GetSizes(long Size)
        {
            if (Size / GB >= 1)//如果当前Byte的值大于等于1GB
                return (Math.Round(Size / (float)GB, 2)).ToString() + "GB";//将其转换成GB
            else if (Size / MB >= 1)//如果当前Byte的值大于等于1MB
                return (Math.Round(Size / (float)MB, 2)).ToString() + "MB";//将其转换成MB
            else if (Size / KB >= 1)//如果当前Byte的值大于等于1KB
                return (Math.Round(Size / (float)KB, 2)).ToString() + "KB";//将其转换成KGB
            else
                return Size.ToString() + "B";//显示Byte值
        }

        const int GB = 1024 * 1024 * 1024;//定义GB的计算常量
        const int MB = 1024 * 1024;//定义MB的计算常量
        const int KB = 1024;//定义KB的计算常量
    }

    /// <summary>
    /// 文件基础类
    /// </summary>
    public class BaseFileModel
    {

        public BaseFileModel()
        {

        }

        public BaseFileModel(string base64string)
        {
            this.Bytes = Convert.FromBase64String(base64string);
        }
        public BaseFileModel(byte[] bytes)
        {
            this.Bytes = bytes;
        }
        public BaseFileModel(Stream stream)
        {
            this.Bytes = stream.ToBytes();
        }
        private byte[] Bytes { set; get; }

        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { set; get; }


        /// <summary>
        /// 文件字节
        /// </summary>
        public byte[] FileBytes
        {
            set
            {
                Bytes = value;
            }
            get
            {
                return this.Bytes;
            }
        }

        /// <summary>
        /// 文件大小
        /// </summary>
        public long Size { set; get; }


    }
}
