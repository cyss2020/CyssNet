﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Cyss.Core
{



    /// <summary>
    /// API返回的对象模型
    /// </summary>
    [Serializable]
    public class OperateResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { set; get; }

        /// <summary>
        ///Api返回的结果 数据
        /// </summary>
        public object Data { set; get; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Message { set; get; }

        //public Exception Exception { set; get; }
        /// <summary>
        /// 错误码
        /// </summary>
        public int Code { set; get; }

        public OperateResultCodeType CodeType
        {
            set { Code=(int)value; }
            get { return (OperateResultCodeType)Code; }
        }

        /// <summary>
        /// Api返回值
        /// </summary>
        public string ResponseString { set; get; }
    }

    /// <summary>
    /// API返回的对象模型
    /// </summary>
    /// <typeparam name="T">返回的结果模型</typeparam>
    public class OperateResult<T>
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { set; get; }

        /// <summary>
        ///Api返回的结果 数据
        /// </summary>
        public T Data { set; get; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// Api返回值
        /// </summary>
        public string ResponseString { set; get; }

        /// <summary>
        /// 错误码
        /// </summary>
        public int Code { set; get; }

        /// <summary>
        /// 错误码
        /// </summary>
        public OperateResultCodeType CodeType
        {
            set { Code=(int)value; }
            get { return (OperateResultCodeType)Code; }
        }

        /// <summary>
        /// 是否压缩(json序列化的时候对Data属性压缩处理)
        /// </summary>
        public bool IsZip { set; get; }

        /// <summary>
        /// 是否加密(json序列化的时候对Data属性加密处理)
        /// </summary>
        public bool IsEncryption { set; get; }
    }



}
