﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Cyss.Core
{
    /// <summary>
    /// 基础视图模型类
    /// </summary>
    public class BaseViewModel : BaseModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { set; get; }

        public string EncrypId { get { return HttpUtility.UrlEncode("[#]" + EncryptHelper.RandomEncrypt(Id.ToString()), Encoding.ASCII); } }
    }

}
