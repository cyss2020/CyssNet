﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 基础多语言模板
    /// </summary>
    public class BaseLanguageModel
    {
        /// <summary>
        /// 保存
        /// </summary>
        [Display(Name = "保存")]
        public string Save { set; get; }

        /// <summary>
        /// 成功
        /// </summary>
        [Display(Name = "成功")]
        public string Success { set; get; }

        /// <summary>
        /// 失败
        /// </summary>
        [Display(Name = "失败")]
        public string Fail { set; get; }

        /// <summary>
        /// 保存成功
        /// </summary>
        [Display(Name = "保存成功")]
        public string SaveSuccess { set; get; }

        /// <summary>
        /// 保存失败
        /// </summary>
        [Display(Name = "保存失败")]
        public string SaveFail { set; get; }

        /// <summary>
        /// 提交
        /// </summary>
        [Display(Name = "提交")]
        public string Submit { set; get; }

        /// <summary>
        /// 确认
        /// </summary>
        [Display(Name = "确认")]
        public string Confirm { set; get; }

        /// <summary>
        /// 确认失败
        /// </summary>
        [Display(Name = "确认失败")]
        public string ConfirmFail { set; get; }

        /// <summary>
        /// 确认成功
        /// </summary>
        [Display(Name = "确认成功")]
        public string ConfirmSuccess { set; get; }

        /// <summary>
        /// 验证
        /// </summary>
        [Display(Name = "验证")]
        public string Verification { set; get; }

        /// <summary>
        /// 验证失败
        /// </summary>
        [Display(Name = "验证失败")]
        public string VerificationFail { set; get; }

        /// <summary>
        /// 验证成功
        /// </summary>
        [Display(Name = "验证成功")]
        public string VerificationSuccess { set; get; }


        /// <summary>
        /// 提示
        /// </summary>
        [Display(Name = "提示")]
        public string Tips { set; get; }

        /// <summary>
        /// 系统提示
        /// </summary>
        [Display(Name = "系统提示")]
        public string SystemTips { set; get; }

        /// <summary>
        /// 账号
        /// </summary>
        [Display(Name = "账号")]
        public string Seller { set; get; }

        /// <summary>
        /// 平台
        /// </summary>
        [Display(Name = "平台")]
        public string Platform { set; get; }

        /// <summary>
        /// 仓库
        /// </summary>
        [Display(Name = "仓库")]
        public string Warehouse { set; get; }

        /// <summary>
        /// 部门
        /// </summary>
        [Display(Name = "部门")]
        public string Dept { set; get; }

        /// <summary>
        /// 用户
        /// </summary>
        [Display(Name = "用户")]
        public string User { set; get; }

        /// <summary>
        /// 角色
        /// </summary>
        [Display(Name = "角色")]
        public string Role { set; get; }


        /// <summary>
        /// 请选择
        /// </summary>
        [Display(Name = "请选择")]
        public string PleaseSelect { set; get; }

        /// <summary>
        /// 全部
        /// </summary>
        [Display(Name = "全部")]
        public string Whole { set; get; }

        /// <summary>
        /// 所有
        /// </summary>
        [Display(Name = "所有")]
        public string All { set; get; }

        /// <summary>
        /// 取消
        /// </summary>
        [Display(Name = "取消")]
        public string Cancel { set; get; }

        /// <summary>
        /// 取消失败
        /// </summary>
        [Display(Name = "取消失败")]
        public string CancelFail { set; get; }

        /// <summary>
        /// 取消成功
        /// </summary>
        [Display(Name = "取消成功")]
        public string CancelSuccess { set; get; }

        /// <summary>
        /// 确认取消{0}吗?
        /// </summary>
        [Display(Name = "确认取消吗?")]
        public string ConfirmCancel { set; get; }

        /// <summary>
        /// 审核
        /// </summary>
        [Display(Name = "审核")]
        public string Audit { set; get; }

        /// <summary>
        /// 审核失败
        /// </summary>
        [Display(Name = "审核失败")]
        public string AuditFail { set; get; }

        /// <summary>
        /// 审核成功
        /// </summary>
        [Display(Name = "审核成功")]
        public string AuditSuccess { set; get; }

        /// <summary>
        /// 确认审核{0}吗?
        /// </summary>
        [Display(Name = "确认审核吗?")]
        public string ConfirmAudit { set; get; }

        /// <summary>
        /// 查看日志
        /// </summary>
        [Display(Name = "查看日志")]
        public string LookLog { set; get; }

        /// <summary>
        /// 查看谷歌地图
        /// </summary>
        [Display(Name = "查看谷歌地图")]
        public string LookGoogleAddress { set; get; }

        /// <summary>
        /// 下载
        /// </summary>
        [Display(Name = "下载")]
        public string Download { set; get; }

        /// <summary>
        /// 下载失败
        /// </summary>
        [Display(Name = "下载失败")]
        public string DownloadFail { set; get; }


        /// <summary>
        /// 下载成功
        /// </summary>
        [Display(Name = "下载成功")]
        public string DownloadSuccess { set; get; }

        /// <summary>
        /// 请选择记录
        /// </summary>
        [Display(Name = "请选择记录")]
        public string PleaseSelectItems { set; get; }

        /// <summary>
        /// 请选择一条记录
        /// </summary>
        [Display(Name = "请选择一条记录")]
        public string PleaseSelectItem { set; get; }

        /// <summary>
        /// 请点击一条记录
        /// </summary>
        [Display(Name = "请点击一条记录")]
        public string PleaseClickItem { set; get; }

        /// <summary>
        /// 获取{0}信息失败
        /// </summary>
        [Display(Name = " 获取{0}信息失败")]
        public string GetPageListFail { set; get; }

        /// <summary>
        /// 生成
        /// </summary>
        [Display(Name = "生成")]
        public string Generate { set; get; }

        /// <summary>
        /// 生成失败
        /// </summary>
        [Display(Name = "生成失败")]
        public string GenerateFail { set; get; }


        /// <summary>
        /// 生成成功
        /// </summary>
        [Display(Name = "生成成功")]
        public string GenerateSuccess { set; get; }

        /// <summary>
        /// 恢复
        /// </summary>
        [Display(Name = "恢复")]
        public string Recovery { set; get; }

        /// <summary>
        /// 恢复成功
        /// </summary>
        [Display(Name = "恢复成功")]
        public string RecoverySuccess { set; get; }

        /// <summary>
        /// 恢复失败
        /// </summary>
        [Display(Name = "恢复失败")]
        public string RecoveryFail { set; get; }

        /// <summary>
        /// 合计
        /// </summary>
        [Display(Name = "合计")]
        public string Summation { set; get; }

        /// <summary>
        /// 文件
        /// </summary>
        [Display(Name = "文件")]
        public string File { set; get; }

        /// <summary>
        /// 选择
        /// </summary>
        [Display(Name = "选择")]
        public string Select { set; get; }

        /// <summary>
        /// 选择文件
        /// </summary>
        [Display(Name = "选择文件")]
        public string SelectFile { set; get; }


        /// <summary>
        /// 导入
        /// </summary>
        [Display(Name = "导入")]
        public string Import { set; get; }

        /// <summary>
        /// 导入成功
        /// </summary>
        [Display(Name = "导入成功")]
        public string ImportSuccess { set; get; }

        /// <summary>
        /// 导入失败
        /// </summary>
        [Display(Name = "导入失败")]
        public string ImportFail { set; get; }

        /// <summary>
        /// 多个{0}用逗号或空格分开
        /// </summary>
        [Display(Name = "多个{0}用逗号或空格分开")]
        public string LaluzListInputPlaceholder { set; get; }

        /// <summary>
        /// 查询
        /// </summary>
        [Display(Name = "查询")]
        public string Query { set; get; }


        /// <summary>
        /// 搜索
        /// </summary>
        [Display(Name = "搜索")]
        public string Search { set; get; }
    }
}
