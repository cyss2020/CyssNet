﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core
{

    /// <summary>
    /// 登陆用户身份信息
    /// </summary>
    public class AuthenticationUser
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int Id { set; get; }

        /// <summary>
        /// 用户账号
        /// </summary>
        public string UserNo { set; get; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string Name { set; get; }
    }
}
