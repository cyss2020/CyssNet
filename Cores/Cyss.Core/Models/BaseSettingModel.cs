﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public class BaseSettingModel : BaseModel
    {
        /// <summary>
        /// 类型说明
        /// </summary>
        [Display(Name = "配置名称")]
        public string TypeTitle { set; get; }

        /// <summary>
        /// 配置说明
        /// </summary>
        [Display(Name = "配置说明")]
        public string TypeDetails { set; get; }
    }
}
