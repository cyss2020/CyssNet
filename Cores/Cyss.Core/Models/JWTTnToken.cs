﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core
{
    /// <summary>
    /// 存放Token 跟过期时间的类 包含了用户的登录信息 可用作用户登录凭证
    /// </summary>
    public class JWTTnToken
    {
        /// <summary>
        /// token 
        /// </summary>
        public string TokenStr { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime Expires { get; set; }
    }
}
