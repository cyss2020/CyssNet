﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core
{
    /// <summary>
    /// 基础实体类型
    /// </summary>
    public partial class BaseEntity
    {

        public virtual object GetIdValue()
        {
            return 0;
        }
    }

    /// <summary>
    /// 默认有Id 字段的基础实体类
    /// </summary>
    public partial class BaseEntity<T> : BaseEntity
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public T Id { set; get; }



        public override object GetIdValue()
        {
            return this.Id;
        }
    }

    /// <summary>
    /// 默认有创建人,创建时间，更新人，更新时间
    /// </summary>
    /// <typeparam name="T">主键I</typeparam>
    public partial class BaseDefaultEntity<T> : BaseEntity<T>
    {
        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public int CreateById { get; set; }

        ///<summary>
        ///更新时间
        ///</summary>
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        public int UpdateById { get; set; }
    }

    /// <summary>
    /// 默认有创建人,创建时间，更新人，更新时间
    /// </summary>
    public partial class BaseDefaultEntity : BaseDefaultEntity<int>
    {

    }

    public partial class BaseLogicalDeletedEntity<T> : BaseDefaultEntity<int>
    {
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDeleted { set; get; }
    }

}
