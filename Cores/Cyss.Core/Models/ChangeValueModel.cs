﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class ChangeValueModel
    {
        public string Field { set; get; }

        public string NewValue { set; get; }

        public string OldValue { set; get; }

        public override string ToString()
        {
            return $"字段:{Field}旧值:[{NewValue}]新值:[{OldValue}]";
        }
    }
}
