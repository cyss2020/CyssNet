﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 记录api请求的日志模型
    /// </summary>
    public class HttpClientMessageModel
    {
        public string Id { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string TraceIdentifier { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string HttpMethod { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string Headers { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public object Content { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string ContentType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSuccessStatusCode { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string StatusMessage { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string ResponseString { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string Exception { set; get; }

        /// <summary>
        /// 执行时间
        /// </summary>
        public long ElaspedTime { get {
                TimeSpan span = (TimeSpan)(ExecuteEndTime - ExecuteStartTime);
                return span.Seconds*1000+ span.Milliseconds;
            } }

        /// <summary>
        /// 
        /// </summary>
        public DateTimeOffset ExecuteStartTime { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public DateTimeOffset ExecuteEndTime { set; get; }
       


    }

    public static class HttpClientMessageModels
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetLogText(this HttpClientMessageModel model)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"Id: {model.Id}" + Environment.NewLine);
            stringBuilder.Append($"Url: {model.Url}" + Environment.NewLine);
            stringBuilder.Append($"TraceIdentifier: {model.TraceIdentifier}" + Environment.NewLine);
            stringBuilder.Append($"Headers: {model.Headers}" + Environment.NewLine);
            stringBuilder.Append($"HttpMethod: {model.HttpMethod}" + Environment.NewLine);
            stringBuilder.Append($"Content: {model.Content}" + Environment.NewLine);
            stringBuilder.Append($"ContentType: {model.ContentType}" + Environment.NewLine);
            stringBuilder.Append($"IsSuccessStatusCode: {model.IsSuccessStatusCode}" + Environment.NewLine);
            stringBuilder.Append($"StatusMessage: {model.StatusMessage}" + Environment.NewLine);
            stringBuilder.Append($"ResponseString: {model.ResponseString}" + Environment.NewLine);
            stringBuilder.Append($"Exception: {model.Exception}" + Environment.NewLine);
            stringBuilder.Append($"ElaspedTime: {model.ElaspedTime}" + Environment.NewLine);
            stringBuilder.Append($"ExecuteStartTime: {model.ExecuteStartTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}" + Environment.NewLine);
            stringBuilder.Append($"ExecuteEndTime: {model.ExecuteEndTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}" + Environment.NewLine);
            return stringBuilder.ToString();
        }
    }
}
