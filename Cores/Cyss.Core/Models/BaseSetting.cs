﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public class BaseSettingEntity : BaseEntity
    {
        /// <summary>
        /// 类型说明
        /// </summary>
        public string TypeTitle { set; get; }

        /// <summary>
        /// 类型详情
        /// </summary>
        public string TypeDetails { set; get; }
    }
}
