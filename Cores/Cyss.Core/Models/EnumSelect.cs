﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class EnumSelect
    {

        public string Language { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public EnumSelect()
        {
            SelectItems = new List<EnumSelectItem>();
        }

        /// <summary>
        /// 
        /// </summary>
        public string Type { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public IList<EnumSelectItem> SelectItems { set; get; }
    }
}
