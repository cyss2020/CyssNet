﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Cyss.Core
{
    /// <summary>
    /// 基础dto模型
    /// </summary>
    public class BaseEntityModel : BaseModel
    {
        public virtual object GetIdValue()
        {
            return 0;
        }
    }
    /// <summary>
    ///  默认有Id 字段的基础实体类
    /// </summary>
    /// <typeparam name="T">主键Id 类型</typeparam>
    public class BaseEntityModel<T> : BaseEntityModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public T Id { set; get; }
        public override object GetIdValue()
        {
            return this.Id;
        }
    }

    /// <summary>
    ///  默认有创建人,创建时间，更新人，更新时间
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class BaseDefaultEntityModel<T> : BaseEntityModel<T>
    {
        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateById { get; set; }

        ///<summary>
        ///更新时间
        ///</summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        [Display(Name = "更新人")]
        public int UpdateById { get; set; }

        /// <summary>
        /// 创建人（扩展属性(CreateById)）
        /// </summary>
        [Display(Name = "创建人")]
        public string CreateByName { set; get; }

        /// <summary>
        /// 更新人（扩展属性(UpdateById)）
        /// </summary>
        [Display(Name = "更新人")]
        public string UpdateByName { set; get; }
    }

    /// <summary>
    ///  默认有创建人,创建时间，更新人，更新时间
    /// </summary>
    public partial class BaseDefaultEntityModel : BaseDefaultEntityModel<int>
    {


    }
}
