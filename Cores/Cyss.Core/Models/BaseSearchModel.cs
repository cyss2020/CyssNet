﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Cyss.Core
{
    /// <summary>
    /// 基础查询对象（泛型）
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    public class BaseSearchModel<TSource> : BaseSearchModel
    {

        public void OrderBy<TKey>(Expression<Func<TSource, TKey>> keySelector)
        {
            var list = BaseSearchModelExpressionHelper.GetFields(keySelector);
            foreach (var item in list)
            {
                _OrderByFields.Add(new OrderByModel { Field = item, Type = OrderByType.ASC });
            }
        }
        public void OrderByDescending<TKey>(Expression<Func<TSource, TKey>> keySelector)
        {
            var list = BaseSearchModelExpressionHelper.GetFields(keySelector);
            foreach (var item in list)
            {
                _OrderByFields.Add(new OrderByModel { Field = item, Type = OrderByType.DESC });
            }
        }
    }

    /// <summary>
    /// 基础查询对象
    /// </summary>
    public class BaseSearchModel
    {

        public BaseSearchModel()
        {
            this.pageSize = 20;
            this.pageIndex = 1;
            this.getOnlyTotalCount = false;
            this.getOnlyData = false;
            _OrderByFields = new List<OrderByModel>();
        }


        /// <summary>
        /// 第一页从1开始
        /// </summary>
        public int pageIndex { set; get; }

        /// <summary>
        ///页大小
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 是否只加载总数
        /// </summary>
        public bool getOnlyTotalCount { set; get; }

        /// <summary>
        /// 是否只加载数据不统计总数
        /// </summary>
        public bool getOnlyData { set; get; }

        protected IList<OrderByModel> _OrderByFields { set; get; }

        public IList<OrderByModel> OrderByFields { get { return _OrderByFields; } set { _OrderByFields = value; } }

        public void OrderBy(string field)
        {
            if (_OrderByFields.Count(x => x.Field == field) >= 1)
            {
                return;
            }
            _OrderByFields.Add(new OrderByModel { Type = OrderByType.ASC, Field = field });
        }

        public void OrderBy<TSource>(Expression<Func<TSource, object>> keySelector)
        {
            var list = BaseSearchModelExpressionHelper.GetFields(keySelector);
            foreach (var item in list)
            {
                if (_OrderByFields.Count(x => x.Field == item) >= 1)
                {
                    return;
                }
                _OrderByFields.Add(new OrderByModel { Field = item, Type = OrderByType.ASC });
            }
        }

        public void OrderByDescending(string field)
        {
            if (_OrderByFields.Count(x => x.Field == field) >= 1)
            {
                return;
            }
            _OrderByFields.Add(new OrderByModel { Type = OrderByType.DESC, Field = field });
        }
        public void OrderByDescending<TSource>(Expression<Func<TSource, object>> keySelector)
        {
            var list = BaseSearchModelExpressionHelper.GetFields(keySelector);
            foreach (var item in list)
            {
                if (_OrderByFields.Count(x => x.Field == item) >= 1)
                {
                    return;
                }
                _OrderByFields.Add(new OrderByModel { Field = item, Type = OrderByType.DESC });
            }
        }
    }

    public class OrderByModel
    {
        public string Field { set; get; }

        public OrderByType Type { set; get; }
    }
    public enum OrderByType
    {
        ASC = 1,
        DESC = 2
    }
}
