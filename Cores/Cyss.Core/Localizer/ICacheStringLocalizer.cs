﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 多语言接口
    /// </summary>
    public interface ICacheStringLocalizer
    {
        string this[string name, bool isUseDefault = false, string DefaultValue = null]
        {
            get;
        }

        /// <summary>
        /// 根据属性查询多语言信息
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        string Property<TItem>(Expression<Func<TItem, object>> expression, params object[] arguments);

        /// <summary>
        /// 根据枚举获取多语言信息
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        string Enum(Enum en);


        string this[string name, params object[] arguments]
        {
            get;
        }
    }
}
