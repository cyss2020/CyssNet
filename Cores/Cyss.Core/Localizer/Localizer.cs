﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public static class Localizer
    {
        public static string GetResouresKey(string modelName, string fileName)
        {
            string[] baseModelFields = { "CreatedBy", "CreatedOn", "UpdateBy", "UpdateOn", "SalePlatformName", "SellerAlias" };
            if (baseModelFields.Contains(fileName))
            {
                modelName=nameof(BaseLanguageModel);
            }
            return $"Model.{modelName}.{fileName}";
        }
        public static string GetResouresKey(Enum en)
        {
           return $"Enum.{en.GetType().Name}.{en.ToString()}";
        }
        public static string GetResouresKey<T>(string FileName)
        {
            var type = typeof(T);
            return GetResouresKey(type, FileName);
        }
        public static string GetResouresKey<TItem>(Expression<Func<TItem, object>> ValueExpression)
        {
            var fileName = ValueExpression.GetPropertyName();
            return GetResouresKey<TItem>(fileName);
        }
        public static string GetResouresKey(Type type, string FileName)
        {
            var modelName = type.GetProperty(FileName)?.DeclaringType?.Name;
            if (string.IsNullOrWhiteSpace(modelName))
            {
                modelName=type.Name;
            }
            return GetResouresKey(modelName, FileName);
        }
    }
}
