﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Cache
{
    /// <summary>
    /// 事务缓存集合
    /// </summary>
    public class TransactionCacheCollections
    {
        private List<TransactionCache> TransactionCaches { set; get; }

        public void Add(string queueName)
        {
            if (TransactionCaches == null)
            {
                TransactionCaches = new List<TransactionCache>();
            }
            TransactionCaches.Add(new TransactionCache { queueName = queueName });
        }

        public TransactionCache GetTransactionCache(string queueName)
        {
            if (TransactionCaches == null)
            {
                return null;
            }
            return TransactionCaches.FirstOrDefault(x => x.queueName == queueName);
        }

        public TransactionCacheStatus GetStatus()
        {
            if (TransactionCaches.Count(x => x.Status == TransactionCacheStatus.Success) == TransactionCaches.Count())
            {
                return TransactionCacheStatus.Success;
            }
            if (TransactionCaches.Any(x => x.Status == TransactionCacheStatus.Fail))
            {
                return TransactionCacheStatus.Fail;
            }
            if (TransactionCaches.Any(x => x.Status == TransactionCacheStatus.Wait))
            {
                return TransactionCacheStatus.Wait;
            }
            return TransactionCacheStatus.Wait;
        }

        public TransactionRollbackStatus IsRollback()
        {
            if (TransactionCaches.Count(x => x.RollbackStatus == TransactionRollbackStatus.Success) == TransactionCaches.Count())
            {
                return TransactionRollbackStatus.Success;
            }
            if (TransactionCaches.Any(x => x.RollbackStatus == TransactionRollbackStatus.Fail))
            {
                return TransactionRollbackStatus.Fail;
            }
            if (TransactionCaches.Any(x => x.RollbackStatus == TransactionRollbackStatus.Wait))
            {
                return TransactionRollbackStatus.Wait;
            }
            return TransactionRollbackStatus.Wait;

        }

    }

    public class TransactionCache
    {
        public TransactionCache()
        {
            Status = TransactionCacheStatus.Wait;
            RollbackStatus = TransactionRollbackStatus.Wait;
        }
        public string queueName { set; get; }

        public TransactionCacheStatus Status { set; get; }

        public TransactionRollbackStatus RollbackStatus { set; get; }
    }


    public enum TransactionRollbackStatus
    {
        /// <summary>
        /// 等待执行
        /// </summary>
        Wait = 1,

        /// <summary>
        /// 成功
        /// </summary>
        Success = 2,

        /// <summary>
        /// 失败
        /// </summary>
        Fail = 3,
    }
    public enum TransactionCacheStatus
    {
        /// <summary>
        /// 等待执行
        /// </summary>
        Wait = 1,

        /// <summary>
        /// 成功
        /// </summary>
        Success = 2,

        /// <summary>
        /// 失败
        /// </summary>
        Fail = 3,
    }

}
