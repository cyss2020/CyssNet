using System;
using System.Threading.Tasks;

namespace Cyss.Core.Cache
{
    /// <summary>
    /// 缓存操作接口
    /// </summary>
    public interface ICacheManager : IDisposable
    {

        /// <summary>
        /// 获取缓存值
        /// </summary>
        /// <typeparam name="T">值类型</typeparam>
        /// <param name="key">键值</param>
        /// <param name="acquire">当缓存没有的时候获取数据的方法</param>
        /// <param name="cacheTime">过期时间</param>
        /// <returns></returns>

        T Get<T>(string key, Func<T> acquire, int? cacheTime = null, int db = 0);


        T Get<T>(string key, int db = 0);

        /// <summary>
        /// 设置缓存值
        /// </summary>
        /// <param name="key">键值</param>
        /// <param name="data">数据</param>
        /// <param name="cacheTime">键值</param>
        bool Set(string key, object data, int cacheTime = 0, int db = 0);


        /// <summary>
        /// 是否存在key
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns></returns>
        bool IsSet(string key, int db = 0);

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">键值</param>
        bool Remove(string key, int db = 0);


        /// <summary>
        /// 批量删除缓存pattern
        /// </summary>
        /// <param name="pattern"></param>
        void RemoveByPattern(string pattern, int db = 0);

        /// <summary>
        /// 清除缓存数据
        /// </summary>
        void Clear(int db = 0);
    }


    /// <summary>
    /// Cache manager interface
    /// </summary>
    public interface ICacheAsyncManager : IDisposable
    {
        /// <summary>
        /// 异步获取缓存值
        /// </summary>
        /// <typeparam name="T">值类型</typeparam>
        /// <param name="key">键值</param>
        /// <param name="acquire">当缓存没有的时候获取数据的方法</param>
        /// <param name="cacheTime">过期时间</param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key, Func<T> acquire, int? cacheTime = null, int db = 0);

        /// <summary>
        /// 异步获取缓存值
        /// </summary>
        /// <typeparam name="T">值类型</typeparam>
        /// <param name="key">键值</param>
        /// <param name="acquire">当缓存没有的时候获取数据的方法</param>
        /// <param name="cacheTime">过期时间</param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key, Func<Task<T>> acquire, int? cacheTime = null, int db = 0);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key, int db = 0);

        /// <summary>
        /// 异步设置缓存值
        /// </summary>
        /// <param name="key">键值</param>
        /// <param name="data">数据</param>
        /// <param name="cacheTime">键值</param>
        Task<bool> SetAsync(string key, object data, int cacheTime, int db = 0);



        /// <summary>
        /// 异步检查是否存在key
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns></returns>
        Task<bool> IsSetAsync(string key, int db = 0);


        /// <summary>
        /// 异步删除缓存
        /// </summary>
        /// <param name="key">键值</param>
        Task<bool> RemoveAsync(string key, int db = 0);


        Task RemoveByPatternAsync(string pattern, int db = 0);

        Task ClearAsync(int db = 0);
    }
}
