﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cyss.Core.Cache
{
    /// <summary>
    /// Represents a manager for caching between HTTP requests (long term caching)
    /// </summary>
    public interface IStaticCacheManager : ICacheManager, ICacheAsyncManager
    {
        /// <summary>
        /// 在列表头部插入值。如果键不存在，先创建再插入值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="redisValue"></param>
        /// <returns></returns>
        long ListLeftPush<T>(string redisKey, T redisValue, int db = 0);

        /// <summary>
        /// 在列表尾部插入值。如果键不存在，先创建再插入值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="redisValue"></param> 
        /// <returns></returns>
        long ListRightPush<T>(string redisKey, T redisValue, int db = 0);

        /// <summary>
        /// 在列表尾部插入数组集合。如果键不存在，先创建再插入值
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="redisValue"></param>
        /// <returns></returns>
        long ListRightPush<T>(string redisKey, IEnumerable<T> redisValue, int db = 0);


        /// <summary>
        /// 移除并返回存储在该键列表的第一个元素  反序列化
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        T ListLeftPop<T>(string redisKey, int db = 0);

        /// <summary>
        /// 移除并返回存储在该键列表的最后一个元素   反序列化
        /// 只能是对象集合
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        T ListRightPop<T>(string redisKey, int db = 0);

        /// <summary>
        /// 列表长度
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        long ListLength(string redisKey, int db = 0);

        /// <summary>
        /// 返回在该列表上键所对应的元素
        /// </summary>
        /// <param name="redisKey"></param>
        /// <returns></returns>
        IEnumerable<T> ListRange<T>(string redisKey, int db = 0);

        /// <summary>
        /// 根据索引获取指定位置数据
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        IEnumerable<T> ListRange<T>(string redisKey, int start, int stop, int db = 0);


        /// <summary>
        /// 删除List中的元素 并返回删除的个数
        /// </summary>
        /// <param name="redisKey">key</param>
        /// <param name="redisValue">元素</param>
        /// <param name="type">大于零 : 从表头开始向表尾搜索，小于零 : 从表尾开始向表头搜索，等于零：移除表中所有与 VALUE 相等的值</param>
        /// <param name="db"></param>
        /// <returns></returns>
        long ListDelRange<T>(string redisKey, T redisValue, long type = 0, int db = 0);


        /// <summary>
        /// 清空List
        /// </summary>
        /// <param name="redisKey"></param>
        /// <param name="db"></param>
        void ListClear(string redisKey, int db = 0);



        string StreamAdd<T>(string redisKey, string streamField, string streamValue, int db = 0);

        string StreamAdd<T>(string redisKey, Dictionary<string, string> keyValues, int db = 0);

        int StreamRead<T>(string redisKey, Func<T, bool> acquire, int db = 0);

        Task<int> StreamRead<T>(string redisKey, Func<T, Task<bool>> acquire, int db = 0);

        bool LockTake(string redisKey, string Token, int expiry = 120);

        bool LockRelease(string redisKey, string Token);
    }
}