﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public interface IWebWorkContext : IWorkContext
    {

        /// <summary>
        /// 当前菜单Id
        /// </summary>
        string CurrentPageUrl { set; get; }

        /// <summary>
        /// 当前用户菜单集合
        /// </summary>
        IEnumerable<object> Menus { set; get; }
    }
}
