﻿using Cyss.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public interface IWorkContext
    {
        /// <summary>
        /// 登录账户
        /// </summary>
        AuthenticationUser User { set; get; }

        ///// <summary>
        ///// 用户登录Token
        ///// </summary>
        //string LoginToken { set; get; }
    }
}
