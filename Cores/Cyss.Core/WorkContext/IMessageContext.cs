﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public interface IMessageContext
    {
        /// <summary>
        /// 添加上下文消息
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="Title"></param>
        void InsertMessage(string Content, string Title = "");


        /// <summary>
        /// 获取上下文消息
        /// </summary>
        /// <returns></returns>
         string GetMessage(bool IsClear = false);


        /// <summary>
        /// 获取上下文消息集合
        /// </summary>
        /// <returns></returns>
        IList<MessageFactoryModel> GetMessages();

    }

    public class MessageFactoryModel
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { set; get; }

        /// <summary>
        /// 类型
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { set; get; }
    }

}
