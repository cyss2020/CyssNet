﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 配置中心
    /// </summary>
    public class ConfigCore
    {
        public static List<BaseConfig> Configs { set; get; }

        static ConfigCore()
        {
            Configs=new List<BaseConfig>();
        }

        public static void AddConfig(BaseConfig config)
        {
            Configs.Add(config);
        }
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetConfig<T>() where T : class, BaseConfig
        {
            var config = Configs.FirstOrDefault(x => x.GetType()==typeof(T)) as T;
            return config;
        }
    }
}
