﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Net.Http.Headers;
using System.Text;

namespace Cyss.Core
{

    /// <summary>
    /// 
    /// </summary>
    public static class DatetimeExtensions
    {
        private static string defaultDatetimeFormat = "yyyy-MM-dd HH:mm:ss";
        private static string defaultDateFormat = "yyyy-MM-dd";

        /// <summary>
        /// 时间值是否有效，如果 时间是一个 最小值表示无效
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool IsValid(this DateTime dt)
        {
            if (dt == DateTime.MinValue)
            {
                return false;
            }
            if (dt == SqlDateTime.MinValue)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ViewDate(this DateTime? dt, string format = "")
        {
            if (!dt.HasValue)
            {
                return string.Empty;
            }
            return dt.ViewDate(format);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ViewDate(this DateTime dt, string format = "")
        {
            if (string.IsNullOrWhiteSpace(format))
            {
                format = defaultDateFormat;
            }
            return dt.ToDateTimeString(format);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ViewDateTime(this DateTime? dt, string format = "")
        {
            if (!dt.HasValue)
            {
                return string.Empty;
            }
            return dt.ViewDateTime(format);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ViewDateTime(this DateTime dt, string format = "")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(format))
                {
                    format = defaultDatetimeFormat;
                }
                return dt.ToDateTimeString(format);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 转成ISO时间格式(yyyy-MM-ddTHH:mm:sszzz)
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ISODateTimeString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddTHH:mm:sszzz");
        }

        private static string ToDateTimeString(this DateTime dt, string format = "")
        {
            if (string.IsNullOrWhiteSpace(format))
            {
                format = defaultDatetimeFormat;
            }
            if (dt == DateTime.MinValue)
            {
                return string.Empty;
            }
            if (dt == SqlDateTime.MinValue)
            {
                return string.Empty;
            }
            return dt.ToString(format);
        }

        /// <summary>
        /// 计算年龄
        /// </summary>
        /// <param name="dtBirthday"></param>
        /// <returns></returns>
        public static string GetAge(this DateTime dtBirthday)
        {
            string strAge = string.Empty; // 年龄的字符串表示
            int intYear = 0; // 岁
            int intMonth = 0; // 月
            int intDay = 0; // 天

            // 如果没有设定出生日期, 返回空
            if (dtBirthday == DateTime.MinValue)
            {
                return string.Empty;
            }
            if (dtBirthday == SqlDateTime.MinValue)
            {
                return string.Empty;
            }

            // 计算天数
            intDay = dtBirthday.Day - dtBirthday.Day;
            if (intDay < 0)
            {
                dtBirthday = dtBirthday.AddMonths(-1);
                intDay += DateTime.DaysInMonth(dtBirthday.Year, dtBirthday.Month);
            }

            // 计算月数
            intMonth = dtBirthday.Month - dtBirthday.Month;
            if (intMonth < 0)
            {
                intMonth += 12;
                dtBirthday = dtBirthday.AddYears(-1);
            }

            // 计算年数
            intYear = dtBirthday.Year - dtBirthday.Year;

            // 格式化年龄输出
            if (intYear >= 1) // 年份输出
            {
                strAge = intYear.ToString() + "岁";
            }

            if (intMonth > 0 && intYear <= 5) // 五岁以下可以输出月数
            {
                strAge += intMonth.ToString() + "月";
            }

            if (intDay >= 0 && intYear < 1) // 一岁以下可以输出天数
            {
                if (strAge.Length == 0 || intDay > 0)
                {
                    strAge += intDay.ToString() + "日";
                }
            }

            return strAge;
        }

        /// <summary>
        /// 当月第一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime MonthFirstDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
        /// <summary>
        /// 当月最后一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime MonthLastDay(this DateTime dateTime)
        {
            return dateTime.AddMonths(1).AddDays(-1).Date;
        }

        /// <summary>
        /// 下月第一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime NextMonthFirstDay(this DateTime dateTime)
        {
            var date = dateTime.AddMonths(1);
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// 上月第一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime PreMonthFirstDay(this DateTime dateTime)
        {
            var date = dateTime.AddMonths(-1);
            return new DateTime(date.Year, date.Month, 1);
        }

        ///// <summary>
        ///// 将美国时间转成北京时间
        ///// </summary>
        ///// <param name="dateTime"></param>
        ///// <returns></returns>
        //public static DateTime ToBeijingTimeByUs(this DateTime dateTime)
        //{
        //    return dateTime.ToUniversalTime().AddHours(8);
        //}


        /// <summary>
        /// 将时间转成北京时间，如果时间不带有时区标识（DateTimeKind）将默认成本地时区,也可指定
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="kind"></param>
        /// <returns></returns>
        public static DateTime ToBeijingTime(this DateTime dateTime, DateTimeKind kind = DateTimeKind.Local)
        {
            if (dateTime.Kind == DateTimeKind.Unspecified)
            {
                dateTime = DateTime.SpecifyKind(dateTime, kind);
            }
            return dateTime.ToUniversalTime().AddHours(8);
        }

        ///// <summary>
        ///// 将北京时间转成美国时间
        ///// </summary>
        ///// <param name="dateTime"></param>
        ///// <returns></returns>
        //public static DateTime ToUsTimeByCN(this DateTime dateTime)
        //{
        //    return dateTime.ToUniversalTime().AddHours(8);
        //}

        /// <summary>
        /// 计算时间相差多少个小时
        /// </summary>
        /// <param name="StartDateTime">开始时间</param>
        /// <param name="EndDateTime">结束时间</param>
        /// <returns></returns>
        public static int DateDiffHour(this DateTime StartDateTime, DateTime EndDateTime)
        {
            TimeSpan ts1 = new TimeSpan(StartDateTime.Ticks);
            TimeSpan ts2 = new TimeSpan(EndDateTime.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            return ts.Days * 24 + ts.Hours;
        }
    }
}
