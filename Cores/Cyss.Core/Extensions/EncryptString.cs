﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 加密字符串(在json序列化的时候加密处理)
    /// </summary>
    [TypeConverterAttribute(typeof(EncryptStringTypeConverter))]
    public class EncryptString
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        public static implicit operator EncryptString(string p)
        {
            EncryptString encryptString = new EncryptString();
            encryptString._Value = p;
            return encryptString;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        public static implicit operator string(EncryptString p)
        {
            return p._Value;
        }

        private string _Value { set; get; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _Value.ToString();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(EncryptString a, EncryptString b)
        {
            return a.ToString() == b.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(EncryptString a, EncryptString b)
        {
            return a.ToString() != b.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(EncryptString a, string b)
        {
            return a.ToString() == b.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(EncryptString a, string b)
        {
            return a.ToString() != b.ToString();
        }

    }


    /// <summary>
    /// 
    /// </summary>
    public class EncryptStringTypeConverter : System.ComponentModel.TypeConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                EncryptString encryptString = new EncryptString();
                encryptString = value as string;
                return encryptString;
            }
            return base.ConvertFrom(context, culture, value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(System.ComponentModel.ITypeDescriptorContext context, System.Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override bool CanConvertTo(System.ComponentModel.ITypeDescriptorContext context, System.Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return true;
            }
            else if (destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor))
            {
                return true;
            }

            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override object ConvertTo(System.ComponentModel.ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, System.Type destinationType)
        {
            return value.ToString();
        }



    }

}
