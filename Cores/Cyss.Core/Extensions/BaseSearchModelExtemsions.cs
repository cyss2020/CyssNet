﻿using System.Collections.Generic;
using System.Linq;

namespace Cyss.Core
{
    public static class BaseSearchModelExtemsions
    {

        public static IQueryable<T> OrderConditions<T>(this IQueryable<T> query, IEnumerable<OrderByModel> orderConditions)
        {
            bool IsOrder = false;
            foreach (var orderinfo in orderConditions)
            {
                if (orderinfo.Type == OrderByType.ASC)
                {
                    if (IsOrder)
                    {
                        query = query.OrderThenBy(orderinfo.Field);
                    }
                    else
                    {
                        query = query.OrderBy(orderinfo.Field);

                    }
                }
                else
                {
                    if (IsOrder)
                    {
                        query = query.OrderThenByDescending(orderinfo.Field);

                    }
                    else
                    {
                        query = query.OrderByDescending(orderinfo.Field);

                    }
                }
                IsOrder = true;
            }
            return query;
        }

    }




}
