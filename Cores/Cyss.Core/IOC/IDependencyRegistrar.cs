﻿using Autofac;

namespace Cyss.Core
{
    /// <summary>
    /// Dependency registrar interface
    /// </summary>
    public interface IDependencyRegistrar
    {
        /// <summary>
        ///  Register services and interfaces
        /// </summary>
        /// <param name="builder"></param>
        void Register(ContainerBuilder builder);


    }
}
