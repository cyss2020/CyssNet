﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public interface IIOCCore
    {
        /// <summary>
        /// 获取注入服务实例，只适用于注册单例服务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Resolve<T>() where T : class;

        T ResolveNamed<T>(string tag) where T : class;

        object Resolve(Type type);

        /// <summary>
        /// 判断类型是否注册
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool IsRegistered<T>() where T : class;

    }
}
