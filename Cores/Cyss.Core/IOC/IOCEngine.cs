﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// ICO管理中心
    /// </summary>
    public static class IOCEngine
    {

        private static IIOCCore IOCCore { set; get; }

        private static IIOCCore GetIOCCore()
        {
            if (IOCCore == null)
            {
                IOCCore = (IIOCCore)ServiceProviderFactory.Services.GetService(typeof(IIOCCore));
            }
            return IOCCore;
        }

        /// <summary>
        /// 获取注入服务实例，只适用于注册单例服务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Resolve<T>() where T : class
        {
            return GetIOCCore().Resolve<T>();
        }

        /// <summary>
        /// 获取注入服务实例，
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T ResolveNamed<T>(string serviceName) where T : class
        {
            return GetIOCCore().ResolveNamed<T>(serviceName);
        }


        /// <summary>
        /// 获取注入服务实例，只适用于注册单例服务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static object Resolve(Type type)
        {
            return GetIOCCore().Resolve(type);
        }

        public static bool IsRegistered<T>() where T : class
        {
            var obj = ServiceProviderFactory.Services.GetService(typeof(T));
            return obj != null;
        }

    }
}
