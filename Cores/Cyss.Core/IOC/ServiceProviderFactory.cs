﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public static class ServiceProviderFactory
    {
        private static IServiceProvider? _provider;

        /// <summary>
        /// 获取系统 Root IServiceProvider 接口
        /// </summary>
        [NotNull]
        public static IServiceProvider? Services => _provider ?? throw new InvalidOperationException($"{nameof(ServiceProviderFactory.Services)} is null. 请添加 app.ApplicationServices.RegisterProvider(); 语句到 Startup 文件中的 Configure 方法内");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider"></param>
        public static void RegisterServiceProvider(this IServiceProvider provider)
        {
            _provider = provider;
        }
    }

}
