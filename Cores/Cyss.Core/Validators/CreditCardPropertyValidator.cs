using System.Linq;
using FluentValidation;
using FluentValidation.Validators;

namespace Cyss.Core.Validators
{
    /// <summary>
    /// Credit card validator
    /// </summary>
    public class CreditCardPropertyValidator<T> : PropertyValidator<T, string>, ICreditCardValidator
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public CreditCardPropertyValidator()
            : base()
        {

        }

        public override string Name => "CreditCardPropertyValidator";



        public override bool IsValid(ValidationContext<T> context, string value)
        {
            var ccValue = value;
            if (string.IsNullOrWhiteSpace(ccValue))
                return false;

            ccValue = ccValue.Replace(" ", "");
            ccValue = ccValue.Replace("-", "");

            var checksum = 0;
            var evenDigit = false;

            //http://www.beachnet.com/~hstiles/cardtype.html
            foreach (var digit in ccValue.Reverse())
            {
                if (!char.IsDigit(digit))
                    return false;

                var digitValue = (digit - '0') * (evenDigit ? 2 : 1);
                evenDigit = !evenDigit;

                while (digitValue > 0)
                {
                    checksum += digitValue % 10;
                    digitValue /= 10;
                }
            }

            return (checksum % 10) == 0;
        }

    }
}
