﻿using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Validators
{
    public class IntPropertyValidator<T, V> : PropertyValidator<T, V> where V : IComparable
    {
        private readonly V _minValue;
        private readonly V _maxValue;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="maxValue">Maximum value</param>
        public IntPropertyValidator(V minValue, V maxValue) :
            base()
        {
            _minValue = minValue;
            _maxValue = maxValue;
        }

        public override string Name => "IntPropertyValidator";

        public override bool IsValid(ValidationContext<T> context, V value)
        {
            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
            {
                return false;
            }

            if (value.CompareTo(_minValue) < 0)
            {
                return false;
            }
            if (_maxValue.CompareTo(0) > 0 && value.CompareTo(_maxValue) > 0)
            {
                return false;
            }
            return true;
        }


    }

}
