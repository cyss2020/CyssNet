﻿using System;
using FluentValidation;
using FluentValidation.Validators;

namespace Cyss.Core.Validators
{
    /// <summary>
    /// Decimal validator
    /// </summary>
    public class DecimalPropertyValidator<T> : PropertyValidator<T, decimal>
    {
        private readonly decimal _maxValue;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="maxValue">Maximum value</param>
        public DecimalPropertyValidator(decimal maxValue) :
            base()
        {
            _maxValue = maxValue;
        }

        public override string Name => "DecimalPropertyValidator";

        public override bool IsValid(ValidationContext<T> context, decimal value)
        {
            if (decimal.TryParse(value.ToString(), out decimal _value))
                return Math.Round(_value, 3) < _maxValue;
            return false;
        }

    }
}