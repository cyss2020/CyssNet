﻿using FluentValidation;
using FluentValidation.Validators;
using System.Text.RegularExpressions;

namespace Cyss.Core.Validators
{
    /// <summary>
    /// Phohe number validator
    /// </summary>
    public class PhoneNumberPropertyValidator<T> : PropertyValidator<T, string>
    {
        public override string Name => "PhoneNumberPropertyValidator";

        /// <summary>
        /// Ctor
        /// </summary>
        public PhoneNumberPropertyValidator()
            : base()
        {
        }

        /// <summary>
        /// Is valid?
        /// </summary>
        /// <param name="phoneNumber">Phone number</param>
        /// <param name="customerSettings">Customer settings</param>
        /// <returns>Result</returns>
        public static bool IsValid(string phoneNumber)
        {


            if (string.IsNullOrEmpty(phoneNumber))
                return false;

            return Regex.IsMatch(phoneNumber, @"^1[3456789]\d{9}$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)
;
        }

        public override bool IsValid(ValidationContext<T> context, string value)
        {
            return IsValid(value);
        }
    }
}
