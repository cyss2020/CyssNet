﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Validators
{
    /// <summary>
    /// Validator extensions
    /// </summary>
    public static class ValidatorExtensions
    {
        /// <summary>
        /// Set credit card validator
        /// </summary>
        /// <typeparam name="TModel">Type of model being validated</typeparam>
        /// <param name="ruleBuilder">Rule builder</param>
        /// <returns>Result</returns>
        public static IRuleBuilderOptions<TModel, string> IsCreditCard<TModel>(this IRuleBuilder<TModel, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new CreditCardPropertyValidator<TModel>());
        }

        /// <summary>
        /// Set decimal validator
        /// </summary>
        /// <typeparam name="TModel">Type of model being validated</typeparam>
        /// <param name="ruleBuilder">Rule builder</param>
        /// <param name="maxValue">Maximum value</param>
        /// <returns>Result</returns>
        public static IRuleBuilderOptions<TModel, decimal> IsDecimal<TModel>(this IRuleBuilder<TModel, decimal> ruleBuilder, decimal maxValue)
        {
            return ruleBuilder.SetValidator(new DecimalPropertyValidator<TModel>(maxValue));
        }


        public static IRuleBuilderOptions<TModel, int> Min<TModel>(this IRuleBuilder<TModel, int> ruleBuilder, int minValue)
        {
            return ruleBuilder.SetValidator(new IntPropertyValidator<TModel, int>(minValue, 0));
        }

        public static IRuleBuilderOptions<TModel, int> Max<TModel>(this IRuleBuilder<TModel, int> ruleBuilder, int maxValue)
        {
            return ruleBuilder.SetValidator(new IntPropertyValidator<TModel, int>(-1, maxValue));
        }
        public static IRuleBuilderOptions<TModel, short> Min<TModel>(this IRuleBuilder<TModel, short> ruleBuilder, short minValue)
        {
            return ruleBuilder.SetValidator(new IntPropertyValidator<TModel, short>(minValue, 0));
        }

        public static IRuleBuilderOptions<TModel, short> Max<TModel>(this IRuleBuilder<TModel, short> ruleBuilder, short maxValue)
        {
            return ruleBuilder.SetValidator(new IntPropertyValidator<TModel, short>(-1, maxValue));
        }

        /// <summary>
        /// Set phone number validator
        /// </summary>
        /// <typeparam name="TModel">Type of model being validated</typeparam>
        /// <param name="ruleBuilder">Rule builder</param>
        /// <param name="customerSettings">Customer settings</param>
        /// <returns>Result</returns>
        public static IRuleBuilderOptions<TModel, string> IsPhoneNumber<TModel>(this IRuleBuilder<TModel, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new PhoneNumberPropertyValidator<TModel>());
        }


    }
}
