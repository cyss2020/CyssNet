﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    public interface IValueConfig : BaseConfig
    {
        string Value { set; get; }
    }
}
