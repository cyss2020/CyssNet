﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 数据库连接字符串
    /// </summary>
    public interface IConnectionString : IValueConfig
    {

    }
}
