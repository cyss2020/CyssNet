﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core
{
    /// <summary>
    /// Represents a paged model
    /// </summary>
    public partial interface IPagedModel<T> where T : BaseModel
    {
    }
}
