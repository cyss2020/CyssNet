﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core
{
    /// <summary>
    /// 用户权限验证
    /// </summary>
    public interface IPermissionVerification
    {
        /// <summary>
        /// 是否有权限
        /// </summary>
        /// <param name="PageCode"></param>
        /// <param name="OperationType"></param>
        /// <returns></returns>
        Task<bool> IsPermission(string PageCode, string OperationType);

    }
}
