﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Infrastructure
{

    /// <summary>
    /// api获取token接口,
    /// </summary>
    public interface IApiToken
    {
        /// <summary>
        /// 异步获取Token信息
        /// </summary>
        Task<string> GetTotkenAsync();
    }
}
