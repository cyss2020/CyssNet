﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Cyss.Core
{

    /// <summary>
    /// 业务逻辑层基础接口
    /// </summary>
    public interface IBaseService<TEntity>
    {
        #region 数据方法

        /// <summary>
        ///  根据主键获取数据(基础方法)
        /// </summary>
        /// <param name="Id">主键</param>
        /// <returns>数据</returns>
        TEntity GetById(object Id, bool IsNoTracking = false);


        /// <summary>
        ///新增数据(基础方法)
        /// </summary>
        /// <param name="model">数据</param>
        void Insert(TEntity entity);

        /// <summary>
        ///新增数据(基础方法)
        /// </summary>
        /// <param name="model">数据</param>
        void Insert(IEnumerable<TEntity> entities);

        /// <summary>
        ///     更新数据(基础方法)
        /// </summary>
        /// <param name="model">数据</param>
        void Update(TEntity entity);



        /// <summary>
        ///     删除 数据(基础方法)
        /// </summary>
        /// <param name="model">数据</param>
        void Delete(TEntity model);


        /// <summary>
        ///     删除 数据(基础方法)
        /// </summary>
        /// <param name="model">数据</param>
        void Delete(IEnumerable<TEntity> entities);


        /// <summary>
        /// 删除数据(基础方法)
        /// </summary>
        /// <param name="model">数据</param>
        void Delete(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        ///删除数据(基础方法)
        /// </summary>
        /// <param name="model">收藏表</param>
        void Delete(int Id);


        /// <summary>
        ///     删除 数据(基础方法)
        /// </summary>
        /// <param name="model">收藏表</param>
        void Delete(IEnumerable<int> ids);


        /// <summary>
        /// 满足条件的数据是否存在(基础方法)
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <returns>是否存在</returns>
        bool Exists(Expression<Func<TEntity, bool>> predicate);


        /// <summary>
        ///  获取满足条件的第一条数据(基础方法)
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns>数据集合</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate = null, bool IsNoTracking = true);




        /// <summary>
        ///  获取所有的  数据(基础方法)
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns>数据集合</returns>
        IList<TEntity> Gets(Expression<Func<TEntity, bool>> predicate = null, bool IsNoTracking = true);


        /// <summary>
        /// 分页获取 数据(基础方法)
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        IPagedList<TEntity> GetPageLists(BaseSearchModel searchModel);


        #endregion
    }
}
