﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Repository
{
    /// <summary>
    /// Represents an entity repository
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public partial interface IRepositoryAsync<TEntity> where TEntity : BaseEntity
    {
        #region Methods

        /// <summary>
        /// Get entity by identifier
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Entity</returns>
        Task<TEntity> GetByIdAsync(object id);

        /// <summary>
        /// Insert entity
        /// </summary>
        /// <param name="entity">Entity</param>
        Task InsertAsync(TEntity entity);

        /// <summary>
        /// Insert entities
        /// </summary>
        /// <param name="entities">Entities</param>
        void InsertAsync(IEnumerable<TEntity> entities);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void UpdateAsync(TEntity entity);

        /// <summary>
        /// Update entities
        /// </summary>
        /// <param name="entities">Entities</param>
        void UpdateAsync(IEnumerable<TEntity> entities);

        /// <summary>
        /// 大批量更新 默认更新所有除主键和自增建之外的所有字段
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <param name="entities"></param>
        /// <param name="keySelector">指定需要更新的字段 x => new { x.a, x.b } </param>
        void BulkUpdateAsync<K>(IEnumerable<TEntity> entities, Expression<Func<TEntity, K>> keySelector=null);

        /// <summary>
        /// 大批量添加
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="DistinctKeySelector">判断是否重复的字段,重复的数据将不会添加</param>
        int BulkInsertAsync(IEnumerable<TEntity> entities, Expression<Func<TEntity, object>> DistinctKeySelector = null);

        /// <summary>
        /// 大批量添加
        /// </summary>
        /// <param name="entities"></param>
        void BulkInsertAsync(params IEnumerable<BaseEntity>[] entities);

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void DeleteAsync(TEntity entity);

        /// <summary>
        /// Delete entities
        /// </summary>
        /// <param name="entities">Entities</param>
        void DeleteAsync(IEnumerable<TEntity> entities);

        #endregion

        #region Properties

        /// <summary>
        /// Gets a table
        /// </summary>
        IQueryable<TEntity> Table { get; }

        /// <summary>
        /// Gets a table with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        IQueryable<TEntity> TableNoTracking { get; }

        #endregion

        #region EF扩展

        void DeleteAsync(int Id);


        void DeleteAsync(IEnumerable<int> Ids);

        #endregion
    }
}
