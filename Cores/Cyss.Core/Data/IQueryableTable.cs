﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Data
{
    public interface IQueryableTable<T>
    {
        #region Properties

        /// <summary>
        /// 返回一个EF表
        /// </summary>
        T Table { get; }

        /// <summary>
        /// 获取一个启用了“无跟踪”（EF功能）的表。仅当您仅为只读操作加载记录时才使用该表
        /// </summary>
        T TableNoTracking { get; }

        #endregion
    }
}
