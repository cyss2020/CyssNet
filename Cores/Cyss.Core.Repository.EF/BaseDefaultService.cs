﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Cyss.Core.Repository.EF
{
    /// <summary>
    /// 主键Id 为int类型的表基础服务
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BaseDefaultService<TEntity> : BaseDefaultService<TEntity, int> where TEntity : BaseDefaultEntity<int>
    {
        public BaseDefaultService(IRepository<TEntity> repository) : base(repository)
        {

        }
    }

    /// <summary>
    /// 逻辑删除表基础服务
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BaseLogicalDeletedService<TEntity, T> : BaseDefaultService<TEntity> where TEntity : BaseLogicalDeletedEntity<T>
    {
        public BaseLogicalDeletedService(IRepository<TEntity> repository) : base(repository)
        {

        }

        public override void Delete(TEntity model)
        {
            model.IsDeleted = true;
            Update(model);
        }
        public override void Delete(IEnumerable<TEntity> models)
        {
            foreach (var model in models)
            {
                model.IsDeleted = true;
            }
            Update(models);
        }

        public override void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            _repository.TableNoTracking.Where(predicate).ExecuteUpdate(x => x.SetProperty(s => s.IsDeleted, s => true));
        }
    }




    /// <summary>
    /// 默认有删除修改表的基础服务
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BaseDefaultService<TEntity, T> : BaseService<TEntity> where TEntity : BaseDefaultEntity<T>
    {
        public BaseDefaultService(IRepository<TEntity> repository) : base(repository)
        {
        }

        public override void Insert(TEntity model)
        {
            if (model == null)
                throw new ArgumentNullException("model");
            model.CreateDate = DateTime.Now;
            model.CreateById = _workContext.User.Id;
            _repository.Insert(model);
        }

        public override void Insert(IEnumerable<TEntity> models)
        {
            if (models == null || !models.Any())
                throw new ArgumentNullException("models");
            foreach (var item in models)
            {
                item.CreateDate = DateTime.Now;
                item.CreateById = _workContext.User.Id;
            }
            _repository.Insert(models);
        }

        public override void Update(TEntity model)
        {
            if (model == null)
                throw new ArgumentNullException("model");
            model.UpdateDate = DateTime.Now;
            model.UpdateById = _workContext.User.Id;
            _repository.Update(model);
        }

        public override void Update(IEnumerable<TEntity> models)
        {
            if (models == null || !models.Any())
                throw new ArgumentNullException("model");
            foreach (var item in models)
            {
                item.UpdateDate = DateTime.Now;
                item.UpdateById = _workContext.User.Id;
            }
            _repository.Update(models);
        }
    }

}
