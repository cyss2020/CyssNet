﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Repository.EF
{
    public static class WithLockUtility
    {
        /// <summary>
        /// 部分查询不使用锁查询的，可以调用此扩展（默认全局查询不使用with(nolock)）
        /// 参考：https://github.com/aspnetboilerplate/aspnetboilerplate/issues/1637/
        /// 示例：
        /// 1、query.OrderByCustom(filters.orderFields).Select({...}).NoLocking(querable => querable.PagingAsync(filters.page, filters.rows));
        /// 2、repository.EntitiesAsNoTracking.Select(...).NoLocking(item=>item.FirstOrDefaultAsync());
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="queryAction"></param>
        /// <returns></returns>
        public static IQueryable<T> NoLocking<T>(this IQueryable<T> query)
        {
            WithNoLockInterceptor.WithNolock = true;
            return query;
        }
    }
}
