﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Metadata;
using SqlSugar;
using System.Linq.Expressions;

namespace Cyss.Core.Repository.SqlSugar
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity, new()
    {
        public SqlSugarContext Context;

        public EfRepository(SqlSugarContext sqlSugarContext)
        {
            Context=sqlSugarContext;
        }


        public SimpleClient<TEntity> SugarTable { get { return new SimpleClient<TEntity>(Context.Db); } }


        public IQueryable<TEntity> Table => throw new NotImplementedException();

        public IQueryable<TEntity> TableNoTracking => throw new NotImplementedException();

        public TQueryableTable TableQueryable<TQueryableTable>()
        {
            return (TQueryableTable)Context.Db.Queryable<TEntity>();
        }

        public int BulkInsert(IEnumerable<TEntity> entities, System.Linq.Expressions.Expression<Func<TEntity, object>> DistinctKeySelector = null)
        {
            throw new NotImplementedException();
        }

        public void BulkInsert(params IEnumerable<BaseEntity>[] entities)
        {
            throw new NotImplementedException();
        }

        public void BulkUpdate<K>(IEnumerable<TEntity> entities, System.Linq.Expressions.Expression<Func<TEntity, K>> keySelector = null)
        {
            throw new NotImplementedException();
        }

        public void Delete(TEntity entity, bool IsSubmit = true)
        {
            SugarTable.Delete(entity);
        }

        public void Delete(IEnumerable<TEntity> entities, bool IsSubmit = true)
        {
        }

        public void Delete(int Id)
        {
            SugarTable.Delete(Id as dynamic);
        }

        public void Delete(IEnumerable<int> Ids)
        {
            SugarTable.AsDeleteable().In(Ids).ExecuteCommand();
        }

        public void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            SugarTable.Delete(predicate);
        }

        public async Task DeleteAsync(TEntity entity, bool IsSubmit = true)
        {
            await SugarTable.DeleteAsync(entity);
        }

        public async Task DeleteAsync(IEnumerable<TEntity> entities, bool IsSubmit = true)
        {
            await SugarTable.DeleteByIdsAsync(null);
        }

        public int ExecuteSqlCommand(string sql)
        {
            throw new NotImplementedException();
        }

        public Task<int> ExecuteSqlCommandAsync(string sql)
        {
            throw new NotImplementedException();
        }

        public TEntity GetById(object id)
        {
            return SugarTable.GetById(id);
        }

        public IEnumerable<IProperty> GetModifiedProperties(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Insert(TEntity entity, bool IsSubmit = true)
        {
            SugarTable.Insert(entity);
        }

        public void Insert(IEnumerable<TEntity> entities, bool IsSubmit = true)
        {
            SugarTable.InsertRange(entities.ToArray());
        }

        public async Task<int> InsertAsync(TEntity entity, bool IsSubmit = true)
        {
            return await SugarTable.InsertReturnIdentityAsync(entity);
        }

        public async Task<int> InsertAsync(IEnumerable<TEntity> entities, bool IsSubmit = true)
        {
            await SugarTable.InsertRangeAsync(entities.ToArray());
            return 0;
        }

        public bool IsModified(TEntity entity, params string[] ProNames)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Query<T>(string Sql, params SqlParameter[] cmdParms) where T : class, new()
        {
            return this.Context.Db.SqlQueryable<T>(Sql).ToList();
        }



        public void Update(TEntity entity, bool IsSubmit = true)
        {
            SugarTable.Update(entity);
        }

        public void Update(IEnumerable<TEntity> entities, bool IsSubmit = true)
        {
            SugarTable.UpdateRange(entities.ToArray());
        }

        public async Task UpdateAsync(TEntity entity, bool IsSubmit = true)
        {
            await SugarTable.UpdateAsync(entity);
        }

        public async Task UpdateAsync(IEnumerable<TEntity> entities, bool IsSubmit = true)
        {
            await SugarTable.UpdateRangeAsync(entities.ToArray());
        }
    }
}
