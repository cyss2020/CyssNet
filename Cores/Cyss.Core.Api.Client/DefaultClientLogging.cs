﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Api.Client
{
    public class DefaultClientLogging : IClientLogging
    {
        public void Insert(HttpClientMessageModel model)
        {
            LogSevice.Write(model.GetLogText(), "client", true);
        }
    }
}
