﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyss.Core.Api.Client
{
    /// <summary>
    /// Api认证配置
    /// </summary>
    public class ApiClientKeyConfig : BaseConfig
    {
        /// <summary>
        /// Api认证Key
        /// </summary>
        public string ApiKey { set; get; }


        /// <summary>
        /// 用于加密的药匙
        /// </summary>
        public string EncryptedKey { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public int DefalutHttpClientTimeout { set; get; } = 180;
    }
}
