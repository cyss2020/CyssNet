﻿using Cyss.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Api.Client
{

    /// <summary>
    /// 客户端基础类型
    /// </summary>
    public class HttpClientBase
    {
        /// <summary>
        /// HttpClient工厂名称
        /// </summary>
        protected string HttpClientFactoryName { set; get; }

        /// <summary>
        /// httpClient 工厂
        /// </summary>
        private IHttpClientFactory _httpClientFactory;

        /// <summary>
        /// HttpClient 对象
        /// </summary>
        internal HttpClient _httpClient
        {
            get
            {
                return _httpClientFactory.CreateClient(HttpClientFactoryName);
            }
        }

        /// <summary>
        /// 日志接口
        /// </summary>
        internal IClientLogging _loggingApi;

        /// <summary>
        /// 方法集合
        /// </summary>
        internal IList<MethodModel> Methods { set; get; }

        /// <summary>
        /// Api Token对象
        /// </summary>
        private IApiToken _tnToken { set; get; }

        /// <summary>
        /// api配置
        /// </summary>
        private ApiClientKeyConfig apiClientKeyConfig { set; get; }

        public HttpClientBase(IApiToken tnToken)
        {
            if (_httpClientFactory==null)
            {
                _httpClientFactory=IOCEngine.Resolve<IHttpClientFactory>();
            }
            Initialize();
            _tnToken = tnToken;
            apiClientKeyConfig = IOCEngine.Resolve<ApiClientKeyConfig>();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        private void Initialize()
        {

            Methods = new List<MethodModel>();
            var methodInfos = this.GetType().GetMethods().Where(x => x.IsPublic);
            foreach (var methodInfo in methodInfos)
            {
                var parameterInfos = methodInfo.GetParameters();
                Methods.Add(new MethodModel { Name = methodInfo.Name, Parameters = parameterInfos.Select(x => new MethodParameterModel { Name = x.Name }).ToList() });
            }
        }

        /// <summary>
        /// 设置请求Token
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        internal async Task SetToken(HttpRequestMessage message)
        {
            if (_tnToken == null)
            {
                return;
            }
            if (message.Headers.Contains("ApiToken"))
            {
                message.Headers.Remove("ApiToken");
            }
            message.Headers.Add("ApiToken", await this._tnToken.GetTotkenAsync());
        }

        /// <summary>
        /// 设置Api Key
        /// </summary>
        /// <param name="message"></param>
        internal void SetApiKey(HttpRequestMessage message)
        {
            if (message.Headers.Contains("ApiKey"))
            {
                message.Headers.Remove("ApiKey");
            }
            message.Headers.Add("ApiKey", KeyHelper.GenerateKey(apiClientKeyConfig.ApiKey, apiClientKeyConfig.EncryptedKey));
        }

        /// <summary>
        /// 插入日志
        /// </summary>
        /// <param name="model"></param>
        public void InsertLog(HttpClientMessageModel model)
        {
            if (_loggingApi == null)
            {
                _loggingApi = IOCEngine.Resolve<IClientLogging>();
            }
            _loggingApi?.Insert(model);
        }

        /// <summary>
        /// 设置 HttpRequestMessage
        /// </summary>
        /// <param name="message"></param>
        public void SetMessage(HttpRequestMessage message)
        {
            SetCustomMessage(message);
        }

        /// <summary>
        /// 设置 HttpRequestMessage
        /// </summary>
        /// <param name="message"></param>
        protected virtual void SetCustomMessage(HttpRequestMessage message)
        {

        }
    }


}
