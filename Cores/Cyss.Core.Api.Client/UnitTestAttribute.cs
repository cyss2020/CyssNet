﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Api.Client
{

    /// <summary>
    /// 单元测试属性
    /// </summary>
    public class AutoUnitTestAttribute : Attribute
    {
        private object[] _parvs = null;

        /// <summary>
        /// 参数值
        /// </summary>
        /// <param name="parvs"></param>
        public AutoUnitTestAttribute(params object[] parvs)
        {
            _parvs = parvs;
        }

        /// <summary>
        /// 参数值
        /// </summary>
        public object[] ParamValues { get { return _parvs; } }
    }
}
