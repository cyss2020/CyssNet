﻿using Cyss.Core;
using System;

namespace Cyss.Core.Api.Client
{
    /// <summary>
    /// 生成Key服务类
    /// </summary>
    public class KeyHelper
    {

        /// <summary>
        /// 生成加密的key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="EncryptKey"></param>
        /// <returns></returns>
        public static string GenerateKey(string key,string EncryptKey)
        {

            string toFileTimeUtc = DateTime.UtcNow.ToFileTimeUtc().ToString();

            Random random = new Random();
            int index = random.Next(0, 10);

            key = key.Insert(index, toFileTimeUtc);

            string head = $"{index}*{toFileTimeUtc.Length}-";

            return EncryptHelper.AESEncrypt(head + key, EncryptKey);
        }


    }
}
