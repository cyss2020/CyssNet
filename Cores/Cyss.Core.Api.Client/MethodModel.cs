﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Api.Client
{
    internal class MethodModel
    {
        public MethodModel()
        {
            Parameters = new List<MethodParameterModel>();
        }

        public string Name { set; get; }

        public IList<MethodParameterModel> Parameters { set; get; }
    }

    internal class MethodParameterModel
    {
        public string Name { set; get; }

        public Type Type { set; get; }
    }
}
