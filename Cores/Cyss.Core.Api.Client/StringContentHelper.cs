﻿using Cyss.Core;
using System.Net.Http;

namespace Cyss.Core.Api.Client
{
    public class StringContentHelper
    {
        public static StringContent CreateStringContent(object model)
        {
            return new StringContent(JsonHelper.SerializeObject(model), System.Text.Encoding.UTF8, "application/json");

        }
    }
}
