﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.Core.Api.Client
{
    public class UrlParameters
    {

        public UrlParameterType UrlParameterType { set; get; } = UrlParameterType.QuestionMark;

        public object[] _pars { set; get; }
        /// <summary>
        /// 生成URL参数
        /// </summary>
        /// <param name="pars"></param>
        public UrlParameters(params object[] pars)
        {
            _pars = pars;
        }

        /// <summary>
        /// 生成URL参数 
        /// </summary>
        /// <param name="urlParameterType">URL参数方法</param>
        /// <param name="pars"></param>
        public UrlParameters(UrlParameterType urlParameterType, params object[] pars)
        {
            this.UrlParameterType = urlParameterType;
            _pars = pars;
        }
    }

    public enum UrlParameterType
    {
        /// <summary>
        /// 问号
        /// </summary>
        QuestionMark = 1,

        /// <summary>
        /// 斜杠
        /// </summary>
        Slash = 2
    }
}
