﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Account.Api.Infrastructure
{
    public class AccountConnectionString : IConnectionString
    {
        public string Value { set; get; }
    }
}
