using Autofac;
using Cyss.Core;
using Cyss.Core.Api;
using Cyss.Core.Api.Authorized;
using Cyss.Core.Api.Client;
using Cyss.Core.Api.JWT;
using Cyss.Core.Helper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Order.Api.Client;
using System;

namespace Account.Api
{
    public class Startup
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        //public static string SqlConnectionString { set; get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {

                services.RegisterCoreSerivce();
                services.RegisterAllConfig(Configuration);
                services.RegisterOrderClient();
                services.RegisterRedisCacheService();
                //services.RegisterRabbitMQService();

                services.AddCors(options =>
                {
                    options.AddPolicy("AllowSpecificOrigins",
                    builder =>
                    {
                        builder.AllowAnyOrigin() //允许任何来源的主机访问
                       .AllowAnyMethod()
                       .AllowAnyHeader();
                    });
                });

                //注册Jwt Token 相关功能
                services.RegisterJwt(Configuration);
                //注册json
                services.AddJsonOptions();

                services.AddControllers();

            }
            catch (Exception ex)
            {
                LogHelper.WriteTextLog(ex.ToString(), "ConfigureServices", "Startup");
            }

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRequestResponseLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();


            app.UseRouting();

            app.UseCors("AllowSpecificOrigins");
            app.UseCors();



            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(name: default, pattern: "{controller}/{action}");
            });

            app.ApplicationServices.RegisterServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<AutoFacModule>();
        }



    }
}
