﻿using Account.Api.Dtos;
using Account.Api.Entitys;
using Account.Api.Services;
using Cyss.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
namespace Account.Api.Controllers
{

    /// <summary>
    /// 获取用户角色 Api 接口
    /// </summary>
    public class UserRoleController : AccountBaseApiController
    {
        #region 初始化
        private readonly UserRoleService _userRoleService;

        public UserRoleController(UserRoleService userRoleService)
        {
            _userRoleService = userRoleService;
        }
        #endregion

        #region 用户角色增删该查

        /// <summary>
        /// 获取用户角色记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<UserRoleModel>> GetUserRoleById(int Id)
        {
            var entity = _userRoleService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<UserRoleModel>());
        }

        /// <summary>
        /// 获取用户角色记录
        /// </summary>
        /// <param name="UserId">用户Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<UserRoleModel>>> GetUserRoleByUserId(int UserId)
        {
            var entitys = _userRoleService.Gets(x => x.UserId == UserId);
            return OperateResultContent(true, entitys.Select(entity => entity.ToModel<UserRoleModel>()));
        }


        /// <summary>
        /// 获取用户角色分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<UserRoleListModel>> GetPageListUserRoles(UserRoleSearchModel model)
        {
            var entitys = _userRoleService.GetPageListUserRoles(model);

            var viewModel = new UserRoleListModel
            {
                Data = entitys.Select(entity => entity.ToModel<UserRoleModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建用户角色
        /// </summary>
        /// <param name="model">用户角色model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateUserRole(UserRoleModel model)
        {
            var entity = model.ToEntity<UserRole>();
            entity.CreateBy = this.CurrentAccountUser.Id;
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _userRoleService.Insert(entity);
            return OperateResultContent(true, entity.Id, "");
        }


        /// <summary>
        /// 编辑用户角色
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditUserRole(UserRoleModel model)
        {
            var entity = _userRoleService.GetById(model.Id);

            entity.UserId = model.UserId;
            entity.RoleId = model.RoleId;
            entity.SetDefaultValue();
            _userRoleService.Update(entity);
            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除用户角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteUserRole([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _userRoleService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion
    }
}




