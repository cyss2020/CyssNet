﻿using Account.Api.Dtos;
using Account.Api.Entitys;
using Account.Api.Services;
using Cyss.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Account.Api.Controllers
{

    /// <summary>
    /// 获取权限 Api 接口
    /// </summary>
    public class PermissionController : AccountBaseApiController
    {
        #region 初始化
        private readonly PermissionService _permissionService;

        public PermissionController(PermissionService permissionService, MenuService menuService)
        {
            _permissionService = permissionService;
        }
        #endregion

        #region 权限增删该查

        /// <summary>
        /// 获取权限记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<PermissionModel>> GetPermissionById(int Id)
        {
            var entity = _permissionService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<PermissionModel>());
        }

        /// <summary>
        /// 获取所有权限
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<PermissionModel>>> GetPermissions()
        {
            var entitys = _permissionService.Gets();
            return OperateResultContent(true, entitys.Select(entity => entity.ToModel<PermissionModel>()));
        }

        /// <summary>
        /// 获取权限树
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IList<PermissionTreeModel>>> GetPermissionTrees()
        {
            return OperateResultContent(true, _permissionService.GetPermissionTrees());
        }


        /// <summary>
        /// 获取权限记录
        /// </summary>
        /// <param name="MenuId">菜单Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<PermissionModel>>> GetPermissionByMenuId(int MenuId)
        {
            var entitys = _permissionService.Gets(x => x.MenuId == MenuId);
            return OperateResultContent(true, entitys.Select(entity => entity.ToModel<PermissionModel>()));
        }


        /// <summary>
        /// 获取权限分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<PermissionListModel>> GetPageListPermissions(PermissionSearchModel model)
        {
            var entitys = _permissionService.GetPageListPermissions(model);

            var viewModel = new PermissionListModel
            {
                Data = entitys.Select(entity => entity.ToModel<PermissionModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建权限
        /// </summary>
        /// <param name="model">权限model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreatePermission(PermissionModel model)
        {
            var entity = model.ToEntity<Permission>();
            entity.CreateBy = this.CurrentAccountUser.Id;
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _permissionService.Insert(entity);
            return OperateResultContent(true, entity.Id, "");
        }


        /// <summary>
        /// 编辑权限
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditPermission(PermissionModel model)
        {
            var entity = _permissionService.GetById(model.Id);

            entity.MenuId = model.MenuId;
            entity.PermissionCode = model.PermissionCode;
            entity.Name = model.Name;
            entity.DisplayOrder = model.DisplayOrder;
            entity.Remarks = model.Remarks;
            entity.UpdateBy = this.CurrentAccountUser.Id;
            entity.UpdateDate = DateTime.Now;
            entity.SetDefaultValue();
            _permissionService.Update(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除权限
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeletePermission([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _permissionService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion
    }
}




