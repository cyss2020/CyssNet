﻿using Account.Api.Dtos;
using Account.Api.Entitys;
using Account.Api.Services;
using Cyss.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Account.Api.Controllers
{

    /// <summary>
    /// 获取角色权限 Api 接口
    /// </summary>
    public class RolePermissionController : AccountBaseApiController
    {
        #region 初始化
        private readonly RolePermissionService _rolePermissionService;

        public RolePermissionController(RolePermissionService rolePermissionService)
        {
            _rolePermissionService = rolePermissionService;
        }
        #endregion

        #region 角色权限增删该查

        /// <summary>
        /// 获取角色权限记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<RolePermissionModel>> GetRolePermissionById(int Id)
        {
            var entity = _rolePermissionService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<RolePermissionModel>());
        }

        /// <summary>
        /// 获取角色权限记录
        /// </summary>
        /// <param name="RoleId">角色Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<RolePermissionModel>>> GetRolePermissionByRoleId(int RoleId)
        {
            var entitys = _rolePermissionService.Gets(x => x.RoleId == RoleId);
            return OperateResultContent(true, entitys.Select(entity => entity.ToModel<RolePermissionModel>()));
        }

        /// <summary>
        /// 编辑角色权限
        /// </summary>
        /// <param name="models">角色权限列表</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditRolePermission(IEnumerable<RolePermissionModel> models)
        {
            return OperateResultContent(_rolePermissionService.EditRolePermission(models));
        }


        /// <summary>
        /// 获取角色权限分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<RolePermissionListModel>> GetPageListRolePermissions(RolePermissionSearchModel model)
        {
            var entitys = _rolePermissionService.GetPageListRolePermissions(model);

            var viewModel = new RolePermissionListModel
            {
                Data = entitys.Select(entity => entity.ToModel<RolePermissionModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建角色权限
        /// </summary>
        /// <param name="model">角色权限model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateRolePermission(RolePermissionModel model)
        {
            var entity = model.ToEntity<RolePermission>();
            entity.CreateBy = this.CurrentAccountUser.Id;
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _rolePermissionService.Insert(entity);
            return OperateResultContent(true, entity.Id, "");
        }

        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteRolePermission([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _rolePermissionService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion
    }
}




