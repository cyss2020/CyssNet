﻿using Account.Api.Dtos;
using Account.Api.Entitys;
using Account.Api.Services;
using Cyss.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Account.Api.Controllers
{

    /// <summary>
    /// 获取部门 Api 接口
    /// </summary>
    public class DepartmentController : AccountBaseApiController
    {
        #region 初始化
        private readonly DepartmentService _departmentService;

        public DepartmentController(DepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        #endregion

        #region 部门增删该查

        /// <summary>
        /// 获取部门记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<DepartmentModel>> GetDepartmentById(int Id)
        {
            var entity = _departmentService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<DepartmentModel>());
        }

        /// <summary>
        /// 获取部门记录
        /// </summary>
        /// <param name="Id">父部门Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<DepartmentModel>>> GetDeptsByParentDeptId(int Id)
        {
            return OperateResultContent(true, _departmentService.Gets(x => x.IsDeleted == false).Select(x => x.ToModel<DepartmentModel>()));
        }

        /// <summary>
        /// 获取部门树
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<DeptTreeModel>>> GetDeptTrees()
        {
            return OperateResultContent(true, _departmentService.GetDeptTrees());
        }

        /// <summary>
        /// 获取部门分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<DepartmentListModel>> GetPageListDepartments(DepartmentSearchModel model)
        {
            var entitys = _departmentService.GetPageListDepartments(model);

            var viewModel = new DepartmentListModel
            {
                Data = entitys.Select(entity => entity.ToModel<DepartmentModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建部门
        /// </summary>
        /// <param name="model">部门model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateDepartment(DepartmentModel model)
        {
            var entity = model.ToEntity<Department>();
            entity.CreateBy = this.CurrentAccountUser.Id;
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _departmentService.Insert(entity);
            return OperateResultContent(true, entity.Id, "");
        }


        /// <summary>
        /// 编辑部门
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditDepartment(DepartmentModel model)
        {
            var entity = _departmentService.GetById(model.Id);

            entity.ParentDeptId = model.ParentDeptId;
            entity.Name = model.Name;
            entity.Remarks = model.Remarks;
            entity.Icon = model.Icon;
            entity.DisplayOrder = model.DisplayOrder;
            entity.IsEnabled = model.IsEnabled;
            entity.IsDeleted = model.IsDeleted;
            entity.UpdateBy = this.CurrentAccountUser.Id;
            entity.UpdateDate = DateTime.Now;
            entity.SetDefaultValue();
            _departmentService.Update(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteDepartment([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _departmentService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion
    }
}




