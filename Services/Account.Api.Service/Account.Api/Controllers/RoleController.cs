﻿using Account.Api.Dtos;
using Account.Api.Entitys;
using Account.Api.Services;
using Cyss.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Account.Api.Controllers
{

    /// <summary>
    /// 获取角色 Api 接口
    /// </summary>
    public class RoleController : AccountBaseApiController
    {
        #region 初始化
        private readonly RoleService _roleService;

        public RoleController(RoleService roleService)
        {
            _roleService = roleService;
        }
        #endregion

        #region 角色增删该查

        /// <summary>
        /// 获取角色记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<RoleModel>> GetRoleById(int Id)
        {
            var entity = _roleService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<RoleModel>());
        }

        /// <summary>
        /// 获取角色分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<RoleListModel>> GetPageListRoles(RoleSearchModel model)
        {
            var entitys = _roleService.GetPageListRoles(model);

            var viewModel = new RoleListModel
            {
                Data = entitys.Select(entity => entity.ToModel<RoleModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建角色
        /// </summary>
        /// <param name="model">角色model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateRole(RoleModel model)
        {
            var entity = model.ToEntity<Role>();
            entity.CreateBy = this.CurrentAccountUser.Id;
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _roleService.Insert(entity);
            return OperateResultContent(true, entity.Id, "");
        }


        /// <summary>
        /// 编辑角色
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditRole(RoleModel model)
        {
            var entity = _roleService.GetById(model.Id);

            entity.Name = model.Name;
            entity.Remarks = model.Remarks;
            entity.DisplayOrder = model.DisplayOrder;
            entity.IsEnabled = model.IsEnabled;
            entity.Icon = model.Icon;
            entity.UpdateBy = this.CurrentAccountUser.Id;
            entity.UpdateDate = DateTime.Now;
            entity.SetDefaultValue();
            _roleService.Update(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteRole([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _roleService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion
    }
}




