﻿using Account.Api.Dtos;
using Account.Api.Entitys;
using Account.Api.Services;
using Cyss.Core;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Account.Api.Controllers
{

    /// <summary>
    /// 获取菜单 Api 接口
    /// </summary>
    public class MenuController : AccountBaseApiController
    {
        #region 初始化
        private readonly MenuService _menuService;
        private readonly PermissionService _permissionService;
        private readonly RolePermissionService _rolePermissionService;
        public MenuController(MenuService menuService, PermissionService permissionService, RolePermissionService rolePermissionService)
        {
            _menuService = menuService;
            _permissionService = permissionService;
            _rolePermissionService = rolePermissionService;
        }
        #endregion

        #region 菜单增删该查

        /// <summary>
        /// 获取菜单记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<MenuModel>> GetMenuById(int Id)
        {
            var entity = _menuService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<MenuModel>());
        }

        /// <summary>
        /// 获取所有菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<MenuModel>>> GetMenus()
        {
            var entitys = _menuService.Gets();
            return OperateResultContent(true, entitys.Select(entity => entity.ToModel<MenuModel>()));
        }

        /// <summary>
        /// 获取菜单树
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IList<MenuTreeModel>>> GetMenuTrees()
        {
            var entitys = _menuService.GetMenuTrees();
            return OperateResultContent(true, entitys);
        }

        /// <summary>
        /// 获取菜单分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<MenuListModel>> GetPageListMenus(MenuSearchModel model)
        {
            var entitys = _menuService.GetPageListMenus(model);

            var viewModel = new MenuListModel
            {
                Data = entitys.Select(entity => entity.ToModel<MenuModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="model">菜单model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateMenu(MenuModel model)
        {
            using (var transaction = new LogicalTransaction())
            {
                try
                {
                    var entity = model.ToEntity<Menu>();
                    entity.CreateBy = this.CurrentAccountUser.Id;
                    entity.CreateDate = DateTime.Now;
                    entity.SetDefaultValue();
                    _menuService.Insert(entity);

                    if (model.IsDefaultParmisson)
                    {
                        _permissionService.Insert(entity.Id, "Insert", "新增");
                        _permissionService.Insert(entity.Id, "Delete", "删除");
                        _permissionService.Insert(entity.Id, "Edit", "编辑");
                    }
                    transaction.Success();
                    return OperateResultContent(true, entity.Id, "");
                }
                catch (Exception ex)
                {
                    transaction.Fail();
                    transaction.Rollback();
                    throw new Exception("创建菜单失败!:" + ex.Message, ex);
                }
            }
        }


        /// <summary>
        /// 编辑菜单
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditMenu(MenuModel model)
        {
            var entity = _menuService.GetById(model.Id);

            entity.ParentMenuId = model.ParentMenuId;
            entity.Rotue = model.Rotue;
            entity.DisplayOrder = model.DisplayOrder;
            entity.Code = model.Code;
            entity.IsEnabled = model.IsEnabled;
            entity.Name = model.Name;
            entity.Remarks = model.Remarks;
            entity.Icon = model.Icon;
            entity.IsEnabled = model.IsEnabled;
            entity.UpdateBy = this.CurrentAccountUser.Id;
            entity.UpdateDate = DateTime.Now;
            entity.SetDefaultValue();
            _menuService.Update(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteMenu([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            var entity = _menuService.GetById(Id);
            if (entity == null)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _menuService.Delete(entity);
            var permissions = _permissionService.Gets(x => x.MenuId == entity.Id);
            _permissionService.Delete(permissions);
            _rolePermissionService.Delete(x => permissions.Select(x => x.Id).Contains(x.PermissionId));
            return OperateResultContent(true);

        }
        #endregion
    }
}




