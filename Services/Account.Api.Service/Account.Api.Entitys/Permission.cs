﻿using System;
using Cyss.Core;

namespace Account.Api.Entitys
{
    /// <summary>
    /// 权限数据模型
    /// </summary>
    public partial class  Permission: BaseEntity<int>
    {
        #region model
        ///<summary>
        ///菜单ID
        ///</summary>
	    public int MenuId{ get; set; }
        
        ///<summary>
        ///权限码
        ///</summary>
	    public string PermissionCode{ get; set; }
        
        ///<summary>
        ///名称
        ///</summary>
	    public string Name{ get; set; }
        
        ///<summary>
        ///排序
        ///</summary>
	    public int DisplayOrder{ get; set; }
        
        ///<summary>
        ///备注
        ///</summary>
	    public string Remarks{ get; set; }
        
        ///<summary>
        ///创建时间
        ///</summary>
	    public DateTime CreateDate{ get; set; }
        
        ///<summary>
        ///创建人
        ///</summary>
	    public int CreateBy{ get; set; }
        
        ///<summary>
        ///更新日期
        ///</summary>
	    public DateTime UpdateDate{ get; set; }
        
        ///<summary>
        ///更新人
        ///</summary>
	    public int UpdateBy{ get; set; }
        
        #endregion



    }
}