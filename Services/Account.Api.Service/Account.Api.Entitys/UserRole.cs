﻿using System;
using Cyss.Core;

namespace Account.Api.Entitys
{
    /// <summary>
    /// 用户角色数据模型
    /// </summary>
    public partial class  UserRole: BaseEntity<int>
    {
        #region model
        ///<summary>
        ///用户Id
        ///</summary>
	    public int UserId{ get; set; }
        
        ///<summary>
        ///角色Id
        ///</summary>
	    public int RoleId{ get; set; }
        
        ///<summary>
        ///创建时间
        ///</summary>
	    public DateTime CreateDate{ get; set; }
        
        ///<summary>
        ///创建人
        ///</summary>
	    public int CreateBy{ get; set; }
        
        #endregion
    }
}