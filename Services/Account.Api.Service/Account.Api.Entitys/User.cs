﻿using System;
using Cyss.Core;

namespace Account.Api.Entitys
{
    /// <summary>
    /// 会员数据模型
    /// </summary>
    public partial class User : BaseEntity<int>
    {
        #region model
        ///<summary>
        ///账户
        ///</summary>
        public string Number { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string Remarks { get; set; }

        ///<summary>
        ///生日
        ///</summary>
        public DateTime Birthday { get; set; }

        ///<summary>
        ///身份证号
        ///</summary>
        public string CardID { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        public int Sex { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        public string Pwd { get; set; }

        ///<summary>
        ///等级
        ///</summary>
        public int Grade { get; set; }

        ///<summary>
        ///账号
        ///</summary>
        public string No { get; set; }

        ///<summary>
        ///昵称
        ///</summary>
        public string Nickname { get; set; }

        ///<summary>
        ///手机号
        ///</summary>
        public string Phone { get; set; }

        ///<summary>
        ///是否启用
        ///</summary>
        public bool IsEnabled { get; set; }

        ///<summary>
        ///是否删除
        ///</summary>
        public bool IsDeleted { get; set; }

        ///<summary>
        ///紧急联系人
        ///</summary>
        public string EmergencyContact { get; set; }

        ///<summary>
        ///紧急联系人手机号
        ///</summary>
        public string EmergencyContact_Phone { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string PortraitRoute { get; set; }

        ///<summary>
        ///
        ///</summary>
        public int PortraitRoutePictureId { get; set; }

        /// <summary>
        /// 部门Id
        /// </summary>
        public int DeptId { set; get; }

        ///<summary>
        ///
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///
        ///</summary>
        public int CreateBy { get; set; }

        ///<summary>
        ///
        ///</summary>
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///
        ///</summary>
        public int UpdateBy { get; set; }

        public int FileId { set; get; }
        #endregion



    }
}