﻿using System;
using Cyss.Core;

namespace Account.Api.Entitys
{
    /// <summary>
    /// 角色权限数据模型
    /// </summary>
    public partial class RolePermission : BaseEntity<int>
    {
        #region
        ///<summary>
        ///角色Id
        ///</summary>
        public int RoleId { get; set; }

        ///<summary>
        ///权限Id
        ///</summary>
        public int PermissionId { get; set; }

        ///<summary>
        ///菜单
        ///</summary>
        public int MenuId { get; set; }

        ///<summary>
        ///权限类型
        ///</summary>
        public int Type { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public int CreateBy { get; set; }

        #endregion



    }
}