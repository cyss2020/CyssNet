﻿using System;
using Cyss.Core;

namespace Account.Api.Entitys
{
    /// <summary>
    /// 菜单数据模型
    /// </summary>
    public partial class Menu : BaseEntity<int>
    {
        #region model
        ///<summary>
        ///父菜单Id
        ///</summary>
	    public int ParentMenuId { get; set; }

        ///<summary>
        ///路由
        ///</summary>
        public string Rotue { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { set; get; }

        ///<summary>
        ///
        ///</summary>
	    public int DisplayOrder { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        public string Remarks { get; set; }

        ///<summary>
        ///图标
        ///</summary>
        public string Icon { get; set; }

        ///<summary>
        ///是否启用
        ///</summary>
        public bool IsEnabled { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        public int CreateBy { get; set; }

        ///<summary>
        ///更新时间
        ///</summary>
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        public int UpdateBy { get; set; }

        #endregion



    }
}