﻿using System;
using Cyss.Core;

namespace Account.Api.Entitys
{
    /// <summary>
    /// 部门数据模型
    /// </summary>
    public partial class  Department: BaseEntity<int>
    {
        #region model
        ///<summary>
        ///父部门Id
        ///</summary>
	    public int ParentDeptId{ get; set; }
        
        ///<summary>
        ///部门名称
        ///</summary>
	    public string Name{ get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public int DisplayOrder { get; set; }

        ///<summary>
        ///备注
        ///</summary>
	    public string Remarks{ get; set; }
        
        ///<summary>
        ///图标
        ///</summary>
	    public string Icon{ get; set; }
        
        ///<summary>
        ///是否启用
        ///</summary>
	    public bool IsEnabled{ get; set; }
        
        ///<summary>
        ///是否删除
        ///</summary>
	    public bool IsDeleted{ get; set; }
        
        ///<summary>
        ///创建时间
        ///</summary>
	    public DateTime CreateDate{ get; set; }
        
        ///<summary>
        ///创建人
        ///</summary>
	    public int CreateBy{ get; set; }
        
        ///<summary>
        ///更新时间
        ///</summary>
	    public DateTime UpdateDate{ get; set; }
        
        ///<summary>
        ///更新人
        ///</summary>
	    public int UpdateBy{ get; set; }
        
        #endregion



    }
}