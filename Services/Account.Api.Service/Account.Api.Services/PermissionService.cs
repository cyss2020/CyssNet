﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Account.Api.Entitys;
using Account.Api.Dtos;
using Cyss.Core.Repository;

namespace Account.Api.Services
{
    /// <summary>
    /// 权限服务
    /// </summary>
    public class PermissionService : BaseService<Permission>
    {
        #region 字段
        /// <summary>
        /// 权限仓储
        /// </summary>
        private readonly IRepository<Permission> _permissionRepository;
        private readonly MenuService _menuService;
        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="permissionRepository">权限 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public PermissionService(IRepository<Permission> permissionRepository, MenuService menuService) : base(permissionRepository)
        {
            _permissionRepository = permissionRepository;
            _menuService = menuService;
        }

        #endregion

        #region 权限方法


        public virtual int Insert(int MenuId, string Code, string Name)
        {
            Permission permission = new Permission();
            permission.CreateDate = DateTime.Now;
            permission.PermissionCode = Code;
            permission.Name = Name;
            permission.MenuId = MenuId;
            permission.SetDefaultValue();
            _permissionRepository.Insert(permission);
            return permission.Id;
        }

        /// <summary>
        /// 获取权限树
        /// </summary>
        /// <returns></returns>
        public virtual IList<PermissionTreeModel> GetPermissionTrees()
        {
            var menuTrees = _menuService.GetMenuTrees();
            var permissions = Gets().Select(x => x.ToModel<PermissionModel>());
            List<PermissionTreeModel> permissionTrees = new List<PermissionTreeModel>();
            foreach (var menuTree in menuTrees)
            {
                PermissionTreeModel permissionTree = new PermissionTreeModel();
                permissionTree.IsMenu = true;
                permissionTree.Menu = menuTree.Menu;
                permissionTrees.Add(permissionTree);
                RecursionTree(permissionTree, menuTree, permissions);
            }
            return permissionTrees;
        }

        /// <summary>
        /// 递归构建权限树
        /// </summary>
        /// <param name="menuTree"></param>
        /// <param name="menus"></param>
        private void RecursionTree(PermissionTreeModel permissionTree, MenuTreeModel menuTree, IEnumerable<PermissionModel> permissions)
        {
            if (menuTree.HasChildren)
            {
                foreach (var temp_menuTree in menuTree.Childrens)
                {
                    PermissionTreeModel temp_permissionTree = new PermissionTreeModel();
                    temp_permissionTree.IsMenu = true;
                    temp_permissionTree.Menu = temp_menuTree.Menu;
                    permissionTree.Childrens.Add(temp_permissionTree);
                    RecursionTree(temp_permissionTree, temp_menuTree, permissions);
                }
            }
            else
            {
                foreach (var permission in permissions.Where(x => x.MenuId == menuTree.Menu.Id))
                {
                    PermissionTreeModel temp_permissionTree = new PermissionTreeModel();
                    temp_permissionTree.IsMenu = false;
                    temp_permissionTree.Permission = permission;
                    permissionTree.Childrens.Add(temp_permissionTree);
                }
            }
        }

        /// <summary>
        /// 分页获取 权限
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<Permission> GetPageListPermissions(PermissionSearchModel searchModel)
        {
            var query = _permissionRepository.TableNoTracking;
            if (searchModel.MenuId > 0)
            {
                query = query.Where(x => x.MenuId == searchModel.MenuId);
            }
            var entitys = new PagedList<Permission>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 权限
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<PermissionModel> GetPageListPermissionModels(PermissionSearchModel searchModel)
        {
            var query = from a in _permissionRepository.TableNoTracking
                        select new PermissionModel
                        {
                            Id = a.Id,
                            MenuId = a.MenuId,
                            PermissionCode = a.PermissionCode,
                            Name = a.Name,
                            Remarks = a.Remarks,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                            UpdateDate = a.UpdateDate,
                            UpdateBy = a.UpdateBy,
                        };

            var entitys = new PagedList<PermissionModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

