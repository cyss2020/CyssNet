﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Account.Api.Entitys;
using Account.Api.Dtos;
using Cyss.Core.Repository;

namespace Account.Api.Services
{
    /// <summary>
    /// 菜单服务
    /// </summary>
    public class MenuService : BaseService<Menu>
    {
        #region 字段
        /// <summary>
        /// 菜单仓储
        /// </summary>
        private readonly IRepository<Menu> _menuRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="menuRepository">菜单 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public MenuService(IRepository<Menu> menuRepository) : base(menuRepository)
        {
            _menuRepository = menuRepository;
        }

        #endregion

        #region 菜单方法

        /// <summary>
        /// 获取菜单树
        /// </summary>
        /// <returns></returns>
        public virtual IList<MenuTreeModel> GetMenuTrees()
        {
            var menus = _menuRepository.TableNoTracking.ToList().Select(x => x.ToModel<MenuModel>());
            List<MenuTreeModel> menuTrees = new List<MenuTreeModel>();
            foreach (var menu in menus.Where(x => x.ParentMenuId == 0))
            {
                MenuTreeModel menuTree = new MenuTreeModel();
                menuTree.Menu = menu;
                menuTrees.Add(menuTree);
                RecursionTree(menuTree, menus);
            }
            return menuTrees;
        }

        /// <summary>
        /// 递归构建菜单树
        /// </summary>
        /// <param name="menuTree"></param>
        /// <param name="menus"></param>
        private void RecursionTree(MenuTreeModel menuTree, IEnumerable<MenuModel> menus)
        {
            var temps = menus.Where(x => x.ParentMenuId == menuTree.Menu.Id).ToList();
            if (temps.Count > 0)
            {
                foreach (var m in temps)
                {
                    MenuTreeModel t_menuTree = new MenuTreeModel();
                    t_menuTree.Menu = m;
                    menuTree.Childrens.Add(t_menuTree);
                    RecursionTree(t_menuTree, menus);
                }
            }
        }


        /// <summary>
        /// 分页获取 菜单
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<Menu> GetPageListMenus(MenuSearchModel searchModel)
        {
            var query = _menuRepository.TableNoTracking;
            var entitys = new PagedList<Menu>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 菜单
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<MenuModel> GetPageListMenuModels(MenuSearchModel searchModel)
        {
            var query = from a in _menuRepository.TableNoTracking
                        select new MenuModel
                        {
                            Id = a.Id,
                            ParentMenuId = a.ParentMenuId,
                            Rotue = a.Rotue,
                            Name = a.Name,
                            Remarks = a.Remarks,
                            Icon = a.Icon,
                            IsEnabled = a.IsEnabled,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                            UpdateDate = a.UpdateDate,
                            UpdateBy = a.UpdateBy,
                        };
            var entitys = new PagedList<MenuModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

