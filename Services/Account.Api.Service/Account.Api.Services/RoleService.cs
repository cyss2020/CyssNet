﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Account.Api.Entitys;
using Account.Api.Dtos;
using Cyss.Core.Repository;

namespace Account.Api.Services
{
    /// <summary>
    /// 角色服务
    /// </summary>
    public class RoleService : BaseService<Role>
    {
        #region 字段
        /// <summary>
        /// 角色仓储
        /// </summary>
        private readonly IRepository<Role> _roleRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="roleRepository">角色 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public RoleService(IRepository<Role> roleRepository) : base(roleRepository)
        {
            _roleRepository = roleRepository;
        }

        #endregion

        #region 角色方法

        /// <summary>
        /// 分页获取 角色
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<Role> GetPageListRoles(RoleSearchModel searchModel)
        {
            var query = _roleRepository.TableNoTracking;
            var entitys = new PagedList<Role>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 角色
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<RoleModel> GetPageListRoleModels(RoleSearchModel searchModel)
        {
            var query = from a in _roleRepository.TableNoTracking
                        select new RoleModel
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Remarks = a.Remarks,
                            IsEnabled = a.IsEnabled,
                            Icon = a.Icon,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                            UpdateDate = a.UpdateDate,
                            UpdateBy = a.UpdateBy,
                        };

            var entitys = new PagedList<RoleModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

