﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Account.Api.Entitys;
using Account.Api.Dtos;
using Cyss.Core.Repository;

namespace Account.Api.Services
{
    /// <summary>
    /// 部门服务
    /// </summary>
    public class DepartmentService : BaseService<Department>
    {
        #region 字段
        /// <summary>
        /// 部门仓储
        /// </summary>
        private readonly IRepository<Department> _departmentRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="departmentRepository">部门 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public DepartmentService(IRepository<Department> departmentRepository) : base(departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        #endregion

        #region 部门方法



        /// <summary>
        /// 获取部门树
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<DeptTreeModel> GetDeptTrees()
        {
            var departments = Gets(x => x.IsDeleted == false && x.IsEnabled == true).Select(x => x.ToModel<DepartmentModel>());
            List<DeptTreeModel> deptTreeModels = new List<DeptTreeModel>();
            foreach (var department in departments.Where(x => x.ParentDeptId == 0))
            {
                DeptTreeModel deptTree = new DeptTreeModel();
                deptTree.Dept = department;
                deptTreeModels.Add(deptTree);
                RecursionTree(deptTree, departments);
            }
            return deptTreeModels;
        }

        /// <summary>
        /// 递归构建部门树
        /// </summary>
        /// <param name="menuTree"></param>
        /// <param name="menus"></param>
        private void RecursionTree(DeptTreeModel deptTree, IEnumerable<DepartmentModel> departments)
        {
            var temps = departments.Where(x => x.ParentDeptId == deptTree.Dept.Id).ToList();
            if (temps.Count > 0)
            {
                foreach (var m in temps)
                {
                    DeptTreeModel t_deptTree = new DeptTreeModel();
                    t_deptTree.Dept = m;
                    deptTree.Childrens.Add(t_deptTree);
                    RecursionTree(t_deptTree, departments);
                }
            }
        }

        /// <summary>
        /// 分页获取 部门
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<Department> GetPageListDepartments(DepartmentSearchModel searchModel)
        {
            var query = _departmentRepository.TableNoTracking;
            var entitys = new PagedList<Department>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 部门
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<DepartmentModel> GetPageListDepartmentModels(DepartmentSearchModel searchModel)
        {
            var query = from a in _departmentRepository.TableNoTracking
                        select new DepartmentModel
                        {
                            Id = a.Id,
                            ParentDeptId = a.ParentDeptId,
                            Name = a.Name,
                            Remarks = a.Remarks,
                            Icon = a.Icon,
                            IsEnabled = a.IsEnabled,
                            IsDeleted = a.IsDeleted,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                            UpdateDate = a.UpdateDate,
                            UpdateBy = a.UpdateBy,
                        };

            var entitys = new PagedList<DepartmentModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

