﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Account.Api.Entitys;
using Account.Api.Dtos;
using Cyss.Core.Repository;

namespace Account.Api.Services
{
    /// <summary>
    /// 用户角色服务
    /// </summary>
    public class UserRoleService : BaseService<UserRole>
    {
        #region 字段
        /// <summary>
        /// 用户角色仓储
        /// </summary>
        private readonly IRepository<UserRole> _userRoleRepository;

        private readonly IRepository<Role> _roleRepository;
        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="userRoleRepository">用户角色 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public UserRoleService(IRepository<UserRole> userRoleRepository, IRepository<Role> roleRepository) : base(userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
        }

        #endregion

        #region 用户角色方法

        /// <summary>
        ///  获取所有的数据
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>数据集合</returns>
        public virtual IList<UserRoleModel> GetModels(Expression<Func<UserRoleModel, bool>> predicate = null)
        {

            var query = from a in _userRoleRepository.TableNoTracking
                        join b in _roleRepository.TableNoTracking on a.RoleId equals b.Id
                        select new UserRoleModel
                        {
                            Id = a.Id,
                            UserId = a.UserId,
                            RoleId = a.RoleId,
                            RoleName = b.Name,
                            CreateBy = a.CreateBy,
                            CreateDate = a.CreateDate

                        };
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToList();
        }

        /// <summary>
        /// 分页获取 用户角色
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<UserRole> GetPageListUserRoles(UserRoleSearchModel searchModel)
        {
            var query = _userRoleRepository.TableNoTracking;
            var entitys = new PagedList<UserRole>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 用户角色
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<UserRoleModel> GetPageListUserRoleModels(UserRoleSearchModel searchModel)
        {
            var query = from a in _userRoleRepository.TableNoTracking
                        select new UserRoleModel
                        {
                            Id = a.Id,
                            UserId = a.UserId,
                            RoleId = a.RoleId,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                        };

            var entitys = new PagedList<UserRoleModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

