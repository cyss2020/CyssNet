﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Account.Api.Entitys;
using Account.Api.Dtos;
using Cyss.Core.Helper;
using Cyss.Core.Repository;

namespace Account.Api.Services
{
    /// <summary>
    /// 角色权限服务
    /// </summary>
    public class RolePermissionService : BaseService<RolePermission>
    {
        #region 字段
        /// <summary>
        /// 角色权限仓储
        /// </summary>
        private readonly IRepository<RolePermission> _rolePermissionRepository;
        private readonly IRepository<UserRole> _userRoleRepository;
        private readonly IRepository<Permission> _permissionRepository;
        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="rolePermissionRepository">角色权限 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public RolePermissionService(IRepository<RolePermission> rolePermissionRepository, IRepository<UserRole> userRoleRepository, IRepository<Permission> permissionRepository) : base(rolePermissionRepository)
        {
            _rolePermissionRepository = rolePermissionRepository;
            _userRoleRepository = userRoleRepository;
            _permissionRepository = permissionRepository;
        }

        #endregion

        #region 角色权限方法

        public virtual IList<RolePermission> GetRolePermissionsByUserId(int UserId)
        {
            var query = from a in _rolePermissionRepository.TableNoTracking
                        join b in _userRoleRepository.TableNoTracking on a.RoleId equals b.RoleId
                        where b.UserId == UserId
                        select a;
            return query.ToList();
        }

        public virtual bool EditRolePermission(IEnumerable<RolePermissionModel> models)
        {
            if (models.Select(x => x.RoleId).Distinct().Count() >= 2)
            {
                return false;
            }
            var roleId = models.First().RoleId;

            var oldRolePermissions = _rolePermissionRepository.TableNoTracking.Where(x => x.RoleId == roleId).ToList();

            //新增的权限
            var news = from a in models
                       join b in oldRolePermissions on new { a.Type, a.MenuId, a.PermissionId } equals new { b.Type, b.MenuId, b.PermissionId } into bj
                       from b in bj.DefaultIfEmpty()
                       where b == null
                       select a;

            //删除的权限
            var deletes = from a in oldRolePermissions
                          join b in models on new { a.Type, a.MenuId, a.PermissionId } equals new { b.Type, b.MenuId, b.PermissionId } into bj
                          from b in bj.DefaultIfEmpty()
                          where b == null
                          select a;

            if (news.Count() > 0)
            {
                var rolePermissions = news.Select(x => { var entity = x.ToEntity<RolePermission>(); entity.SetDefaultValue(); return entity; });
                this.Insert(rolePermissions);
            }
            if (deletes.Count() > 0)
            {
                this.Delete(deletes.Select(x => x.Id));
            }
            return true;
        }

        /// <summary>
        /// 分页获取 角色权限
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<RolePermission> GetPageListRolePermissions(RolePermissionSearchModel searchModel)
        {
            var query = _rolePermissionRepository.TableNoTracking;
            var entitys = new PagedList<RolePermission>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 角色权限
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<RolePermissionModel> GetPageListRolePermissionModels(RolePermissionSearchModel searchModel)
        {
            var query = from a in _rolePermissionRepository.TableNoTracking
                        select new RolePermissionModel
                        {
                            Id = a.Id,
                            RoleId = a.RoleId,
                            PermissionId = a.PermissionId,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                        };

            var entitys = new PagedList<RolePermissionModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

