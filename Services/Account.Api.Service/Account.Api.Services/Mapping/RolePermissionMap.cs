﻿using Account.Api.Entitys;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Account.Api.Services.Mapping
{
    /// <summary>
    /// 角色权限
    /// </summary>
    public partial class  RolePermissionMap:  BaseEntityTypeConfiguration<RolePermission>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<RolePermission> builder)
        {
            builder.ToTable(nameof(RolePermission));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}