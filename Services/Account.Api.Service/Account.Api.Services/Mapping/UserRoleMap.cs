﻿using Account.Api.Entitys;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Account.Api.Services.Mapping
{
    /// <summary>
    /// 用户角色
    /// </summary>
    public partial class  UserRoleMap:  BaseEntityTypeConfiguration<UserRole>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable(nameof(UserRole));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}