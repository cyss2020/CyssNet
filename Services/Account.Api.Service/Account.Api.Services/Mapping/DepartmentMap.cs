﻿using Account.Api.Entitys;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Account.Api.Services.Mapping
{
    /// <summary>
    /// 部门
    /// </summary>
    public partial class  DepartmentMap:  BaseEntityTypeConfiguration<Department>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.ToTable(nameof(Department));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}