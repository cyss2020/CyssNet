﻿using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Account.Api.Services.Mapping
{


    public class UserObjectContext : BaseObjectContext, IUserObjectContext
    {

        public UserObjectContext(DbContextOptions options) : base(options, typeof(UserObjectContext))
        {

        }

    }

    public interface IUserObjectContext : IDbContext
    {

    }

}
