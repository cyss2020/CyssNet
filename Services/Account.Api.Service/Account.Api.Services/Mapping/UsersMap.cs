﻿using Account.Api.Entitys;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Account.Api.Services.Mapping
{
    /// <summary>
    /// 会员
    /// </summary>
    public partial class  UserMap:  BaseEntityTypeConfiguration<User>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}