﻿using Account.Api.Entitys;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Account.Api.Services.Mapping
{
    /// <summary>
    /// 权限
    /// </summary>
    public partial class  PermissionMap:  BaseEntityTypeConfiguration<Permission>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.ToTable(nameof(Permission));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}