﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 角色分页查询参数对象
    /// </summary>
    public class RoleSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
