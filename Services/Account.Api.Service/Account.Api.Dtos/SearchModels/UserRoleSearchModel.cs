﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 用户角色分页查询参数对象
    /// </summary>
    public class UserRoleSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
