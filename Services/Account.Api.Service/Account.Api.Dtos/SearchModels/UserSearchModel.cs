﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 会员分页查询参数对象
    /// </summary>
    public class UserSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
