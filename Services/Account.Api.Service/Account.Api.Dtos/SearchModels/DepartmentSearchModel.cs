﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 部门分页查询参数对象
    /// </summary>
    public class DepartmentSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
