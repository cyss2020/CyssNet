﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 权限分页查询参数对象
    /// </summary>
    public class PermissionSearchModel : BaseSearchModel
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { set; get; }
    }
}
