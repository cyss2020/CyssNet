﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 角色权限分页查询参数对象
    /// </summary>
    public class RolePermissionSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
