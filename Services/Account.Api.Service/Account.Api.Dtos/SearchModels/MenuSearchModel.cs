﻿using Cyss.Core;
namespace Account.Api.Dtos
{
    /// <summary>
    /// 菜单分页查询参数对象
    /// </summary>
    public class MenuSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
