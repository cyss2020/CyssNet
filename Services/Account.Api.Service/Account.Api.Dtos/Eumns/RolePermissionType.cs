﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 权限类型
    /// </summary>
    public enum RolePermissionType
    {
        /// <summary>
        /// 菜单
        /// </summary>
        Menu = 1,

        /// <summary>
        /// 按钮
        /// </summary>
        Button = 2,
    }
}
