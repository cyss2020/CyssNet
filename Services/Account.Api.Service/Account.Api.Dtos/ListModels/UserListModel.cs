﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 会员分页查询返回对象
    /// </summary>
    public class UserListModel: BasePagedListModel<UserModel>
    {
    }
}
