﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 用户角色分页查询返回对象
    /// </summary>
    public class UserRoleListModel: BasePagedListModel<UserRoleModel>
    {
    }
}
