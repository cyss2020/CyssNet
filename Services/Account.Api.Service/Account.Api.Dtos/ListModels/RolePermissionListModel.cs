﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 角色权限分页查询返回对象
    /// </summary>
    public class RolePermissionListModel: BasePagedListModel<RolePermissionModel>
    {
    }
}
