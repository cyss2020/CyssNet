﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 权限分页查询返回对象
    /// </summary>
    public class PermissionListModel: BasePagedListModel<PermissionModel>
    {
    }
}
