﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 部门分页查询返回对象
    /// </summary>
    public class DepartmentListModel: BasePagedListModel<DepartmentModel>
    {
    }
}
