﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 角色分页查询返回对象
    /// </summary>
    public class RoleListModel: BasePagedListModel<RoleModel>
    {
    }
}
