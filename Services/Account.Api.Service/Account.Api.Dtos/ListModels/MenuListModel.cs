﻿using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 菜单分页查询返回对象
    /// </summary>
    public class MenuListModel: BasePagedListModel<MenuModel>
    {
    }
}
