﻿using System;
using Cyss.Core;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 角色
    /// </summary>
    public partial class RoleModel : BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///名称
        ///</summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Display(Name = "备注")]
        public string Remarks { get; set; }

        ///<summary>
        ///是否启用
        ///</summary>
        [Display(Name = "是否启用")]
        public bool IsEnabled { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        [Display(Name = "排序")]
        public int DisplayOrder { get; set; }

        ///<summary>
        ///图标
        ///</summary>
        [Display(Name = "图标")]
        public string Icon { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateBy { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        [Display(Name = "更新日期")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        [Display(Name = "更新人")]
        public int UpdateBy { get; set; }


        #endregion

        #region 扩展属性

        /// <summary>
        /// 角色权限Id集合
        /// </summary>
        public IEnumerable<int> RolePermissionIds { set; get; }


        #endregion


    }
}