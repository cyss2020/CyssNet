﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 角色权限
    /// </summary>
    public partial class RolePermissionModel : BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///角色Id
        ///</summary>
        [Display(Name = "角色Id")]
        public int RoleId { get; set; }

        ///<summary>
        ///权限Id
        ///</summary>
        [Display(Name = "权限Id")]
        public int PermissionId { get; set; }

        ///<summary>
        ///菜单
        ///</summary>
        [Display(Name = "菜单")]
        public int MenuId { get; set; }

        ///<summary>
        ///权限类型
        ///</summary>
        [Display(Name = "权限类型")]
        public int Type { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateBy { get; set; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)角色Id(RoleId)
        ///</summary>
        [Display(Name = "角色Id")]
        public string RoleName { get; set; }

        ///<summary>
        ///(扩展属性)权限Id(PermissionId)
        ///</summary>
        [Display(Name = "权限Id")]
        public string PermissionName { get; set; }

        ///<summary>
        ///(扩展属性)菜单(MenuId)
        ///</summary>
        [Display(Name = "菜单")]
        public string MenuName { get; set; }

        /// <summary>
        /// 权限类型
        /// </summary>
        public RolePermissionType PermissionType
        {
            set { Type = (int)value; }
            get { return (RolePermissionType)Type; }
        }

        #endregion


    }
}