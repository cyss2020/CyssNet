﻿using FluentValidation;
using Cyss.Core.Validators;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Account.Api.Dtos
{
    public class LoginModelValidator : BaseValidator<LoginModel>
    {
        public LoginModelValidator()
        {
            RuleFor(x => x.UserNo)
               .NotEmpty()
               .WithMessage("user no not null");

            RuleFor(x => x.UserPwd)
              .NotEmpty()
              .WithMessage("user pwd not null");
        }
    }
}
