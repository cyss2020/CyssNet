﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 部门
    /// </summary>
    public partial class  DepartmentModel: BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///父部门Id
        ///</summary>
        [Display(Name = "父部门Id")]
        public int ParentDeptId { get; set; }

        ///<summary>
        ///部门名称
        ///</summary>
        [Display(Name = "部门名称")]
        public string Name { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        [Display(Name = "排序")]
        public int DisplayOrder { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Display(Name = "备注")]
        public string Remarks { get; set; }

        ///<summary>
        ///图标
        ///</summary>
        [Display(Name = "图标")]
        public string Icon { get; set; }

        ///<summary>
        ///是否启用
        ///</summary>
        [Display(Name = "是否启用")]
        public bool IsEnabled { get; set; }

        ///<summary>
        ///是否删除
        ///</summary>
        [Display(Name = "是否删除")]
        public bool IsDeleted { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateBy { get; set; }

        ///<summary>
        ///更新时间
        ///</summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        [Display(Name = "更新人")]
        public int UpdateBy { get; set; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)父部门Id(ParentDeptId)
        ///</summary>
        public string ParentDeptName { get; set; }
        #endregion



    }
}