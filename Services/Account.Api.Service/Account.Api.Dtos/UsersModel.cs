﻿using Cyss.Core;
using Cyss.Core.Api.JWT;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 会员
    /// </summary>
    public partial class UserModel : BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///账户
        ///</summary>
        [Display(Name = "账户")]
        [JWParameter]
        public string Number { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [Display(Name = "名称")]
        [JWParameter]
        public string Name { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Display(Name = "备注")]
        public string Remarks { get; set; }

        ///<summary>
        ///生日
        ///</summary>
        [Display(Name = "生日")]
        public DateTime Birthday { get; set; }

        ///<summary>
        ///身份证号
        ///</summary>
        [Display(Name = "身份证号")]
        public string CardID { get; set; }

        ///<summary>
        ///性别
        ///</summary>
        [Display(Name = "性别")]
        public int Sex { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        [Display(Name = "密码")]
        public string Pwd { get; set; }

        ///<summary>
        ///等级
        ///</summary>
        [Display(Name = "等级")]
        public int Grade { get; set; }

        ///<summary>
        ///账号
        ///</summary>
        [Display(Name = "账号")]
        public string No { get; set; }

        ///<summary>
        ///昵称
        ///</summary>
        [Display(Name = "昵称")]
        public string Nickname { get; set; }

        ///<summary>
        ///手机号
        ///</summary>
        [Display(Name = "手机号")]
        public string Phone { get; set; }

        ///<summary>
        ///是否启用
        ///</summary>
        [Display(Name = "是否启用")]
        public bool IsEnabled { get; set; }

        ///<summary>
        ///是否删除
        ///</summary>
        [Display(Name = "是否删除")]
        public bool IsDeleted { get; set; }

        ///<summary>
        ///紧急联系人
        ///</summary>
        [Display(Name = "紧急联系人")]
        public string EmergencyContact { get; set; }

        ///<summary>
        ///紧急联系人手机号
        ///</summary>
        [Display(Name = "紧急联系人手机号")]
        public string EmergencyContact_Phone { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "")]
        public string PortraitRoute { get; set; }


        /// <summary>
        /// 部门Id
        /// </summary>
        [Display(Name = "部门")]
        public int DeptId { set; get; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "头像")]
        public int PortraitRoutePictureId { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "")]
        public int CreateBy { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "")]
        public int UpdateBy { get; set; }

        /// <summary>
        /// 文件
        /// </summary>
        [Display(Name = "文件")]
        public int FileId { set; get; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)(PortraitRoutePictureId)
        ///</summary>
        [Display(Name = "")]
        public string PortraitRoutePictureName { get; set; }


        /// <summary>
        /// 用户角色Id集合
        /// </summary>
        public IEnumerable<int> UserRoleIds { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string UserRoleNames { set; get; }

        /// <summary>
        /// 部门
        /// </summary>
        [Display(Name = "部门")]
        public string DeptName { set; get; }
        #endregion



    }
}