﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 用户角色
    /// </summary>
    public partial class  UserRoleModel: BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///用户Id
        ///</summary>
        [Display(Name = "用户Id")]
        public int UserId { get; set; }

        ///<summary>
        ///角色Id
        ///</summary>
        [Display(Name = "角色Id")]
        public int RoleId { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateBy { get; set; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)用户Id(UserId)
        ///</summary>
        [Display(Name = "用户Id")]
        public string UserName { get; set; }

        ///<summary>
        ///(扩展属性)角色Id(RoleId)
        ///</summary>
        [Display(Name = "角色Id")]
        public string RoleName { get; set; }

        #endregion
    }
}