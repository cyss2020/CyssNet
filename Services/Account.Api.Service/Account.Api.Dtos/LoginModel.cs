﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 用户登陆model
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserNo { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public string UserPwd { set; get; }
    }

    /// <summary>
    /// 登陆成功返回对象
    /// </summary>
    public class LoginReturnModel : BaseModel
    {

        /// <summary>
        /// 登陆返回对象
        /// </summary>
        public LoginReturnModel()
        {
            UserRoles = new List<int>();
        }
        ///<summary>
        ///
        ///</summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string No { set; get; }

        ///<summary>
        ///
        ///</summary>
        public string Name { get; set; }




        /// <summary>
        /// 用户角色
        /// </summary>
        public IList<int> UserRoles { set; get; }

        /// <summary>
        /// 用户权限
        /// </summary>
        public IEnumerable<MenuModel> Menus { set; get; }

        /// <summary>
        /// 权限
        /// </summary>
        public IEnumerable<PermissionModel> Permissions { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public JWTTnToken Token { set; get; }

    }
}
