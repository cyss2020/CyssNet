﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 权限
    /// </summary>
    public partial class PermissionModel : BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///菜单ID
        ///</summary>
        [Display(Name = "菜单ID")]
        public int MenuId { get; set; }

        ///<summary>
        ///权限码
        ///</summary>
        [Display(Name = "权限码")]
        public string PermissionCode { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [Display(Name = "名称")]
        public string Name { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        [Display(Name = "排序")]
        public int DisplayOrder { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Display(Name = "备注")]
        public string Remarks { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateBy { get; set; }

        ///<summary>
        ///更新日期
        ///</summary>
        [Display(Name = "更新日期")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        [Display(Name = "更新人")]
        public int UpdateBy { get; set; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)菜单ID(MenuId)
        ///</summary>
        [Display(Name = "菜单")]
        public string MenuName { get; set; }
        #endregion



    }
}