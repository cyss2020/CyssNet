﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 部门树
    /// </summary>
    public class DeptTreeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public DeptTreeModel()
        {
            Childrens = new List<DeptTreeModel>();
        }
        /// <summary>
        /// 部门
        /// </summary>
        public DepartmentModel Dept { set; get; }

        /// <summary>
        /// 是否有子部门
        /// </summary>
        public bool HasChildren
        {
            get
            {
                if (Childrens == null || Childrens.Count() <= 0)
                    return false;
                return true;
            }
        }

        /// <summary>
        /// 子部门
        /// </summary>
        public IList<DeptTreeModel> Childrens { set; get; }
    }
}
