﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 权限树
    /// </summary>
    public class PermissionTreeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public PermissionTreeModel()
        {
            Childrens = new List<PermissionTreeModel>();
        }

        /// <summary>
        /// 是否是菜单
        /// </summary>
        public bool IsMenu { set; get; }

        /// <summary>
        /// 菜单
        /// </summary>
        public MenuModel Menu { set; get; }

        /// <summary>
        /// 权限
        /// </summary>
        public PermissionModel Permission { set; get; }

        /// <summary>
        /// 子项
        /// </summary>
        public IList<PermissionTreeModel> Childrens { set; get; }
    }
}
