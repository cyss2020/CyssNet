﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 修改密码模型
    /// </summary>
    public partial class EditPasswordModel : BaseModel
    {

        public int Id { set; get; }

        [Display(Name = "账号")]
        public string UserNo { set; get; }

        [Display(Name = "旧密码")]
        public string OldPassword { set; get; }

        [Display(Name = "新密码")]
        public string NewPassword { set; get; }

        [Display(Name = "确认密码")]
        public string ConfirmNewPassword { set; get; }
    }
}
