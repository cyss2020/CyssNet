﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 菜单树
    /// </summary>
    public class MenuTreeModel:BaseModel
    {
        /// <summary>
        /// 
        /// </summary>
        public MenuTreeModel()
        {
            this.Childrens = new List<MenuTreeModel>();
        }
        /// <summary>
        /// 菜单
        /// </summary>
        public MenuModel Menu { set; get; }


        /// <summary>
        /// 是否有子菜单
        /// </summary>
        public bool HasChildren
        {
            get
            {
                if (Childrens == null || Childrens.Count() <= 0)
                    return false;
                return true;
            }
        }

        /// <summary>
        /// 子项
        /// </summary>
        public List<MenuTreeModel> Childrens { set; get; }
    }
}
