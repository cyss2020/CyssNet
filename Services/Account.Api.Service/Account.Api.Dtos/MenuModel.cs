﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Account.Api.Dtos
{
    /// <summary>
    /// 菜单
    /// </summary>
    public partial class MenuModel : BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///父菜单Id
        ///</summary>
        [Display(Name = "父菜单Id")]
        public int ParentMenuId { get; set; }

        ///<summary>
        ///路由
        ///</summary>
        [Display(Name = "路由")]
        public string Rotue { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [Display(Name = "名称")]
        public string Name { get; set; }


        /// <summary>
        /// 编码
        /// </summary>
        [Display(Name = "编码")]
        public string Code { set; get; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "排序")]
        public int DisplayOrder { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Display(Name = "备注")]
        public string Remarks { get; set; }

        ///<summary>
        ///图标
        ///</summary>
        [Display(Name = "图标")]
        public string Icon { get; set; }

        ///<summary>
        ///是否启用
        ///</summary>
        [Display(Name = "是否启用")]
        public bool IsEnabled { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public int CreateBy { get; set; }

        ///<summary>
        ///更新时间
        ///</summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        [Display(Name = "更新人")]
        public int UpdateBy { get; set; }


        #endregion

        #region 扩展属性


        ///<summary>
        ///(扩展属性)父菜单Id(ParentMenuId)
        ///</summary>
        [Display(Name = "父菜单")]
        public string ParentMenuName { get; set; }

        /// <summary>
        /// 是否添加默认权限(新增,删除,修改)
        /// </summary>
        [Display(Name = "是否添加默认权限(新增,删除,修改)")]
        public bool IsDefaultParmisson { set; get; }


        #endregion


    }
}