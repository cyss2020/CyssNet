﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 上传图片返回值
    /// </summary>
    public class PictureResult
    {
        public int PictureId { set; get; }

        public string ImageUrl { set; get; }
    }
}
