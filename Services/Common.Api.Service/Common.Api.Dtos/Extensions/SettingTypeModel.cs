﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{

    public class SettingTypeModel
    {
        /// <summary>
        /// 类型名称
        /// </summary>
        [Display(Name = "配置类型")]
        public string TypeName { set; get; }

        /// <summary>
        /// 类型说明
        [Display(Name = "配置名称")]
        public string TypeTitle { set; get; }
    }
}
