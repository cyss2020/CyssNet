﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 更新配置模型
    /// </summary>
    public class EditSettingModel
    {
        /// <summary>
        /// 设置名
        /// </summary>
        public string TableName { set; get; }


        public string TypeName
        {
            get
            {
                return this.TableName.Replace("Model", "");
            }
        }

        /// <summary>
        /// 设置对象
        /// </summary>
        public object Setting { set; get; }
    }
}
