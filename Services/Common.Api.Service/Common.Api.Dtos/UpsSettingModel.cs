﻿using Cyss.Core;
using Cyss.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{
    public class UpsSettingModel : BaseSettingModel
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Display(Name = "账号")]
        public string Shiper { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>
        [Display(Name = "登录名")]
        public int UserName { set; get; }

        /// <summary>
        ///登录密码
        /// </summary>
        [Display(Name = "登录密码")]
        public string UserPwd { set; get; }
    }
}
