﻿using System;
using Cyss.Core;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务配置表
    /// </summary>
    public partial class TaskConfigModel : BaseEntityModel<int>
    {
        #region model

        ///<summary>
        ///任务Id
        ///</summary>
	    public int TaskId { get; set; }

        ///<summary>
        ///(扩展属性)任务Id
        ///</summary>
        public string TaskName { get; set; }
        ///<summary>
        ///任务类型
        ///</summary>
        public int TaskType { get; set; }

        ///<summary>
        ///执行间隔
        ///</summary>
        public int Interval { get; set; }

        ///<summary>
        ///开始时间
        ///</summary>
        [JsonConverter(typeof(JsonTimeSpanConverter))]
        public TimeSpan StartDatetime { get; set; }

        ///<summary>
        ///结束时间
        ///</summary>
        [JsonConverter(typeof(JsonTimeSpanConverter))]
        public TimeSpan EndDatetime { get; set; }

        ///<summary>
        ///最多执行次数
        ///</summary>
        public int MaxExecutionTimes { get; set; }

        ///<summary>
        ///失败不计算次数
        ///</summary>
        public bool FailNotCounted { get; set; }

        /// <summary>
        /// 周(多个用逗号隔开)
        /// </summary>
        public string Weeks { set; get; }

        /// <summary>
        /// 每月第几天执行
        /// </summary>
        public string Days { set; get; }

        #endregion

        public IEnumerable<DayModel> DayOfMonths
        {
            set
            {
                if (value == null)
                {
                    Days = string.Empty;
                }
                Days = string.Join(",", value.Select(x => x.Day));
            }
            get
            {
                if (string.IsNullOrWhiteSpace(Days))
                {
                    return new List<DayModel>();
                }
                return Days.Split(",", StringSplitOptions.RemoveEmptyEntries).Select(x => new DayModel { Day = int.Parse(x) });
            }
        }

        public TaskExecuteType TaskExecuteType
        {
            set { TaskType = (int)value; }
            get { return (TaskExecuteType)TaskType; }
        }

        public IEnumerable<DayOfWeek> DayOfWeeks
        {
            set
            {

                if (value == null)
                {
                    Weeks = string.Empty;
                }
                Weeks = string.Join(",", value.Select(x => (int)x));
            }
            get
            {
                if (string.IsNullOrWhiteSpace(Weeks))
                {
                    return new List<DayOfWeek>();
                }
                return Weeks.Split(",", StringSplitOptions.RemoveEmptyEntries).Select(x => (DayOfWeek)int.Parse(x));
            }
        }

    }
}