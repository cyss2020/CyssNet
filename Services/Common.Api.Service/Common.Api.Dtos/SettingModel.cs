﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 基础配置
    /// </summary>
    public partial class SettingModel : BaseDefaultEntityModel
    {
        #region model
        ///<summary>
        ///配置名
        ///</summary>
        [Display(Name = "配置名")]
        public string TypeName { get; set; }

        ///<summary>
        ///外键表名
        ///</summary>
        [Display(Name = "外键表名")]
        public string ForeignKey { get; set; }

        ///<summary>
        ///外键值Id
        ///</summary>
        [Display(Name = "外键值Id")]
        public int ForeignKeyId { get; set; }

        ///<summary>
        ///属性名
        ///</summary>
        [Display(Name = "属性名")]
        public string PropertyName { get; set; }

        ///<summary>
        ///属性值
        ///</summary>
        [Display(Name = "属性值")]
        public string PropertyValue { get; set; }

        ///<summary>
        ///备注
        ///</summary>
        [Display(Name = "备注")]
        public string Remarks { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///创建人
        ///</summary>
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }

        ///<summary>
        ///更新时间
        ///</summary>
        [Display(Name = "更新时间")]
        public DateTime UpdateDate { get; set; }

        ///<summary>
        ///更新人
        ///</summary>
        [Display(Name = "更新人")]
        public string UpdateBy { get; set; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)外键值Id(ForeignKeyId)
        ///</summary>
        [Display(Name = "外键值Id")]
        public string ForeignKeyName { get; set; }

        /// <summary>
        /// 属性类型
        /// </summary>
        public string PropertyType { set; get; }

        /// <summary>
        /// 属性说明
        /// </summary>
        public string PropertyTitle { set; get; }

        /// <summary>
        /// 排序
        /// </summary>
        public int PropertySeq { set; get; }
        #endregion
    }
}
