﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// API服务分页查询返回对象
    /// </summary>
    public class ApiServiceListModel: BasePagedListModel<ApiServiceModel>
    {
    }
}
