﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务配置表分页查询返回对象
    /// </summary>
    public class TaskConfigListModel: BasePagedListModel<TaskConfigModel>
    {
    }
}
