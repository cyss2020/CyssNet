﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务日志分页查询返回对象
    /// </summary>
    public class TaskRecordListModel: BasePagedListModel<TaskRecordModel>
    {
    }
}
