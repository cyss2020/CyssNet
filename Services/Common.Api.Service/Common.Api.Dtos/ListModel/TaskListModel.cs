﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务分页查询返回对象
    /// </summary>
    public class TaskListModel: BasePagedListModel<JobModel>
    {
    }
}
