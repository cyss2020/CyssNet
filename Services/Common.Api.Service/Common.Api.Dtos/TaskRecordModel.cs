﻿using System;
using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务日志
    /// </summary>
    public partial class TaskRecordModel : BaseEntityModel<long>
    {
        #region model

        ///<summary>
        ///任务Id
        ///</summary>
        public int TaskId { get; set; }

        ///<summary>
        ///(扩展属性)任务Id
        ///</summary>
        public string TaskName { get; set; }
        ///<summary>
        ///开始时间
        ///</summary>
        public DateTime StartTime { get; set; }

        ///<summary>
        ///结束时间
        ///</summary>
        public DateTime EndTime { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool Status { get; set; }

        ///<summary>
        ///内容
        ///</summary>
        public string Content { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateDate { get; set; }


        #endregion
    }
}