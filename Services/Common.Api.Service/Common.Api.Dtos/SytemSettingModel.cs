﻿using Cyss.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.Api.Dtos
{
    public class SystemSettingModel : BaseSettingModel
    {
        /// <summary>
        /// 网站名称
        /// </summary>
        [Display(Name = "网站名称")]
        public string WebName { set; get; }

        /// <summary>
        /// 日志保留天数
        /// </summary>
        [Display(Name = "日志保留天数")]
        public int LogDay { set; get; }

        /// <summary>
        ///Date
        /// </summary>
        [Display(Name = "Date")]
        public DateTime Date { set; get; }

        [Display(Name = "用户默认头像")]
        public int UserDefaultPictureId { set; get; }

        [Display(Name = "暂无图片")]
        public int NullDefaultPictureId { set; get; }
    }
}
