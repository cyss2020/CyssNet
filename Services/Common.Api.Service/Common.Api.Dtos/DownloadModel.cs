﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 文件表
    /// </summary>
    public partial class DownloadModel : BaseEntity<int>
    {
        #region model

        [Display(Name = "Content Type")]
        public string ContentType { set; get; }


        ///<summary>
        ///文件名
        ///</summary>
        [Display(Name = "文件名")]
        public string FileName { get; set; }

        ///<summary>
        ///扩展名
        ///</summary>
        [Display(Name = "扩展名")]
        public string Extension { get; set; }

        ///<summary>
        ///类型
        ///</summary>
        [Display(Name = "类型")]
        public string MimeType { get; set; }

        ///<summary>
        ///Seo
        ///</summary>
        [Display(Name = "Seo")]
        public string SeoFilename { get; set; }

        ///<summary>
        ///alt属性
        ///</summary>
        [Display(Name = "alt属性")]
        public string AltAttribute { get; set; }

        ///<summary>
        ///Title属性
        ///</summary>
        [Display(Name = "Title属性")]
        public string TitleAttribute { get; set; }

        ///<summary>
        ///大小
        ///</summary>
        [Display(Name = "大小")]
        public long Size { get; set; }

        ///<summary>
        ///是否新增
        ///</summary>
        [Display(Name = "是否新增")]
        public bool IsNew { get; set; }

        ///<summary>
        ///路径
        ///</summary>
        [Display(Name = "路径")]
        public string VirtualPath { get; set; }

        ///<summary>
        ///
        ///</summary>
        [Display(Name = "")]
        public DateTime CreateDateTime { get; set; }


        #endregion

        #region 扩展属性
        #endregion
    }
}
