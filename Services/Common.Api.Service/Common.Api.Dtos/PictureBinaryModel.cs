﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 图片字节表
    /// </summary>
    public partial class  PictureBinaryModel: BaseEntityModel<int>
    {
        #region model
        ///<summary>
        ///图片Id
        ///</summary>
        [Display(Name = "图片Id")]
	    public int PictureId{ get; set; }
       
        ///<summary>
        ///字节数据
        ///</summary>
        [Display(Name = "字节数据")]
	    public byte[] BinaryData{ get; set; }
       
		
        #endregion

        #region 扩展属性
         ///<summary>
        ///(扩展属性)图片Id(PictureId)
        ///</summary>
        [Display(Name = "图片Id")]
        public string PictureName{ get; set; }
        
        #endregion
    }
}
