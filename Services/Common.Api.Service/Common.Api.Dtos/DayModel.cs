﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{
    public class DayModel
    {

        public int Day { set; get; }

        public override string ToString()
        {
            return Day == 0 ? "最后一天" : $"{ Day}号";
        }

    }
}
