﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务类型
    /// </summary>
    public enum Tasktype
    {
        /// <summary>
        /// 本地任务
        /// </summary>
        Load = 1,

        /// <summary>
        /// api任务
        /// </summary>
        Url = 2,
    }
}
