﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Dtos
{
    public enum TaskExecuteType
    {
        /// <summary>
        /// 默认循环执行
        /// </summary>
        loop = 1,
        /// <summary>
        /// 
        /// </summary>
        Daily = 2,

        /// <summary>
        /// 
        /// </summary>
        Weekly = 3,

        /// <summary>
        /// 
        /// </summary>
        Month = 4
    }
}
