﻿using Cyss.Core;
namespace Common.Api.Dtos
{
    /// <summary>
    /// API服务分页查询参数对象
    /// </summary>
    public class ApiServiceSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
