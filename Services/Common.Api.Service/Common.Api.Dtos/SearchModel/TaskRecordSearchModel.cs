﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务日志分页查询参数对象
    /// </summary>
    public class TaskRecordSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
