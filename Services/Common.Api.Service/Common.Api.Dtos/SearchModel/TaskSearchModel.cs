﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务分页查询参数对象
    /// </summary>
    public class TaskSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
