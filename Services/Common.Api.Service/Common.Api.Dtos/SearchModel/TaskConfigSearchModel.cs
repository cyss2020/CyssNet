﻿using Cyss.Core;

namespace Common.Api.Dtos
{
    /// <summary>
    /// 任务配置表分页查询参数对象
    /// </summary>
    public class TaskConfigSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
