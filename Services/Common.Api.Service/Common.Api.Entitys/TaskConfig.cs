﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 任务配置表数据模型
    /// </summary>
    public partial class TaskConfig : BaseEntity<int>
    {
        #region model

        ///<summary>
        ///任务Id
        ///</summary>
	    public int TaskId { get; set; }

        ///<summary>
        ///任务类型
        ///</summary>
        public int TaskType { get; set; }

        ///<summary>
        ///执行间隔
        ///</summary>
        public int Interval { get; set; }

        /// <summary>
        /// 周(多个用逗号隔开)
        /// </summary>
        public string Weeks { set; get; }

        /// <summary>
        /// 每月第几天执行
        /// </summary>
        public string Days { set; get; }

        ///<summary>
        ///开始时间
        ///</summary>
	    public TimeSpan StartDatetime { get; set; }

        ///<summary>
        ///结束时间
        ///</summary>
        public TimeSpan EndDatetime { get; set; }

        ///<summary>
        ///最多执行次数
        ///</summary>
        public int MaxExecutionTimes { get; set; }

        ///<summary>
        ///失败不计算次数
        ///</summary>
        public bool FailNotCounted { get; set; }

        #endregion



    }
}