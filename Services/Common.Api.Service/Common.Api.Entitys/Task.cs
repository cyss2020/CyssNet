﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 任务数据模型
    /// </summary>
    public partial class Job : BaseEntity<int>
    {
        #region model

        ///<summary>
        ///任务名称
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///
        ///</summary>
        public int ApiServiceId { get; set; }

        ///<summary>
        ///
        ///</summary>
        public string Url { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool Status { get; set; }

        ///<summary>
        ///正在运行
        ///</summary>
        public bool IsRunning { get; set; }

        /// <summary>
        /// 执行次数
        /// </summary>
        public long Count { set; get; }

        ///<summary>
        ///最后执行开始时间
        ///</summary>
	    public DateTime LastStartTime { get; set; }

        ///<summary>
        ///最后执行结束时间
        ///</summary>
        public DateTime LaseEndTime { get; set; }

        #endregion



    }
}