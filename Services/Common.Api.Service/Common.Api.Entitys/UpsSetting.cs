﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Entitys
{
    public class UpsSetting : BaseSettingEntity
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string Shiper { set; get; }

        /// <summary>
        /// 登录名
        /// </summary>
        public int UserName { set; get; }

        /// <summary>
        ///登录密码
        /// </summary>
        public string UserPwd { set; get; }
    }
}
