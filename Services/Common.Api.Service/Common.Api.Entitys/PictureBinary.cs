﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 图片字节表数据模型
    /// </summary>
    public partial class PictureBinary : BaseEntity<int>
    {
        #region model
        ///<summary>
        ///图片Id
        ///</summary>
	    public int PictureId { get; set; }

        ///<summary>
        ///字节数据
        ///</summary>
        public byte[] BinaryData { get; set; }

        #endregion



    }
}
