﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 系统配置
    /// </summary>
    public class SystemSetting : BaseSettingEntity
    {

        /// <summary>
        /// 网站名称
        /// </summary>
        public string WebName { set; get; }

        /// <summary>
        /// 日志保留天数
        /// </summary>
        public int LogDay { set; get; }

        /// <summary>
        ///Date
        /// </summary>
        public DateTime Date { set; get; }

        /// <summary>
        /// 用户默认头像
        /// </summary>
        public int UserDefaultPictureId { set; get; }

        /// <summary>
        /// 暂无图片
        /// </summary>
        public int NullDefaultPictureId { set; get; }

    }
}
