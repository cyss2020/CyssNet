﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 文件表数据模型
    /// </summary>
    public partial class Download : BaseEntity<int>
    {
        #region model

        public string ContentType { set; get; }

        ///<summary>
        ///文件名
        ///</summary>
	    public string FileName { get; set; }

        ///<summary>
        ///扩展名
        ///</summary>
        public string Extension { get; set; }

        ///<summary>
        ///类型
        ///</summary>
        public string MimeType { get; set; }

        ///<summary>
        ///Seo
        ///</summary>
        public string SeoFilename { get; set; }

        ///<summary>
        ///alt属性
        ///</summary>
        public string AltAttribute { get; set; }

        ///<summary>
        ///Title属性
        ///</summary>
        public string TitleAttribute { get; set; }

        ///<summary>
        ///大小
        ///</summary>
        public long Size { get; set; }

        ///<summary>
        ///是否新增
        ///</summary>
        public bool IsNew { get; set; }

        ///<summary>
        ///路径
        ///</summary>
        public string VirtualPath { get; set; }

        ///<summary>
        ///
        ///</summary>
        public DateTime CreateDateTime { get; set; }

        #endregion



    }
}
