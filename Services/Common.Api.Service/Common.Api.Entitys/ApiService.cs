﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// API服务数据模型
    /// </summary>
    public partial class  ApiService: BaseEntity<int>
    {
        #region model

        ///<summary>
        ///服务名称
        ///</summary>
        public string Name{ get; set; }
        
        ///<summary>
        ///服务地址
        ///</summary>
	    public string BaseAddress{ get; set; }
        
        #endregion



    }
}