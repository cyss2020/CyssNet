﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 基础配置数据模型
    /// </summary>
    public partial class  Setting: BaseDefaultEntity
    {
        #region model
        ///<summary>
        ///配置名
        ///</summary>
	    public string TypeName{ get; set; }
        
        ///<summary>
        ///外键表名
        ///</summary>
	    public string ForeignKey{ get; set; }
        
        ///<summary>
        ///外键值Id
        ///</summary>
	    public int ForeignKeyId{ get; set; }
        
        ///<summary>
        ///属性名
        ///</summary>
	    public string PropertyName{ get; set; }
        
        ///<summary>
        ///属性值
        ///</summary>
	    public string PropertyValue{ get; set; }
        
        ///<summary>
        ///备注
        ///</summary>
	    public string Remarks{ get; set; }
        

        #endregion



    }
}
