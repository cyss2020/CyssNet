﻿using System;
using Cyss.Core;

namespace Common.Api.Entitys
{
    /// <summary>
    /// 图片表数据模型
    /// </summary>
    public partial class Picture : BaseEntity<int>
    {
        #region model
        ///<summary>
        ///类型
        ///</summary>
	    public string MimeType { get; set; }

        ///<summary>
        ///Seo
        ///</summary>
        public string SeoFilename { get; set; }

        ///<summary>
        ///alt属性
        ///</summary>
        public string AltAttribute { get; set; }

        ///<summary>
        ///Title属性
        ///</summary>
        public string TitleAttribute { get; set; }

        ///<summary>
        ///大小
        ///</summary>
        public long Size { get; set; }

        ///<summary>
        ///是否新增
        ///</summary>
        public bool IsNew { get; set; }

        ///<summary>
        ///路径
        ///</summary>
        public string VirtualPath { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDateTime { set; get; }
        #endregion



    }
}
