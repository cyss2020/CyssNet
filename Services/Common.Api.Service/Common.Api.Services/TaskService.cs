﻿using Common.Api.Dtos;
using Common.Api.Entitys;
using Cyss.Core;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Common.Api.Services
{
    /// <summary>
    /// 任务服务
    /// </summary>
    public class TaskService : BaseService<Job>
    {
        #region 字段
        /// <summary>
        /// 任务仓储
        /// </summary>
        private readonly IRepository<Job> _taskRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="taskRepository">任务 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public TaskService(IRepository<Job> taskRepository) : base(taskRepository)
        {
            _taskRepository = taskRepository;
        }

        #endregion

        #region 任务方法

        /// <summary>
        ///  根据主键获取任务
        /// </summary>
        /// <param name="Id">主键</param>
        /// <returns>任务</returns>
        public virtual Job GetTaskById(int Id)
        {
            if (Id == 0)
                return null;

            return _taskRepository.GetById(Id);
        }


        /// <summary>
        ///     新增任务
        /// </summary>
        /// <param name="model">任务</param>
        public virtual void InsertTask(Job model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskRepository.Insert(model);

        }



        /// <summary>
        ///     更新任务
        /// </summary>
        /// <param name="model">任务</param>
        public virtual void UpdateTask(Job model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskRepository.Update(model);

        }


        /// <summary>
        ///     删除 任务
        /// </summary>
        /// <param name="model">任务</param>
        public virtual void DeleteTask(Job model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskRepository.Delete(model);

        }

        /// <summary>
        ///     删除 任务
        /// </summary>
        /// <param name="model">收藏表</param>
        public virtual void DeleteTasks(List<int> ids)
        {
            if (ids == null || ids.Count <= 0)
            {
                return;
            }
            _taskRepository.Delete(ids);

        }

        /// <summary>
        ///  获取所有的  任务
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns>任务集合</returns>
        public virtual IList<Job> GetTasks(Expression<Func<Job, bool>> predicate, bool IsNoTracking = true)
        {
            var query = _taskRepository.Table;
            if (IsNoTracking)
            {
                query = _taskRepository.TableNoTracking;
            }
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToList();

        }

        /// <summary>
        /// 分页获取 任务
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<Job> GetPageListTasks(TaskSearchModel searchModel)
        {
            var query = _taskRepository.TableNoTracking;
            var entitys = new PagedList<Job>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 任务
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<JobModel> GetPageListJobModels(TaskSearchModel searchModel)
        {
            var query = from a in _taskRepository.TableNoTracking
                        select new JobModel
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Url = a.Url,
                            Status = a.Status,
                            IsRunning = a.IsRunning,
                            LastStartTime = a.LastStartTime,
                            LaseEndTime = a.LaseEndTime,
                        };

            var entitys = new PagedList<JobModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

