﻿using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Common.Api.Entitys;
using Cyss.Core.Repository.EF;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 任务日志
    /// </summary>
    public partial class  TaskRecordMap:  BaseEntityTypeConfiguration<TaskRecord>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<TaskRecord> builder)
        {
            builder.ToTable(nameof(TaskRecord));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}