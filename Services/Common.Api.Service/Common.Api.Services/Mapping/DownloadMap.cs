﻿using Common.Api.Entitys;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 文件表
    /// </summary>
    public partial class  DownloadMap:  BaseEntityTypeConfiguration<Download>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<Download> builder)
        {
            builder.ToTable(nameof(Download));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}
