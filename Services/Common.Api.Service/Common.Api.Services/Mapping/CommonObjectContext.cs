﻿using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Account.Api.Services.Mapping
{


    public class CommonObjectContext : BaseObjectContext, ICommonObjectContext
    {

        public CommonObjectContext(DbContextOptions options) : base(options, typeof(CommonObjectContext))
        {

        }

    }

    public interface ICommonObjectContext : IDbContext
    {

    }

}
