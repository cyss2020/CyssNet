﻿using Common.Api.Entitys;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 图片表
    /// </summary>
    public partial class  PictureMap:  BaseEntityTypeConfiguration<Picture>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<Picture> builder)
        {
            builder.ToTable(nameof(Picture));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}
