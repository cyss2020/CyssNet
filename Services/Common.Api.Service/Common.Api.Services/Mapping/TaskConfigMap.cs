﻿using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Common.Api.Entitys;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 任务配置表
    /// </summary>
    public partial class  TaskConfigMap:  BaseEntityTypeConfiguration<TaskConfig>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<TaskConfig> builder)
        {
            builder.ToTable(nameof(TaskConfig));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}