﻿using Common.Api.Entitys;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 基础配置
    /// </summary>
    public partial class  SettingMap:  BaseEntityTypeConfiguration<Setting>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<Setting> builder)
        {
            builder.ToTable(nameof(Setting));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}
