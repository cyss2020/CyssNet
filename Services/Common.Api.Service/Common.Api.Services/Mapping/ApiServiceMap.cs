﻿using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Common.Api.Entitys;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// API服务
    /// </summary>
    public partial class  ApiServiceMap:  BaseEntityTypeConfiguration<ApiService>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<ApiService> builder)
        {
            builder.ToTable(nameof(ApiService));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}