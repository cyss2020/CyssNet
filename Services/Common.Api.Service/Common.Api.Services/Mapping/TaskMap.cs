﻿using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Common.Api.Entitys;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 任务
    /// </summary>
    public partial class  TaskMap:  BaseEntityTypeConfiguration<Job>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.ToTable(nameof(Job));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}