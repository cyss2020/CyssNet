﻿using Common.Api.Entitys;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Common.Api.Services.Mapping
{
    /// <summary>
    /// 图片字节表
    /// </summary>
    public partial class  PictureBinaryMap:  BaseEntityTypeConfiguration<PictureBinary>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<PictureBinary> builder)
        {
            builder.ToTable(nameof(PictureBinary));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}
