﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository;
using Common.Api.Entitys;
using Common.Api.Dtos;
using Cyss.Core.Repository.EF;

namespace Common.Api.Services
{
    /// <summary>
    /// 图片字节表服务
    /// </summary>
    public class PictureBinaryService : BaseService<PictureBinary>
    {
        #region 字段
        /// <summary>
        /// 图片字节表仓储
        /// </summary>
        private readonly IRepository<PictureBinary> _pictureBinaryRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="pictureBinaryRepository">图片字节表 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public PictureBinaryService(IRepository<PictureBinary> pictureBinaryRepository) : base(pictureBinaryRepository)
        {
            _pictureBinaryRepository = pictureBinaryRepository;
        }

        #endregion

        #region 图片字节表方法




        #endregion

    }
}

