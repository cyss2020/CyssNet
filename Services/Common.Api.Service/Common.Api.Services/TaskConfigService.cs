﻿using Common.Api.Dtos;
using Common.Api.Entitys;
using Cyss.Core;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Common.Api.Services
{
    /// <summary>
    /// 任务配置表服务
    /// </summary>
    public class TaskConfigService : BaseService<TaskConfig>
    {
        #region 字段
        /// <summary>
        /// 任务配置表仓储
        /// </summary>
        private readonly IRepository<TaskConfig> _taskConfigRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="taskConfigRepository">任务配置表 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public TaskConfigService(IRepository<TaskConfig> taskConfigRepository) : base(taskConfigRepository)
        {
            _taskConfigRepository = taskConfigRepository;
        }

        #endregion

        #region 任务配置表方法

        /// <summary>
        ///  根据主键获取任务配置表
        /// </summary>
        /// <param name="Id">主键</param>
        /// <returns>任务配置表</returns>
        public virtual TaskConfig GetTaskConfigById(int Id)
        {
            if (Id == 0)
                return null;

            return _taskConfigRepository.GetById(Id);
        }


        /// <summary>
        ///     新增任务配置表
        /// </summary>
        /// <param name="model">任务配置表</param>
        public virtual void InsertTaskConfig(TaskConfig model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskConfigRepository.Insert(model);

        }



        /// <summary>
        ///     更新任务配置表
        /// </summary>
        /// <param name="model">任务配置表</param>
        public virtual void UpdateTaskConfig(TaskConfig model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskConfigRepository.Update(model);

        }


        /// <summary>
        ///     删除 任务配置表
        /// </summary>
        /// <param name="model">任务配置表</param>
        public virtual void DeleteTaskConfig(TaskConfig model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskConfigRepository.Delete(model);

        }

        /// <summary>
        ///     删除 任务配置表
        /// </summary>
        /// <param name="model">收藏表</param>
        public virtual void DeleteTaskConfigs(List<int> ids)
        {
            if (ids == null || ids.Count <= 0)
            {
                return;
            }
            _taskConfigRepository.Delete(ids);

        }

        /// <summary>
        ///  获取所有的  任务配置表
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns>任务配置表集合</returns>
        public virtual IList<TaskConfig> GetTaskConfigs(Expression<Func<TaskConfig, bool>> predicate, bool IsNoTracking = true)
        {
            var query = _taskConfigRepository.Table;
            if (IsNoTracking)
            {
                query = _taskConfigRepository.TableNoTracking;
            }
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToList();

        }

        /// <summary>
        /// 分页获取 任务配置表
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<TaskConfig> GetPageListTaskConfigs(TaskConfigSearchModel searchModel)
        {
            var query = _taskConfigRepository.TableNoTracking;
            var entitys = new PagedList<TaskConfig>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 任务配置表
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<TaskConfigModel> GetPageListTaskConfigModels(TaskConfigSearchModel searchModel)
        {
            var query = from a in _taskConfigRepository.TableNoTracking
                        select new TaskConfigModel
                        {
                            Id = a.Id,
                            TaskId = a.TaskId,
                            TaskType = a.TaskType,
                            Interval = a.Interval,
                            StartDatetime = a.StartDatetime,
                            EndDatetime = a.EndDatetime,
                            MaxExecutionTimes = a.MaxExecutionTimes,
                            FailNotCounted = a.FailNotCounted,
                        };

            var entitys = new PagedList<TaskConfigModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

