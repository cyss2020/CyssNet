﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cyss.Core;
using Cyss.Core.Repository.EF;
using Common.Api.Entitys;
using Common.Api.Dtos;
using Cyss.Core.Repository;

namespace Common.Api.Services
{
    /// <summary>
    /// API服务服务
    /// </summary>
    public class ApiServiceService : BaseService<ApiService>
    {
        #region 字段
        /// <summary>
        /// API服务仓储
        /// </summary>
        private readonly IRepository<ApiService> _apiServiceRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="apiServiceRepository">API服务 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public ApiServiceService(IRepository<ApiService> apiServiceRepository):base(apiServiceRepository)
        {
            _apiServiceRepository = apiServiceRepository;
        }

        #endregion

        #region API服务方法

        /// <summary>
        ///  根据主键获取API服务
        /// </summary>
        /// <param name="Id">主键</param>
        /// <returns>API服务</returns>
        public virtual ApiService GetApiServiceById(int Id)
        {
            if (Id == 0)
                return null;

            return _apiServiceRepository.GetById(Id);
        }


        /// <summary>
        ///     新增API服务
        /// </summary>
        /// <param name="model">API服务</param>
        public virtual void InsertApiService(ApiService model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _apiServiceRepository.Insert(model);

        }



        /// <summary>
        ///     更新API服务
        /// </summary>
        /// <param name="model">API服务</param>
        public virtual void UpdateApiService(ApiService model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _apiServiceRepository.Update(model);

        }


        /// <summary>
        ///     删除 API服务
        /// </summary>
        /// <param name="model">API服务</param>
        public virtual void DeleteApiService(ApiService model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _apiServiceRepository.Delete(model);

        }

        /// <summary>
        ///     删除 API服务
        /// </summary>
        /// <param name="model">收藏表</param>
        public virtual void DeleteApiServices(List<int> ids)
        {
            if (ids == null || ids.Count <= 0)
            {
                return;
            }
            _apiServiceRepository.Delete(ids);

        }

        /// <summary>
        ///  获取所有的  API服务
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns>API服务集合</returns>
        public virtual IList<ApiService> GetApiServices(Expression<Func<ApiService, bool>> predicate = null, bool IsNoTracking = true)
        {
            var query = _apiServiceRepository.Table;
            if (IsNoTracking)
            {
                query = _apiServiceRepository.TableNoTracking;
            }
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToList();

        }

        /// <summary>
        /// 分页获取 API服务
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<ApiService> GetPageListApiServices(ApiServiceSearchModel searchModel)
        {
            var query = _apiServiceRepository.TableNoTracking;
            var entitys = new PagedList<ApiService>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 API服务
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<ApiServiceModel> GetPageListApiServiceModels(ApiServiceSearchModel searchModel)
        {
            var query = from a in _apiServiceRepository.TableNoTracking
                        select new ApiServiceModel
                        {
                            Id = a.Id,
                            Name = a.Name,
                            BaseAddress = a.BaseAddress,
                        };

            var entitys = new PagedList<ApiServiceModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

