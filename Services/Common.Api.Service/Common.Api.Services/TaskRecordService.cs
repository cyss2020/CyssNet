﻿using Common.Api.Dtos;
using Common.Api.Entitys;
using Cyss.Core;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Common.Api.Services
{
    /// <summary>
    /// 任务日志服务
    /// </summary>
    public class TaskRecordService : BaseService<TaskRecord>
    {
        #region 字段
        /// <summary>
        /// 任务日志仓储
        /// </summary>
        private readonly IRepository<TaskRecord> _taskRecordRepository;

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="taskRecordRepository">任务日志 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public TaskRecordService(IRepository<TaskRecord> taskRecordRepository) : base(taskRecordRepository)
        {
            _taskRecordRepository = taskRecordRepository;
        }

        #endregion

        #region 任务日志方法

        /// <summary>
        ///  根据主键获取任务日志
        /// </summary>
        /// <param name="Id">主键</param>
        /// <returns>任务日志</returns>
        public virtual TaskRecord GetTaskRecordById(long Id)
        {
            if (Id == 0)
                return null;

            return _taskRecordRepository.GetById(Id);
        }


        /// <summary>
        ///     新增任务日志
        /// </summary>
        /// <param name="model">任务日志</param>
        public virtual void InsertTaskRecord(TaskRecord model)
        {
            if (model == null)
                throw new ArgumentNullException("model");
            _taskRecordRepository.Insert(model);

        }



        /// <summary>
        ///     更新任务日志
        /// </summary>
        /// <param name="model">任务日志</param>
        public virtual void UpdateTaskRecord(TaskRecord model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskRecordRepository.Update(model);

        }


        /// <summary>
        ///     删除 任务日志
        /// </summary>
        /// <param name="model">任务日志</param>
        public virtual void DeleteTaskRecord(TaskRecord model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            _taskRecordRepository.Delete(model);

        }

        /// <summary>
        ///     删除 任务日志
        /// </summary>
        /// <param name="model">收藏表</param>
        public virtual void DeleteTaskRecords(List<int> ids)
        {
            if (ids == null || ids.Count <= 0)
            {
                return;
            }
            _taskRecordRepository.Delete(ids);

        }

        /// <summary>
        ///  获取所有的  任务日志
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns>任务日志集合</returns>
        public virtual IList<TaskRecord> GetTaskRecords(Expression<Func<TaskRecord, bool>> predicate, bool IsNoTracking = true)
        {
            var query = _taskRecordRepository.Table;
            if (IsNoTracking)
            {
                query = _taskRecordRepository.TableNoTracking;
            }
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.ToList();

        }

        /// <summary>
        /// 分页获取 任务日志
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<TaskRecord> GetPageListTaskRecords(TaskRecordSearchModel searchModel)
        {
            var query = _taskRecordRepository.TableNoTracking;
            var entitys = new PagedList<TaskRecord>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 任务日志
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<TaskRecordModel> GetPageListTaskRecordModels(TaskRecordSearchModel searchModel)
        {
            var query = from a in _taskRecordRepository.TableNoTracking
                        select new TaskRecordModel
                        {
                            Id = a.Id,
                            TaskId = a.TaskId,
                            StartTime = a.StartTime,
                            EndTime = a.EndTime,
                            Status = a.Status,
                            Content = a.Content,
                            CreateDate = a.CreateDate,
                        };

            var entitys = new PagedList<TaskRecordModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

