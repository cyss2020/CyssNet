using Account.Api.Client;
using Autofac;
using Cyss.Core;
using Cyss.Core.Api;
using Cyss.Core.Api.Authorized;
using Cyss.Core.Api.Client;
using Cyss.Core.Api.JWT;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Common.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.RegisterCoreSerivce();
            services.RegisterAllConfig(Configuration);
            services.RegisterAccountClient();
            services.RegisterRedisCacheService();
            //services.RegisterRabbitMQService();

            //注册Jwt Token 相关功能
            services.RegisterJwt(Configuration);

            //注册json
            services.AddJsonOptions();
            services.AddFluentValidation();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigins",
                builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问
                   .AllowAnyMethod()
                   .AllowAnyHeader();
                });
            });


            services.AddControllers();
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Common.Api", Version = "v1" });
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseSwagger();
                //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Common.Api v1"));
            }

            app.UseRequestResponseLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowSpecificOrigins");
            app.UseCors();



            app.UseAuthorization();

            app.ApplicationServices.RegisterServiceProvider();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(name: default, pattern: "{controller}/{action}");

            });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<AutoFacModule>();
        }


    }
}
