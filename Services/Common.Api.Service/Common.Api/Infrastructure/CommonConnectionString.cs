﻿using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Api.Infrastructure
{
    public class CommonConnectionString : IConnectionString
    {
        public string Value { set; get; }
    }
}
