﻿using Common.Api.Dtos;
using Common.Api.Entitys;
using Common.Api.Services;
using Cyss.Core;
using Cyss.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Api.Controllers
{
    public class SettingController : CommonBaseApiController
    {
        private SettingService _settingService;
        public SettingController(SettingService settingService)
        {
            _settingService = settingService;
        }


        /// <summary>
        /// 获取配置类型集合
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OperateResult<IEnumerable<SettingTypeModel>> GetSettingTypes()
        {
            var types = AssembliesExtensions.GetTypesByBaseType<BaseSettingEntity>();
            var settings = _settingService.Gets(x => x.PropertyName == nameof(BaseSettingModel.TypeTitle));
            var models = types.Select(type => new SettingTypeModel
            {
                TypeName = type.Name,
                TypeTitle = settings.FirstOrDefault(x => x.TypeName == type.Name)?.PropertyValue
            });
            return OperateResultContent(true, models);
        }

        /// <summary>
        /// 获取配置类型集合
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OperateResult<List<SettingModel>> GetSettings(string TypeName)
        {
            return OperateResultContent(true, _settingService.GetSettings(TypeName));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        [HttpPost]
        public OperateResult UpdateSettings(IEnumerable<Setting> models)
        {
            var isSuccess = _settingService.UpdateSettings(models);
            return OperateResultContent(isSuccess, MessageContext.GetMessage());
        }


        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OperateResult<SystemSettingModel> GetSytemSetting()
        {
            return OperateResultContent(true, _settingService.GetSetting<SystemSetting>().ToModel<SystemSettingModel>());
        }


        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OperateResult<object> GetSettingByTypeName(string TypeName)
        {
            TypeName = TypeName.Replace("Model", "");
            return OperateResultContent(true, _settingService.GetSetting(TypeName));
        }

        /// <summary>
        /// 更新配置
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public OperateResult UpdateSetting(EditSettingModel model)
        {
            var type = AssembliesExtensions.GetTypesByBaseType<BaseSettingEntity>().FirstOrDefault(x => x.Name.Replace("Model", "") == model.TypeName);
            var setting = JsonHelper.DeserializeObject(model.Setting.ToSerializeObject(), type);
            _settingService.SetSeting(setting);
            return OperateResultContent(true);
        }
    }
}
