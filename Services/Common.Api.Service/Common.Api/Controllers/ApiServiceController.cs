﻿using Common.Api.Dtos;
using Common.Api.Entitys;
using Common.Api.Services;
using Cyss.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Api.Controllers
{

    /// <summary>
    /// 获取API服务 Api 接口
    /// </summary>
    public class ApiServiceController : CommonBaseApiController
    {
        #region 初始化
        private readonly ApiServiceService _apiServiceService;

        public ApiServiceController(ApiServiceService apiServiceService)
        {
            _apiServiceService = apiServiceService;
        }
        #endregion

        #region API服务增删该查

        /// <summary>
        /// 获取API服务记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<ApiServiceModel>> GetApiServiceById(int Id)
        {
            var entity = _apiServiceService.GetApiServiceById(Id);
            return OperateResultContent(true, entity.ToModel<ApiServiceModel>());
        }

        /// <summary>
        /// 获取API服务分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public OperateResult<IEnumerable<ApiServiceModel>> GetApiServices()
        {
            var entitys = _apiServiceService.GetApiServices();
            return OperateResultContent(true, entitys.Select(entity => entity.ToModel<ApiServiceModel>()));
        }


        /// <summary>
        /// 获取API服务分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<ApiServiceListModel>> GetPageListApiServices(ApiServiceSearchModel model)
        {
            var entitys = _apiServiceService.GetPageListApiServices(model);

            var viewModel = new ApiServiceListModel
            {
                Data = entitys.Select(entity => entity.ToModel<ApiServiceModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }
        
        /// <summary>
        /// 创建API服务
        /// </summary>
        /// <param name="model">API服务model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateApiService(ApiServiceModel model)
        {
                var entity = model.ToEntity<ApiService>();
                entity.SetDefaultValue();
                _apiServiceService.InsertApiService(entity);
                return OperateResultContent(true, entity.Id, "");
        }       

       
        /// <summary>
        /// 编辑API服务
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditApiService(ApiServiceModel model)
        {
                var entity =_apiServiceService.GetApiServiceById(model.Id);

                 entity.Name = model.Name;
                 entity.BaseAddress = model.BaseAddress;
                entity.SetDefaultValue();
                _apiServiceService.UpdateApiService(entity);

               return OperateResultContent(true);

        }
        
        /// <summary>
        /// 删除API服务
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteApiService([FromBody]int Id)
        {
            if(Id<=0)
            {
                 return OperateResultContent(false, "参数验证失败");
            }
          var entity = _apiServiceService.GetApiServiceById(Id);
          _apiServiceService.DeleteApiService(entity);
          return OperateResultContent(true);
             
        }
        #endregion
    }
}




