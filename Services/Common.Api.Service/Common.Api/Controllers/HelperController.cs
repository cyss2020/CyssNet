﻿using Cyss.Core;
using Cyss.Core.Cache;
using System;
using System.Threading.Tasks;

namespace Common.Api.Controllers
{
    public class HelperController : CommonBaseApiController
    {
        private IStaticCacheManager _cacheManager;
        public HelperController(IStaticCacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        /// <summary>
        /// 获取幂等性Id
        /// </summary>
        /// <returns></returns>
        public async Task<OperateResult<string>> GetIdempotence()
        {
            var IdempotenceId = Guid.NewGuid().ToString().Replace("-", "");
            var isSuccess = await _cacheManager.SetAsync(IdempotenceId, 0, 5);
            return OperateResultContent<string>(isSuccess, IdempotenceId);
        }
    }
}
