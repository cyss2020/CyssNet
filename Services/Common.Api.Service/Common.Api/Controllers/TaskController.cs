﻿using Common.Api.Dtos;
using Common.Api.Entitys;
using Common.Api.Services;
using Cyss.Core;
using Cyss.Core.RabbitMQ;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace Common.Api.Controllers
{

    /// <summary>
    /// 获取任务 Api 接口
    /// </summary>
    public class TaskController : CommonBaseApiController
    {
        #region 初始化
        private readonly TaskService _taskService;

        private readonly TaskRecordService _taskRecordService;

        private readonly TaskConfigService _taskConfigService;


        public TaskController(TaskService taskService, TaskRecordService taskRecordService, TaskConfigService taskConfigService)
        {
            _taskService = taskService;
            _taskRecordService = taskRecordService;
            _taskConfigService = taskConfigService;
        }
        #endregion

        #region 任务增删该查

        /// <summary>
        /// 获取任务记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<JobModel>> GetTaskById(int Id)
        {
            var entity = _taskService.GetTaskById(Id);
            return OperateResultContent(true, entity.ToModel<JobModel>());
        }

        /// <summary>
        /// 获取任务分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<TaskListModel>> GetPageListTasks(TaskSearchModel model)
        {
            var entitys = _taskService.GetPageListTasks(model);

            var viewModel = new TaskListModel
            {
                Data = entitys.Select(entity => entity.ToModel<JobModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建任务
        /// </summary>
        /// <param name="model">任务model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateTask(JobModel model)
        {
            var entity = model.ToEntity<Job>();
            entity.SetDefaultValue();
            _taskService.InsertTask(entity);
            return OperateResultContent(true, entity.Id, "");
        }


        /// <summary>
        /// 编辑任务
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditTask(JobModel model)
        {
            var entity = _taskService.GetTaskById(model.Id);
            //if (entity.Status != model.Status)
            //{
            //    _rabbitMQService.Send<int>("EditTaskStatus", model.Id);
            //}
            entity.Name = model.Name;
            entity.Url = model.Url;
            entity.Status = model.Status;
            entity.Count = model.Count;
            entity.IsRunning = model.IsRunning;
            entity.LastStartTime = model.LastStartTime;
            entity.LaseEndTime = model.LaseEndTime;
            entity.SetDefaultValue();
            _taskService.UpdateTask(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除任务
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteTask([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            var entity = _taskService.GetTaskById(Id);
            _taskService.DeleteTask(entity);
            return OperateResultContent(true);

        }
        #endregion

        #region 任务配置表增删该查

        /// <summary>
        /// 获取任务配置表记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<TaskConfigModel>> GetTaskConfigById(int Id)
        {
            var entity = _taskConfigService.GetTaskConfigById(Id);
            return OperateResultContent(true, entity.ToModel<TaskConfigModel>());
        }

        /// <summary>
        /// 获取任务配置表分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<TaskConfigListModel>> GetPageListTaskConfigs(TaskConfigSearchModel model)
        {
            var entitys = _taskConfigService.GetPageListTaskConfigs(model);

            var viewModel = new TaskConfigListModel
            {
                Data = entitys.Select(entity => entity.ToModel<TaskConfigModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建任务配置表
        /// </summary>
        /// <param name="model">任务配置表model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<int>> CreateTaskConfig(TaskConfigModel model)
        {
            var entity = model.ToEntity<TaskConfig>();
            entity.SetDefaultValue();
            _taskConfigService.InsertTaskConfig(entity);
            return OperateResultContent(true, entity.Id, "");
        }


        /// <summary>
        /// 编辑任务配置表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditTaskConfig(TaskConfigModel model)
        {
            var entity = _taskConfigService.GetTaskConfigById(model.Id);
            entity.TaskType = model.TaskType;
            entity.Interval = model.Interval;
            entity.StartDatetime = model.StartDatetime;
            entity.EndDatetime = model.EndDatetime;
            entity.MaxExecutionTimes = model.MaxExecutionTimes;
            entity.FailNotCounted = model.FailNotCounted;
            entity.Weeks = model.Weeks;
            entity.Days = model.Days;
            entity.SetDefaultValue();
            _taskConfigService.UpdateTaskConfig(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除任务配置表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteTaskConfig([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            var entity = _taskConfigService.GetTaskConfigById(Id);
            _taskConfigService.DeleteTaskConfig(entity);
            return OperateResultContent(true);

        }
        #endregion

        #region 任务日志增删该查

        /// <summary>
        /// 获取任务日志记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<TaskRecordModel>> GetTaskRecordById(int Id)
        {
            var entity = _taskRecordService.GetTaskRecordById(Id);
            return OperateResultContent(true, entity.ToModel<TaskRecordModel>());
        }

        /// <summary>
        /// 获取任务日志分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<TaskRecordListModel>> GetPageListTaskRecords(TaskRecordSearchModel model)
        {
            var entitys = _taskRecordService.GetPageListTaskRecords(model);

            var viewModel = new TaskRecordListModel
            {
                Data = entitys.Select(entity => entity.ToModel<TaskRecordModel>()),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建任务日志
        /// </summary>
        /// <param name="model">任务日志model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult<long>> CreateTaskRecord(TaskRecordModel model)
        {
            var entity = model.ToEntity<TaskRecord>();
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _taskRecordService.InsertTaskRecord(entity);
            return OperateResultContent(true, entity.Id, "");
        }

        /// <summary>
        /// 删除任务日志
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteTaskRecord([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            var entity = _taskRecordService.GetTaskRecordById(Id);
            _taskRecordService.DeleteTaskRecord(entity);
            return OperateResultContent(true);
        }

        #endregion
    }
}




