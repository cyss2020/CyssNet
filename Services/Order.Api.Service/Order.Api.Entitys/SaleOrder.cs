﻿using System;
using Cyss.Core;

namespace Order.Api.Entitys
{
    /// <summary>
    /// 订单表数据模型
    /// </summary>
    public partial class SaleOrder : BaseDefaultEntity
    {
        #region model

        ///<summary>
        ///状态
        ///</summary>
        public int State { get; set; }

        ///<summary>
        ///单号
        ///</summary>
        public string Number { get; set; }

        ///<summary>
        ///订单日期
        ///</summary>
        public DateTime OrderDate { get; set; }

        ///<summary>
        ///客户Id
        ///</summary>
        public int CustomerId { get; set; }

        #endregion



    }
}