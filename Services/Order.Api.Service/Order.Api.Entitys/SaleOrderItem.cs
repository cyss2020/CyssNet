﻿using System;
using Cyss.Core;

namespace Order.Api.Entitys
{
    /// <summary>
    /// 订单产品表数据模型
    /// </summary>
    public partial class  SaleOrderItem: BaseDefaultEntity
    {
        #region model
        
        ///<summary>
        ///销售订单Id
        ///</summary>
	    public int SaleOrderId{ get; set; }
        
        ///<summary>
        ///状态
        ///</summary>
	    public int State{ get; set; }
        
        ///<summary>
        ///产品Id
        ///</summary>
	    public int ProductId{ get; set; }
        
        ///<summary>
        ///产品单价
        ///</summary>
	    public decimal Price{ get; set; }
        
        ///<summary>
        ///产品数量
        ///</summary>
	    public int Quantity{ get; set; }
        
        #endregion



    }
}