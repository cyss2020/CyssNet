﻿using Cyss.Core;
using Cyss.Core.Api;
using Cyss.Core.Api.Authorized;
using Cyss.Core.Librarys;
using Cyss.Core.Repository;
using Microsoft.AspNetCore.Mvc;
using Order.Api.Dtos;
using Order.Api.Entitys;
using Order.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Api.Controllers
{

    /// <summary>
    /// 获取订单表 Api 接口
    /// </summary>
    public class SaleOrderController : OrderBaseApiController
    {
        #region 初始化
        /// <summary>
        /// 销售订单表服务
        /// </summary>
        private readonly SaleOrderService _saleOrderService;

        /// <summary>
        /// 订单产品表服务
        /// </summary>
        private readonly SaleOrderItemService _saleOrderItemService;

        public SaleOrderController(SaleOrderService saleOrderService, SaleOrderItemService saleOrderItemService)
        {
            _saleOrderService = saleOrderService;
            _saleOrderItemService = saleOrderItemService;
        }
        #endregion

        #region 订单表增删该查

        /// <summary>
        /// 获取订单表记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<OperateResult<SaleOrderModel>> GetSaleOrderById(int Id)
        {
            _saleOrderService.TEST();
            var entity = _saleOrderService.GetById(Id);
            return OperateResultContent(true, await entity.ToModel<SaleOrderModel>(true));
        }

        /// <summary>
        /// 获取订单表分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<OperateResult<SaleOrderListModel>> GetPageListSaleOrders(SaleOrderSearchModel model)
        {
            var entitys = _saleOrderService.GetPageListSaleOrders(model);
            var viewModel = new SaleOrderListModel
            {
                Data = await Task.WhenAll(entitys.Select(async entity =>
                {
                    var m = await entity.ToModel<SaleOrderModel>(true);
                    return m;
                })),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建订单表
        /// </summary>
        /// <param name="model">订单表model</param>
        /// <returns></returns>
        [HttpPost]
        [Idempotence]
        public ActionResult<OperateResult<int>> CreateSaleOrder(SaleOrderModel model)
        {

            var entity = model.ToEntity<SaleOrder>();
            entity.SetDefaultValue();
            _saleOrderService.Insert(entity);
            return OperateResultContent(true, entity.Id, model.Number);
        }


        /// <summary>
        /// 编辑订单表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditSaleOrder(SaleOrderModel model)
        {
            var entity = _saleOrderService.GetById(model.Id);

            entity.Number = model.Number;
            entity.OrderDate = model.OrderDate;
            entity.CustomerId = model.CustomerId;
            entity.SetDefaultValue();
            _saleOrderService.Update(entity);
            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除订单表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteSaleOrder([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            var entity = _saleOrderService.GetById(Id);
            _saleOrderService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion

        #region 订单产品表增删该查

        /// <summary>
        /// 获取订单产品表记录
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<SaleOrderItemModel>> GetSaleOrderItemById(int Id)
        {
            var entity = _saleOrderItemService.GetById(Id);
            return OperateResultContent(true, entity.ToModel<SaleOrderItemModel>());
        }

        /// <summary>
        /// 根据销售订单Id获取订单产品明细
        /// </summary>
        /// <param name="OrderId">销售订单Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<OperateResult<IEnumerable<SaleOrderItemModel>>> GetSaleOrderItemByOrderId(int OrderId)
        {
            var entitys = _saleOrderItemService.GetSaleOrderItemsByOrderId(OrderId);
            return OperateResultContent(true, entitys.Select(x => x.ToModel<SaleOrderItemModel>()));
        }


        /// <summary>
        /// 获取订单产品表分页列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<OperateResult<SaleOrderItemListModel>> GetPageListSaleOrderItems(SaleOrderItemSearchModel model)
        {
            var entitys = _saleOrderItemService.GetPageListSaleOrderItems(model);
            var viewModel = new SaleOrderItemListModel
            {
                Data = await Task.WhenAll(entitys.Select(async entity =>
                {
                    var m = await entity.ToModel<SaleOrderItemModel>(true);
                    return m;
                })),
                Total = entitys.TotalCount
            };
            return OperateResultContent(true, viewModel);
        }

        /// <summary>
        /// 创建订单产品表
        /// </summary>
        /// <param name="model">订单产品表model</param>
        /// <returns></returns>
        [HttpPost]
        [Idempotence]
        public ActionResult<OperateResult<int>> CreateSaleOrderItem(SaleOrderItemModel model)
        {
            var entity = model.ToEntity<SaleOrderItem>();
            entity.CreateById = this.CurrentAccountUser.Id;
            entity.CreateDate = DateTime.Now;
            entity.SetDefaultValue();
            _saleOrderItemService.Insert(entity);
            return OperateResultContent(true, entity.Id, model.ProductName);
        }


        /// <summary>
        /// 编辑订单产品表
        /// </summary>
        /// <param name="model">用户model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> EditSaleOrderItem(SaleOrderItemModel model)
        {
            var entity = _saleOrderItemService.GetById(model.Id);

            entity.ProductId = model.ProductId;
            entity.Price = model.Price;
            entity.Quantity = model.Quantity;
            entity.CreateDate = model.CreateDate;
            entity.CreateById = model.CreateById;
            entity.UpdateDate = model.UpdateDate;
            entity.UpdateById = this.CurrentAccountUser.Id;
            entity.UpdateDate = DateTime.Now;
            entity.SetDefaultValue();
            _saleOrderItemService.Update(entity);

            return OperateResultContent(true);

        }

        /// <summary>
        /// 删除订单产品表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<OperateResult> DeleteSaleOrderItem([FromBody] int Id)
        {
            if (Id <= 0)
            {
                return OperateResultContent(false, "参数验证失败");
            }
            _saleOrderItemService.Delete(Id);
            return OperateResultContent(true);

        }
        #endregion

    }
}




