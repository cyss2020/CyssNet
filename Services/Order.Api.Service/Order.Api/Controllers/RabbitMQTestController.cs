﻿using Cyss.Core;
using Cyss.Core.RabbitMQ;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Api.Controllers
{
    public class RabbitMQTestController : Controller
    {
        private RabbitMQService _rabbitMQService;
        public RabbitMQTestController(RabbitMQService rabbitMQService)
        {
            _rabbitMQService = rabbitMQService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public string InsertMQ()
        {
            TetModel tetModel = new TetModel();
            tetModel.Name = "tset";
            _rabbitMQService.CreateConsumer<TetModel>("test", test);
            _rabbitMQService.Send("test", tetModel);
            return string.Empty;
        }

        private void test(TetModel s)
        {

        }

        public class TetModel:BaseModel
        {
            public string Name { set; get; }
        }
    }
}
