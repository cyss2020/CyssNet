using Account.Api.Client;
using Autofac;
using Cyss.Core;
using Cyss.Core.Api;
using Cyss.Core.Api.Authorized;
using Cyss.Core.Api.Client;
using Cyss.Core.Api.JWT;
using Cyss.Core.Helper;
using Cyss.Core.Infrastructure;
using Cyss.Core.RabbitMQ;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Order.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {

            services.RegisterCoreSerivce();
            services.RegisterAllConfig(Configuration);
            services.RegisterClientConfig(Configuration);
            services.RegisterAccountClient();
            services.RegisterRedisCacheService();
            //services.RegisterRabbitMQService();

            //注册Jwt Token 相关功能
            services.RegisterJwt(Configuration);

            //注册json
            services.AddJsonOptions();
            services.AddFluentValidation();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigins",
                builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问
                   .AllowAnyMethod()
                   .AllowAnyHeader();
                });
            });



            services.AddControllers();



        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRequestResponseLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowSpecificOrigins");
            app.UseCors();



            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(name: default, pattern: "{controller}/{action}");

            });

            app.ApplicationServices.RegisterServiceProvider();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<AutoFacModule>();
        }


    }
}
