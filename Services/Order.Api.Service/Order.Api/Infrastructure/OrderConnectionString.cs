﻿using Cyss.Core;

namespace Order.Api.Infrastructure
{
    public class OrderConnectionString : IConnectionString
    {
        public string Value { set; get; }

    }
}
