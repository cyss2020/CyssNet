﻿using Cyss.Core;
namespace Order.Api.Dtos
{
    /// <summary>
    /// 订单表分页查询参数对象
    /// </summary>
    public class SaleOrderSearchModel: BaseSearchModel
    {
         public string SearchName { set; get; }
    }
}
