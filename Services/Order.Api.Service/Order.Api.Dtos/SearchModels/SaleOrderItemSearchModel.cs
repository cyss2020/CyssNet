﻿using Cyss.Core;
namespace Order.Api.Dtos
{
    /// <summary>
    /// 订单产品表分页查询参数对象
    /// </summary>
    public class SaleOrderItemSearchModel : BaseSearchModel
    {
        public int SaleOrderId { set; get; }
        public string SearchName { set; get; }
    }
}
