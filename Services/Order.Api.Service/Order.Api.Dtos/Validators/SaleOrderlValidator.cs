﻿using Cyss.Core.Validators;
using FluentValidation;

namespace Order.Api.Dtos.Validators
{
    /// <summary>
    /// 
    /// </summary>
    public class SaleOrderlValidator : BaseValidator<SaleOrderModel>
    {
        /// <summary>
        /// 
        /// </summary>
        public SaleOrderlValidator()
        {
            RuleFor(x => x.Number)
               .NotEmpty()
               .WithMessage("Number 不能为null");

            RuleFor(x => x.CustomerId)
              .Min(1)
              .WithMessage("客户必填");
        }
    }
}
