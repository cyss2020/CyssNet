﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Order.Api.Dtos
{
    /// <summary>
    /// 订单产品表
    /// </summary>
    public partial class  SaleOrderItemModel: BaseDefaultEntityModel
    {
        #region model
        ///<summary>
        ///销售订单Id
        ///</summary>
        [Display(Name = "销售订单Id")]
	    public int SaleOrderId{ get; set; }
       
        ///<summary>
        ///状态
        ///</summary>
        [Display(Name = "状态")]
	    public int State{ get; set; }
       
        ///<summary>
        ///产品Id
        ///</summary>
        [Display(Name = "产品Id")]
	    public int ProductId{ get; set; }
       
        ///<summary>
        ///产品单价
        ///</summary>
        [Display(Name = "产品单价")]
	    public decimal Price{ get; set; }
       
        ///<summary>
        ///产品数量
        ///</summary>
        [Display(Name = "产品数量")]
	    public int Quantity{ get; set; }

        #endregion

        #region 扩展属性
         ///<summary>
        ///(扩展属性)销售订单Id(SaleOrderId)
        ///</summary>
        [Display(Name = "销售订单Id")]
        public string SaleOrderName{ get; set; }
        
         ///<summary>
        ///(扩展属性)产品Id(ProductId)
        ///</summary>
        [Display(Name = "产品")]
        public string ProductName{ get; set; }

        #endregion
    }
}