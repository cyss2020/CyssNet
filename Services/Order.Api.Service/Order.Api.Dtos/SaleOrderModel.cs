﻿using System;
using System.ComponentModel.DataAnnotations;
using Cyss.Core;

namespace Order.Api.Dtos
{
    /// <summary>
    /// 订单表
    /// </summary>
    public partial class SaleOrderModel : BaseDefaultEntityModel
    {
        #region model
        ///<summary>
        ///状态
        ///</summary>
        [Display(Name = "状态")]
        public int State { get; set; }

        ///<summary>
        ///单号
        ///</summary>
        [Display(Name = "单号")]
        public string Number { get; set; }

        ///<summary>
        ///订单日期
        ///</summary>
        [Display(Name = "订单日期")]
        public DateTime OrderDate { get; set; }

        ///<summary>
        ///客户Id
        ///</summary>
        [Display(Name = "客户Id")]
        public int CustomerId { get; set; }


        #endregion

        #region 扩展属性
        ///<summary>
        ///(扩展属性)客户Id(CustomerId)
        ///</summary>
        [Display(Name = "客户")]
        [Required]
        [StringLength(100,MinimumLength =10, ErrorMessage = "客户必填")]
        public string CustomerName { get; set; }

        ///<summary>
        ///(扩展属性)总金额
        ///</summary>
        [Display(Name = "总金额")]
        public decimal TotalAmount { set; get; }
        #endregion
    }
}