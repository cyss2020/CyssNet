﻿using Cyss.Core;

namespace Order.Api.Dtos
{
    /// <summary>
    /// 订单表分页查询返回对象
    /// </summary>
    public class SaleOrderListModel: BasePagedListModel<SaleOrderModel>
    {
    }
}
