﻿using Cyss.Core;

namespace Order.Api.Dtos
{
    /// <summary>
    /// 订单产品表分页查询返回对象
    /// </summary>
    public class SaleOrderItemListModel: BasePagedListModel<SaleOrderItemModel>
    {
    }
}
