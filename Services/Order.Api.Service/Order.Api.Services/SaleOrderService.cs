﻿using Cyss.Core;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Order.Api.Dtos;
using Order.Api.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Order.Api.Services
{
    /// <summary>
    /// 订单表服务
    /// </summary>
    public class SaleOrderService : BaseDefaultService<SaleOrder, int>
    {
        #region 字段

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="saleOrderRepository">订单表 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public SaleOrderService(IRepository<SaleOrder> saleOrderRepository) : base(saleOrderRepository)
        {

        }

        #endregion

        #region 订单表方法


        /// <summary>
        /// 分页获取 订单表
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<SaleOrder> GetPageListSaleOrders(SaleOrderSearchModel searchModel)
        {
            var query = _repository.TableNoTracking;
            var entitys = new PagedList<SaleOrder>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 订单表
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<SaleOrderModel> GetPageListSaleOrderModels(SaleOrderSearchModel searchModel)
        {
            var query = from a in _repository.TableNoTracking
                        select new SaleOrderModel
                        {
                            Id = a.Id,
                            Number = a.Number,
                            OrderDate = a.OrderDate,
                            CustomerId = a.CustomerId,
                            CreateDate = a.CreateDate,
                            CreateById = a.CreateById,
                            UpdateDate = a.UpdateDate,
                            UpdateById = a.UpdateById,
                        };

            var entitys = new PagedList<SaleOrderModel>(query, searchModel);
            return entitys;
        }

        public void TEST()
        {
            List<SaleOrder> saleOrders = new List<SaleOrder>();
            saleOrders.Add(new SaleOrder());
            _repository.BulkInsert(saleOrders);
        }

        #endregion

    }
}

