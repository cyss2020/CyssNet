﻿using Cyss.Core;
using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Order.Api.Dtos;
using Order.Api.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Order.Api.Services
{
    /// <summary>
    /// 订单产品表服务
    /// </summary>
    public class SaleOrderItemService : BaseDefaultService<SaleOrderItem,int>
    {
        #region 字段

        #endregion

        #region 构造函数

        /// <summary>
        ///     构造函数
        /// </summary>
        /// <param name="saleOrderItemRepository">订单产品表 仓储</param>
        /// <param name="eventPublisher">事件发布者</param>
        public SaleOrderItemService(IRepository<SaleOrderItem> saleOrderItemRepository) : base(saleOrderItemRepository)
        {

        }

        #endregion

        #region 订单产品表方法


        /// <summary>
        /// 根据销售Id获取订单产品集合
        /// </summary>
        /// <param name="OrderId">销售订单Id</param>
        /// <param name="IsNoTracking">是否进行实体跟踪,如果查询的对象不用于修改更新默认即可，实体跟踪会消耗资源</param>
        /// <returns></returns>
        public virtual IList<SaleOrderItem> GetSaleOrderItemsByOrderId(int OrderId, bool IsNoTracking = true)
        {
            if (OrderId <= 0)
            {
                return new List<SaleOrderItem>();
            }
            var query = _repository.Table;
            if (IsNoTracking)
            {
                query = _repository.TableNoTracking;
            }
            query = query.Where(x => x.SaleOrderId == OrderId);
            return query.ToList();

        }

        /// <summary>
        /// 分页获取 订单产品表
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<SaleOrderItem> GetPageListSaleOrderItems(SaleOrderItemSearchModel searchModel)
        {
            var query = _repository.TableNoTracking;
            query = query.Where(x => x.SaleOrderId == searchModel.SaleOrderId);
            var entitys = new PagedList<SaleOrderItem>(query, searchModel);
            return entitys;
        }

        /// <summary>
        /// 分页获取 订单产品表
        /// </summary>
        /// <param name="searchModel">查询model</param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public virtual IPagedList<SaleOrderItemModel> GetPageListSaleOrderItemModels(SaleOrderItemSearchModel searchModel)
        {
            var query = from a in _repository.TableNoTracking
                        select new SaleOrderItemModel
                        {
                            Id = a.Id,
                            ProductId = a.ProductId,
                            Price = a.Price,
                            Quantity = a.Quantity,
                            CreateDate = a.CreateDate,
                            CreateById = a.CreateById,
                            UpdateDate = a.UpdateDate,
                            UpdateById = a.UpdateById,
                        };
            query = query.Where(x => x.SaleOrderId == searchModel.SaleOrderId);
            var entitys = new PagedList<SaleOrderItemModel>(query, searchModel);
            return entitys;
        }

        #endregion

    }
}

