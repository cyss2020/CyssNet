﻿using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Order.Api.Entitys;

namespace Order.Api.Services.Mapping
{
    /// <summary>
    /// 订单产品表
    /// </summary>
    public partial class  SaleOrderItemMap:  BaseEntityTypeConfiguration<SaleOrderItem>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<SaleOrderItem> builder)
        {
            builder.ToTable(nameof(SaleOrderItem));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}