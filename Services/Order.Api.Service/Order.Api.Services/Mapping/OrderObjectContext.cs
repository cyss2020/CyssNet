﻿using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Api.Services.Mapping
{


    public class OrderObjectContext : BaseObjectContext, IOrdersObjectContext
    {

        public OrderObjectContext(DbContextOptions options) : base(options, typeof(OrderObjectContext))
        {

        }

    }

    public interface IOrdersObjectContext : IDbContext
    {

    }

}
