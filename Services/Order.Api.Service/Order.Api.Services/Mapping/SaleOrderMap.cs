﻿using Cyss.Core.Repository;
using Cyss.Core.Repository.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Order.Api.Entitys;

namespace Order.Api.Services.Mapping
{
    /// <summary>
    /// 订单表
    /// </summary>
    public partial class  SaleOrderMap:  BaseEntityTypeConfiguration<SaleOrder>
    {
        #region 构造函数

       public override void Configure(EntityTypeBuilder<SaleOrder> builder)
        {
            builder.ToTable(nameof(SaleOrder));
            builder.HasKey(entity => entity.Id);
            base.Configure(builder);
        }

        #endregion

    }
}