﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.TaskManager.Models
{
    public class TaskContext
    {

        public TaskContext()
        {
            this.begin = 1;
            this.end = 50;
            this.minute = 10;
        }

        public int begin { set; get; }

        public int end { set; get; }

        public int minute { set; get; }

    }


}
