﻿using Cyss.TaskManager.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cyss.TaskManager
{
    public partial class FormLog : Form
    {
        private int TaskId { set; get; }
        public FormLog(int taskId)
        {
            TaskId = taskId;
            InitializeComponent();
            if (dataGridViewMain != null)
            {
                dataGridViewMain.AutoGenerateColumns = false;
                dataGridViewMain.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }

        }

        private void FormLog_Load(object sender, EventArgs e)
        {
            if (dataGridViewMain != null)
            {
                var list = LoggingManager.GetLoggins(TaskId);
                dataGridViewMain.DataSource = list;
            }
            LoggingManager.LogsChangeEvent += LoggingManager_LogsChangeEvent;
        }

        private void LoggingManager_LogsChangeEvent(int id)
        {
            if (IsDisposed || this == null) return;
            if (id != this.TaskId) return;
            if (dataGridViewMain == null || !dataGridViewMain.IsHandleCreated)
            {
                return;
            }
            var list = LoggingManager.GetLoggins(TaskId);
            dataGridViewMain.Invoke(new Action(delegate
            {
                dataGridViewMain.DataSource = null;
                dataGridViewMain.DataSource = list;
            }));
        }

        private void dataGridViewMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void FormLog_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoggingManager.LogsChangeEvent -= LoggingManager_LogsChangeEvent;

        }
    }
}
