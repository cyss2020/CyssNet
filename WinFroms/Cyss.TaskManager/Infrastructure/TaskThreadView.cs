﻿using Cyss.Core;
using System;
using System.Windows.Forms;
using System.Text;
using Common.Api.Dtos;
using System.Linq;
namespace Cyss.TaskManager.Infrastructure
{
    public class TaskThreadView : TaskThread
    {

        public TaskThreadView()
        {
            this.ButtonLogTitle = "日志";
            this.ButtonConfigTitle = "配置";
            this.ButtonExecuteTitle = "执行";
        }

        public DataGridView dgv;
        public string IsRunings { get { return this.IsRuning ? "正在执行" : "空闲"; } }

        #region 服务状态
        public string IsStart { get { return this.State ? "开启" : "关闭"; } }

        /// <summary>
        /// 服务状态
        /// </summary>
        public override bool State
        {

            set
            {
                if (_State != value)
                {
                    _State = value;


                    OnPropertyChanged("IsStart");
                    OnPropertyChanged("ButtonTitle");
                }

            }
            get { return _State; }
        }


        #endregion

        /// <summary>
        /// 是否正在执行中
        /// </summary>
        public override bool IsRuning
        {
            set
            {
                if (this._IsRuning != value)
                {
                    this._IsRuning = value;
                    OnPropertyChanged("IsRunings");
                    OnPropertyChanged("Counter");
                }
            }
            get { return this._IsRuning; }
        }

        public void ConfigChange()
        {
            OnPropertyChanged("ConfigString");
        }
        public string ConfigString
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder();
                if (this.Config.TaskExecuteType == TaskExecuteType.loop)
                {
                    stringBuilder.Append($"循环执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.TaskExecuteType == TaskExecuteType.Daily)
                {
                    stringBuilder.Append($"每天定时执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.TaskExecuteType == TaskExecuteType.Weekly)
                {
                    stringBuilder.Append($"每周定时执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.TaskExecuteType == TaskExecuteType.Month)
                {
                    stringBuilder.Append($"每月定时执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.StartDatetime > TimeSpan.Zero)
                {
                    stringBuilder.Append($";开始时间{this.Config.StartDatetime.ToString()}");
                }
                if (this.Config.EndDatetime > TimeSpan.Zero)
                {
                    stringBuilder.Append($";结束时间{this.Config.EndDatetime.ToString()}");
                }
                if (this.Config.DayOfWeeks != null && this.Config.DayOfWeeks.Count() > 0)
                {
                    stringBuilder.Append($"[{string.Join("/", this.Config.DayOfWeeks)}]");
                }
                if (this.Config.DayOfMonths != null && this.Config.DayOfMonths.Count() > 0)
                {
                    stringBuilder.Append($"[{string.Join("/", this.Config.DayOfMonths)}]");
                }
                return stringBuilder.ToString();
            }
        }

        #region 开始执行时间
        /// <summary>
        /// 开始执行时间
        /// </summary>
        public override DateTime StartDatetime
        {
            set
            {
                if (_StartDatetime != value)
                {
                    _StartDatetime = value;
                    OnPropertyChanged("StartDatetimeStr");
                }
            }
            get { return _StartDatetime; }
        }


        public string StartDatetimeStr
        {
            get
            {
                return this.StartDatetime.ViewDateTime();
            }

        }

        #endregion

        #region 结束执行时间

        /// <summary>
        /// 结束执行时间
        /// </summary>
        public override DateTime EndDateTime
        {
            set
            {
                if (_EndDateTime != value)
                {
                    _EndDateTime = value;
                    OnPropertyChanged("EndDateTimeStr");

                }
            }
            get { return _EndDateTime; }
        }


        public string EndDateTimeStr
        {
            get
            {
                return this.EndDateTime.ViewDateTime();
            }
        }

        #endregion

        /// <summary>
        /// 开启关闭按钮文本
        /// </summary>
        public string ButtonTitle
        {
            get
            {
                if (this.State == false)
                {
                    return "开启";
                }
                else
                {
                    if (this.Colseing)
                    {
                        return "关闭中";
                    }
                    return "关闭";
                }
            }

        }

        /// <summary>
        /// 日志按钮文本
        /// </summary>
        public string ButtonLogTitle { set; get; }

        /// <summary>
        /// 执行按钮文本
        /// </summary>
        public string ButtonExecuteTitle { set; get; }

        /// <summary>
        /// 配置按钮文本
        /// </summary>
        public string ButtonConfigTitle { set; get; }
        /// <summary>
        /// 属性值变化是更新界面
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (dgv == null)
            {
                return;
            }

            var cell = dgv.Rows[this.Id - 1].Cells[propertyName + "Column"];

            if (cell != null)
            {

                dgv.Invoke(new Action(
                  delegate
                  {
                      dgv.InvalidateCell(cell);
                  }
              ));

            }
        }

    }
}
