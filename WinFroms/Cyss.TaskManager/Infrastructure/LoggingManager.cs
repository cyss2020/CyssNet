﻿using Cyss.TaskManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cyss.TaskManager.Infrastructure
{

    /// <summary>
    /// 日志变更委托
    /// </summary>
    public delegate void LogsChangeEventHander(int id);

    public class LoggingManager
    {
        /// <summary>
        /// 日志变更事件
        /// </summary>
        public static event LogsChangeEventHander LogsChangeEvent;
        private static object lockObject = new object();
        public static List<TaskLogging> GetLoggins(int TaskId)
        {
            return Loggings.Where(x => x.TaskId == TaskId).OrderByDescending(x => x.CreateTime).ToList();
        }

        private static List<TaskLogging> Loggings { set; get; }

        static LoggingManager()
        {
            Loggings = new List<TaskLogging>();
        }

        public static void Insert(TaskLogging taskLogging)
        {
            lock (lockObject)
            {
                if (Loggings.Count(x => x.TaskId == taskLogging.TaskId) >= 200)
                {
                    var clears = Loggings.Where(x => x.TaskId == taskLogging.TaskId).OrderByDescending(x => x.CreateTime).Skip(200).ToList();
                    clears.ForEach(x => Loggings.Remove(x));
                    clears = null;
                }
                Loggings.Add(taskLogging);
            }
            LogsChangeEvent?.Invoke(taskLogging.TaskId);
            insesrtDatabase(taskLogging);
        }

        private async static void insesrtDatabase(TaskLogging taskLogging)
        {
            await Common.Api.Client.CommonClientFactory.Task.CreateTaskRecord(new Common.Api.Dtos.TaskRecordModel
            {
                TaskId = taskLogging.TaskId,
                Content = taskLogging.Content,
                CreateDate = DateTime.Now,
            });
        }
    }
}
