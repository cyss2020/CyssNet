﻿using Common.Api.Dtos;
using Cyss.TaskManager.Infrastructure;
using Cyss.TaskManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cyss.TaskManager
{
    public partial class FormTaskConfig : Form
    {
        private TaskConfigModel Config { set; get; }
        private TaskThreadView taskThreadView { set; get; }
        public FormTaskConfig(string Name, TaskThreadView taskThread, TaskConfigModel taskConfig)
        {
            taskThreadView = taskThread;
            Config = taskConfig;
            InitializeComponent();
            //默认值
            this.txtInterval.Text = "300";
            this.dtStartDatetime.Value = DateTime.Now.Date;
            this.dtEndDatetime.Value = DateTime.Now.Date;
            this.rbloop.Checked = true;
            this.Text = Name;
        }


        private void FormTaskConfig_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void Init()
        {
            if (Config.TaskExecuteType == TaskExecuteType.loop)
            {
                this.rbloop.Checked = true;
            }
            if (Config.TaskExecuteType == TaskExecuteType.Daily)
            {
                this.rbDaily.Checked = true;
            }
            if (Config.TaskExecuteType == TaskExecuteType.Weekly)
            {
                this.rbWeekly.Checked = true;
            }
            if (Config.TaskExecuteType == TaskExecuteType.Month)
            {
                this.rbMonth.Checked = true;
            }
            this.txtInterval.Text = Config.Interval.ToString();
            this.txtMaxExecutionTimes.Text = Config.MaxExecutionTimes.ToString();
            this.cheboxFailNotCounted.Checked = Config.FailNotCounted;
            this.dtStartDatetime.Value = DateTime.Now.Date.Add(Config.StartDatetime);
            this.dtEndDatetime.Value = DateTime.Now.Date.Add(Config.EndDatetime);

            checkedListBoxWeek.Items.Add(DayOfWeek.Monday, Config.DayOfWeeks.Contains(DayOfWeek.Monday));
            checkedListBoxWeek.Items.Add(DayOfWeek.Tuesday, Config.DayOfWeeks.Contains(DayOfWeek.Tuesday));
            checkedListBoxWeek.Items.Add(DayOfWeek.Wednesday, Config.DayOfWeeks.Contains(DayOfWeek.Wednesday));
            checkedListBoxWeek.Items.Add(DayOfWeek.Thursday, Config.DayOfWeeks.Contains(DayOfWeek.Thursday));
            checkedListBoxWeek.Items.Add(DayOfWeek.Friday, Config.DayOfWeeks.Contains(DayOfWeek.Friday));
            checkedListBoxWeek.Items.Add(DayOfWeek.Saturday, Config.DayOfWeeks.Contains(DayOfWeek.Saturday));
            checkedListBoxWeek.Items.Add(DayOfWeek.Sunday, Config.DayOfWeeks.Contains(DayOfWeek.Sunday));

            for (int day = 1; day <= 30; day++)
            {
                checkedListBoxDays.Items.Add(new DayModel { Day = day }, Config.DayOfMonths.Select(x => x.Day).Contains(day));
            }
            checkedListBoxDays.Items.Add(new DayModel { Day = 0 }, Config.DayOfMonths.Select(x => x.Day).Contains(0));

        }
        private void Read()
        {
            if (this.rbloop.Checked)
            {
                Config.TaskExecuteType = TaskExecuteType.loop;
            }
            if (this.rbDaily.Checked)
            {
                Config.TaskExecuteType = TaskExecuteType.Daily;
            }
            if (this.rbWeekly.Checked)
            {
                Config.TaskExecuteType = TaskExecuteType.Weekly;
            }
            if (this.rbMonth.Checked)
            {
                Config.TaskExecuteType = TaskExecuteType.Month;
            }
            Config.Interval = int.Parse(this.txtInterval.Text);
            Config.MaxExecutionTimes = int.Parse(this.txtMaxExecutionTimes.Text);
            Config.FailNotCounted = this.cheboxFailNotCounted.Checked;
            Config.StartDatetime = this.dtStartDatetime.Value.TimeOfDay;
            Config.EndDatetime = this.dtEndDatetime.Value.TimeOfDay;
            Config.DayOfWeeks = checkedListBoxWeek.CheckedItems.Cast<DayOfWeek>();// as IEnumerable<DayOfWeek>;
            Config.DayOfMonths = checkedListBoxDays.CheckedItems.Cast<DayModel>();
        }

        private void txtInterval_KeyPress(object sender, KeyPressEventArgs e)
        {
            //如果输入的不是退格和数字，则屏蔽输入
            if (!(e.KeyChar == '\b' || (e.KeyChar >= '0' && e.KeyChar <= '9')))
            {
                e.Handled = true;
            }
        }

        private void txtMaxExecutionTimes_KeyPress(object sender, KeyPressEventArgs e)
        {
            //如果输入的不是退格和数字，则屏蔽输入
            if (!(e.KeyChar == '\b' || (e.KeyChar >= '0' && e.KeyChar <= '9')))
            {
                e.Handled = true;
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSave_Click(object sender, EventArgs e)
        {
            Read();
            var operateResult = await Common.Api.Client.CommonClientFactory.Task.EditTaskConfig(Config);
            taskThreadView.ConfigChange();
            MessageBox.Show(operateResult.IsSuccess ? "保存成功!" : "保存失败:" + operateResult.Message);
        }

        private void rbloop_Click(object sender, EventArgs e)
        {
            txtMaxExecutionTimes.Text = "0";
            txtMaxExecutionTimes.Enabled = false;
            cheboxFailNotCounted.Enabled = false;
            cheboxFailNotCounted.Checked = false;
            checkedListBoxDays.Enabled = false;
            groupBoxDays.Enabled = false;
            checkedListBoxWeek.Enabled = false;
            groupBoxWeeks.Enabled = false;
            checkedListBoxDays.ClearSelected();
            checkedListBoxWeek.ClearSelected();
        }

        private void rbDaily_Click(object sender, EventArgs e)
        {
            txtMaxExecutionTimes.Enabled = true;
            cheboxFailNotCounted.Enabled = true;
            cheboxFailNotCounted.Checked = true;
            checkedListBoxDays.Enabled = false;
            groupBoxDays.Enabled = false;
            checkedListBoxWeek.Enabled = false;
            groupBoxWeeks.Enabled = false;
            checkedListBoxDays.ClearSelected();
            checkedListBoxWeek.ClearSelected();
        }

        private void rbWeekly_Click(object sender, EventArgs e)
        {
            txtMaxExecutionTimes.Enabled = true;
            cheboxFailNotCounted.Enabled = true;
            cheboxFailNotCounted.Checked = true;
            checkedListBoxDays.Enabled = false;
            groupBoxDays.Enabled = false;
            checkedListBoxWeek.Enabled = true;
            groupBoxWeeks.Enabled = true;
            checkedListBoxWeek.ClearSelected();
        }

        private void rbMonth_Click(object sender, EventArgs e)
        {
            txtMaxExecutionTimes.Enabled = true;
            cheboxFailNotCounted.Enabled = true;
            cheboxFailNotCounted.Checked = true;
            checkedListBoxDays.Enabled = true;
            groupBoxDays.Enabled = true;
            checkedListBoxWeek.Enabled = false;
            groupBoxWeeks.Enabled = false;
            checkedListBoxWeek.ClearSelected();
        }
    }
}
