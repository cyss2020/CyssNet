﻿namespace Cyss.TaskManager
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnAutoService = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.notifyIconMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.ServiceNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfigStringColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsRuningsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CounterColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartDatetimeStrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndDateTimeStrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonTitleColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ButtonExecuteTitleColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ButtonLogTitleColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ButtonConfigColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnAutoService);
            this.splitContainer1.Panel1.Controls.Add(this.btnClose);
            this.splitContainer1.Panel1.Controls.Add(this.btnStart);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewMain);
            this.splitContainer1.Size = new System.Drawing.Size(1444, 450);
            this.splitContainer1.SplitterDistance = 62;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // btnAutoService
            // 
            this.btnAutoService.ForeColor = System.Drawing.Color.Red;
            this.btnAutoService.Location = new System.Drawing.Point(639, 25);
            this.btnAutoService.Name = "btnAutoService";
            this.btnAutoService.Size = new System.Drawing.Size(440, 23);
            this.btnAutoService.TabIndex = 2;
            this.btnAutoService.Text = "自动开启服务";
            this.btnAutoService.UseVisualStyleBackColor = true;
            this.btnAutoService.Click += new System.EventHandler(this.btnAutoService_Click);
            // 
            // btnClose
            // 
            this.btnClose.Enabled = false;
            this.btnClose.Location = new System.Drawing.Point(196, 25);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(159, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "关闭任务管理";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(33, 25);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(96, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "开启任务管理";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.AllowUserToOrderColumns = true;
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ServiceNameColumn,
            this.ConfigStringColumn,
            this.IsStartColumn,
            this.IsRuningsColumn,
            this.CounterColumn,
            this.StartDatetimeStrColumn,
            this.EndDateTimeStrColumn,
            this.ButtonTitleColumn,
            this.ButtonExecuteTitleColumn,
            this.ButtonLogTitleColumn,
            this.ButtonConfigColumn});
            this.dataGridViewMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMain.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.Size = new System.Drawing.Size(1444, 384);
            this.dataGridViewMain.TabIndex = 0;
            this.dataGridViewMain.Text = "dataGridView1";
            this.dataGridViewMain.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMain_CellContentClick);
            // 
            // notifyIconMain
            // 
            this.notifyIconMain.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconMain.Icon")));
            this.notifyIconMain.Text = "Lalu后台任务服务控制台";
            this.notifyIconMain.Visible = true;
            this.notifyIconMain.Click += new System.EventHandler(this.notifyIconMain_Click);
            this.notifyIconMain.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIconMain_MouseDoubleClick);
            // 
            // ServiceNameColumn
            // 
            this.ServiceNameColumn.DataPropertyName = "ServiceName";
            this.ServiceNameColumn.FillWeight = 820.8123F;
            this.ServiceNameColumn.HeaderText = "服务名称";
            this.ServiceNameColumn.Name = "ServiceNameColumn";
            this.ServiceNameColumn.ReadOnly = true;
            this.ServiceNameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ServiceNameColumn.Width = 150;
            // 
            // ConfigStringColumn
            // 
            this.ConfigStringColumn.DataPropertyName = "ConfigString";
            this.ConfigStringColumn.FillWeight = 107.7297F;
            this.ConfigStringColumn.HeaderText = "间隔时间";
            this.ConfigStringColumn.Name = "ConfigStringColumn";
            this.ConfigStringColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ConfigStringColumn.Width = 250;
            // 
            // IsStartColumn
            // 
            this.IsStartColumn.DataPropertyName = "IsStart";
            this.IsStartColumn.FillWeight = 142.144F;
            this.IsStartColumn.HeaderText = "状态";
            this.IsStartColumn.Name = "IsStartColumn";
            this.IsStartColumn.ReadOnly = true;
            this.IsStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // IsRuningsColumn
            // 
            this.IsRuningsColumn.DataPropertyName = "IsRunings";
            this.IsRuningsColumn.FillWeight = 3.664275F;
            this.IsRuningsColumn.HeaderText = "是否正在执行任务";
            this.IsRuningsColumn.Name = "IsRuningsColumn";
            this.IsRuningsColumn.ReadOnly = true;
            this.IsRuningsColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // CounterColumn
            // 
            this.CounterColumn.DataPropertyName = "Counter";
            this.CounterColumn.FillWeight = 3.664275F;
            this.CounterColumn.HeaderText = "执行次数";
            this.CounterColumn.Name = "CounterColumn";
            this.CounterColumn.ReadOnly = true;
            this.CounterColumn.Width = 80;
            // 
            // StartDatetimeStrColumn
            // 
            this.StartDatetimeStrColumn.DataPropertyName = "StartDatetimeStr";
            this.StartDatetimeStrColumn.FillWeight = 3.664275F;
            this.StartDatetimeStrColumn.HeaderText = "最后一次开始时间";
            this.StartDatetimeStrColumn.Name = "StartDatetimeStrColumn";
            this.StartDatetimeStrColumn.ReadOnly = true;
            this.StartDatetimeStrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.StartDatetimeStrColumn.Width = 150;
            // 
            // EndDateTimeStrColumn
            // 
            this.EndDateTimeStrColumn.DataPropertyName = "EndDateTimeStr";
            this.EndDateTimeStrColumn.FillWeight = 3.664275F;
            this.EndDateTimeStrColumn.HeaderText = "最后一次结束时间";
            this.EndDateTimeStrColumn.Name = "EndDateTimeStrColumn";
            this.EndDateTimeStrColumn.ReadOnly = true;
            this.EndDateTimeStrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EndDateTimeStrColumn.Width = 150;
            // 
            // ButtonTitleColumn
            // 
            this.ButtonTitleColumn.DataPropertyName = "ButtonTitle";
            this.ButtonTitleColumn.FillWeight = 3.664275F;
            this.ButtonTitleColumn.HeaderText = "操作";
            this.ButtonTitleColumn.Name = "ButtonTitleColumn";
            this.ButtonTitleColumn.ReadOnly = true;
            this.ButtonTitleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ButtonExecuteTitleColumn
            // 
            this.ButtonExecuteTitleColumn.DataPropertyName = "ButtonExecuteTitle";
            this.ButtonExecuteTitleColumn.FillWeight = 3.664275F;
            this.ButtonExecuteTitleColumn.HeaderText = "";
            this.ButtonExecuteTitleColumn.Name = "ButtonExecuteTitleColumn";
            // 
            // ButtonLogTitleColumn
            // 
            this.ButtonLogTitleColumn.DataPropertyName = "ButtonLogTitle";
            this.ButtonLogTitleColumn.FillWeight = 3.664275F;
            this.ButtonLogTitleColumn.HeaderText = "日志";
            this.ButtonLogTitleColumn.Name = "ButtonLogTitleColumn";
            this.ButtonLogTitleColumn.ReadOnly = true;
            this.ButtonLogTitleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ButtonConfigColumn
            // 
            this.ButtonConfigColumn.DataPropertyName = "ButtonConfigTitle";
            this.ButtonConfigColumn.FillWeight = 3.664275F;
            this.ButtonConfigColumn.HeaderText = "配置";
            this.ButtonConfigColumn.Name = "ButtonConfigColumn";
            this.ButtonConfigColumn.ReadOnly = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1444, 450);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "后台任务服务控制台";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.SizeChanged += new System.EventHandler(this.FormMain_SizeChanged);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewMain;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.NotifyIcon notifyIconMain;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAutoService;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfigStringColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsStartColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsRuningsColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CounterColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartDatetimeStrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndDateTimeStrColumn;
        private System.Windows.Forms.DataGridViewLinkColumn ButtonTitleColumn;
        private System.Windows.Forms.DataGridViewLinkColumn ButtonExecuteTitleColumn;
        private System.Windows.Forms.DataGridViewLinkColumn ButtonLogTitleColumn;
        private System.Windows.Forms.DataGridViewLinkColumn ButtonConfigColumn;
    }
}

