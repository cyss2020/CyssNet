﻿
namespace Cyss.TaskManager
{
    partial class FormTaskConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbWeekly = new System.Windows.Forms.RadioButton();
            this.rbDaily = new System.Windows.Forms.RadioButton();
            this.rbloop = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtEndDatetime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtStartDatetime = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cheboxFailNotCounted = new System.Windows.Forms.CheckBox();
            this.txtMaxExecutionTimes = new System.Windows.Forms.TextBox();
            this.lbaMaxExecutionTimes = new System.Windows.Forms.Label();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBoxWeeks = new System.Windows.Forms.GroupBox();
            this.checkedListBoxWeek = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxDays = new System.Windows.Forms.CheckedListBox();
            this.rbMonth = new System.Windows.Forms.RadioButton();
            this.groupBoxDays = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxWeeks.SuspendLayout();
            this.groupBoxDays.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbMonth);
            this.groupBox1.Controls.Add(this.rbWeekly);
            this.groupBox1.Controls.Add(this.rbDaily);
            this.groupBox1.Controls.Add(this.rbloop);
            this.groupBox1.Location = new System.Drawing.Point(19, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "类型";
            // 
            // rbWeekly
            // 
            this.rbWeekly.AutoSize = true;
            this.rbWeekly.Location = new System.Drawing.Point(229, 31);
            this.rbWeekly.Name = "rbWeekly";
            this.rbWeekly.Size = new System.Drawing.Size(106, 21);
            this.rbWeekly.TabIndex = 2;
            this.rbWeekly.TabStop = true;
            this.rbWeekly.Text = "定时执行(每周)";
            this.rbWeekly.UseVisualStyleBackColor = true;
            this.rbWeekly.Click += new System.EventHandler(this.rbWeekly_Click);
            // 
            // rbDaily
            // 
            this.rbDaily.AutoSize = true;
            this.rbDaily.Location = new System.Drawing.Point(106, 31);
            this.rbDaily.Name = "rbDaily";
            this.rbDaily.Size = new System.Drawing.Size(106, 21);
            this.rbDaily.TabIndex = 1;
            this.rbDaily.TabStop = true;
            this.rbDaily.Text = "定时执行(每日)";
            this.rbDaily.UseVisualStyleBackColor = true;
            this.rbDaily.Click += new System.EventHandler(this.rbDaily_Click);
            // 
            // rbloop
            // 
            this.rbloop.AutoSize = true;
            this.rbloop.Location = new System.Drawing.Point(22, 31);
            this.rbloop.Name = "rbloop";
            this.rbloop.Size = new System.Drawing.Size(74, 21);
            this.rbloop.TabIndex = 0;
            this.rbloop.TabStop = true;
            this.rbloop.Text = "循环执行";
            this.rbloop.UseVisualStyleBackColor = true;
            this.rbloop.Click += new System.EventHandler(this.rbloop_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtEndDatetime);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dtStartDatetime);
            this.groupBox2.Location = new System.Drawing.Point(19, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(515, 72);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "执行时段";
            // 
            // dtEndDatetime
            // 
            this.dtEndDatetime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtEndDatetime.Location = new System.Drawing.Point(311, 28);
            this.dtEndDatetime.Name = "dtEndDatetime";
            this.dtEndDatetime.Size = new System.Drawing.Size(161, 23);
            this.dtEndDatetime.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "结束时间";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "开始时间";
            // 
            // dtStartDatetime
            // 
            this.dtStartDatetime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtStartDatetime.Location = new System.Drawing.Point(71, 28);
            this.dtStartDatetime.Name = "dtStartDatetime";
            this.dtStartDatetime.Size = new System.Drawing.Size(129, 23);
            this.dtStartDatetime.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cheboxFailNotCounted);
            this.groupBox3.Controls.Add(this.txtMaxExecutionTimes);
            this.groupBox3.Controls.Add(this.lbaMaxExecutionTimes);
            this.groupBox3.Controls.Add(this.txtInterval);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(19, 85);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(515, 62);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // cheboxFailNotCounted
            // 
            this.cheboxFailNotCounted.AutoSize = true;
            this.cheboxFailNotCounted.Location = new System.Drawing.Point(366, 20);
            this.cheboxFailNotCounted.Name = "cheboxFailNotCounted";
            this.cheboxFailNotCounted.Size = new System.Drawing.Size(99, 21);
            this.cheboxFailNotCounted.TabIndex = 5;
            this.cheboxFailNotCounted.Text = "失败不计次数";
            this.cheboxFailNotCounted.UseVisualStyleBackColor = true;
            // 
            // txtMaxExecutionTimes
            // 
            this.txtMaxExecutionTimes.Location = new System.Drawing.Point(304, 19);
            this.txtMaxExecutionTimes.Name = "txtMaxExecutionTimes";
            this.txtMaxExecutionTimes.Size = new System.Drawing.Size(56, 23);
            this.txtMaxExecutionTimes.TabIndex = 3;
            this.txtMaxExecutionTimes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxExecutionTimes_KeyPress);
            // 
            // lbaMaxExecutionTimes
            // 
            this.lbaMaxExecutionTimes.AutoSize = true;
            this.lbaMaxExecutionTimes.Location = new System.Drawing.Point(217, 24);
            this.lbaMaxExecutionTimes.Name = "lbaMaxExecutionTimes";
            this.lbaMaxExecutionTimes.Size = new System.Drawing.Size(80, 17);
            this.lbaMaxExecutionTimes.TabIndex = 2;
            this.lbaMaxExecutionTimes.Text = "最多执行次数";
            // 
            // txtInterval
            // 
            this.txtInterval.Location = new System.Drawing.Point(93, 19);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(100, 23);
            this.txtInterval.TabIndex = 1;
            this.txtInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterval_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "时间间隔(秒)";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(408, 461);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBoxWeeks
            // 
            this.groupBoxWeeks.Controls.Add(this.checkedListBoxWeek);
            this.groupBoxWeeks.Location = new System.Drawing.Point(19, 257);
            this.groupBoxWeeks.Name = "groupBoxWeeks";
            this.groupBoxWeeks.Size = new System.Drawing.Size(212, 162);
            this.groupBoxWeeks.TabIndex = 4;
            this.groupBoxWeeks.TabStop = false;
            this.groupBoxWeeks.Text = "每周执行";
            // 
            // checkedListBoxWeek
            // 
            this.checkedListBoxWeek.FormattingEnabled = true;
            this.checkedListBoxWeek.Location = new System.Drawing.Point(7, 22);
            this.checkedListBoxWeek.Name = "checkedListBoxWeek";
            this.checkedListBoxWeek.Size = new System.Drawing.Size(193, 130);
            this.checkedListBoxWeek.TabIndex = 0;
            // 
            // checkedListBoxDays
            // 
            this.checkedListBoxDays.FormattingEnabled = true;
            this.checkedListBoxDays.Location = new System.Drawing.Point(10, 21);
            this.checkedListBoxDays.Name = "checkedListBoxDays";
            this.checkedListBoxDays.Size = new System.Drawing.Size(213, 130);
            this.checkedListBoxDays.TabIndex = 5;
            // 
            // rbMonth
            // 
            this.rbMonth.AutoSize = true;
            this.rbMonth.Location = new System.Drawing.Point(358, 31);
            this.rbMonth.Name = "rbMonth";
            this.rbMonth.Size = new System.Drawing.Size(106, 21);
            this.rbMonth.TabIndex = 3;
            this.rbMonth.TabStop = true;
            this.rbMonth.Text = "定时执行(每月)";
            this.rbMonth.UseVisualStyleBackColor = true;
            this.rbMonth.Click += new System.EventHandler(this.rbMonth_Click);
            // 
            // groupBoxDays
            // 
            this.groupBoxDays.Controls.Add(this.checkedListBoxDays);
            this.groupBoxDays.Location = new System.Drawing.Point(302, 257);
            this.groupBoxDays.Name = "groupBoxDays";
            this.groupBoxDays.Size = new System.Drawing.Size(232, 162);
            this.groupBoxDays.TabIndex = 6;
            this.groupBoxDays.TabStop = false;
            this.groupBoxDays.Text = "每月执行";
            // 
            // FormTaskConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 514);
            this.Controls.Add(this.groupBoxDays);
            this.Controls.Add(this.groupBoxWeeks);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormTaskConfig";
            this.Text = "任务配置";
            this.Load += new System.EventHandler(this.FormTaskConfig_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxWeeks.ResumeLayout(false);
            this.groupBoxDays.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbWeekly;
        private System.Windows.Forms.RadioButton rbDaily;
        private System.Windows.Forms.RadioButton rbloop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtEndDatetime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtStartDatetime;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMaxExecutionTimes;
        private System.Windows.Forms.Label lbaMaxExecutionTimes;
        private System.Windows.Forms.CheckBox cheboxFailNotCounted;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBoxWeeks;
        private System.Windows.Forms.CheckedListBox checkedListBoxWeek;
        private System.Windows.Forms.CheckedListBox checkedListBoxDays;
        private System.Windows.Forms.RadioButton rbMonth;
        private System.Windows.Forms.GroupBox groupBoxDays;
    }
}