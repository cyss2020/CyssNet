﻿using Account.Api.Client;
using Common.Api.Client;
using Cyss.Core;
using Cyss.Core.Helper;
using Cyss.TaskManager.Infrastructure;
using Order.Api.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cyss.TaskManager.Services
{
    [ServiceEnable]
    public class TestService : IService
    {

        public TestService()
        {
            this.ServiceName = "测试服务";
            this.TaskId = 1;
        }


        protected async override Task<bool> ExecuteAsync(object state)
        {

            try
            {
                var sf = EncryptHelper.MD5Encrypt("123456");

                var operateResult = await CommonClientFactory.Helper.GetIdempotence();
                var id = operateResult.Data;

                List<int> list = new List<int>();
                for (int i = 0; i <= 20; i++)
                {
                    list.Add(i);
                }

                //var asdf = await OrderClientFactory.SaleOrder.CreateSaleOrderItem(new Order.Api.Dtos.SaleOrderItemModel() { ProductName = "test:"+DateTime.Now.ToString() });

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }


        }







    }
}
