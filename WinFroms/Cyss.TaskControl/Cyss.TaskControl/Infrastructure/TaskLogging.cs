﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.TaskControl.Infrastructure
{

    /// <summary>
    /// 工作日志
    /// </summary>
    public  class TaskLogging
    {

        public int TaskId { set; get; }

        /// <summary>
        /// 日志分类
        /// </summary>
        public string Type { set; get; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { set; get; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateTime { set; get; }

        public string CreateTimeStr { get { return CreateTime.ToString("yyyy-MM-dd HH:mm:ss"); } }
    }
}
