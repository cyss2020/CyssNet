﻿using Common.Api.Dtos;
using Cyss.Core;
using System.Text;
namespace Cyss.TaskControl.Infrastructure
{
    public class TaskThreadView : TaskThread
    {

        public TaskThreadView()
        {
            this.ButtonLogTitle = "日志";
            this.ButtonConfigTitle = "配置";
            this.ButtonExecuteTitle = "执行";
        }



        public string ConfigString
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder();
                if (this.Config.TaskExecuteType == TaskExecuteType.loop)
                {
                    stringBuilder.Append($"循环执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.TaskExecuteType == TaskExecuteType.Daily)
                {
                    stringBuilder.Append($"每天定时执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.TaskExecuteType == TaskExecuteType.Weekly)
                {
                    stringBuilder.Append($"每周定时执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.TaskExecuteType == TaskExecuteType.Month)
                {
                    stringBuilder.Append($"每月定时执行;间隔{this.Config.Interval}秒");
                }
                if (this.Config.StartDatetime > TimeSpan.Zero)
                {
                    stringBuilder.Append($";开始时间{this.Config.StartDatetime.ToString()}");
                }
                if (this.Config.EndDatetime > TimeSpan.Zero)
                {
                    stringBuilder.Append($";结束时间{this.Config.EndDatetime.ToString()}");
                }
                if (this.Config.DayOfWeeks != null && this.Config.DayOfWeeks.Count() > 0)
                {
                    stringBuilder.Append($"[{string.Join("/", this.Config.DayOfWeeks)}]");
                }
                if (this.Config.DayOfMonths != null && this.Config.DayOfMonths.Count() > 0)
                {
                    stringBuilder.Append($"[{string.Join("/", this.Config.DayOfMonths)}]");
                }
                return stringBuilder.ToString();
            }
        }

       
        /// <summary>
        /// 开启关闭按钮文本
        /// </summary>
        public string ButtonTitle
        {
            get
            {
                if (this.State == false)
                {
                    return "开启";
                }
                else
                {
                    if (this.Colseing)
                    {
                        return "关闭中";
                    }
                    return "关闭";
                }
            }

        }

        /// <summary>
        /// 日志按钮文本
        /// </summary>
        public string ButtonLogTitle { set; get; }

        /// <summary>
        /// 执行按钮文本
        /// </summary>
        public string ButtonExecuteTitle { set; get; }

        /// <summary>
        /// 配置按钮文本
        /// </summary>
        public string ButtonConfigTitle { set; get; }


    }
}
