﻿using Common.Api.Client;
using Common.Api.Dtos;
using Cyss.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.TaskControl.Infrastructure
{
    public class UrlService : IService
    {
        private HttpClient httpClient { set; get; }

        public string BaseAddress { set; get; }

        public string Url { set; get; }

        public UrlService(string BaseAddress)
        {
            this.BaseAddress = BaseAddress;
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(BaseAddress);
        }

        protected async override Task<bool> ExecuteAsync(object state)
        {
            try
            {
                //var operateResult = await httpClient.se<OperateResult>(HttpMethod.Get, Url);
                //return operateResult.IsSuccess;
                return await Task.FromResult(false);
            }
            catch (Exception ex)
            {
                InsertLog($"失败{ex.Message}");
                return false;
            }


        }
    }
}
