﻿using Cyss.Core.Infrastructure;
using System.Threading.Tasks;

namespace Cyss.TaskControl.Infrastructure
{
    public class ApiToken : IApiToken
    {
        public string Totken => string.Empty;

        public string GetTotken() => string.Empty;


        public Task<string> GetTotkenAsync()
        {
            return Task.FromResult(string.Empty);
        }
    }
}
