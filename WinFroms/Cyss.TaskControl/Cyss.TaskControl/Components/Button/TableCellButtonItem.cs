﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyss.TaskControl.Components
{
    /// <summary>
    /// 单元格内按钮组件
    /// </summary>
    public class TableCellButtonItem<TItem> : BootstrapBlazor.Components.Button
    {
        /// <summary>
        /// 获得/设置 当前行绑定数据
        /// </summary>
        [Parameter]
        public TItem? Item { get; set; }

        /// <summary>
        /// 获得/设置 按钮点击后的回调方法
        /// </summary>
        [Parameter]
        public Func<TItem, Task>? OnClickCallbackItem { get; set; }

        /// <summary>
        /// 获得/设置 OnClick 事件不刷新父组件
        /// </summary>
        [Parameter]
        public Func<TItem, Task>? OnClickWithoutRenderItem { get; set; }

        [Parameter]
        public string LoadingText { set; get; } = "...";



        [Parameter]
        public bool IsConfirm { set; get; }

        /// <summary>
        /// 确认提交提示消息
        /// </summary>
        [Parameter]
        public string ConfirmTitle { set; get; }

        /// <summary>
        /// 获得/设置 点击确认时回调方法
        /// </summary>
        [Parameter]
        public Func<TItem, Task> OnConfirm { get; set; }

        [Inject]
        private SwalService _swalService { set; get; }

        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override void OnInitialized()
        {
            base.OnInitialized();

            if (Size == Size.None)
            {
                Size = Size.ExtraSmall;
            }
            string text = this.Text;
            string icon = this.Icon;
            OnClickButton = EventCallback.Factory.Create<MouseEventArgs>(this, async e =>
            {

                if (IsConfirm)
                {
                    var isOk = await ShowConfirm(ConfirmTitle);
                    if (isOk)
                    {
                        if (IsAsync)
                        {
                            await Task.Run(async () => await InvokeAsync(HandlerClick));
                        }
                        else
                        {
                            await HandlerClick();
                        }
                    }
                }
                else
                {
                    if (IsAsync)
                    {
                        await Task.Run(async () => await InvokeAsync(HandlerClick));
                    }
                    else
                    {
                        await HandlerClick();
                    }
                }


            });


            async Task<bool> ShowConfirm(string Msg)
            {
                return await _swalService.ShowModal(new SwalOption()
                {
                    Category = SwalCategory.Information,
                    Title = Msg
                });
            }

            async Task HandlerClick()
            {
                if (IsAsync)
                {
                    ButtonIcon = LoadingIcon;
                    IsDisabled = true;
                    if (string.IsNullOrWhiteSpace(this.LoadingText))
                    {
                        this.Text = this.LoadingText;
                    }
                    await InvokeAsync(StateHasChanged);
                }

                if (IsConfirm)
                {
                    await OnConfirm.Invoke(Item);
                }
                else
                {
                    if (OnClickWithoutRenderItem != null)
                    {
                        await OnClickWithoutRenderItem.Invoke(Item);
                    }
                    if (OnClick.HasDelegate)
                    {
                        await OnClick.InvokeAsync();
                    }

                    if (OnClickCallbackItem != null)
                    {
                        await OnClickCallbackItem(Item);
                    }
                }

                if (IsAsync)
                {
                    ButtonIcon = Icon;
                    IsDisabled = false;
                    this.Text = text;
                    await InvokeAsync(StateHasChanged);
                }
            }

        }
    }
}
