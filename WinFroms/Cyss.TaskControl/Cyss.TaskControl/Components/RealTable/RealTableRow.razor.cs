﻿using Microsoft.AspNetCore.Components;
using System.Diagnostics.CodeAnalysis;

namespace Cyss.TaskControl.Components
{
    public partial class RealTableRow<TItem> : ComponentBase
    {
        [Parameter]
        [NotNull]
        public TItem? Item { set; get; }


        /// <summary>
        /// 获得/设置 Table 实例
        /// </summary>
        [CascadingParameter]
        protected IRealTable? Table { get; set; }


    }
}
