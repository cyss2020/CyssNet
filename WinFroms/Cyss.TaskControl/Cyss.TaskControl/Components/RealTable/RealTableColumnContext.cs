﻿using System.Diagnostics.CodeAnalysis;

namespace Cyss.TaskControl.Components
{
    /// <summary>
    /// TableColumn 上下文类
    /// </summary>
    public class RealTableColumnContext<TItem, TValue>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        public RealTableColumnContext(TItem model, TValue value)
        {
            Row = model ?? throw new ArgumentNullException(nameof(model));
            Value = value;
        }

        /// <summary>
        /// 获得/设置 行数据实例
        /// </summary>
        [NotNull]
        public TItem Row { get; set; }

        /// <summary>
        /// 获得/设置 当前绑定字段数据实例
        /// </summary>
        public TValue Value { get; set; }
    }
}
