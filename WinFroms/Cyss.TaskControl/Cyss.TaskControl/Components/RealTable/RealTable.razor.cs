﻿using Microsoft.AspNetCore.Components;

namespace Cyss.TaskControl.Components
{

    public interface IRealTable
    {
        /// <summary>
        /// 获得 ITableColumn 集合
        /// </summary>
        List<IRealTableColumn> Columns { get; }
    }

    [CascadingTypeParameter(nameof(TItem))]
    public partial class RealTable<TItem> : ComponentBase, IRealTable where TItem : RealTableViewModelBase, new()
    {

        private bool FirstRender { set; get; }

        private int RowCount { set; get; }

        /// <summary>
        /// 获得 表头集合
        /// </summary>
        public List<IRealTableColumn> Columns { get; } = new(50);

        /// <summary>
        /// 获得/设置 TableHeader 实例
        /// </summary>
        [Parameter]
        public RenderFragment<TItem>? TableColumns { get; set; }

        [Parameter]
        public List<TItem>? Items { set; get; }

        private IEnumerable<IRealTableColumn> GetColumns()
        {
            return this.Columns;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                FirstRender = true;
                StateHasChanged();
                ChangedItems();
            }
            base.OnAfterRender(firstRender);
        }

        private void ChangedItems()
        {
            Task.Run(async () =>
            {

                while (true)
                {
                    if (Items.Count != this.RowCount)
                    {
                        await InvokeAsync(StateHasChanged);
                    }
                    await Task.Delay(1000);
                }

            });
        }
    }
}
