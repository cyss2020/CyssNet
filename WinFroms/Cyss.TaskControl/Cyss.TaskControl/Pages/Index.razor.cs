﻿using Common.Api.Client;
using Common.Api.Dtos;
using Cyss.TaskControl.Components;
using Cyss.TaskControl.Infrastructure;

namespace Cyss.TaskControl.Pages
{
    public partial class Index
    {
        private RealTable<TaskThreadView> realTable { set; get; }
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await InvokeAsync(StateHasChanged);

            }
            await base.OnAfterRenderAsync(firstRender);
        }


        private void StartAllService()
        {
            ServiceControlCenter.StartAllService();
        }

        /// <summary>
        /// 开启服务
        /// </summary>
        private async Task OnStart(TaskThreadView task)
        {
            task.State = true;
        }

        /// <summary>
        /// 开启服务
        /// </summary>
        private async Task OnStop(TaskThreadView task)
        {
            task.State = false;
        }
    }
}
