﻿using Common.Api.Client;
using Cyss.Core;
using Cyss.Core.Api.Client;
using Cyss.Core.Infrastructure;
using Cyss.TaskControl.Infrastructure;

namespace Cyss.TaskControl
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection Services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = new System.Globalization.CultureInfo("zh-CN", true)
            {
                DateTimeFormat = { ShortDatePattern = "yyyy-MM-dd", FullDateTimePattern = "yyyy-MM-dd HH:mm:ss", LongTimePattern = "HH:mm:ss" },
            };
            // 添加本行代码
            Services.AddBootstrapBlazor();
            Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            Services.AddSingleton<IIOCCore, IOCCore>();
            Services.RegisterClientConfig(configuration);
            //注册api客户端
            Services.AddSingleton<IApiToken,ApiToken>();
            Services.RegisterCommonClient();

            return Services;
        }
    }
}
