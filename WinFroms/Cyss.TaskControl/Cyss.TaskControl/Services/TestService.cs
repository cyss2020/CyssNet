﻿using Common.Api.Client;
using Cyss.Core;
using Cyss.TaskControl.Infrastructure;

namespace Cyss.TaskControl.Services
{
    [ServiceEnable]
    public class TestService : IService
    {

        public TestService()
        {
            this.ServiceName = "测试服务";
            this.TaskId = 1;
            this.Interval = 10;
        }


        protected async override Task<bool> ExecuteAsync(object state)
        {


            var sf = EncryptHelper.MD5Encrypt("123456");

            var operateResult = await CommonClientFactory.Helper.GetIdempotence();
            var id = operateResult.Data;

            List<int> list = new List<int>();
            for (int i = 0; i <= 20; i++)
            {
                list.Add(i);
            }

            //var asdf = await OrderClientFactory.SaleOrder.CreateSaleOrderItem(new Order.Api.Dtos.SaleOrderItemModel() { ProductName = "test:"+DateTime.Now.ToString() });

            return true;
        }







    }
}
