﻿using Account.Api.Client;
using Common.Api.Client;
using Common.Api.Dtos.Eumns;
using Laluz.Api.Client.Core;
using Laluz.Core;
using Laluz.Core.Extensions;
using Laluz.Core.Infrastructure;
using Order.Api.Client;
using Order.Api.Dtos;
using Outside.Api.Dtos.Overstock;
using Outside.Api.Dtos.Wayfair;
using Product.Api.Client;
using Product.Api.Dtos.SearchModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Laluz.Core.ImageHelper;

namespace ApiTest
{
    public partial class Form1 : Form
    {
        public class ApiToken : IApiToken
        {
            public string Totken => string.Empty;
        }
        private IApiToken apiToken { set; get; }
        public Form1()
        {
            ApiClientKeyConfig.Config.ApiKey = "laluz20200918";
            ApiClientKeyConfig.Config.EncryptedKey = "laluz@a#$%&zulal";

            ApiWebsiteConfig.Config = new ApiWebsiteConfig();
            ApiWebsiteConfig.Config.OutsideServiceWebsite = "http://localhost:8084/";
            ApiWebsiteConfig.Config.OrderServiceWebsite = "http://192.168.1.18:8082/";
            ApiWebsiteConfig.Config.CommonServiceWebsite = "http://localhost:53908/";
            ApiWebsiteConfig.Config.ProductServiceWebsite = "http://192.168.1.18:8086/";
            ApiWebsiteConfig.Config.AccountServiceWebsite = "http://localhost:8081/";

            fBAShipOrderClient = new FBAShipOrderClient(apiToken);

            InitializeComponent();
        }
        FBAShipOrderClient fBAShipOrderClient = null;

        private async void button1_Click(object sender, EventArgs e)
        {
            var model = new Order.Api.Dtos.SearchModels.FBAShipOrderSearchModel();
            model.pageSize = 500;
            var apiResponse = await fBAShipOrderClient.GetPageListFBAShipOrders(model);

            await ParallelExtensions.ForEach(apiResponse.Data.Data, Item);

            MessageBox.Show("完成");

        }

        private async Task Item(FBAShipOrderModel item)
        {

            var sf = await fBAShipOrderClient.GetFBAShipOrderById(item.Id);
            richTextBox1.Invoke(new Action(
              delegate
              {
                  richTextBox1.Text = richTextBox1.Text + sf.Data.Id + Environment.NewLine;
              }));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var afasdf = File.ReadAllBytes("test.csv");
            var index = CsvHelper.GeRowOf(afasdf.ToStream(), "Includes Amazon Marketplace, Fulfillment by Amazon (FBA), and Amazon Webstore transactions");
            var af = CsvHelper.GetDatable("test.csv", index > 0 ? 8 : 1);
        }

        public class RequstTest
        {
            public RequstTest()
            {
                TrackingNumber = new List<string>();
            }
            public string Locale { set; get; }
            public List<string> TrackingNumber { set; get; }

            public string Requester { set; get; }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var afdsf = DateTime.Parse("2021-02-02 16:14:56.000000 +00:00");

            WayfairPurchaseOrderModel model = new WayfairPurchaseOrderModel();
            model.poDate = DateTime.Now;
            string sfssss = model.ToSerializeObject();
            var sf = JsonHelper.DeserializeObject<WayfairPurchaseOrderModel>(richTextBox1.Text);

            var str = sf.ToSerializeObject();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {


        }

        private void button7_Click(object sender, EventArgs e)
        {

        }


    }


}
