using Laluz.Api.Client.Core;
using Laluz.Core.Infrastructure;
using Laluz.Core.IOC;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Product.Api.Client;
using Product.Api.Dtos;
using Product.Api.Dtos.SearchModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BlazorTest.Client
{
    public class Program
    {
        public static WebAssemblyHost Host { set; get; }

        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            ApiClientKeyConfig.Config.ApiKey = "laluz20200918";
            ApiClientKeyConfig.Config.EncryptedKey = "laluz@a#$%&zulal";
            ApiWebsiteConfig.Config = new ApiWebsiteConfig();
            ApiWebsiteConfig.Config.OutsideServiceWebsite = "http://192.168.1.18:8084/";
            ApiWebsiteConfig.Config.OrderServiceWebsite = "http://192.168.1.18:8082/";
            ApiWebsiteConfig.Config.CommonServiceWebsite = "http://192.168.1.18:8085/";
            ApiWebsiteConfig.Config.ProductServiceWebsite = "http://192.168.1.18:8086/";
            ApiWebsiteConfig.Config.AccountServiceWebsite = "http://192.168.1.18:8081/";
            ApiWebsiteConfig.Config.ReportServiceWebsite = "http://192.168.1.18:8087/";

            //ע��api�ͻ���
            builder.Services.AddSingleton<IApiToken, ApiToken>();
            builder.Services.RegisterProductClient();

            Host = builder.Build();
            IOCEngine.ServiceProvider = Host.Services;
            await Host.RunAsync();

           

        }
    }
}
