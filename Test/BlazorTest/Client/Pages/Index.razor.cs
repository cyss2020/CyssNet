﻿using Microsoft.AspNetCore.Components;
using Product.Api.Client;
using Product.Api.Dtos.SearchModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorTest.Client.Pages
{
    public partial class Index: ComponentBase
    {
        protected async override Task OnInitializedAsync()
        {
            var searchModel = new ProductStockSearchModel();

            var apiResponse = await ProductClientFactory.ProductStock.GetPageListProductStocks(searchModel);

            if (apiResponse.IsSuccess && apiResponse.Data != null)
            {

            }
            await base.OnInitializedAsync();
        }

    }
}
